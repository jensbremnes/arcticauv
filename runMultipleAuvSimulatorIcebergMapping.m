% runMultipleAuvSimulatorIcebergMapping is a script that initializes and runs the
% iceberg mapping algorithm of the AUV simulator.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
close all; clc; clear all;

[folder, ~,  ~] = fileparts(mfilename('fullpath'));
simRoot = [folder, '/'];
clear folder;
cd(simRoot);
addpath([simRoot, 'simulator/config']);
slamMethods = struct('id', {0, 2, 3}, 'name', {'BPSLAM', 'ModifiedBPSLAM', 'ICPBPSLAM'});

%% Multiple runs
nParticleRuns = [500];
SLAMcases = [4];
slamMethodsSelect = [1,2,3];
NrunsPerCase = 5;

tStart = tic;
simulationsPerformed = 0;
for nParticles = nParticleRuns
    for SLAMcase=SLAMcases
        for slamMethodIdx=slamMethodsSelect
            % Load and setup default config
            clear conf modules mdlFiles;
            rng('shuffle');
            [conf, modules, mdlFiles] = confDefault(simRoot);
            [conf, modules, mdlFiles] = confRangeSim(conf, modules, mdlFiles);
            [conf, modules, mdlFiles] = confIcebergMapping(conf, modules, mdlFiles);

            conf.debugOn = 0;
            conf.mbe.enable = 1;
            conf.mbe.enableLogging = 0;
            conf.mbe.noiseEnable = 1;
            conf.iceslam.pfNoiseEnable = 1;
            conf.iceslam.saveObservationMapOnExit = 1;
            conf.T = 1300;
            conf.x0 = [0, 30, 116, 0, 0, deg2rad(80), knots2ms(2.5), 0, 0, 0, 0, 0]';
            conf.VCurrentMs = 0.0;
            conf.betaCurrentDeg = 45;
            conf.VIceMs = 0.30;
            conf.betaIceDeg = 45;
            
            % Config cases
            switch(SLAMcase)
                case 1
                    conf.rotationRateIceDegs = 0/3600;
                    conf.iceberg.initalVelocityError = 0.0; 
                    conf.iceberg.initalRotRateError = 0.0;
                    conf.iceslam.diableRotationEstimate = 1;
                case 2
                    conf.rotationRateIceDegs = 0/3600;
                    conf.iceberg.initalVelocityError = 0.1;
                    conf.iceberg.initalRotRateError = 0.0;
                    conf.iceslam.diableRotationEstimate = 1;
                case 3
                    conf.rotationRateIceDegs = 30/3600;
                    conf.iceberg.initalVelocityError = 0.0;
                    conf.iceberg.initalRotRateError = 0.0;
                    conf.iceslam.diableRotationEstimate = 0;
                case 4
                    conf.rotationRateIceDegs = 30/3600;
                    conf.iceberg.initalVelocityError = 0.1;
                    conf.iceberg.initalRotRateError = 0.1;
                    conf.iceslam.diableRotationEstimate = 0;
                otherwise
                    break;
            end
            conf.iceslam.weightingMethod = slamMethods(slamMethodIdx).id;
            conf.iceslam.nParticles = nParticles;

            % Generic config
            conf.iceberg.nu0 = diag([1 - conf.iceberg.initalVelocityError,...
                                    1 - conf.iceberg.initalVelocityError,...
                                    1 - conf.iceberg.initalRotRateError])*...
                                    [conf.VIceMs*cos(deg2rad(conf.betaIceDeg)), ...
                                    conf.VIceMs*sin(deg2rad(conf.betaIceDeg)), ...
                                    deg2rad(conf.rotationRateIceDegs)]';
            conf.iceberg.nuRel0 = conf.x0([7:8,11]) - Rzyx(0, 0, conf.x0(6))'*conf.iceberg.nu0;

            % Run simulator
            for i=1:NrunsPerCase
                clc;
                conf.saveDir = [conf.rootDir, 'save/', num2str(conf.iceslam.nParticles), 'particles/case', num2str(SLAMcase), '/', slamMethods(slamMethodIdx).name, '/run', num2str(i), '/'];
                auvSimulator_exe(conf, modules, mdlFiles);
                simulationsPerformed = simulationsPerformed + 1;
                pause(1);
            end
        end
    end
end
tElapsed = toc(tStart);
fprintf('INFO: Total time to run %i simulations: %.02f hours.\n', simulationsPerformed, tElapsed/3600);

