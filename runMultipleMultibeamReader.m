% runMultipleMultibeamReader is a script that initializes and runs the
% iceberg mapping algorithm using a real dataset.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
close all; clc; clear all;

[folder, ~,  ~] = fileparts(mfilename('fullpath'));
simRoot = [folder, '/'];
clear folder;
cd(simRoot);
addpath([simRoot, 'simulator/config']);
slamMethods = struct('id', {0, 2, 3}, 'name', {'BPSLAM', 'ModifiedBPSLAM', 'ICPBPSLAM'});

%% Multiple runs
nParticleRuns = [200];
SLAMcases = [2];
slamMethodsSelect = [1,2,3];
NrunsPerCase = 3;

tStart = tic;
simulationsPerformed = 0;
for nParticles = nParticleRuns
    for SLAMcase=SLAMcases
        for slamMethodIdx=slamMethodsSelect
            % Load and setup default config
            clear conf modules mdlFiles;
            rng('shuffle');
            [conf, modules, mdlFiles] = confMultibeamReader(simRoot);

            conf.T = 1700;
            conf.VCurrentMs = 0.0;
            conf.betaCurrentDeg = 45;
            conf.VIceMs = 0.0;
            conf.betaIceDeg = 45;
            conf.iceberg.eta0 = [0, 0, 0]';
            conf.iceslam.pfNoiseEnable = 1;
            conf.iceslam.saveObservationMapOnExit = 0;
            conf.debugOn = 0;
            
            conf.mbe.startIndex = 255373; %navlabtime: 1510835058588723000 beamIdx: 62176; beamTime: 1510835058626000000
            conf.mbe.endIndex = 289400; 
            
            simReturn = struct;
            
            % Config cases
            switch(SLAMcase)
                case 1
                    conf.iceekf.TsPosMeas = 0.05;
                    conf.rotationRateIceDegs = 0/3600;
                    conf.iceberg.initalVelocityError = 0.0; 
                    conf.iceberg.initalRotRateError = 0.0;
                    conf.iceslam.diableRotationEstimate = 1;
                case 2
                    conf.iceekf.TsPosMeas = 2000;
                    conf.rotationRateIceDegs = 0/3600;
                    conf.iceberg.initalVelocityError = 0.0;
                    conf.iceberg.initalRotRateError = 0.0;
                    conf.iceslam.diableRotationEstimate = 1;
                otherwise
                    break;
            end
            conf.iceslam.weightingMethod = slamMethods(slamMethodIdx).id;
            conf.iceslam.nParticles = nParticles;

            % Generic config
            conf.iceberg.nu0 = diag([1 - conf.iceberg.initalVelocityError,...
                                    1 - conf.iceberg.initalVelocityError,...
                                    1 - conf.iceberg.initalRotRateError])*...
                                    [conf.VIceMs*cos(deg2rad(conf.betaIceDeg)), ...
                                    conf.VIceMs*sin(deg2rad(conf.betaIceDeg)), ...
                                    deg2rad(conf.rotationRateIceDegs)]';
            conf.iceberg.nuRel0 = conf.x0([7:8,11]) - Rzyx(0, 0, conf.x0(6))'*conf.iceberg.nu0;

            % Run simulator
            for i=1:NrunsPerCase
                clc;
                conf.saveDir = [conf.rootDir, 'save/mbeReader/', num2str(conf.iceslam.nParticles), 'particles/case', num2str(SLAMcase), '/', slamMethods(slamMethodIdx).name, '/run', num2str(i), '/'];
                auvSimulator_exe(conf, modules, mdlFiles);
                simulationsPerformed = simulationsPerformed + 1;
                pause(1);
            end
        end
    end
end
tElapsed = toc(tStart);
fprintf('INFO: Total time to run %i simulations: %.02f hours.\n', simulationsPerformed, tElapsed/3600);

