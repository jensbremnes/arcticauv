function plotDEM(xx, yy, zz, gridRes, color, ax)
% plotDEM displays a digitial elevation map.
%
%  plotDEM(xx, yy, zz, gridRes, color, ax)
%
% Input parameters:
%   xx          - X-values
%   yy          - Y-Values
%   zz          - Z-values corresponding to X- and Y-values.
%   gridRes     - Grid resolution
%   color       - Cube RGB color (color = [r g b]./255)
%   ax          - Parent axes handle
%
% Output parameters:
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    
    for j = 1:length(yy)
        for i = 1:length(xx)
            if(~isnan(zz(j,i)))
                origin = [xx(i), yy(j), 0];
                size = [gridRes, gridRes, zz(j,i)];
                drawCube(origin, size, color, ax);
            end
        end
    end
    
end

function c = drawCube ( origin, size, color, ax )
% drawCube displays a 3D cube.
%
%  pt = drawCube ( origin, size, color, ax )
%
% Input parameters:
%   origin      - origin of cube (x0, y0, z0)
%   size        - Cube size (dx, dy, dz)
%   color       - Cube RGB color (color = [r g b]./255)
%   ax          - Parent axes
%
% Output parameters:
%   c           - Cube patch
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    vert = [1 1 0; 0 1 0; 0 1 1; 1 1 1; 0 0 1; 1 0 1; 1 0 0; 0 0 0];
    fac = [1 2 3 4; 4 3 5 6; 6 7 8 5; 1 2 8 7; 6 7 1 4; 2 3 5 8];

    cube = [vert(:,1)*size(1)+origin(1),vert(:,2)*size(2)+origin(2),vert(:,3)*size(3)+origin(3)];
    c = patch('Faces', fac, 'Vertices', cube, 'FaceVertexCData', hsv(8), 'FaceColor', color, 'EdgeColor', 'none', 'Parent', ax);
end
