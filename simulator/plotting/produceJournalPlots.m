close all; clear all; clc;
%% Script for producing plots for Journal papers
export = 1;
exportAsPdf = 1;
useOpenGL = 0;
plotMap = 0;
plotStandard = 1;
plotTrajectory = 0;
plotHuginTrajectory = 1;
realData = 1;
fontSize = 28;
fontSizeExtraSmallFigs = 20;

%% Load data
simDir = 'D:\Github\ArcticAuvSim\';
dataRoot = 'D:\Github\ArcticAuvSim\save\'; %'R:\SLAM_results\';
vendorDir = [simDir, 'vendor\matlab\'];
gfxDir = [simDir, 'simulator\gfx\matlab\'];
addpath(genpath(vendorDir));
addpath(genpath(gfxDir));

%% Change configs as neccessary
flags.baselineCloud = [simDir, 'data\particleCloud\', 'baseCloud1000_1_0_1_0_1e-3_1350s'];
flags.colorPlot = [153, 51, 51; 0, 102, 0; 51, 51, 153; 0, 0, 0]./255;
flags.lineWidth = 2.5;
flags.showLegend = 1;
flags.generateVelocityPlots = 0;
flags.plotHeading = 0;
flags.legStr = {};
flags.Tend = 1300;

% DEM output config
flags.plotObsMap = 0;
flags.plotErrMap = 1;
flags.plotUncMap = 0;
demDisplayMethod = 3; % ICP

% Trajecory output config
trjDisplayMethod = 3; % ICP
flags.enableParticlePlots = 1;
flags.Tend = 1260;
flags.ParticleCloudSnapshots = [1, 13, 25, 36, 50, 62, 71, 83].*15 + 2;

% Case 1
% flags.legStr = {'BPSLAM', 'Modified BPSLAM', 'ICP BPSLAM'};
% runCase = '200particles\case1\';
% resStr{1} = [dataRoot, runCase, 'BPSLAM\run5\'];
% resStr{2} = [dataRoot, runCase, 'ModifiedBPSLAM\run5\'];
% resStr{3} = [dataRoot, runCase, 'ICPBPSLAM\run5\'];

% Case 2
% flags.legStr = {'BPSLAM', 'Modified BPSLAM', 'ICP BPSLAM'};
% flags.generateVelocityPlots = 1;
% runCase = '200particles\case2\';
% resStr{1} = [dataRoot, runCase, 'BPSLAM\run3\'];
% resStr{2} = [dataRoot, runCase, 'ModifiedBPSLAM\run4\'];
% resStr{3} = [dataRoot, runCase, 'ICPBPSLAM\run2\'];

% Case 4
% flags.plotHeading = 1;
% flags.generateVelocityPlots = 1;
% flags.legStr = {'BPSLAM', 'Modified BPSLAM', 'ICP BPSLAM'};
% runCase = '200particles\case4\';
% resStr{1} = [dataRoot, runCase, 'BPSLAM\run5\'];
% resStr{2} = [dataRoot, runCase, 'ModifiedBPSLAM\run2\'];
% resStr{3} = [dataRoot, runCase, 'ICPBPSLAM\run2\'];

% Case HUGIN data
flags.ParticleCloudSnapshots = [15, 100, 200, 300, 400, 500, 600, 720, 830, 900, 1000, 1100, 1200, 1300, 1400, 1475, 1550];
flags.Tend = 1550;
flags.fixBrokenOldRun = 0;
flags.disableEkfPlot = 0;
flags.selectedCloud = 2;
flags.legStr = {'BPSLAM', 'Modified BPSLAM', 'ICP BPSLAM', 'AUV nav'};
runCase = 'mbeReader\200particles\case2\';
resStr{1} = [dataRoot, runCase, 'BPSLAM\run0\'];
resStr{2} = [dataRoot, runCase, 'ModifiedBPSLAM\run3\'];
resStr{3} = [dataRoot, runCase, 'ICPBPSLAM\run0\'];

%% Load case
results = cell(length(resStr), 1);
configs = cell(length(resStr), 1);
for i=1:length(results)
    results{i} = load([resStr{i}, 'auvsim.mat']);
    configs{i} = load([resStr{i}, 'conf.mat']);
    if(~isfield(flags, 'baselineCloud'))
        flags.newBaseCloud = configs{i}.conf.iceslam.baselineCloud;
    end
    results{i} = getExtraFields(results{i}, flags, configs{i}.conf.mbe.realData);
    if(~isfield(flags, 'cTime'))
        flags.cTime = results{i}.tSlam;
    end
    configs{i}.conf.saveDir = resStr{i};
    configs{i}.conf.anim.enableParticlePlots = flags.enableParticlePlots;
    
    if(configs{i}.conf.mbe.realData)
        results{i} = calculateSlamTrajectory(results{i}, configs{i}.conf, resStr{i});
    end
end
flags.exportDir = [dataRoot, runCase];

%% Generate plots
figs = [];
if(plotStandard)
    figs = [figs; plotIcebergPoseVariance(results, configs, flags, fontSize + fontSizeExtraSmallFigs, flags.lineWidth)];
    if(~realData)
        figs = [figs; plotIcebergPoseError(results, configs, flags, fontSize, flags.lineWidth)];
    else
        figs = [figs; plotIcebergPoseError(results, configs, flags, fontSize + fontSizeExtraSmallFigs, flags.lineWidth)];
    end
    if(flags.generateVelocityPlots)
        figs = [figs; plotIcebergVelocityError(results, configs, flags, fontSize + fontSizeExtraSmallFigs, flags.lineWidth-1.5)];
    end
end
if(plotMap)
    figs = [figs; plotObservationMap(results{demDisplayMethod}, configs{demDisplayMethod}.conf, flags, fontSize + fontSizeExtraSmallFigs)];
end
if(plotTrajectory)
    figs = [figs; plotTrajectoryFigure(results{trjDisplayMethod}, configs{trjDisplayMethod}.conf, flags, fontSize)];
end
if(plotHuginTrajectory)
    figs = [figs; plotHuginTrajectoryFigure(results, configs, flags, fontSize, flags.selectedCloud)];
end

%% Export plots
if(export)
    for i = 1:length(figs)
        fig = figs(i);
        str_name = [flags.exportDir, get(fig, 'Name')];
        if(exportAsPdf)
            if(useOpenGL)
                export_fig(str_name, '-pdf', '-q101', '-transparent', '-opengl', fig); 
            else
                export_fig(str_name, '-pdf', '-q100', '-transparent', fig); 
            end
        else
           export_fig(str_name, '-png', '-q101', '-transparent', fig); 
        end
    end
end

function simReturn = getExtraFields(simReturn, flags, isRealData)
    %% Get custom datafields and add to results
    if(~isRealData)
        if(isfield(simReturn, 'simOut'))
            simReturn.tAuv = simReturn.simOut.get('WS_t_auv').signals.values;
            simReturn.tMbe = simReturn.simOut.get('WS_MbeRanges').time;
            simReturn.mbeRanges = simReturn.simOut.get('WS_MbeRanges').signals.values;
            simReturn.mbeAngles = simReturn.simOut.get('WS_MbeAngles').signals.values;
            simReturn.mbeBeamsWNoise = simReturn.simOut.get('WS_MbeBeams_wnoise').signals.values;
            simReturn.altitude = simReturn.simOut.get('WS_Altimeter').signals.values;
            simReturn.tIce = simReturn.simOut.get('WS_eta_iceberg').time;
            simReturn.etaIceberg = simReturn.simOut.get('WS_eta_iceberg').signals.values;
            simReturn.nuIceberg = simReturn.simOut.get('WS_nu_iceberg').signals.values;
            simReturn.etaRelative = simReturn.simOut.get('WS_eta_rel').signals.values;
            simReturn.nuRelative = simReturn.simOut.get('WS_nu_rel').signals.values;
            simReturn.nuRelativeMeas = simReturn.simOut.get('WS_nu_rel_meas').signals.values;
            simReturn.tSlam = simReturn.simOut.get('WS_eta_relative_hat').time;
            simReturn.etaRelativeHat = simReturn.simOut.get('WS_eta_relative_hat').signals.values;
            simReturn.xSlam = simReturn.simOut.get('WS_x_slam').signals.values;
            simReturn.xEkf = simReturn.simOut.get('WS_x_ekf').signals.values;
            simReturn.tEkf = simReturn.simOut.get('WS_x_ekf').time;
            simReturn.entropy = simReturn.simOut.get('WS_entropy_icebergslam').signals.values;
            simReturn.etaIcebergSlam = simReturn.xSlam(:,1:3);
            simReturn.etaRelativeEkf = simReturn.xEkf(:,1:3);
            simReturn.nuRelativeEkf = simReturn.xEkf(:,4:6);
            simReturn.etaIcebergEkf = simReturn.xEkf(:,7:9);
            simReturn.nuIcebergEkf = simReturn.xEkf(:,10:12);
            simReturn.xDot = simReturn.simOut.get('WS_nu_dot_3dof').signals.values;

            for i=1:length(simReturn.simOut.get('xout').signals)
                if(strcmp(simReturn.simOut.get('xout').signals(i).blockName(end-11:end), 'sIcebergSLAM'))
                   simReturn.slamParticleStates = simReturn.simOut.get('xout').signals(i).values;
                   simReturn.tPart = simReturn.simOut.get('xout').time;
                   break;
                end
            end
        end
    else
        if(~isfield('etaSlamNavlab', simReturn) || ~isfield('etaSlamAuv', simReturn))
            if(isfield(simReturn, 'simOut'))
                % Iceberg
                if(~isempty(simReturn.simOut.find('WS_eta_iceberg')))
                    simReturn.tIce = simReturn.simOut.get('WS_eta_iceberg').time;
                    simReturn.etaIceberg = simReturn.simOut.get('WS_eta_iceberg').signals.values;
                    simReturn.nuIceberg = simReturn.simOut.get('WS_nu_iceberg').signals.values;
                    simReturn.etaRelative = simReturn.simOut.get('WS_eta_rel').signals.values;
                    simReturn.nuRelative = simReturn.simOut.get('WS_nu_rel').signals.values;
                    simReturn.nuRelativeMeas = simReturn.simOut.get('WS_nu_rel_meas').signals.values;
                    %% REMOVE (FIX for wrong parameter in simulations)
                    if(flags.fixBrokenOldRun)
                        headingErrorRad = 60;
                        simReturn.etaIceberg(:,3) = zeros(length(simReturn.etaIceberg(:,3)),1);
                        simReturn.etaRelative(:,6) = simReturn.etaRelative(:,6) + headingErrorRad*ones(length(simReturn.etaRelative(:,6)),1);
                        rotFix = mod(headingErrorRad, 2*pi);
                        for i=1:length(simReturn.etaRelative)
                            simReturn.etaRelative(i,1:3) = (Rzyx(0,0,rotFix)*simReturn.etaRelative(i,1:3)')';
                        end
                    end
                end
                % SLAM
                if(~isempty(simReturn.simOut.find('WS_eta_relative_hat')))
                    simReturn.tSlam = simReturn.simOut.get('WS_eta_relative_hat').time;
                    simReturn.etaRelativeHat = simReturn.simOut.get('WS_eta_relative_hat').signals.values;
                    simReturn.xSlam = simReturn.simOut.get('WS_x_slam').signals.values;
                    simReturn.xEkf = simReturn.simOut.get('WS_x_ekf').signals.values;
                    simReturn.tEkf = simReturn.simOut.get('WS_x_ekf').time;
                    simReturn.entropy = simReturn.simOut.get('WS_entropy_icebergslam').signals.values;
                    simReturn.etaIcebergSlam = simReturn.xSlam(:,1:3);
                    simReturn.etaRelativeEkf = simReturn.xEkf(:,1:3);
                    simReturn.nuRelativeEkf = simReturn.xEkf(:,4:6);
                    simReturn.etaIcebergEkf = simReturn.xEkf(:,7:9);
                    simReturn.nuIcebergEkf = simReturn.xEkf(:,10:12);
                end
                % Multibeam reader
                if(~isempty(simReturn.simOut.find('WS_x_auv')))
                    simReturn.tMbe = simReturn.simOut.get('WS_x_auv').time;
                    simReturn.xAuv = simReturn.simOut.get('WS_x_auv').signals.values;
                    simReturn.tAuv = simReturn.simOut.get('WS_t_auv').signals.values;
                    simReturn.xNavlab = simReturn.simOut.get('WS_x_navlab').signals.values;
                    simReturn.mbeRanges = simReturn.simOut.get('WS_mbe_ranges').signals.values;
                    simReturn.mbeAngles = simReturn.simOut.get('WS_mbe_angles').signals.values;
                    simReturn.eta = simReturn.xAuv(:,1:6);
                    simReturn.nu = simReturn.xAuv(:,7:12);
                    simReturn.etaNavlab = simReturn.xNavlab(:,1:6);
                    simReturn.velNavlab = simReturn.xNavlab(:,7:9);
                    simReturn.xDot = simReturn.simOut.get('WS_nu_dot_3dof').signals.values;
                    simReturn.mbeDx = simReturn.simOut.get('WS_mbe_dx').signals.values;
                    simReturn.mbeDy = simReturn.simOut.get('WS_mbe_dy').signals.values;
                    simReturn.mbeDz = simReturn.simOut.get('WS_mbe_dz').signals.values;
                end

                if(~isempty(simReturn.simOut.find('xout')))
                    for i=1:length(simReturn.simOut.get('xout').signals)
                        if(strcmp(simReturn.simOut.get('xout').signals(i).blockName(end-11:end), 'sIcebergSLAM'))
                           simReturn.slamParticleStates = simReturn.simOut.get('xout').signals(i).values;
                           simReturn.tPart = simReturn.simOut.get('xout').time;
                           break;
                        end
                    end
                end
            end
        end
    end
end

function simReturn = calculateSlamTrajectory(simReturn, conf, matStr)
    if(~isfield(simReturn, 'etaSlamNavlab') || ~isfield(simReturn, 'etaSlamAuv'))
        etaSlamNavlab = zeros(length(simReturn.slamParticleStates(:,1)), 3);
        etaSlamAuv = zeros(length(simReturn.slamParticleStates(:,1)), 3);
        for k=1:length(simReturn.slamParticleStates(:,1))
            for i=1:conf.iceslam.nParticles
                idx = (i-1)*3 + 1;
                etaIceberg = simReturn.slamParticleStates(k,idx:idx+2)';
                etaSlamCurrent = Rzyx(0, 0, etaIceberg(3))'*(simReturn.etaNavlab(k,[1:2,6])' - etaIceberg);
                etaSlamNavlab(k,:) = etaSlamNavlab(k,:) + etaSlamCurrent';
                etaSlamAuv(k,:) = etaSlamAuv(k,:) + etaSlamCurrent';
            end
            etaSlamNavlab(k,:) = etaSlamNavlab(k,:)./conf.iceslam.nParticles;
            etaSlamAuv(k,:) = etaSlamAuv(k,:)./conf.iceslam.nParticles;
        end
        simReturn.etaSlamNavlab = etaSlamNavlab;
        simReturn.etaSlamAuv = etaSlamAuv;
        save([matStr, 'auvsim.mat'], '-struct', 'simReturn');
    end
end

%% Function for plotting iceberg pose variance
function fig = plotIcebergPoseVariance(results, configs, flags, fontSize, lineWidth)
    
    % Baseline cloud
    baseCloud = load(flags.baselineCloud);  
    sigmaXBase =  interp1(baseCloud.time, baseCloud.sigmaXBase, flags.cTime);
    sigmaYBase = interp1(baseCloud.time, baseCloud.sigmaYBase, flags.cTime);
    sigmaPsiBase = interp1(baseCloud.time, baseCloud.sigmaPsiBase, flags.cTime);
    
    fig = [];
    legh = zeros(length(results), 1);
    fig =[fig; figure];
    set(fig(end), 'Name', 'SlamNorthVariancePlot');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    ax = axes;
    set(ax,'FontSize',fontSize);
    set(gcf,'Color', [1 1 1]);
    hold on;
    grid on;
    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, cTime);

        xLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
        for i=1:length(slamParticleStates(:,1))
            for j=1:conf.iceslam.nParticles
                xLoc(i,j) = slamParticleStates(i,(j-1)*3+1);
            end
        end

        sigmaX = std(xLoc, 0, 2);

        Xlim = [-3*max([max(sigmaXBase), max(sigmaX)]), 3*max([max(sigmaXBase), max(sigmaX)])];

        plot(cTime, 3.*sigmaX, 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        plot(cTime, -3.*sigmaX, 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
    end
    legh(length(results)+1) = plot(cTime, 3.*sigmaXBase, 'k--', 'LineWidth', lineWidth);
    plot(cTime, -3.*sigmaXBase, 'k--', 'LineWidth', flags.lineWidth);
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('3\sigma bound [m]', 'FontSize', fontSize);
    xlim([0 flags.Tend]);
    ylim(Xlim);
    
    fig =[fig; figure];
    set(fig(end), 'Name', 'SlamEastVariancePlot');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    ax = axes;
    set(ax,'FontSize',fontSize);
    set(gcf,'Color', [1 1 1]);
    hold on;
    grid on;
    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, cTime);

        yLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
        for i=1:length(slamParticleStates(:,1))
            for j=1:conf.iceslam.nParticles
                yLoc(i,j) = slamParticleStates(i,(j-1)*3+2);
            end
        end

        sigmaY = std(yLoc, 0, 2);

        Ylim = [-3*max([max(sigmaYBase), max(sigmaY)]), 3*max([max(sigmaYBase), max(sigmaY)])];

        legh(ix) = plot(cTime, 3.*sigmaY, 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        plot(cTime, -3.*sigmaY, 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
    end
    plot(cTime, 3.*sigmaYBase, 'k--', 'LineWidth', flags.lineWidth);
    plot(cTime, -3.*sigmaYBase, 'k--', 'LineWidth', flags.lineWidth);
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('3\sigma bound [m]', 'FontSize', fontSize);
    xlim([0 flags.Tend]);
    ylim(Ylim);     

    if(flags.plotHeading)
        fig =[fig; figure];
        set(fig(end), 'Name', 'SlamPsiVariancePlot');
        set(fig(end), 'Position', get(0,'Screensize'));
        set(fig(end), 'Units', 'Normalized');
        set(fig(end), 'outerposition', [0 0 1 1]);
        set(fig(end), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        hold on;
        grid on;
        for ix=1:length(results)
            conf = configs{ix}.conf;
            simReturn = results{ix};
            cTime = simReturn.tSlam;
            slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, cTime);
            
            psiLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
            for i=1:length(slamParticleStates(:,1))
                for j=1:conf.iceslam.nParticles
                    psiLoc(i,j) = slamParticleStates(i,(j-1)*3+3);
                end
            end
            sigmaPsi = std(psiLoc, 0, 2);

            Psilim = rad2deg([-3*max([max(sigmaPsiBase), max(sigmaPsi)]), 3*max([max(sigmaPsiBase), max(sigmaPsi)])]);

            plot(cTime, 3.*rad2deg(sigmaPsi), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
            plot(cTime, -3.*rad2deg(sigmaPsi), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        end
        plot(cTime, 3.*rad2deg(sigmaPsiBase), 'k--', 'LineWidth', flags.lineWidth);
        plot(cTime, -3.*rad2deg(sigmaPsiBase), 'k--', 'LineWidth', flags.lineWidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('3\sigma bound [deg]', 'FontSize', fontSize);
        xlim([0 flags.Tend]);
        ylim(Psilim);
    end
end

%% Function for plotting iceberg pose error
function fig = plotIcebergPoseError(results, configs, flags, fontSize, lineWidth)
    fig = [];
    legh = zeros(length(results), 1);
    fig =[fig; figure];
    set(fig(end), 'Name', 'EkfPositionErrorPlot');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    ax = axes;
    set(ax,'FontSize',fontSize);
    set(gcf,'Color', [1 1 1]);
    hold on;
    grid on;
    maxError = zeros(length(flags.colorPlot(:,1)), 1);
    legStr = cell(length(flags.colorPlot(:,1)), 1);
    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        if(~conf.mbe.realData)
            etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, cTime);
            etaIcebergEkf = interp1(simReturn.tEkf, simReturn.etaIcebergEkf, cTime);
            
            error = zeros(length(etaIcebergEkf(:,1)), 1);
            for i = 1:length(error)
               error(i) = norm(etaIcebergEkf(i,1:2) - etaIceberg(i,1:2), 2);  
            end
        else
            etaSlam = interp1(simReturn.tSlam, simReturn.etaSlamNavlab, cTime);
            etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, cTime);
            
            error = zeros(length(etaSlam(:,1)), 1);
            for i = 1:length(error)
               error(i) = norm(etaSlam(i,1:2) - etaNavlab(i,1:2), 2);  
            end
        end

        legh(ix) = plot(cTime, error, 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        maxError(ix) = max(error);
        legStr{ix} = sprintf("%s (max error = %0.2f)", flags.legStr{ix}, maxError(ix));
    end
    
    if(conf.mbe.realData)
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, cTime);
        eta = interp1(simReturn.tAuv, simReturn.eta, cTime);
        
        error = zeros(length(etaNavlab(:,1)), 1);
        for i = 1:length(error)
           error(i) = norm(eta(i,1:2) - etaNavlab(i,1:2), 2);  
        end
        legh(ix+1) = plot(cTime, error, 'Color', flags.colorPlot(ix+1,:), 'LineWidth', lineWidth);
        maxError(ix+1) = max(error);
        legStr{ix+1} = sprintf("%s (max error = %0.2f)", flags.legStr{ix+1}, maxError(ix+1));
    end
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Error norm [m]', 'FontSize', fontSize);
    xlim([0 flags.Tend]);
    ylim([0, ceil(max(maxError))]);
    if(flags.showLegend)
        if(length(legStr) == 3)
            legend(legh, legStr{1}, legStr{2}, legStr{3});
        elseif(length(legStr) == 2)
            legend(legh, legStr{1}, legStr{2});
        elseif(length(legStr) == 4)
            legend(legh, legStr{1}, legStr{2}, legStr{3}, legStr{4});
        else
            legend(legh, legStr{1});
        end  
    end
    
    if(flags.plotHeading)
        fig =[fig; figure];
        set(fig(end), 'Name', 'EkfPsiErrorPlot');
        set(fig(end), 'Position', get(0,'Screensize'));
        set(fig(end), 'Units', 'Normalized');
        set(fig(end), 'outerposition', [0 0 1 1]);
        set(fig(end), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        hold on;
        grid on;
        for ix=1:length(results)
            conf = configs{ix}.conf;
            simReturn = results{ix};
            cTime = simReturn.tSlam;
            psiIceberg = interp1(simReturn.tIce, simReturn.etaIceberg(:,3), cTime);
            psiIcebergEkf = interp1(simReturn.tEkf, simReturn.etaIcebergEkf(:,3), cTime);
            
            plot(cTime, rad2deg(abs(psiIcebergEkf - psiIceberg)), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        end
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Abs. error [deg]', 'FontSize', fontSize);
        xlim([0 flags.Tend]);
    end
end

%% Function for plotting iceberg pose error
function fig = plotIcebergVelocityError(results, configs, flags, fontSize, lineWidth)
    fig = [];
    fig =[fig; figure];
    set(fig(end), 'Name', 'EkfVelocityUErrorPlot');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    ax = axes;
    set(ax,'FontSize',fontSize);
    set(gcf,'Color', [1 1 1]);
    hold on;
    grid on;
    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        nuIceberg = interp1(simReturn.tIce, simReturn.nuIceberg, cTime);
        nuIcebergEkf = interp1(simReturn.tEkf, simReturn.nuIcebergEkf, cTime);
    
        plot(cTime(2:end), abs(nuIcebergEkf(2:end,1) - nuIceberg(2:end,1)), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
    end
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Abs. velocity error [m/s]', 'FontSize', fontSize);
    xlim([0 flags.Tend]);

    fig =[fig; figure];
    set(fig(end), 'Name', 'EkfVelocityVErrorPlot');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    ax = axes;
    set(ax,'FontSize',fontSize);
    set(gcf,'Color', [1 1 1]);
    hold on;
    grid on;
    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        cTime = simReturn.tSlam;
        nuIceberg = interp1(simReturn.tIce, simReturn.nuIceberg, cTime);
        nuIcebergEkf = interp1(simReturn.tEkf, simReturn.nuIcebergEkf, cTime);
    
        plot(cTime(2:end), abs(nuIcebergEkf(2:end,2) - nuIceberg(2:end,2)), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
    end
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Abs. velocity error [m/s]', 'FontSize', fontSize);
    xlim([0 flags.Tend]);
    
    if(flags.plotHeading)
        fig =[fig; figure];
        set(fig(end), 'Name', 'EkfVelocityRErrorPlot');
        set(fig(end), 'Position', get(0,'Screensize'));
        set(fig(end), 'Units', 'Normalized');
        set(fig(end), 'outerposition', [0 0 1 1]);
        set(fig(end), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        hold on;
        grid on;
        for ix=1:length(results)
            conf = configs{ix}.conf;
            simReturn = results{ix};
            cTime = simReturn.tSlam;
            nuIceberg = interp1(simReturn.tIce, simReturn.nuIceberg, cTime);
            nuIcebergEkf = interp1(simReturn.tEkf, simReturn.nuIcebergEkf, cTime);

            plot(cTime(2:end), rad2deg(abs(nuIcebergEkf(2:end,3) - nuIceberg(2:end,3))), 'Color', flags.colorPlot(ix,:), 'LineWidth', lineWidth);
        end
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Abs. velocity error [deg/s]', 'FontSize', fontSize);
        xlim([0 flags.Tend]);
    end
end

function fig = plotTrajectoryFigure(simReturn, conf, flags, fontSize)

    auvSize = conf.anim.auvSize;
    mbeStruct = conf.mbe;
    mapSize = conf.iceslam.mapSize;
    nParticles = conf.iceslam.nParticles;
    
    fig = [];
    
    % Interpolate data to fit times
    animStop = floor(min(simReturn.tSlam(end), flags.Tend));
    animTime = 0:1:animStop;
    etaAuv = interp1(simReturn.tAuv, simReturn.eta, animTime);
    etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, animTime);
    etaRelReal = interp1(simReturn.tIce, simReturn.etaRelative, animTime);
    poseRelReal = [etaRelReal(:, 1), etaRelReal(:, 2), etaRelReal(:, 6)];
    ranges = interp1(simReturn.tMbe, simReturn.mbeRanges, animTime);
    angles = interp1(simReturn.tMbe, simReturn.mbeAngles, animTime);
    slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, animTime);
    etaRelEst = interp1(simReturn.tEkf, simReturn.etaRelativeEkf, animTime);
    etaIcebergEst = interp1(simReturn.tEkf, simReturn.etaIcebergEkf, animTime);
    
    if(~conf.mbe.realData)
        WP = conf.wplist;
        icebergFileName = conf.anim.icebergFile;
        rangesNoise = interp1(simReturn.tMbe, simReturn.mbeBeamsWNoise, animTime);
        etaNavlab = [];
    else
       WP = [];
       icebergFileName = '';
       rangesNoise = zeros(size(ranges));
       etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, animTime);
    end
  
    % Ice grid figure
    fig =[fig; figure];
    set(fig(end), 'Name', 'IcebergRelativeTrajectory');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    set(gcf,'Color', [1 1 1]);

    ice = animateIceGrid(fig, 121, auvSize, mapSize, conf.iceslam.mapNegativePart, ...
                        icebergFileName, etaRelEst(1,:)', poseRelReal(1,:)', etaIcebergEst(1,:)', ...
                        etaIceberg(1,:)', [etaAuv(1,1); etaAuv(1,2); etaAuv(1,6)], [], ...
                        etaNavlab, nParticles, fontSize, flags.lineWidth, 1);
    
    if(conf.anim.enableParticlePlots)           
        particles = repmat(struct('etaIceberg',[0; 0; 0], 'nuIceberg', [0; 0; 0]), 0, 1);
    else
        particles = [];
    end
    
    particleCM = flipud(autumn(length(flags.ParticleCloudSnapshots)));
    for i=1:length(flags.ParticleCloudSnapshots)
        time = flags.ParticleCloudSnapshots(i);
        
        if(conf.anim.enableParticlePlots)
            for j=1:conf.iceslam.nParticles
                idx = (j-1)*3 + 1;
                particles(j).etaIceberg = slamParticleStates(time, idx:idx+2)';
            end
            ice.setEtaAuv([etaAuv(time,1); etaAuv(time,2); etaAuv(time,6)]);
            ice.updateParticles(particles, particleCM(i,:), 1);
        end
    end
    
    if(~conf.mbe.realData)
        ice.update(etaRelEst(flags.Tend,:)', poseRelReal(flags.Tend,:)', etaIceberg(flags.Tend,:)', ...
            etaIcebergEst(flags.Tend,:)', [etaAuv(flags.Tend,1); etaAuv(flags.Tend,2); etaAuv(flags.Tend,6)], []);
    else
        ice.update(etaRelEst(flags.Tend,:)', poseRelReal(flags.Tend,:)', etaIceberg(flags.Tend,:)', ...
            etaIcebergEst(flags.Tend,:)', [etaAuv(flags.Tend,1); etaAuv(flags.Tend,2); etaAuv(flags.Tend,6)], etaNavlab(flags.Tend, :));
    end
    
    ice.addFullRealTrajectory(etaRelReal(1:flags.Tend,1:2));
    ice.addFullEstTrajectory(etaRelEst(1:flags.Tend,1:2));
    if(conf.mbe.realData)
        ice.addFullNavlabTrajectory(etaNavlab(1:flags.Tend,1:2));
    end
    axis tight;
    
     % Multibeam figure                
    fig =[fig; figure];
    set(fig(end), 'Name', 'MultibeamSwath');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    set(gcf,'Color', [1 1 1]);

    mbe = animateMbe2dBeams(fig, 222, mbeStruct, etaAuv(1,:)', etaIceberg(1,:)', etaRelReal(1,:)', ...
                        ranges(1,:), rangesNoise(1,:), angles(1,:), 1.0, icebergFileName, ...
                        fontSize, flags.lineWidth, 1);
                    
    mbe.update(etaAuv(flags.Tend,:)', etaIceberg(flags.Tend,:)', etaRelReal(flags.Tend,:)', ranges(flags.Tend,:), rangesNoise(flags.Tend,:), angles(flags.Tend,:));
    axis tight;
    
    % Trajectory in NED frame
    fig =[fig; figure];
    set(fig(end), 'Name', 'GlobalTrajectory');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    set(gcf,'Color', [1 1 1]);

    trj = animateAuvAndIcebergTrajectory(fig, 224, auvSize, icebergFileName, ...
                   WP, etaAuv(1,1:6)', etaIceberg(1,:)', etaIcebergEst(1,:)', ...
                   fontSize, flags.lineWidth, 1);
               
    trj.setAuvPose(etaAuv(flags.Tend,1), etaAuv(flags.Tend,2), etaAuv(flags.Tend,6));		  
    trj.setIcebergPose(etaIceberg(flags.Tend,1), etaIceberg(flags.Tend,2), etaIceberg(flags.Tend,3));
    trj.setEstimatorPose(etaIcebergEst(flags.Tend,1), etaIcebergEst(flags.Tend,2), etaIcebergEst(flags.Tend,3));
    trj.update();
               
    trj.addFullAuvTrajectory(etaAuv(1:flags.Tend,1:2));
    trj.addFullIcebergTrajectory(etaIceberg(1:flags.Tend,1:2));
    trj.addFullIcebergEstTrajectory(etaIcebergEst(1:flags.Tend,1:2));
    axis tight;
end

function fig = plotHuginTrajectoryFigure(results, configs, flags, fontSize, selectedCloud)

    fig = [];
    
    fig =[fig; figure];
    set(fig(end), 'Name', 'HuginRelativeTrajectory');
    set(fig(end), 'Position', get(0,'Screensize'));
    set(fig(end), 'Units', 'Normalized');
    set(fig(end), 'outerposition', [0 0 1 1]);
    set(fig(end), 'Units', 'Pixels');
    set(gcf,'Color', [1 1 1]);

    for ix=1:length(results)
        conf = configs{ix}.conf;
        simReturn = results{ix};
        
        auvSize = conf.anim.auvSize;
        mapSize = conf.iceslam.mapSize;
        nParticles = conf.iceslam.nParticles;
    
        % Interpolate data to fit times
        animStop = floor(min(simReturn.tSlam(end), flags.Tend));
        animTime = 0:1:animStop;
        etaAuv = interp1(simReturn.tAuv, simReturn.eta(:,[1:2,6]), animTime);
        etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, animTime);
        etaRelReal = interp1(simReturn.tIce, simReturn.etaRelative, animTime);
        poseRelReal = [etaRelReal(:, 1), etaRelReal(:, 2), etaRelReal(:, 6)];
        slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, animTime);
        etaRelEst = interp1(simReturn.tSlam, simReturn.etaSlamNavlab, animTime);
        etaIcebergEst = interp1(simReturn.tSlam, simReturn.etaIcebergSlam, animTime);

       icebergFileName = '';
       etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, animTime);

        ice = animateIceGrid(fig, 121, auvSize, mapSize, conf.iceslam.mapNegativePart, ...
                            icebergFileName, etaRelEst(1,:)', poseRelReal(1,:)', etaIcebergEst(1,:)', ...
                            etaIceberg(1,:)', etaAuv(1,:)', [], ...
                            etaNavlab(1,:)', nParticles, fontSize, flags.lineWidth, flags.colorPlot(ix,:), 1, flags.disableEkfPlot);

        if(conf.anim.enableParticlePlots && ix == selectedCloud)           
            particles = repmat(struct('etaIceberg',[0; 0; 0], 'nuIceberg', [0; 0; 0]), 0, 1);
            
            particleCM = flipud(autumn(length(flags.ParticleCloudSnapshots)));
            for i=1:length(flags.ParticleCloudSnapshots)
                time = flags.ParticleCloudSnapshots(i);

                if(conf.anim.enableParticlePlots)
                    for j=1:conf.iceslam.nParticles
                        idx = (j-1)*3 + 1;
                        particles(j).etaIceberg = slamParticleStates(time, idx:idx+2)';
                        particles(j).etaAuv = etaNavlab(time,[1:2,6])';
                    end
                    ice.setEtaAuv(etaAuv(time,1:3)');
                    ice.updateParticles(particles, particleCM(i,:), 1, 1);
                end
            end
        end

        if(~conf.mbe.realData)
            ice.update(etaRelEst(flags.Tend,:)', poseRelReal(flags.Tend,:)', etaIceberg(flags.Tend,:)', ...
                etaIcebergEst(flags.Tend,:)', [etaAuv(flags.Tend,1); etaAuv(flags.Tend,2); etaAuv(flags.Tend,6)], []);
        else
            ice.update(etaRelEst(flags.Tend,:)', poseRelReal(flags.Tend,:)', etaIceberg(flags.Tend,:)', ...
                etaIcebergEst(flags.Tend,:)', etaAuv(flags.Tend,:)', etaNavlab(flags.Tend, :));
        end

        ice.addFullRealTrajectory(etaRelReal(1:flags.Tend,1:2));
        ice.addFullEstTrajectory(etaRelEst(1:flags.Tend,1:2));
        if(conf.mbe.realData)
            ice.addFullNavlabTrajectory(etaNavlab(1:flags.Tend,1:2));
        end
    end
    xlabel('North [m]', 'FontSize', fontSize);
    ylabel('East [m]', 'FontSize', fontSize);
    axis tight;
end

function fig = plotObservationMap(simReturn, conf, flags, fontSize)
    fig = [];
    
    %% Extract pointcloud
    if(exist([conf.saveDir, 'PointCloud.txt'], 'file'))
        try
            cloud = dlmread([conf.saveDir, 'PointCloud.txt']);
        catch ex
            for i=1:length(ex.stack)
                fprintf('ERROR: Reading PointCloud.txt failed. Exception: "%s" on line %i in file %s.\n'...
                    , ex.message, ex.stack(i).line, ex.stack(i).file);
            end
            return;
        end
    end
    
    %%
    % Extract data from file
    if(exist([conf.saveDir, 'ObservationMap.txt'], 'file'))
        try
            map = dlmread([conf.saveDir, 'ObservationMap.txt']);
        catch ex
            for i=1:length(ex.stack)
                fprintf('ERROR: Reading ObservationMap.txt failed. Exception: "%s" on line %i in file %s.\n'...
                    , ex.message, ex.stack(i).line, ex.stack(i).file);
            end
            return;
        end

        res = conf.iceslam.mapResolution;
        bounds = [min(map(:,1)) - res/2, max(map(:,1)) - res/2, min(map(:,2)) - res/2, max(map(:,2)) - res/2];

        xx = bounds(1):res:bounds(2);
        yy = bounds(3):res:bounds(4);
        zz = zeros(length(xx), length(yy));
        zzOmega = zeros(length(xx), length(yy));

        for i = 1:length(map(:,1))
            zz(map(i, 6) + 1, map(i, 5) + 1) =  map(i, 3)/map(i, 4);
            zzOmega(map(i, 6) + 1, map(i, 5) + 1) =  1/map(i, 4);
        end
    else
        return;
    end
    
    % Check
    for j = 1:length(yy)
        for i = 1:length(xx)
            idx = length(xx)*(j - 1) + i;
            if((i - 1) ~= map(idx, 5) || (j - 1) ~= map(idx, 6))
                fprintf('Error: Index is wrong (i,j) = (%i,%i) while (map.i,map.j)= (%i,%i).\n', i, j, map(idx, 5), map(idx, 6)); 
                return;
            end
            if((yy(j) + res/2) ~= map(idx, 2) || (xx(i) + res/2) ~= map(idx, 1))
                fprintf('Error: Point (%f, %f) ~= (%f,%f) is wrong at (i,j) = (%i,%i).\n', xx(i) + res/2, yy(j) + res/2, map(idx, 1), map(idx, 2), i, j); 
                return;
            end
            if(~isnan(zz(j,i)) || ~isnan(map(idx, 3)/map(idx, 4)))
                if(map(idx, 3)/map(idx, 4) ~= zz(j,i))
                    fprintf('Error: Value = %f ~= %f at (i,j) = (%i,%i).\n', map(idx, 3)/map(idx, 4), zz(j,i), i, j); 
                    return;
                end
            end
        end
    end
    
    if(~isempty(xx) && ~isempty(yy) && ~isempty(zz))
        if(flags.plotObsMap)
            fig = [fig; figure];
            titlestr = sprintf("Observation map.");
            set(fig(end), 'Position', get(0,'Screensize'));
            set(fig(end), 'Units', 'Normalized');
            set(fig(end), 'outerposition', [0 0 1 1]);
            set(fig(end), 'Name', 'ObservationMap');
            set(gcf,'Color', [1 1 1]);
            ax = axes(fig(end));
            hold on;
            set(get(ax, 'Title'), 'String', titlestr);
            set(ax,'FontSize',fontSize);
            plotDEM(xx, yy, zz, res, 'interp', ax);
            xlabel(ax, 'North [m]', 'FontSize', fontSize);
            ylabel(ax, 'East [m]', 'FontSize', fontSize);
            zlabel(ax, 'Depth [m]', 'FontSize', fontSize);
            set(ax,'zdir','reverse');
            axis tight;
            view(35, -15);
        end
            
        if(flags.plotErrMap)
            if(~isempty(conf.anim.icebergFile))
                iceberg = load(conf.anim.icebergFile);

                Zestgrid = griddata(xx, yy, zz, iceberg.grid.Nq, iceberg.grid.Eq, 'cubic');

                ErrGrid = iceberg.grid.Zq - Zestgrid;
                rmsGrid = sqrt(sum(sum(ErrGrid(~isnan(ErrGrid)).^2))/sum(sum(~isnan(ErrGrid))));

                fig = [fig; figure];
                titlestr = sprintf("Observation error map (RMSE = %.2f).", rmsGrid);
                set(fig(end), 'Position', get(0,'Screensize'));
                set(fig(end), 'Units', 'Normalized');
                set(fig(end), 'outerposition', [0 0 1 1]);
                set(fig(end), 'Name', 'ObservationErrorMap');
                set(gcf,'Color', [1 1 1]);
                set(gcf,'renderer','opengl');
                ax = axes(fig(end));
                hold on;
                set(get(ax, 'Title'), 'String', titlestr);
                set(ax,'FontSize',fontSize);
                surf(iceberg.grid.Nq, iceberg.grid.Eq, abs(ErrGrid), 'EdgeColor','none', 'FaceColor', 'interp');
                cb = colorbar;
                cb.Label.String = 'Topography error [m]';
                cb.Label.FontSize = fontSize;
                xlabel(ax, 'North [m]', 'FontSize', fontSize);
                ylabel(ax, 'East [m]', 'FontSize', fontSize);
                zlabel(ax, 'Error [m]', 'FontSize', fontSize);
                set(ax,'zdir','reverse', 'FontSize', fontSize);
                axis tight;
                view(0, 90);
            end
        end

        if(flags.plotUncMap)
            fig = [fig; figure];
            titlestr = sprintf("Uncertainty map.");
            set(fig(end), 'Position', get(0,'Screensize'));
            set(fig(end), 'Units', 'Normalized');
            set(fig(end), 'outerposition', [0 0 1 1]);
            set(fig(end), 'Name', 'UncertaintyMap');
            set(gcf,'Color', [1 1 1]);
            ax = axes(fig(end));
            hold on;
            set(get(ax, 'Title'), 'String', titlestr);
            surf(xx, yy, zzOmega, 'EdgeColor','none', 'FaceColor', 'interp');
            colorbar;
            xlabel(ax, 'North [m]');
            ylabel(ax, 'East [m]');
            zlabel(ax, 'Uncertainty [m]');
            set(ax,'zdir','reverse');
            axis tight;
            view(0, 90);
        end
    end
end