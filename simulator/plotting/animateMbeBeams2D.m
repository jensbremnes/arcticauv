function beamFig = animateMbeBeams2D(simReturn, conf)
% animateMbeBeams2D generate a video animatimation of the MBE ranges.
%
% beamFig = animateMbeBeams2D(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink.
%   conf            - Configuration structure.
%
% Output parameters:
%   beamFig         - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2016.02.05  PN Created function
%

    tvCommon = simReturn.mbe.time;
    tvDelta = min(diff(tvCommon));
    etaMbe = interp1(simReturn.t, simReturn.eta, tvCommon);
    rangesMbe = simReturn.mbe.ranges;

    % Check that start and end time is not out of bounds and find indices
    conf.animoptions.endTime = min(max(simReturn.t), conf.animoptions.endTime);
    conf.animoptions.startTime = min(conf.animoptions.endTime, conf.animoptions.startTime);
    [~, startTimeIdx] = min(abs(tvCommon - conf.animoptions.startTime));
    [~, endTimeIdx] = min(abs(tvCommon - conf.animoptions.endTime));
    
    try
        if(~exist(conf.videoDir, 'dir'))
            mkdir(conf.videoDir);
        end
        cd(conf.videoDir);

        % Set up video object
        if(~conf.animoptions.showFirstFrameOnly)
            fileName = [datestr(datetime, 'yyyymmddHHMMSS'), '_2DBeamAnimation.avi'];
            vidObj = VideoWriter(fileName);
            vidObj.Quality = 100;
            vidObj.FrameRate = 1/tvDelta;
            open(vidObj);
        end
        beamFig = figure;
        hold on;
        title('Multibeam points and beams 2D', 'FontSize', conf.animoptions.fontSize);
        set(beamFig, 'Position', conf.animoptions.figPosition);
        set(gcf, 'Renderer', 'painters');

       %  Set up world (first frame)
        ptc = plot(0, etaMbe(startTimeIdx, 3), 'bo');
        axis equal;
        set(get(beamFig, 'CurrentAxes'), 'Ydir', 'reverse');
        set(get(beamFig, 'CurrentAxes'), 'XLim', conf.animoptions.xLim, 'YLim', conf.animoptions.yLim, 'FontSize', conf.animoptions.numberSize);
        xlabel('Cross-track distance [m]', 'FontSize', conf.animoptions.fontSize);
        ylabel('Depth [m]', 'FontSize', conf.animoptions.fontSize);
        grid;
        
        % Create AUV object
        beamDrawer2D = BeamDraw2D(conf.mbeConfig, 1);
        beamDrawer2D.createBeams(etaMbe(startTimeIdx,:)', rangesMbe(startTimeIdx,:), tvCommon(startTimeIdx));
        
        % Write first frame
        set(beamFig, 'Position', conf.animoptions.figPosition);
        if(~conf.animoptions.showFirstFrameOnly)
            writeVideo(vidObj, getframe(beamFig));
        end
        timeIdx = startTimeIdx + 1;

        % Write animation (adjust framerate to sample time)
        if(~conf.animoptions.showFirstFrameOnly)
            for i=timeIdx:endTimeIdx
                beamDrawer2D.update(etaMbe(i,:)', rangesMbe(i,:), tvCommon(i));
                set(ptc, 'YData', etaMbe(i,3));
                drawnow;
                set(beamFig, 'Position', conf.animoptions.figPosition);
                writeVideo(vidObj, getframe(beamFig));
            end
            close(vidObj);
        end
        
    catch ME
        if(~conf.animoptions.showFirstFrameOnly)
            close(vidObj);  
        end
        cd(conf.rootDir);
        for j = 1:length(ME.stack)
            msg = ['Error: ', ME.message, ' In function: ', ME.stack(j).name, ' at line: ', num2str(ME.stack(j).line), '.'];
            disp(msg);
        end
        return;
    end
    cd(conf.rootDir);
end

