function fig = plotEkf(simReturn, conf, flags)
% plotEkf shows different EKF plots for the iceberg SLAM.
%
%  fig = plotEkf(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%   flags           - Plotting flags
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    
    % Interpolate data to fit times
    idxEnd = min(ceil(conf.plot.Tend/diff(simReturn.tSlam(1:2)))+1, length(simReturn.tSlam));
    cTime = simReturn.tSlam(1:idxEnd);
    etaRelative = interp1(simReturn.t, simReturn.etaRelative(:, [1:2 6]), cTime);
    nuRelative = interp1(simReturn.t, simReturn.nuRelative(:, [1:2 6]), cTime);
    nuRelativeMeas = interp1(simReturn.t, simReturn.nuRelativeMeas(:, [1:2 6]), cTime);
    etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, cTime);
    nuIceberg = interp1(simReturn.tIce, simReturn.nuIceberg, cTime);
    etaRelativeEkf = interp1(simReturn.tSlam, simReturn.xEkf(:,1:3), cTime);
    nuRelativeEkf = interp1(simReturn.tSlam, simReturn.xEkf(:,4:6), cTime);
    etaIcebergEkf = interp1(simReturn.tSlam, simReturn.xEkf(:,7:9), cTime);
    nuIcebergEkf = interp1(simReturn.tSlam, simReturn.xEkf(:,10:12), cTime);
    etaIcebergSlam = interp1(simReturn.tSlam, simReturn.xSlam(:,1:3), cTime);
    
%     Xlim = [-3*max(sigmaXBase), 3*max(sigmaXBase)];
%     Ylim = [-3*max(sigmaYBase), 3*max(sigmaYBase)];
%     Psilim = [-3*max(sigmaPsiBase), 3*max(sigmaPsiBase)];
    
    fontSize = conf.plot.fontsize + 8;
    
    % Read EKF covariance
    covMat = dlmread([conf.saveDir, 'covaranceMatrix.txt']);
    traceCov = zeros(length(covMat(:,1) + 1), 1);
    P_cell = cell(length(covMat(:,1) + 1), 1);
    sigma_Xrel = zeros(length(covMat(:,1) + 1), 1);
    sigma_Yrel = zeros(length(covMat(:,1) + 1), 1);
    sigma_psirel = zeros(length(covMat(:,1) + 1), 1);
    for i = 0:length(traceCov)
        if(i == 0)
            P = zeros(12);
        else
            P = reshape(covMat(i, :), 12, 12);
        end
        P_cell{i+1} = P;
        sigma_Xrel(i+1) = sqrt(P(1,1));
        sigma_Yrel(i+1) = sqrt(P(2,2));
        sigma_psirel(i+1) = sqrt(P(3,3));
        traceCov(i+1) = trace(P);
    end
    sigma_Xrel = interp1(simReturn.tSlam, sigma_Xrel, cTime);
    sigma_Yrel = interp1(simReturn.tSlam, sigma_Yrel, cTime);
    sigma_psirel = interp1(simReturn.tSlam, sigma_psirel, cTime);
    
    % Read EKF covariance without update
    covMatnU = dlmread([conf.saveDir, 'covaranceMatrixNoUpdate.txt']);
    traceCovnU = zeros(length(covMatnU(:,1) + 1), 1);
    P_cellnU = cell(length(covMatnU(:,1) + 1), 1);
    sigma_XrelnU = zeros(length(covMatnU(:,1) + 1), 1);
    sigma_YrelnU = zeros(length(covMatnU(:,1) + 1), 1);
    sigma_psirelnU = zeros(length(covMatnU(:,1) + 1), 1);
    for i = 0:length(traceCovnU)
        if(i == 0)
            P = zeros(12);
        else
            P = reshape(covMatnU(i, :), 12, 12);
        end
        P_cellnU{i+1} = P;
        sigma_XrelnU(i+1) = sqrt(P(1,1));
        sigma_YrelnU(i+1) = sqrt(P(2,2));
        sigma_psirelnU(i+1) = sqrt(P(3,3));
        traceCovnU(i+1) = trace(P);
    end
    sigma_XrelnU = interp1(simReturn.tSlam, sigma_XrelnU, cTime);
    sigma_YrelnU = interp1(simReturn.tSlam, sigma_YrelnU, cTime);
    sigma_psirelnU = interp1(simReturn.tSlam, sigma_psirelnU, cTime);
    
    fig = [];
    count = 0;
    if(~flags.exportFigs)
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'IcebergStateComparisonPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        s1 = subplot(2, 3, 1, 'FontSize', fontSize);
        title(s1, 'p_{ice,x}');
        hold on;
        grid on;
        plot(cTime, etaIceberg(:,1), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaIcebergSlam(:,1), 'r', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaIcebergEkf(:,1), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg x-position [m]', 'FontSize', fontSize);
    %     xlim([0 conf.plot.Tend]);
    %     ylim(Xlim);
        s2 = subplot(2, 3, 2, 'FontSize', fontSize);
        title(s2, 'p_{ice,y}');
        hold on;
        grid on;
        plot(cTime, etaIceberg(:,2), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaIcebergSlam(:,2), 'r', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaIcebergEkf(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg y-position [m]', 'FontSize', fontSize);
    %     xlim([0 conf.plot.Tend]);
    %     ylim(Ylim);
        s3 = subplot(2, 3, 3, 'FontSize', fontSize);
        title(s3, '\psi_{ice}');
        hold on;
        grid on;
        h1 = plot(cTime, rad2deg(etaIceberg(:,3)), 'k', 'LineWidth', conf.plot.linewidth);
        h2 = plot(cTime, rad2deg(etaIcebergSlam(:,3)), 'r', 'LineWidth', conf.plot.linewidth);
        h3 = plot(cTime, rad2deg(etaIcebergEkf(:,3)), 'b', 'LineWidth', conf.plot.linewidth);
        legend([h1, h2, h3], 'Real', 'SLAM estimate', 'EKF estimate');
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg heading [deg]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ymin = rad2deg(min([min(etaIceberg(:,3)), min(etaIcebergEkf(:,3)), min(etaIcebergSlam(:,3))]));
        ymax = rad2deg(max([max(etaIceberg(:,3)), max(etaIcebergEkf(:,3)), min(etaIcebergSlam(:,3))]));
        ylim([ymin - 0.2, ymax + 0.2]);
        s4 = subplot(2, 3, 4, 'FontSize', fontSize);
        title(s4, 'u_{ice}');
        hold on;
        grid on;
        plot(cTime, nuIceberg(:,1), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuIcebergEkf(:,1), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg u-velocity [m/s]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ymin = min([min(nuIceberg(:,1)), min(nuIcebergEkf(:,1))]);
        ymax = max([max(nuIceberg(:,1)), max(nuIcebergEkf(:,1))]);
        ylim([ymin - 0.2, ymax + 0.2]);
        s5 = subplot(2, 3, 5, 'FontSize', fontSize);
        title(s5, 'v_{ice}');
        hold on;
        grid on;
        plot(cTime, nuIceberg(:,2), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuIcebergEkf(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg v-velocity [m/s]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ymin = min([min(nuIceberg(:,2)), min(nuIcebergEkf(:,2))]);
        ymax = max([max(nuIceberg(:,2)), max(nuIcebergEkf(:,2))]);
        ylim([ymin - 0.2, ymax + 0.2]);
        s6 = subplot(2, 3, 6, 'FontSize', fontSize);
        title(s6, 'r_{ice}');
        hold on;
        grid on;
        plot(cTime, rad2deg(nuIceberg(:,3)), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, rad2deg(nuIcebergEkf(:,3)), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Iceberg rotation [deg/s]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ymin = rad2deg(min([min(nuIceberg(:,3)), min(nuIcebergEkf(:,3))]));
        ymax = rad2deg(max([max(nuIceberg(:,3)), max(nuIcebergEkf(:,3))]));
        ylim([ymin - 0.02, ymax + 0.02]);

        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'RelativeStateComparisonPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        s1 = subplot(2, 3, 1, 'FontSize', fontSize);
        title(s1, 'p_{rel,x}');
        hold on;
        grid on;
        plot(cTime, etaRelative(:,1), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaRelativeEkf(:,1), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative x-position [m]', 'FontSize', fontSize);
    %     xlim([0 conf.plot.Tend]);
    %     ylim(Xlim);
        s2 = subplot(2, 3, 2, 'FontSize', fontSize);
        title(s2, 'p_{rel,y}');
        hold on;
        grid on;
        plot(cTime, etaRelative(:,2), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, etaRelativeEkf(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative y-position [m]', 'FontSize', fontSize);
    %     xlim([0 conf.plot.Tend]);
    %     ylim(Ylim);
        s3 = subplot(2, 3, 3, 'FontSize', fontSize);
        title(s3, '\psi_{rel}');
        hold on;
        grid on;
        plot(cTime, rad2deg(etaRelative(:,3)), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, rad2deg(etaRelativeEkf(:,3)), 'b', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative heading [deg]', 'FontSize', fontSize);
        ymin = rad2deg(min(min(etaRelative(:,3)), min(etaRelativeEkf(:,3))));
        ymax = rad2deg(max(max(etaRelative(:,3)), max(etaRelativeEkf(:,3))));
        ylim([ymin - 0.2, ymax + 0.2]);
        s4 = subplot(2, 3, 4, 'FontSize', fontSize);
        title(s4, 'u_{rel}');
        hold on;
        grid on;
        plot(cTime, nuRelative(:,1), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeEkf(:,1), 'b', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeMeas(:,1), 'g--', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative u-velocity [m/s]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ymin = min([min(nuRelative(:,1)), min(nuRelativeEkf(:,1)), min(nuRelativeMeas(:,1))]);
        ymax = max([max(nuRelative(:,1)), max(nuRelativeEkf(:,1)), max(nuRelativeMeas(:,1))]);
        ylim([ymin - 0.2, ymax + 0.2]);
        s5 = subplot(2, 3, 5, 'FontSize', fontSize);
        title(s5, 'v_{rel}');
        hold on;
        grid on;
        plot(cTime, nuRelative(:,2), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeEkf(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeMeas(:,2), 'g--', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative v-velocity [m/s]', 'FontSize', fontSize);
        ymin = min([min(nuRelative(:,2)), min(nuRelativeEkf(:,2)), min(nuRelativeMeas(:,2))]);
        ymax = max([max(nuRelative(:,2)), max(nuRelativeEkf(:,2)), max(nuRelativeMeas(:,2))]);
        ylim([ymin - 0.2, ymax + 0.2]);
        s6 = subplot(2, 3, 6, 'FontSize', fontSize);
        title(s6, 'r_{rel}');
        hold on;
        grid on;
        plot(cTime, nuRelative(:,3), 'k', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeEkf(:,3), 'b', 'LineWidth', conf.plot.linewidth);
        plot(cTime, nuRelativeMeas(:,3), 'g--', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Relative rotation [rad/s]', 'FontSize', fontSize);
        ymin = min([min(nuRelative(:,3)), min(nuRelativeEkf(:,3)), min(nuRelativeMeas(:,3))]);
        ymax = max([max(nuRelative(:,3)), max(nuRelativeEkf(:,3)), max(nuRelativeMeas(:,3))]);
        ylim([ymin - 0.01, ymax + 0.01]);
    %     ylim([rad2deg(ymin) - 0.2, rad2deg(ymax) + 0.2]);
    end

    % Figure 1
    if(flags.exportFigs)
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'EtaXRelError');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        legFontSize = fontSize;
    else
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'RelativeErrorPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        legFontSize = 14;
        s1 = subplot(3, 1, 1, 'FontSize', fontSize);
        title(s1, '\eta_{rel,x} error');
    end
    hold on;
    grid on;
    h1 = plot(cTime, etaRelativeEkf(:,1) - etaRelative(:,1), 'b', 'LineWidth', conf.plot.linewidth);
    h2 = plot(cTime, 3.*sigma_Xrel, 'r', 'LineWidth', conf.plot.linewidth);
    plot(cTime, -3.*sigma_Xrel, 'r', 'LineWidth', conf.plot.linewidth);
    h3 = plot(cTime, 3.*sigma_XrelnU, 'k--', 'LineWidth', conf.plot.linewidth);
    plot(cTime, -3.*sigma_XrelnU, 'k--', 'LineWidth', conf.plot.linewidth);
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Error [m]', 'FontSize', fontSize);
    xlim([0 conf.plot.Tend]);
    ylim([-3*max(max(sigma_Xrel), max(sigma_XrelnU)), 3*max(max(sigma_Xrel), max(sigma_XrelnU))]);
    leg = legend([h1, h2, h3], 'EKF estimate error', '3\sigma bound with position update', '3\sigma bound without position update');
    leg.FontSize = legFontSize;
    % Figure 2
    if(flags.exportFigs)
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'EtaYRelError');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
    else
        s2 = subplot(3, 1, 2, 'FontSize', fontSize);
        title(s2, '\eta_{rel,y} error');
    end
    hold on;
    grid on;
    plot(cTime, etaRelativeEkf(:,2) - etaRelative(:,2), 'b', 'LineWidth', conf.plot.linewidth);
    plot(cTime, 3.*sigma_Yrel, 'r', 'LineWidth', conf.plot.linewidth);
    plot(cTime, -3.*sigma_Yrel, 'r', 'LineWidth', conf.plot.linewidth);
    plot(cTime, 3.*sigma_YrelnU, 'k--', 'LineWidth', conf.plot.linewidth);
    plot(cTime, -3.*sigma_YrelnU, 'k--', 'LineWidth', conf.plot.linewidth);
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Error [m]', 'FontSize', fontSize);
    xlim([0 conf.plot.Tend]);
    ylim([-3*max(max(sigma_Yrel), max(sigma_YrelnU)), 3*max(max(sigma_Yrel), max(sigma_YrelnU))]);
    if(flags.exportFigs)
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'EtaPsiRelError');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
    else
        s3 = subplot(3, 1, 3, 'FontSize', fontSize);
        title(s3, '\eta_{rel,\psi} error');
    end
    hold on;
    grid on;
    plot(cTime, rad2deg(etaRelativeEkf(:,3) - etaRelative(:,3)), 'b', 'LineWidth', conf.plot.linewidth);
    plot(cTime, 3.*rad2deg(sigma_psirel), 'r', 'LineWidth', conf.plot.linewidth);
    plot(cTime, -3.*rad2deg(sigma_psirel), 'r', 'LineWidth', conf.plot.linewidth);
%     plot(cTime, 3.*rad2deg(sigma_psirelnU), 'k--', 'LineWidth', conf.plot.linewidth);
%     plot(cTime, -3.*rad2deg(sigma_psirelnU), 'k--', 'LineWidth', conf.plot.linewidth);
    xlabel('Time [s]', 'FontSize', fontSize);
    ylabel('Error [deg]', 'FontSize', fontSize);
    xlim([0 conf.plot.Tend]);
    ylim([-3*rad2deg(max(max(sigma_psirel), max(sigma_psirelnU))), 3*rad2deg(max(max(sigma_psirel), max(sigma_psirelnU)))]);
end