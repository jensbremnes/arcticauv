% animationIcebegMappingSFunction is an s-function for animating the AUV and iceberg trajectory
%
% Input ports:
%   x_hat 			- AUV state vector
% 	eta_iceberg 	- Iceberg pose vector
%
% Output ports:
% 	-
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

function animationIcebergMappingSFunction(block)
    % Level-2 MATLAB file S-function for visualizing AUV and iceberg trajectory
    setup(block)
end
%
% Called when the block is added to a model.
function setup(block)
    %
    block.NumDialogPrms = 5;
    block.NumInputPorts  = 2;
    block.NumOutputPorts = 0;
    %
    % Setup functional port properties
    block.SetPreCompInpPortInfoToDynamic;
    %
    % Set input dimensions
    block.InputPort(1).Dimensions = 12;
    block.InputPort(2).Dimensions = 3;
    %
    % Register block methods
    block.RegBlockMethod('Start',   @Start);
    block.RegBlockMethod('Outputs', @Output);
    %
    % To work in external mode
    block.SetSimViewingDevice(true);
    %
    block.SampleTimes = [block.DialogPrm(5).Data 0];
end
%
% Called when the simulation starts.
function Start(block)
    %
    % Check to see if we already have an instance of trajectory animation
    if(block.DialogPrm(4).Data == 1)
        ud = get_param(block.BlockHandle, 'UserData');
        if isempty(ud)
            trj = [];
        else
            trj = ud.vis;
        end
        %
        % If not, create one
        if isempty(trj) || ~isa(trj,'animateAuvAndIcebergTrajectory') || ~trj.isAlive
            % Get input parameters
            auvSize = block.DialogPrm(1).Data;
            icebergFileName = block.DialogPrm(2).Data;
            WP = block.DialogPrm(3).Data;
            % Create figure handle
            fig = figure;
            set(fig, 'Position', get(0,'Screensize'));
            set(fig, 'Units', 'Normalized');
            trj = animateAuvAndIcebergTrajectory(fig, 121, auvSize, icebergFileName, WP);
            s2 = subplot(222);
            s3 = subplot(224);
        else
            trj.clearPoints();
        end
        %
        ud.vis = trj;
        %
        % Save it in UserData
        set_param(block.BlockHandle,'UserData',ud);
    end
end
%
% Called when the simulation time changes.
function Output(block)
    if block.IsMajorTimeStep && block.DialogPrm(4).Data == 1
    % Every time step, call set pose functions
    ud = get_param(block.BlockHandle,'UserData');
    trj = ud.vis;
    if isempty(trj) || ~isa(trj,'animateAuvAndIcebergTrajectory') || ~trj.isAlive
        return;
    end
        trj.setAuvPose(block.InputPort(1).Data(1), block.InputPort(1).Data(2), block.InputPort(1).Data(6));		  
        trj.setIcebergPose(block.InputPort(2).Data(1), block.InputPort(2).Data(2), block.InputPort(2).Data(3));
        trj.update();
    end
end