function h = plotGeneral(simReturn, plotSelect, showSelected)
% plotGeneral plots the generic states from a run of the AuvSim.
%
%  h = plotGeneral(simReturn, plotSelect, showSelected, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink 
%   plotSelect      - Vector defining plots to run [a, b, c, d]
%                       a = 1 -> Plots eta
%                       b = 1 -> Plots nu
%                       c = 1 -> Plots tau
%                       d = 1 -> Plots u
%   showSelected    - Vector defining plots to show(hide) [a]
%                       a = 1 -> Show estimated states
%
% Output parameters:
%   h               - Vector of plot handles
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

%% Run plotting routines
    t = simReturn.t;
    tg = simReturn.tg;
    to = simReturn.to;
    th = simReturn.th;
    tl = simReturn.tl;
    tm = simReturn.tMbe;
    
    eta = simReturn.eta;
    eta_hat = simReturn.x_hat(:,1:6);
    nu = simReturn.nu;
    nu_hat = simReturn.x_hat(:,7:12);
    tau = simReturn.tau;
    u = simReturn.u;

    z_d = simReturn.z_d;
    psi_d = simReturn.psi_d;
    U_d = simReturn.U_d;
    
    z_r = simReturn.z_r;
    alt = simReturn.altitude;
    theta_r = simReturn.theta_r;
    theta_d = simReturn.theta_d;
    psi_r = simReturn.psi_r;
    U_r = simReturn.U_r;
    
    rpm = simReturn.rpm_motor;
    rpm_ff = simReturn.rpm_feedforward;
    fin_angle = simReturn.fin_angle;
    rudder_angle = simReturn.rudder_angle;
    
    h = [];
    
    if(plotSelect(1))
        h = [h; figure];
        subplot(2,3,1);
        hold on;
        plot(t, eta(:,1), 'b');
        if(showSelected(1))
           plot(to, eta_hat(:,1), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('North position [m]');
        subplot(2,3,2);
        hold on;
        plot(t, eta(:,2), 'b');
        if(showSelected(1))
           plot(to, eta_hat(:,2), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('East position [m]');
        pzh = subplot(2,3,3);
        hold on;
        plot(t, eta(:,3), 'b');
        plot(th, z_d, 'r');
        plot(tg, z_r, 'g');
        plot(tm, alt, 'm');
        if(showSelected(1))
           plot(to, eta_hat(:,3), 'k'); 
           legend true desired reference altitude estimate
        else
            legend true desired reference altitude
        end
        xlabel('Time [s]');
        ylabel('Depth [m]');
        set(pzh, 'Ydir', 'reverse');
        subplot(2,3,4);
        hold on;
        plot(t, rad2deg(eta(:,4)), 'b');
        if(showSelected(1))
           plot(to, rad2deg(eta_hat(:,4)), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Roll [deg]');
        subplot(2,3,5);
        hold on;
        plot(t, rad2deg(eta(:,5)), 'b');
        plot(tl, rad2deg(theta_d), 'r');
        plot(th, rad2deg(theta_r), 'g');
        if(showSelected(1))
           plot(to, rad2deg(eta_hat(:,5)), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Pitch [deg]');
        subplot(2,3,6);
        hold on;
        plot(t, rad2deg(eta(:,6)), 'b');
        plot(tl, rad2deg(psi_d), 'r');
        plot(tg, rad2deg(psi_r), 'g');
        if(showSelected(1))
           plot(to, rad2deg(eta_hat(:,6)), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Yaw [deg]');
    end

    if(plotSelect(2))
        h = [h; figure];
        subplot(2,3,1);
        hold on;
        plot(t, nu(:,1), 'b');
        plot(th, U_d, 'r');
        plot(tg, U_r, 'g');
        if(showSelected(1))
           plot(to, nu_hat(:,1), 'm'); 
           legend true desired reference estimate
        elseif(showSelected(2))
            plot(tl, u(:,1), 'm');
            legend true desired reference command
        else
            legend true desired reference
        end
        xlabel('Time [s]');
        ylabel('Surge [m/s]');
        subplot(2,3,2);
        hold on;
        plot(t, nu(:,2), 'b');
        if(showSelected(1))
           plot(to, nu_hat(:,2), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Sway [m/s]');
        subplot(2,3,3);
        hold on;
        plot(t, nu(:,3), 'b');
        if(showSelected(1))
           plot(to, nu_hat(:,3), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Heave [m/s]');
        subplot(2,3,4);
        hold on;
        plot(t, nu(:,4), 'b');
        if(showSelected(1))
           plot(to, nu_hat(:,4), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Roll rate [rad/s]');
        subplot(2,3,5);
        hold on;
        plot(t, nu(:,5), 'b');
        if(showSelected(1))
           plot(to, nu_hat(:,5), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Pitch rate [rad/s]');
        subplot(2,3,6);
        hold on;
        plot(t, nu(:,6), 'b');
        if(showSelected(1))
           plot(to, nu_hat(:,6), 'm'); 
        end
        xlabel('Time [s]');
        ylabel('Yaw rate [rad/s]');
    end

    if(plotSelect(3))
        h = [h; figure];
        subplot(2,3,1);
        plot(t, tau(:,1), 'b');
        xlabel('Time [s]');
        ylabel('X [N]');
        subplot(2,3,2);
        plot(t, tau(:,2), 'b');
        xlabel('Time [s]');
        ylabel('Y [N]');
        subplot(2,3,3);
        plot(t, tau(:,3), 'b');
        xlabel('Time [s]');
        ylabel('Z [N]');
        subplot(2,3,4);
        plot(t, tau(:,4), 'b');
        xlabel('Time [s]');
        ylabel('K [N/s]');
        subplot(2,3,5);
        plot(t, tau(:,5), 'b');
        xlabel('Time [s]');
        ylabel('M [N/s]');
        subplot(2,3,6);
        plot(t, tau(:,6), 'b');
        xlabel('Time [s]');
        ylabel('N [N/s]');
    end

    if(plotSelect(4))
        h = [h; figure];
        subplot(1,3,1);
        hold on;
        plot(t, rpm, 'b');
        plot(th, rpm_ff, 'g');
        plot(tl, u(:,1), 'r')
        xlabel('Time [s]');
        ylabel('Thrust cmd [rpm]');
        legend actual feedforward command
        subplot(1,3,2);
        hold on;
        plot(tl, rad2deg(u(:,2)), 'r');
        plot(t, rad2deg(fin_angle), 'b');
        xlabel('Time [s]');
        ylabel('Pitch fin [deg]');
        subplot(1,3,3);
        hold on;
        plot(tl, rad2deg(u(:,3)), 'r');
        plot(t, rad2deg(rudder_angle), 'b');
        xlabel('Time [s]');
        ylabel('Rudder fin [deg]');
    end
    
    if(plotSelect(5))
        WP = simReturn.auvsim_workspace.conf.wplist;
        h = [h; figure];
        hold on;
        plot3(eta(:,1), eta(:,2), eta(:,3), 'b');
        plot3(WP(:,1), WP(:,2), WP(:,3), 'ro');
        xlabel('North [m]');
        ylabel('East [m]');
        zlabel('Down [m]');
    end
    
end