function h = plotObservationMap(simReturn, conf, parent)
% plotObservationMap displays the observation map.
%
%  h = plotObservationMap(simReturn, conf, parent)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%   parent          - Parent handle.
%
% Output parameters:
%   h               - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    h = [];
    pointCloud = [];
    
    %% Extract data from file
    if(exist([conf.saveDir, 'ObservationMap.txt'], 'file'))
        try
            map = dlmread([conf.saveDir, 'ObservationMap.txt']);
        catch ex
            for i=1:length(ex.stack)
                fprintf('ERROR: Reading ObservationMap.txt failed. Exception: "%s" on line %i in file %s.\n'...
                    , ex.message, ex.stack(i).line, ex.stack(i).file);
            end
            return;
        end

        %res = min(abs(diff(map(:,1))));
        res = conf.iceslam.mapResolution;
        bounds = [min(map(:,1)) - res/2, max(map(:,1)) - res/2, min(map(:,2)) - res/2, max(map(:,2)) - res/2];

        xx = bounds(1):res:bounds(2);
        yy = bounds(3):res:bounds(4);
        zz = zeros(length(xx), length(yy));

        for i = 1:length(map(:,1))
            zz(map(i, 6) + 1, map(i, 5) + 1) =  map(i, 3)/map(i, 4);
        end
    else
        xx = [];
        yy = [];
        zz = [];
        return;
    end
    
    %% Check
    for j = 1:length(yy)
        for i = 1:length(xx)
            idx = length(xx)*(j - 1) + i;
            if((i - 1) ~= map(idx, 5) || (j - 1) ~= map(idx, 6))
                fprintf('Error: Index is wrong (i,j) = (%i,%i) while (map.i,map.j)= (%i,%i).\n', i, j, map(idx, 5), map(idx, 6)); 
                return;
            end
            if((yy(j) + res/2) ~= map(idx, 2) || (xx(i) + res/2) ~= map(idx, 1))
                fprintf('Error: Point (%f, %f) ~= (%f,%f) is wrong at (i,j) = (%i,%i).\n', xx(i) + res/2, yy(j) + res/2, map(idx, 1), map(idx, 2), i, j); 
                return;
            end
            if(~isnan(zz(j,i)) || ~isnan(map(idx, 3)/map(idx, 4)))
                if(map(idx, 3)/map(idx, 4) ~= zz(j,i))
                    fprintf('Error: Value = %f ~= %f at (i,j) = (%i,%i).\n', map(idx, 3)/map(idx, 4), zz(j,i), i, j); 
                    return;
                end
            end
        end
    end
    
    %% Extract point cloud from MBE ranges
    kvec = [0; 0; 1];
    if(conf.mbe.isUpLooking)
       kvec = -1*kvec;
    end
    
    if(conf.anim.plotMbePointsInMap)
        if(~conf.mbe.realData)
            etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, simReturn.tMbe);
            etaRelative = interp1(simReturn.tIce, simReturn.etaRelative, simReturn.tMbe);
            pointCloud = zeros(length(simReturn.mbeRanges(:,1))*length(simReturn.mbeRanges(1,:)),3);
            idx = 1;
            for k=1:length(simReturn.mbeRanges(:,1)) 
                psi_ice = etaIceberg(k,3);
                p_rel = etaRelative(k,1:3)';
                phi_auv = simReturn.eta(k,4);
                theta_auv = simReturn.eta(k,5);
                psi_rel = etaRelative(k,6);

                for i=1:length(simReturn.mbeRanges(k,:))
                    beam_angle = simReturn.mbeAngles(k,i);
                    range = simReturn.mbeRanges(k,i);
                    if(isnan(beam_angle) || isnan(range))
                        continue;
                    end
                    E = Rzyx(0,0,psi_ice)'*p_rel + Rzyx(phi_auv, theta_auv, psi_rel)*Rzyx(beam_angle, 0, 0)*kvec*range;
                    if(sum(isnan(E)) == 0)
                        pointCloud(idx,:) = E';
                        idx = idx + 1;
                    end
                end
            end
        else
            etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, simReturn.tMbe);
            etaRelative = interp1(simReturn.tIce, simReturn.etaRelative, simReturn.tMbe);
            etaAuv = interp1(simReturn.tAuv, simReturn.eta, simReturn.tMbe);
            pointCloud = zeros(length(simReturn.mbeDx(:,1))*length(simReturn.mbeDx(1,:)),3);
            idx = 1;
            for k=1:length(simReturn.mbeDx(:,1)) 
                psi_ice = etaIceberg(k,3);
                psi_auv = etaAuv(k,6);
                p_rel = etaRelative(k,1:3)';

                for i=1:length(simReturn.mbeDx(1,:))
                    if(isnan(simReturn.mbeDx(k,i)) || isnan(simReturn.mbeDy(k,i)) || isnan(simReturn.mbeDz(k,i)))
                        continue;
                    end
                    p_mbe = Rzyx(0,0,psi_auv)*[simReturn.mbeDx(k,i); simReturn.mbeDy(k,i); simReturn.mbeDz(k,i)];
                    
                    E = Rzyx(0,0,psi_ice)'*(p_rel + p_mbe);
                    if(sum(isnan(E)) == 0)
                        pointCloud(idx,:) = E';
                        idx = idx + 1;
                    end
                end
            end
        end
    end
    
    %% Plotting
    if(~isempty(xx) && ~isempty(yy) && ~isempty(zz))
        if(nargin < 3)
           h = [h; figure];
        else
           h = [h; parent];
        end
        
        if(~isempty(conf.anim.icebergFile))
            iceberg = load(conf.anim.icebergFile);
        end
        titlestr = sprintf("Observation map.");

        % Configure figure
        set(h, 'Position', get(0,'Screensize'));
        set(h, 'Units', 'Normalized');
        set(h, 'outerposition', [0 0 1 1]);
        set(h, 'Name', 'ObservationMap');
        set(gcf,'Color', [1 1 1]);
        ax = axes(h);
        hold on;
        set(get(ax, 'Title'), 'String', titlestr);
        if(exist('iceberg', 'var'))
            surf(iceberg.grid.Nq, iceberg.grid.Eq, iceberg.grid.Zq, 'EdgeColor','none', 'FaceColor', 'r'); %
        end
%         if(~isempty(pointCloud))
%             scatter3(pointCloud(:,1), pointCloud(:,2), pointCloud(:,3), 1, pointCloud(:,3));
%             colormap(jet);
%             colorbar;
%         end
%         plotDEM(xx, yy, zz, res, 'b', ax); %[209, 250, 255]./255
        surf(xx, yy, zz, 'EdgeColor','none', 'FaceColor', 'interp');
        xlabel(ax, 'North [m]');
        ylabel(ax, 'East [m]');
        zlabel(ax, 'Depth [m]');
        set(ax,'zdir','reverse');
        view(20, -5);
        
        if(exist('iceberg', 'var') && ~isempty(pointCloud))
            zerr = griddata(pointCloud(:,1), pointCloud(:,2), pointCloud(:,3), iceberg.grid.Nq, iceberg.grid.Eq, 'linear') - iceberg.grid.Zq;
            figure;
            surf(iceberg.grid.Nq, iceberg.grid.Eq, abs(zerr), 'EdgeColor','none', 'FaceColor', 'interp');
        end
    end
end