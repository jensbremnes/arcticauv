function fig = plotIcebergPlots(simReturn, conf, flags)
% plotIcebergPlots shows different iceberg SLAM related plots.
%
%  fig = plotIcebergPlots(simReturn, conf, flags)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%   flags           - Plot select
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    
    % Interpolate data to fit times
    idxEnd = min(ceil(conf.plot.Tend/diff(simReturn.tSlam(1:2)))+1, length(simReturn.tSlam));
    cTime = simReturn.tSlam(1:idxEnd);
    etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, cTime);
    slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, cTime);
    etaIcebergEkf = interp1(simReturn.tEkf, simReturn.etaIcebergEkf, cTime);
    etaIcebergSlam = interp1(simReturn.tSlam, simReturn.etaIcebergSlam, cTime);
  
    xLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
    yLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
    psiLoc = zeros(length(slamParticleStates(:,1)), conf.iceslam.nParticles);
    for i=1:length(slamParticleStates(:,1))
        for j=1:conf.iceslam.nParticles
            xLoc(i,j) = slamParticleStates(i,(j-1)*3+1);
            yLoc(i,j) = slamParticleStates(i,(j-1)*3+2);
            psiLoc(i,j) = slamParticleStates(i,(j-1)*3+3);
        end
    end
    
    sigmaX = std(xLoc, 0, 2);
    sigmaY = std(yLoc, 0, 2);
    sigmaPsi = std(psiLoc, 0, 2);
    
    % Baseline cloud
    baseCloud = load(conf.iceslam.baselineCloud);  
    sigmaXBase =  interp1(baseCloud.time, baseCloud.sigmaXBase, cTime);
    sigmaYBase = interp1(baseCloud.time, baseCloud.sigmaYBase, cTime);
    sigmaPsiBase = interp1(baseCloud.time, baseCloud.sigmaPsiBase, cTime);
    
    Xlim = [-3*max([max(sigmaXBase), max(sigmaX)]), 3*max([max(sigmaXBase), max(sigmaX)])];
    Ylim = [-3*max([max(sigmaYBase), max(sigmaY)]), 3*max([max(sigmaYBase), max(sigmaY)])];
    Psilim = [-3*max([max(sigmaPsiBase), max(sigmaPsi)]), 3*max([max(sigmaPsiBase), max(sigmaPsi)])];
    
    fontSize = conf.plot.fontsize + 8;
    
    fig = [];
    if(flags.icebergPlots(1))
        if(flags.icebergPlots(2))
            sub = 3;
        else
            sub = 2;
        end
        if(flags.exportFigs)
            fig =[fig; figure];
            set(fig(end), 'Name', 'SlamXErrorPlot');
            set(fig(end), 'Position', get(0,'Screensize'));
            set(fig(end), 'Units', 'Normalized');
            set(fig(end), 'outerposition', [0 0 1 1]);
            set(fig(end), 'Units', 'Pixels');
            ax = axes;
            set(ax,'FontSize',fontSize);
            set(gcf,'Color', [1 1 1]);
            legFontSize = fontSize;
        else
            fig =[fig; figure];
            set(fig(end), 'Name', 'SlamErrorPositionPlot');
            set(fig(end), 'Position', get(0,'Screensize'));
            set(fig(end), 'Units', 'Normalized');
            set(fig(end), 'outerposition', [0 0 1 1]);
            set(fig(end), 'Units', 'Pixels');
            ax = axes;
            set(ax,'FontSize',fontSize);
            set(gcf,'Color', [1 1 1]);
            legFontSize = 14;
            s1 = subplot(sub, 1, 1, 'FontSize', fontSize);
            title(s1, 'p_{i,x} error');
        end
        hold on;
        grid on;
        h1 = plot(cTime, etaIcebergSlam(:,1) - etaIceberg(:,1), '--', 'LineWidth', conf.plot.linewidth, 'Color', [102 0 128]./255);
        h2 = plot(cTime, etaIcebergEkf(:,1) - etaIceberg(:,1), 'b', 'LineWidth', conf.plot.linewidth);
        h3 = plot(cTime, 3.*sigmaX, 'r', 'LineWidth', conf.plot.linewidth);
        plot(cTime, -3.*sigmaX, 'r', 'LineWidth', conf.plot.linewidth);
        h4 = plot(cTime, 3.*sigmaXBase(), 'k--', 'LineWidth', conf.plot.linewidth);
        plot(cTime, -3.*sigmaXBase, 'k--', 'LineWidth', conf.plot.linewidth);
        leg = legend([h1, h2, h3, h4], 'SLAM estimate error', 'EKF estimate error', '3\sigma bound with resampling', '3\sigma bound without resampling');
        leg.FontSize = legFontSize;
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Error [m]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ylim(Xlim);
        if(flags.exportFigs)
            fig =[fig; figure];
            set(fig(end), 'Name', 'SlamYErrorPlot');
            set(fig(end), 'Position', get(0,'Screensize'));
            set(fig(end), 'Units', 'Normalized');
            set(fig(end), 'outerposition', [0 0 1 1]);
            set(fig(end), 'Units', 'Pixels');
            ax = axes;
            set(ax,'FontSize',fontSize);
            set(gcf,'Color', [1 1 1]);
        else
            s2 = subplot(sub, 1, 2, 'FontSize', fontSize);
            title(s2, 'p_{i,y} error');
        end
        hold on;
        grid on;
        plot(cTime, etaIcebergSlam(:,2) - etaIceberg(:,2), '--', 'LineWidth', conf.plot.linewidth, 'Color', [102 0 128]./255);
        plot(cTime, etaIcebergEkf(:,2) - etaIceberg(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        plot(cTime, 3.*sigmaY, 'r', 'LineWidth', conf.plot.linewidth);
        plot(cTime, -3.*sigmaY, 'r', 'LineWidth', conf.plot.linewidth);
        plot(cTime, 3.*sigmaYBase(), 'k--', 'LineWidth', conf.plot.linewidth);
        plot(cTime, -3.*sigmaYBase, 'k--', 'LineWidth', conf.plot.linewidth);
        xlabel('Time [s]', 'FontSize', fontSize);
        ylabel('Error [m]', 'FontSize', fontSize);
        xlim([0 conf.plot.Tend]);
        ylim(Ylim);
        
        if(sub == 3)
            if(flags.exportFigs)
                fig =[fig; figure];
                set(fig(end), 'Name', 'SlamPsiErrorPlot');
                set(fig(end), 'Position', get(0,'Screensize'));
                set(fig(end), 'Units', 'Normalized');
                set(fig(end), 'outerposition', [0 0 1 1]);
                set(fig(end), 'Units', 'Pixels');
                ax = axes;
                set(ax,'FontSize',fontSize);
                set(gcf,'Color', [1 1 1]);
            else
                s3 = subplot(sub, 1, 3, 'FontSize', fontSize);
                title(s3, 'p_{i,\psi} error');
            end
            hold on;
            grid on;
            plot(cTime, rad2deg(etaIcebergSlam(:,3) - etaIceberg(:,3)), '--', 'LineWidth', conf.plot.linewidth, 'Color', [102 0 128]./255);
            plot(cTime, rad2deg(etaIcebergEkf(:,3) - etaIceberg(:,3)), 'b', 'LineWidth', conf.plot.linewidth);
            plot(cTime, 3.*rad2deg(sigmaPsi), 'r', 'LineWidth', conf.plot.linewidth);
            plot(cTime, -3.*rad2deg(sigmaPsi), 'r', 'LineWidth', conf.plot.linewidth);
            plot(cTime, 3.*rad2deg(sigmaPsiBase), 'k--', 'LineWidth', conf.plot.linewidth);
            plot(cTime, -3.*rad2deg(sigmaPsiBase), 'k--', 'LineWidth', conf.plot.linewidth);
            xlabel('Time [s]', 'FontSize', fontSize);
            ylabel('Error [deg]', 'FontSize', fontSize);
            xlim([0 conf.plot.Tend]);
%             ylim(Psilim);
        end 
    end  
end