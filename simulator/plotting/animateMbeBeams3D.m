function simReturn = animateMbeBeams3D(simReturn, conf)
% animateMbeBeams3D generate a 3D video animatimation of the MBE ranges.
%
% beamFig = animateMbeBeams3D(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink.
%   conf            - Configuration structure.
%
% Output parameters:
%   simReturn       - Return updated structure.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2016.02.08  PN Created function
%

    tvCommon = simReturn.mbe.time;
    tvDelta = min(diff(tvCommon));
    etaMbe = interp1(simReturn.t, simReturn.eta, tvCommon);
    etaIceberg = interp1(simReturn.iceberg.time, [simReturn.iceberg.north, simReturn.iceberg.east, simReturn.iceberg.psi], tvCommon);
    rangesMbe = simReturn.mbe.ranges;

    % Check that start and end time is not out of bounds and find indices
    conf.animoptions.endTime = min(max(simReturn.t), conf.animoptions.endTime);
    conf.animoptions.startTime = min(conf.animoptions.endTime, conf.animoptions.startTime);
    [~, startTimeIdx] = min(abs(tvCommon - conf.animoptions.startTime));
    [~, endTimeIdx] = min(abs(tvCommon - conf.animoptions.endTime));
    
    try
        if(~exist(conf.videoDir, 'dir'))
            mkdir(conf.videoDir);
        end
        cd(conf.videoDir);

        % Set up video object
        if(~conf.animoptions.showFirstFrameOnly)
            fileName = [datestr(datetime, 'yyyymmddHHMMSS'), '_IcebergMapping3DAnimation.avi'];
            vidObj = VideoWriter(fileName);
            vidObj.Quality = 100;
            vidObj.FrameRate = 1/tvDelta;
            open(vidObj);
        end
        beamFig = figure;
        hold on;
        title('Iceberg mapping 3D', 'FontSize', conf.animoptions.fontSize);
        set(beamFig, 'Position', conf.animoptions.figPosition);
        set(gcf, 'Renderer', 'painters');

       %  Set up world (first frame)
        axis equal;
        set(get(beamFig, 'CurrentAxes'), 'XLim', conf.animoptions.xLim3d, 'YLim', conf.animoptions.yLim3d, 'ZLim', conf.animoptions.zLim3d, 'FontSize', conf.animoptions.numberSize);
        set(get(beamFig, 'CurrentAxes'),'Color', conf.animoptions.backgroundColor);
        set(get(beamFig, 'CurrentAxes'), 'XColor', conf.animoptions.backgroundColor, 'YColor', conf.animoptions.backgroundColor, 'ZColor', conf.animoptions.backgroundColor);
        set(get(beamFig, 'CurrentAxes'), 'Zdir', 'reverse');
        view(-90, 90); % Swap x and y axis
        xlabel('North [m]');
        ylabel('East [m]');
        zlabel('Depth [m]');
        grid;
        camlight left; lighting gouraud;
        
        % Create AUV object
        rotationOffset = deg2rad(-3);
        auvDrawer = AUVDraw3D;
        auvDrawer.createAuv(1, etaMbe(startTimeIdx,:)', 0.20, rotationOffset);    
        
        % Create AUV object
        beamDrawer3D = BeamDraw3D(conf.mbeConfig, 1);
        beamDrawer3D.createBeams(etaMbe(startTimeIdx,:)', rangesMbe(startTimeIdx,:));
        
        % Create iceberg object
        icebergDrawer = IcebergDraw3D(conf);
        icebergDrawer.createIceberg(etaIceberg(startTimeIdx,:));
        
        % Write first frame
        set(beamFig, 'Position', conf.animoptions.figPosition);
        if(~conf.animoptions.showFirstFrameOnly)
            writeVideo(vidObj, getframe(beamFig));
        end
        timeIdx = startTimeIdx + 1;

        % Write animation (adjust framerate to sample time)
        if(~conf.animoptions.showFirstFrameOnly)
            for i=timeIdx:endTimeIdx
                beamDrawer3D.update(etaMbe(i,:)', rangesMbe(i,:));
                auvDrawer.update(etaMbe(i,:)', rotationOffset);
                icebergDrawer.update(etaIceberg(i,:));
                drawnow;
                set(beamFig, 'Position', conf.animoptions.figPosition);
                writeVideo(vidObj, getframe(beamFig));
            end
            close(vidObj);
        end
        
    catch ME
        if(~conf.animoptions.showFirstFrameOnly)
            close(vidObj);  
        end
        cd(conf.rootDir);
        for j = 1:length(ME.stack)
            msg = ['Error: ', ME.message, ' In function: ', ME.stack(j).name, ' at line: ', num2str(ME.stack(j).line), '.'];
            disp(msg);
        end
        return;
    end
    cd(conf.rootDir);
    
    % Update data structure
    simReturn.anim3d.figure = beamFig;
    simReturn.anim3d.tvCommon = tvCommon;
    simReturn.anim3d.etaIceberg = etaIceberg;
    simReturn.anim3d.etaMbe = etaMbe;
    simReturn.anim3d.rangesMbe = rangesMbe;
end
