function h = plotPointCloud(conf)
% plotPointCloud plots the point cloud from the multibeam sensor.
%
%  h = plotPointCloud(simReturn)
%
% Input parameters:
%   conf        - Config structure.
%
% Output parameters:
%   h               - Vector of plot handles
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2016.04.22
%

    xyz = loadMbePointCloudFromFile(conf);
    
    h = figure;
    scatter3(xyz.points(:,1), xyz.points(:,2), xyz.points(:,3), 1, xyz.points(:,3));
    xlabel('North position [m]');
    ylabel('East position [m]');
    zlabel('Depth [m]');
    set(gca, 'Zdir', 'reverse');
    view(-30,-10)
    axis equal;
end