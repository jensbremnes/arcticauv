function fig = plotIcebergRelativeTrajectory(simReturn, conf)
% plotIcebergRelativeTrajectory plots the relative iceberg trajectory.
%
%  fig = plotIcebergRelativeTrajectory(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    % Video object
    if(conf.anim.saveAnimation)
        video = VideoWriter([conf.saveDir, 'TrajectoryAnimation.avi']);
        video.FrameRate = conf.anim.animationFps;
        open(video);
    end

    % Get user data
    auvSize = conf.anim.auvSize;
    mbeStruct = conf.mbe;
    mapSize = conf.iceslam.mapSize;
    nParticles = conf.iceslam.nParticles;
    particles = [];
    
    % Interpolate data to fit times
    animStop = floor(min(simReturn.tSlam(end), conf.anim.animationStop));
    animTime = 0:1:animStop;
    etaAuv = interp1(simReturn.tAuv, simReturn.eta, animTime);
    etaIceberg = interp1(simReturn.tIce, simReturn.etaIceberg, animTime);
    etaRelReal = interp1(simReturn.tIce, simReturn.etaRelative, animTime);
    poseRelReal = [etaRelReal(:, 1), etaRelReal(:, 2), etaRelReal(:, 6)];
    ranges = interp1(simReturn.tMbe, simReturn.mbeRanges, animTime);
    angles = interp1(simReturn.tMbe, simReturn.mbeAngles, animTime);
    slamParticleStates = interp1(simReturn.tPart, simReturn.slamParticleStates, animTime);
    if(~conf.mbe.realData)
        etaRelEst = interp1(simReturn.tEkf, simReturn.etaRelativeEkf, animTime);
    else
        etaRelEst = interp1(simReturn.tEkf, simReturn.etaRelativeEkf, animTime);
    end
    etaIcebergEst = interp1(simReturn.tEkf, simReturn.etaIcebergEkf, animTime);
    
    if(~conf.mbe.realData)
        WP = conf.wplist;
        icebergFileName = conf.anim.icebergFile;
        rangesNoise = interp1(simReturn.tMbe, simReturn.mbeBeamsWNoise, animTime);
        etaNavlab = [];
    else
       WP = [];
       icebergFileName = '';
       rangesNoise = zeros(size(ranges));
       etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, animTime);
    end
  
    fig = figure;
    set(fig, 'Name', 'IcebergRelativeTrajectory');
    set(fig, 'Position', get(0,'Screensize'));
    set(fig, 'Units', 'Normalized');
    set(fig, 'outerposition', [0 0 1 1]);
    set(fig, 'Units', 'Pixels');
    set(gcf,'Color', [1 1 1]);

    % Trajectory and particles in ICE frame
    ice = animateIceGrid(fig, 121, auvSize, mapSize, conf.iceslam.mapNegativePart, ...
                        icebergFileName, etaRelEst(1,:)', poseRelReal(1,:)', etaIcebergEst(1,:)', ...
                        etaIceberg(1,:)', [etaAuv(1,1); etaAuv(1,2); etaAuv(1,6)], WP, ...
                        etaNavlab, nParticles, conf.plot.fontsize, conf.plot.linewidth, [87 176 255]./255, 0, 0);

    % Multibeam in BEAM frame
    mbe = animateMbe2dBeams(fig, 222, mbeStruct, etaAuv(1,:)', etaIceberg(1,:)', etaRelReal(1,:)', ...
                        ranges(1,:), rangesNoise(1,:), angles(1,:), 1.0, icebergFileName, ...
                        conf.plot.fontsize, conf.plot.linewidth, 0);

    % Trajectory in NED frame
    trj = animateAuvAndIcebergTrajectory(fig, 224, auvSize, icebergFileName, ...
                   WP, etaAuv(1,1:6)', etaIceberg(1,:)', etaIcebergEst(1,:)', ...
                   conf.plot.fontsize, conf.plot.linewidth, 0);
    if(conf.anim.enableParticlePlots)           
        particles = repmat(struct('etaIceberg',[0; 0; 0], 'nuIceberg', [0; 0; 0]), 0, 1);
    end
           
    if(conf.enable2dAnimation)
        animTime(1) = [];
    else
        animTime = animStop;
    end
        
    for i=animTime
        
        if(conf.anim.enableParticlePlots)
            for j=1:conf.iceslam.nParticles
                idx = (j-1)*3 + 1;
                particles(j).etaIceberg = slamParticleStates(i, idx:idx+2)';
            end
        end
        
        % Update trajectory
        if~isempty(trj) && isa(trj,'animateAuvAndIcebergTrajectory') && trj.isAlive
            trj.setAuvPose(etaAuv(i,1), etaAuv(i,2), etaAuv(i,6));		  
            trj.setIcebergPose(etaIceberg(i,1), etaIceberg(i,2), etaIceberg(i,3));
            trj.setEstimatorPose(etaIcebergEst(i,1), etaIcebergEst(i,2), etaIcebergEst(i,3));
            trj.update();
        end
        
        % Update mbe
        if(~isempty(mbe) && isa(mbe,'animateMbe2dBeams') && mbe.isAlive)
            mbe.update(etaAuv(i,:)', etaIceberg(i,:)', etaRelReal(i,:)', ranges(i,:), rangesNoise(i,:), angles(i,:));
        end
        
        % Update ice grid
        if(~isempty(ice) && isa(ice,'animateIceGrid') && ice.isAlive)
            if(~conf.mbe.realData)
                ice.update(etaRelEst(i,:)', poseRelReal(i,:)', etaIceberg(i,:)', ...
                    etaIcebergEst(i,:)', [etaAuv(i,1); etaAuv(i,2); etaAuv(i,6)], []);
            else
                ice.update(etaRelEst(i,:)', poseRelReal(i,:)', etaIceberg(i,:)', ...
                etaIcebergEst(i,:)', [etaAuv(i,1); etaAuv(i,2); etaAuv(i,6)], etaNavlab(i, :));
            end
            
            if(~isempty(particles))
                ice.updateParticles(particles, [102 0 128]./255, 0, 0);
            end
            
            if(length(animTime) == 1)
                ice.addFullRealTrajectory(etaRelReal(1:i,1:2));
                ice.addFullEstTrajectory(etaRelEst(1:i,1:2));
                if(conf.mbe.realData)
                	ice.addFullNavlabTrajectory(etaNavlab(1:i,1:2));
                end
                trj.addFullAuvTrajectory(etaAuv(1:i,1:2));
                trj.addFullIcebergTrajectory(etaIceberg(1:i,1:2));
                trj.addFullIcebergEstTrajectory(etaIcebergEst(1:i,1:2));
            end
        end
        
        drawnow update;
        if(conf.anim.saveAnimation)
            frame = getframe(fig);
            writeVideo(video, frame);
        end
    end
    
    if(conf.anim.saveAnimation)
        close(video);
    end
end