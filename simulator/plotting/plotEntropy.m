function fig = plotEntropy(simReturn, conf)
% plotEntropy shows the position entropy for a SLAM run.
%
%  fig = plotEntropy(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    fntSize = 16;
    lnWidth = 2;

    fig = figure;
    set(fig, 'Name', 'SlamEnmtropyPlot');
    set(fig, 'Position', get(0,'Screensize'));
    set(fig, 'Units', 'Normalized');
    set(fig, 'outerposition', [0 0 1 1]);
    set(fig, 'Units', 'Pixels');
    hold on;
    plot(simReturn.tSlam, simReturn.entropy, 'LineWidth', lnWidth);
    xlabel('Simulation time [s]', 'FontSize', fntSize);
    ylabel('Entropy [nats]', 'FontSize', fntSize);
end