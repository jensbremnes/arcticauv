function fig = plotExecutionTime(simReturn, conf)
% plotExecutionTime shows the execution time for a run.
%
%  fig = plotExecutionTime(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    fntSize = 16;
    lnWidth = 2;

    fig = figure;
    set(fig, 'Name', 'SlamExecutionTimePlot');
    set(fig, 'Position', get(0,'Screensize'));
    set(fig, 'Units', 'Normalized');
    set(fig, 'outerposition', [0 0 1 1]);
    set(fig, 'Units', 'Pixels');
    hold on;
    plot(simReturn.tSlam, simReturn.executionTime, 'LineWidth', lnWidth);
    xlabel('Simulation time [s]', 'FontSize', fntSize);
    ylabel('Execution time for SLAM [ms]', 'FontSize', fntSize);
end