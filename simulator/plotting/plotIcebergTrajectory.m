function h = plotIcebergTrajectory(simReturn)
% plotIcebergTrajectory plots the iceberg poses from a run of the AuvSim.
%
%  h = plotIcebergTrajectory(simReturn)
%
% Input parameters:
%   simReturn       - Return structure from simulink 
%
% Output parameters:
%   h               - Vector of plot handles
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2016.02.03  PN Create function
%

%% Run plotting routines
    t = simReturn.t;
    th = simReturn.th;
    tl = simReturn.tl;
    td = simReturn.td;
    wp = simReturn.auvsim_workspace.conf.wplist;
    
    eta = simReturn.eta;
    eta_hat = simReturn.x_hat(:,1:6);
    nu = simReturn.nu;
    nu_hat = simReturn.x_hat(:,7:12);
    tau = simReturn.tau;
    u = simReturn.u;

    z_d = simReturn.z_d;
    psi_d = simReturn.psi_d;
    U_d = simReturn.U_d;
    
    z_r = simReturn.z_r;
    theta_r = simReturn.theta_r;
    theta_d = simReturn.theta_d;
    psi_r = simReturn.psi_r;
    U_r = simReturn.U_r;
    
    rpm = simReturn.rpm_motor;
    rpm_ff = simReturn.rpm_feedforward;
    fin_angle = simReturn.fin_angle;
    rudder_angle = simReturn.rudder_angle;
    
    iceberg = simReturn.iceberg;
    mbe = simReturn.mbe;
    
    h(1) = figure;
    hold on;
    plot(eta(:,1), eta(:,2), 'm');
    plot(iceberg.north, iceberg.east, 'b+');
    for i=1:length(wp)-1
       plot(wp(i, 1), wp(i, 2), 'r+'); 
    end
    rectangle('Position', [iceberg.north(end) + iceberg.bounds(1) iceberg.east(end) + iceberg.bounds(3) (iceberg.bounds(2) - iceberg.bounds(1)) (iceberg.bounds(4) - iceberg.bounds(3))]);    
    xlabel('North position [m]');
    ylabel('East position [m]');
    legend('auv trajectory', 'iceberg trajectory');  
    view(90, -90);
    axis equal;
    
end