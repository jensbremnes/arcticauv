function fig = plotTiming(simReturn, conf)
% plotTiming shows the execution time for different processes.
%
%  fig = plotTiming(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink
%   conf            - Config structure
%
% Output parameters:
%   fig             - Figure handle
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    %%
    NRUNTIMESMAX = 20;
    START_TIME = 0;
    PLOT_SELECT = [0 0 0 1 1 1 1 0];
    fig = [];
    
    % Extract data from file
    times = cell(NRUNTIMESMAX,1);
    if(exist([conf.saveDir, 'timing.txt'], 'file'))
        try
            timing_array = importdata([conf.saveDir, 'timing.txt'], ' ', 1);
            
            if(~isfield(timing_array, 'data'))
                return;
            end
            
        catch ex
            for i=1:length(ex.stack)
                fprintf('ERROR: Reading timing.txt failed. Exception: "%s" on line %i in file %s.\n'...
                    , ex.message, ex.stack(i).line, ex.stack(i).file);
            end
            return;
        end
        
        for i=1:length(timing_array.data(1,:))
            times{i} = timing_array.data(:,i);
        end
    else
        return;
    end
    times(length(timing_array.data(1,:))+1:NRUNTIMESMAX) = [];

    fntSize = 16;
    lnWidth = 2;
    
    plotRange = times{1} >= START_TIME;

    fig = figure;
    set(fig, 'Name', 'SlamExecutionTimePlot');
    set(fig, 'Position', get(0,'Screensize'));
    set(fig, 'Units', 'Normalized');
    set(fig, 'outerposition', [0 0 1 1]);
    set(fig, 'Units', 'Pixels');
    hold on;
    ph = zeros(length(times),1);
    names = cell(length(times),1);
    simTime = times{1};
    
    for i=2:length(times)
        if(PLOT_SELECT(i-1))
            ph(i) = plot(simTime(plotRange), times{i}(plotRange)./1000, 'LineWidth', lnWidth);
            names{i} = timing_array.colheaders{i};
        end
    end
    names_empty = cellfun('isempty', names);
    names_legend = cell(sum(~names_empty,1),1);
    count = 1;
    for i=1:length(names_empty)
        if(~names_empty(i))
            names_legend{count} = names{i};
            count = count + 1;
        end
    end
    for i=length(names_empty):-1:1
        if(names_empty(i))
            ph(i) = [];
        end
    end
    ylabel('Execution time for SLAM steps [ms]', 'FontSize', fntSize);
    legend(ph,names_legend);
    
    fprintf('INFO: Execution time analysis:\n');
    fprintf('      Average runtimes:\n');
    for i=2:length(times)-1
        fprintf('           %s: \t\t%i [ms].\n', timing_array.colheaders{i}, round(mean(times{i}(plotRange)./1000)));
    end
    fprintf('           %s: \t\t%i [ms].\n', 'Total PF step', round(mean(times{length(times)}(plotRange))./1000));
    fprintf('      Maximum runtimes:\n');
    for i=2:length(times)-1
        fprintf('           %s: \t\t%i [ms].\n', timing_array.colheaders{i}, round(max(times{i})./1000));
    end
    fprintf('           %s: \t\t%i [ms].\n', 'Total PF step', round(max(times{length(times)})./1000));
end