function fig = plotHuginData(simReturn, conf, flags)
% plotGeneral plots the generic states from a run of the AuvSim.
%
%  h = plotHuginData(simReturn, conf)
%
% Input parameters:
%   simReturn       - Return structure from simulink 
%   conf            - Config structure
%   flags           - Plot flags
%
% Output parameters:
%   h               - Vector of plot handles
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    cTime = simReturn.tAuv;
    etaAuv = interp1(simReturn.tAuv, simReturn.eta, cTime);
    nuAuv = interp1(simReturn.tAuv, simReturn.nu, cTime);
    etaNavlab = interp1(simReturn.tAuv, simReturn.etaNavlab, cTime);
    xDot = interp1(simReturn.tAuv, simReturn.xDot, cTime);
    
    fontSize = conf.plot.fontsize + 8;
    
    limoffset = 10;
    count = 0;
    fig = [];
    if(flags(1))
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'HuginDataTrajectoryPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        hold on;
        plot(etaAuv(:,1), etaAuv(:,2), 'b', 'LineWidth', conf.plot.linewidth);
        plot(etaNavlab(:,1), etaNavlab(:,2), 'r', 'LineWidth', conf.plot.linewidth);
        xlabel('North position [m]', 'FontSize', fontSize);
        ylabel('East position [m]', 'FontSize', fontSize);
        xlim([min(etaAuv(:,1))-limoffset max(etaAuv(:,1))+limoffset]);
        ylim([min(etaAuv(:,2))-limoffset max(etaAuv(:,2))+limoffset]);
        view(-90, 90);
        daspect(ax, [1 1 1]);
    end
    if(flags(2))
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'HuginDataEtaPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        subplot(2,3,1);
        hold on;
        plot(cTime, etaAuv(:,1), 'b');
        plot(cTime, etaNavlab(:,1), 'r');
        xlabel('Time [s]');
        ylabel('North position [m]');
        subplot(2,3,2);
        hold on;
        plot(cTime, etaAuv(:,2), 'b');
        plot(cTime, etaNavlab(:,2), 'r');
        xlabel('Time [s]');
        ylabel('East position [m]');
        pzh = subplot(2,3,3);
        hold on;
        plot(cTime, etaAuv(:,3), 'b');
        plot(cTime, etaNavlab(:,3), 'r');
        xlabel('Time [s]');
        ylabel('Depth [m]');
        set(pzh, 'Ydir', 'reverse');
        subplot(2,3,4);
        hold on;
        plot(cTime, etaAuv(:,4), 'b');
        plot(cTime, etaNavlab(:,4), 'r');
        xlabel('Time [s]');
        ylabel('Roll [deg]');
        subplot(2,3,5);
        hold on;
        plot(cTime, etaAuv(:,5), 'b');
        plot(cTime, etaNavlab(:,5), 'r');
        xlabel('Time [s]');
        ylabel('Pitch [deg]');
        subplot(2,3,6);
        hold on;
        plot(cTime, etaAuv(:,6), 'b');
        plot(cTime, etaNavlab(:,6), 'r');
        xlabel('Time [s]');
        ylabel('Yaw [deg]');
    end
    if(flags(3))
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'HuginDataNuPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        subplot(2,3,1);
        hold on;
        plot(cTime, nuAuv(:,1), 'b');
        xlabel('Time [s]');
        ylabel('x-velocity [m/s]');
        subplot(2,3,2);
        hold on;
        plot(cTime, nuAuv(:,2), 'b');
        xlabel('Time [s]');
        ylabel('y-velocity [m/s]');
        subplot(2,3,3);
        hold on;
        plot(cTime, nuAuv(:,3), 'b');
        xlabel('Time [s]');
        ylabel('z-velocity [m/s]');
        subplot(2,3,4);
        hold on;
        plot(cTime, nuAuv(:,4), 'b');
        xlabel('Time [s]');
        ylabel('Roll angular rate [rad/s]');
        subplot(2,3,5);
        hold on;
        plot(cTime, nuAuv(:,5), 'b');
        xlabel('Time [s]');
        ylabel('Pitch angular rate [rad/s]');
        subplot(2,3,6);
        hold on;
        plot(cTime, nuAuv(:,6), 'b');
        xlabel('Time [s]');
        ylabel('Yaw angular rate [rad/s]');
    end
    if(flags(4))
        count = count + 1;
        fig = [fig; figure];
        set(fig(count), 'Name', 'HuginDataAccPlot');
        set(fig(count), 'Position', get(0,'Screensize'));
        set(fig(count), 'Units', 'Normalized');
        set(fig(count), 'outerposition', [0 0 1 1]);
        set(fig(count), 'Units', 'Pixels');
        ax = axes;
        set(ax,'FontSize',fontSize);
        set(gcf,'Color', [1 1 1]);
        subplot(3,1,1);
        hold on;
        plot(cTime, xDot(:,1), 'b');
        xlabel('Time [s]');
        ylabel('Surge acc. [m/s^2]');
        subplot(3,1,2);
        hold on;
        plot(cTime, xDot(:,2), 'b');
        xlabel('Time [s]');
        ylabel('Sway acc. [m/s^2]');
        subplot(3,1,3);
        hold on;
        plot(cTime, xDot(:,3), 'b');
        xlabel('Time [s]');
        ylabel('Angular acc. yaw [rad/s^2]');
    end