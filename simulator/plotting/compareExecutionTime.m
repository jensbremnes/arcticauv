% Script for comparing execution time from different runs
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

close all; clear all; clc;
export = 1;
exportAsPdf = 1;
fontSize = 28;
fontSizeExtraSmallFigs = 20;
lineWidth = 3.0;
colorPlot = [153, 51, 51; 0, 102, 0; 51, 51, 153]./255;

%% Load data
dataRoot = 'C:/Users/norgren/OneDrive - NTNU/PhD/Research/Iceberg SLAM/Results/Final/';
caseStr = 'case1';
particles200Path = [dataRoot, '200particles', '/', caseStr, '/'];
particles500Path = [dataRoot, '500particles', '/', caseStr, '/'];

ORIBPSLAM200 = loadRunTime([particles200Path, 'BPSLAM/run1/']);
MODBPSLAM200 = loadRunTime([particles200Path, 'ModifiedBPSLAM/run1/']);
ICPBPSLAM200 = loadRunTime([particles200Path, 'ICPBPSLAM/run1/']);

ORIBPSLAM500 = loadRunTime([particles500Path, 'BPSLAM/run1/']);
MODBPSLAM500 = loadRunTime([particles500Path, 'ModifiedBPSLAM/run1/']);
ICPBPSLAM500 = loadRunTime([particles500Path, 'ICPBPSLAM/run1/']);

%% Extract data
runtime1 = ORIBPSLAM200.Total./1000000;
runtime2 = MODBPSLAM200.Total./1000000;
runtime3 = ICPBPSLAM200.Total./1000000;
runtime4 = ORIBPSLAM500.Total./1000000;
runtime5 = MODBPSLAM500.Total./1000000;
runtime6 = ICPBPSLAM500.Total./1000000;

%% Plot
fig = figure;
set(fig, 'Name', 'SlamExecutionTimeComparisonPlot');
set(fig, 'Position', get(0,'Screensize'));
set(fig, 'Units', 'Normalized');
set(fig, 'outerposition', [0 0 1 1]);
set(fig, 'Units', 'Pixels');
hold on;
grid on;
h1 = plot(runtime1, 'Color', colorPlot(1,:), 'LineWidth', lineWidth, 'LineStyle', '-');
h2 = plot(runtime2, 'Color', colorPlot(2,:),'LineWidth', lineWidth, 'LineStyle', '-');
h3 = plot(runtime3, 'Color', colorPlot(3,:), 'LineWidth', lineWidth, 'LineStyle', '-');
% plot(ones(length(runtime1),1).*mean(runtime1), 'Color', colorPlot(1,:), 'LineWidth', lineWidth, 'LineStyle', '-');
% plot(ones(length(runtime2),1).*mean(runtime2), 'Color', colorPlot(2,:),'LineWidth', lineWidth, 'LineStyle', '-');
% plot(ones(length(runtime3),1).*mean(runtime3), 'Color', colorPlot(3,:), 'LineWidth', lineWidth, 'LineStyle', '-');
h4 = plot(runtime4, 'Color', colorPlot(1,:), 'LineWidth', lineWidth, 'LineStyle', '--');
h5 = plot(runtime5, 'Color', colorPlot(2,:),'LineWidth', lineWidth, 'LineStyle', '--');
h6 = plot(runtime6, 'Color', colorPlot(3,:), 'LineWidth', lineWidth, 'LineStyle', '--');
% plot(ones(length(runtime4),1).*mean(runtime4), 'Color', colorPlot(1,:), 'LineWidth', lineWidth, 'LineStyle', '--');
% plot(ones(length(runtime5),1).*mean(runtime5), 'Color', colorPlot(2,:),'LineWidth', lineWidth, 'LineStyle', '--');
% plot(ones(length(runtime6),1).*mean(runtime6), 'Color', colorPlot(3,:), 'LineWidth', lineWidth, 'LineStyle', '--');
set(gca,'FontSize', fontSize);
xlabel('SLAM iteration [-]', 'FontSize', fontSize);
ylabel('Execution time for SLAM [s]', 'FontSize', fontSize);
l1 = sprintf('BPSLAM:          200 particles - mean ex. time =  %5.02fs. ', mean(runtime1));
l2 = sprintf('Mod. BPSLAM: 200 particles - mean ex. time =  %5.02fs. ', mean(runtime2));
l3 = sprintf('ICP BPSLAM:   200 particles - mean ex. time = %5.02fs. ', mean(runtime3));
l4 = sprintf('BPSLAM:          500 particles - mean ex. time =  %5.02fs. ', mean(runtime4));
l5 = sprintf('Mod. BPSLAM: 500 particles - mean ex. time = %5.02fs. ', mean(runtime5));
l6 = sprintf('ICP BPSLAM:   500 particles - mean ex. time = %5.02fs. ', mean(runtime6));
leg = legend([h1, h2, h3, h4, h5, h6], l1, l2, l3, l4, l5, l6);
set(leg, 'FontSize', fontSize);
xlim([1, length(runtime1)]);

if(export)
    str_name = [dataRoot, get(fig, 'Name')];
    if(exportAsPdf)
        export_fig(str_name, '-pdf', '-q100', '-transparent', fig); 
    else
       export_fig(str_name, '-png', '-q100', '-transparent', fig); 
    end
end

function times = loadRunTime(dataPath)
% Extract data from file
    if(exist([dataPath, 'timing.txt'], 'file'))
        try
            timing_array = importdata([dataPath, 'timing.txt'], ' ', 1);
            
            if(~isfield(timing_array, 'data'))
                return;
            end
            
        catch ex
            for i=1:length(ex.stack)
                fprintf('ERROR: Reading timing.txt failed. Exception: "%s" on line %i in file %s.\n'...
                    , ex.message, ex.stack(i).line, ex.stack(i).file);
            end
            return;
        end
        
        times = array2table(timing_array.data(:,:), 'VariableName', timing_array.colheaders);
    else
        return;
    end
end