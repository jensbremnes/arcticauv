function flag = runDune(duneBuildDir, conf)
% runDune is a function that calls dune.exe of the correct build and with
% desired configuration.
%
% runDune(duneBuildDir, debug, confFile)
%
% Input parameters:
%   duneBuildDir    - Dune build directory (terminated with \)
%   conf            - AuvSim configuration structure
%
% Output parameters:
%   flag            - Fail flag, 0 means success, 1 means fail
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.11.10
%
    oldDir = cd;

    if(conf.debug)
        duneDir = [duneBuildDir, 'Debug'];
    else
        duneDir = [duneBuildDir, 'Release'];
    end

    cmdStr = ['start dune.exe -c ', conf.duneConfigFile, ' -p ', conf.duneProfile];
    
    cd(duneDir);
    try
        dos(cmdStr);
        flag = 0;
    catch ex
        fprintf('ERROR: Execution of DUNE failed. Exception: "%s" on line %i.\n', ex.message, ex.stack(2).line);
        flag = 1;
    end
    cd(oldDir);
end

