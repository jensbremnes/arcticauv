function saveBaseCloud(filename, simReturn, conf)
    slamParticleStates = simReturn.slamParticleStates;
    nParticles = conf.iceslam.nParticles;
    tPart = simReturn.tPart;
    time = simReturn.tSlam;
    
    baseParticles = interp1(tPart, slamParticleStates, time);
    xLoc = zeros(length(baseParticles(:,1)), nParticles);
    yLoc = zeros(length(baseParticles(:,1)), nParticles);
    psiLoc = zeros(length(baseParticles(:,1)), nParticles);
    for i=1:length(baseParticles)
        for j=1:nParticles
            xLoc(i,j) = baseParticles(i,(j-1)*3+1);
            yLoc(i,j) = baseParticles(i,(j-1)*3+2);
            psiLoc(i,j) = baseParticles(i,(j-1)*3+3);
        end
    end
    
    sigmaXBase = std(xLoc, 0, 2);
    sigmaYBase = std(yLoc, 0, 2);
    sigmaPsiBase = std(psiLoc, 0, 2);
    
    save(filename, 'sigmaXBase', 'sigmaYBase', 'sigmaPsiBase', 'time');
end