function xyz = loadMbePointCloudFromFile(conf)
% loadMbePointCloudFromFile loads the multibeam point cloud from DUNE generated file.
%
%  mbe = loadMbePointCloudFromFile(conf)
%
% Input parameters:
%   conf        - Config structure.
%
% Output parameters:
%   xyz         - MBE point cloud structure.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2016.04.22
%
%%
    s = dir(conf.mbePointCloudFileName);
    if(s.bytes == 0)
        % Return empty struct if no file
        xyz = struct;
        return;
    end

    M = csvread(conf.mbePointCloudFileName);
    
    time = M(:,1);
    
    points = zeros(length(time), 3);

    for i=1:length(time)
        points(i,:) = M(i, 2:4);
    end
    
    xyz.time = time;
    xyz.points = points;
    
    if(~exist(conf.saveDir, 'dir'))
        mkdir(conf.saveDir);
    end
    
    save([conf.saveDir, 'mbeXYZ.mat'], '-struct', 'xyz');
end