function mbe = loadMbeRangesFromFile(conf)
% loadMbeRangesFromFile loads the multibeam ranges from DUNE generated file.
%
%  mbe = loadMbeRangesFromFile(conf)
%
% Input parameters:
%   conf        - Config structure.
%
% Output parameters:
%   mbe         - MBE ranges structure.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2016.02.03
%
%%
    s = dir(conf.mbeRangesFileName);
    if(s.bytes == 0)
        % Return empty struct if no file
        mbe = struct;
        return;
    end

    M = csvread(conf.mbeRangesFileName);
    
    time = M(:,1);
    
    nBeams = length(M(1,:))-2;
    
    ranges = zeros(length(time), nBeams);

    for i=1:length(time)
        ranges(i,:) = M(i, 2:nBeams+1);
    end
    
    mbe.time = time;
    mbe.ranges = ranges;
    %%
    % Remove duplicates
    duplicateIdx = find(diff(mbe.time)==0);
    if(~isempty(duplicateIdx))
        for i=length(duplicateIdx):-1:1
           mbe.time(duplicateIdx(i)) = []; 
           mbe.ranges(duplicateIdx(i),:) = []; 
        end
    end
    save([conf.saveDir, 'mbeRanges.mat'], '-struct', 'mbe');
end