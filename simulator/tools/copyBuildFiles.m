function copyBuildFiles(modules, sfunctionDir, buildDir, DEBUG)
% copyBuildFiles copies the desired mexFiles to the directory specified in the
% input argument.
% 
% copyBuildFiles(modules, sfunctionDir, buildDir, DEBUG)
% 
% Input parameters:
%   modules         - Which modules to copy mex-files from
%   sfunctionDir    - Location of s-functions
%   buildDir        - Build directory
%   DEBUG           - DEBUG boolean
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%    Date created:  2014.11.26
%    

    if(DEBUG && strcmp(computer('arch'), 'win64'))
        id = 'x64\Debug\';
        ext = '.mexw64';
    elseif(DEBUG)
        id = 'Win32\Debug\';
        ext = '.mexw32';
    elseif(strcmp(computer('arch'), 'win64'))
        id = 'x64\Release\';
        ext = '.mexw64';
    else
        id = 'Win32\Release\';
        ext = '.mexw32';
    end

    for i=1:length(modules)
        
        % Source filenames
        sMexFile = [sfunctionDir, modules{i}, '\lib\', id, modules{i}, ext];
        sPdbFile = [sfunctionDir, modules{i}, '\lib\', id, modules{i}, '.pdb'];
        
        if(~exist(sMexFile, 'file'))
            fprintf('ERROR: Check mex-file: %s\n', sMexFile);
            error('Module mex-file for %s not found\n', modules{i});
           
        end
        
        if(~exist(sPdbFile, 'file') && DEBUG)
            fprintf('ERROR: Check pdb-file: %s\n', sPdbFile);
            error('Module pdb-file for: %s not found\n', modules{i}); 
        end
        
        % Destination filenames
        mexFile = [buildDir, '\', modules{i}, ext];
        pdbFile = [buildDir, '\', modules{i}, '.pdb'];
        
        % Copy mex-file
        if(exist(mexFile, 'file'))
            delete(mexFile);
        end
        copyfile(sMexFile, mexFile);
        
        % Copy pdb-file
        if(exist(pdbFile, 'file'))
            delete(pdbFile);
        end
        if(DEBUG)
            copyfile(sPdbFile, pdbFile);
        end
        fprintf('INFO: Necessary build-files for: "%s" copied.\n', modules{i});
    end
end

