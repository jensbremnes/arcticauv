function iceberg = loadIcebergPoseFromFile(conf)
% loadIcebergPoseFromFile loads the iceberg pose from DUNE generated file.
%
%  iceberg = loadIcebergPoseFromFile(conf)
%
% Input parameters:
%   conf       - Config structure.
%
% Output parameters:
%   iceberg    - Iceberg pose structure.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2016.02.03  PN Create function
%
    s = dir(conf.icebergPoseFileName);
    if(s.bytes == 0)
        % Return empty struct if no file
        iceberg = struct;
        return;
    end

    M = csvread(conf.icebergPoseFileName);
    
    iceberg.time = M(:,1);
    iceberg.north = M(:,2);
    iceberg.east = M(:,3);
    iceberg.psi = M(:,4);
    
    % Remove duplicates
    duplicateIdx = find(diff(iceberg.time)==0);
    if(~isempty(duplicateIdx))
        for i=length(duplicateIdx):-1:1
           iceberg.time(duplicateIdx(i)) = []; 
           iceberg.north(duplicateIdx(i)) = [];
           iceberg.east(duplicateIdx(i)) = []; 
           iceberg.psi(duplicateIdx(i)) = [];
        end
    end
    
    boundsFileName = [conf.icebergDataDir, conf.icebergName, '_bounds.dat'];
    bounds = dlmread(boundsFileName);
    iceberg.offsetNorth = -(bounds(2) - (bounds(2) - bounds(1))/2);
    iceberg.offsetEast = -(bounds(4) - (bounds(4) - bounds(3))/2);
    iceberg.bounds = bounds;
    
    save([conf.saveDir, 'iceberg.mat'], '-struct', 'iceberg');
end