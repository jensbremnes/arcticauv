/* 
    Source file for functions defining the s-function.

    References:		S-function template created by Roger Skjetne.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	#####################################  MODEL FUNCTIONS	 ##############################################
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	Parameters:
	SystemStruct->Param[i].tag			i, j zero - indexed.
	SystemStruct->Param[i].size
	SystemStruct->Param[i].pvec[j], The row array 'pvec[]' stores a matrix row - by - row.

	Inputs :
	    u[0], u[1], u[2], etc.

    Continuous states :
	    xc[0], xc[1], xc[2], etc.

    Discrete states :
    xd[0], xd[1], xd[2], etc.

    Objective of function : To assign the outputs
    y[0], y[1], y[2], etc.

    Copyright: Department of Marine Technology, NTNU
    Author:    Petter Norgren
*/

#include "sFunction_model.hpp"
#include "MultibeamReader.hpp"

// Define indices of params
#define NAV_IDX 0
#define POS_IDX 1
#define ATT_IDX 2
#define BEAM_IDX 3
#define NSD_IDX 4
#define XYZ_IDX 5
#define NAVLABFILE_IDX 6
#define SENSOR_IDX 7

sFunction_model::sFunction_model(SimStruct *, sSystemCFG *System)
	: m_reader(nullptr)
{
	ReaderParams param;
	param.nav.file = System->Param[NAV_IDX].tag;
	param.nav.startIdx = (hsize_t)System->Param[NAV_IDX].pvec[0];
	param.nav.endIdx = (hsize_t)System->Param[NAV_IDX].pvec[1];
	param.pos.file = System->Param[POS_IDX].tag;
	param.pos.startIdx = (hsize_t)System->Param[POS_IDX].pvec[0];
	param.pos.endIdx = (hsize_t)System->Param[POS_IDX].pvec[1];
	param.att.file = System->Param[ATT_IDX].tag;
	param.att.startIdx = (hsize_t)System->Param[ATT_IDX].pvec[0];
	param.att.endIdx = (hsize_t)System->Param[ATT_IDX].pvec[1];
	param.beam.file = System->Param[BEAM_IDX].tag;
	param.beam.startIdx = (hsize_t)System->Param[BEAM_IDX].pvec[0];
	param.beam.endIdx = (hsize_t)System->Param[BEAM_IDX].pvec[1];
	param.nsd.file = System->Param[NSD_IDX].tag;
	param.nsd.startIdx = (hsize_t)System->Param[NSD_IDX].pvec[0];
	param.nsd.endIdx = (hsize_t)System->Param[NSD_IDX].pvec[1];
	param.xyz.file = System->Param[XYZ_IDX].tag;
	param.xyz.startIdx = (hsize_t)System->Param[XYZ_IDX].pvec[0];
	param.xyz.endIdx = (hsize_t)System->Param[XYZ_IDX].pvec[1];
	param.navOutFile = System->Param[NAVLABFILE_IDX].tag;
	param.sensor = System->Param[SENSOR_IDX].tag;
	param.nBeams = (int)System->Param[SENSOR_IDX].pvec[0];

	m_reader = std::unique_ptr<MultibeamReader>(new MultibeamReader(param));
}

sFunction_model::~sFunction_model()
{
	;
}

// Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
void sFunction_model::ModelOutputs(SimStruct *S, sSystemCFG *, real_T *, real_T *, real_T *y)
{
	if (m_reader != nullptr)
	{
		if (m_reader->step())
		{
			m_reader->fillStateVector(y, ssGetOutputPortWidth(S, 0));
			if (m_reader->doExit())
				ssSetStopRequested(S, 1);
		}
		else
		{
			printf("ERROR: sMultibeamReader::ModelOutputs() - Unable to step. Exiting.\n");
			ssSetStopRequested(S, 1);
		}
	}
}

// Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
void sFunction_model::ModelXcDerivatives(SimStruct *, sSystemCFG *, real_T *, real_T *, real_T *, const real_T *)
{
	;
}

// Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
void sFunction_model::ModelXdUpdate(SimStruct *, sSystemCFG *, real_T *, real_T *, const real_T *)
{
	;
}
