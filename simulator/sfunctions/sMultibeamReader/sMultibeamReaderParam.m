function [Param, Fname, Xc0, Xd0] = sMultibeamReaderParam(Ts, conf)
%
%   [Param, Fname, Xc0, Xd0] = sMultibeamReaderParam(Ts, conf)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%      

    %% Dynamical system configuration File
    % Project description 
    System.Project          = 'AUV simulator';
    System.Model            = 'sMultibeamReader';
    
    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= 0;
    System.NumInputSignals	= 3;
    System.NumOutputSignals	= conf.mbe.nOutputs;
    System.ConfigFile       = 'sMultibeamReaderParam';

    %% Model parameters
    System.Param{1}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.navlabDataFile];
    System.Param{1}.values	= [conf.mbe.startIndex, conf.mbe.endIndex];
    
    System.Param{2}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.posDataFile];
    System.Param{2}.values	= [0, 0];
    
    System.Param{3}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.attitudeDataFile];
    System.Param{3}.values	= [0, 0];
    
    System.Param{4}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.beamDataFile];
    System.Param{4}.values	= [0, 0];
    
    System.Param{5}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.nsdDataFile];
    System.Param{5}.values	= [0, 0];
    
    System.Param{6}.tag  	= [conf.mbeDir, conf.mbe.missionName, '/', conf.mbe.xyzDataFile];
    System.Param{6}.values	= [0, 0];
    
    System.Param{7}.tag  	= [conf.saveDir, conf.mbe.navlabOutFile];
    System.Param{7}.values	= [0, 0];
    
    System.Param{8}.tag  	= conf.mbe.sensor;
    System.Param{8}.values	= conf.mbe.nBeams;
    
    System.NumParameters	= length(System.Param);

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = []';   
end