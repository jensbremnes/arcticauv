/*
IcebergModel.cpp

Header file for global defines

Copyright: 		Petter Norgren, Department of Marine Technology, NTNU
Author: 			Petter Norgren

*/

#include "IcebergModel.hpp"

#include <fstream>
#include <string>

IcebergModel::IcebergModel(const std::string &mapPath, const std::string &mapName,
	const double *x0_iceberg, const double *x0_auv)
	: m_eta_iceberg_n(x0_iceberg[0], x0_iceberg[1], x0_iceberg[2])
	, m_nu_iceberg_i(x0_iceberg[3], x0_iceberg[4], x0_iceberg[5])
	, m_eta_auv_n(Eigen::Matrix<double, 6, 1>::Zero())
	, m_nu_auv_b(Eigen::Matrix<double, 6, 1>::Zero())
	, m_eta_offset(0, 0, 0)
{
	char fileName[128];
	Eigen::MatrixXd offset;

	setAuvState(x0_auv);

	offset = Eigen::MatrixXd::Zero(1, 2);
	sprintf(fileName, "%s%s_rotation.dat", mapPath.c_str(), mapName.c_str());
	if (!loadMatrixFromFile(offset, std::string(fileName)))
		return;

	m_eta_offset << offset(1), offset(0), 0;
}

IcebergModel::~IcebergModel()
{
	;
}

void IcebergModel::step(const double &ts)
{
	Eigen::Vector3d x_dot;

	x_dot = Rz(m_eta_iceberg_n(2))*m_nu_iceberg_i;
	m_eta_iceberg_n += x_dot*ts;
}

void IcebergModel::setVelocity(const double &u, const double &v, const double &r)
{
	m_nu_iceberg_i << u, v, r;
}

void IcebergModel::setAuvState(const double *x)
{
	m_eta_auv_n = Eigen::Map<const Eigen::Matrix<double, 6, 1>>(&x[0], 6);
	m_nu_auv_b = Eigen::Map<const Eigen::Matrix<double, 6, 1>>(&x[6], 6);
}

void IcebergModel::getIcebergPosition(double *y, int size) const
{
	memcpy_s(y, size, &m_eta_iceberg_n(0), m_eta_iceberg_n.size() * sizeof(double));
}

void IcebergModel::getIcebergVelocity(double *y, int size) const
{
	memcpy_s(y, size, &m_nu_iceberg_i(0), m_nu_iceberg_i.size() * sizeof(double));
}

void IcebergModel::getRelativePosition(double *y, int size) const
{
	Eigen::Matrix<double, 6, 1> eta_rel;
	Eigen::Vector3d eta_auv_n, eta_rel_3dof;
	Eigen::Matrix3d R;

	eta_auv_n << m_eta_auv_n(0), m_eta_auv_n(1), m_eta_auv_n(5);

	eta_rel_3dof = RzT(m_eta_iceberg_n(2))*(eta_auv_n - m_eta_iceberg_n - m_eta_offset) + m_eta_offset;

	eta_rel << eta_rel_3dof(0), eta_rel_3dof(1), m_eta_auv_n(2), m_eta_auv_n(3), m_eta_auv_n(4), eta_rel_3dof(2);
	memcpy_s(y, size, eta_rel.data(), eta_rel.size() * sizeof(double));
}

void IcebergModel::getRelativeVelocity(double *y, int size) const
{
	Eigen::Matrix<double, 6, 1> nu_rel;
	Eigen::Vector3d nu_rel_3dof;
	double psi_auv, psi_ice;

	psi_auv = m_eta_auv_n(5);
	psi_ice = m_eta_iceberg_n(2);

	nu_rel_3dof = Eigen::Vector3d(m_nu_auv_b(0), m_nu_auv_b(1), m_nu_auv_b(5))
		- RzT(psi_auv)*Rz(psi_ice)*m_nu_iceberg_i;

	nu_rel << nu_rel_3dof(0), nu_rel_3dof(1), m_nu_auv_b(2), m_nu_auv_b(3), m_nu_auv_b(4), nu_rel_3dof(2);

	memcpy_s(y, size, nu_rel.data(), nu_rel.size() * sizeof(double));
}

bool IcebergModel::loadMatrixFromFile(Eigen::MatrixXd &data, const std::string &filename) const
{
	int i, j;
	std::ifstream infile(filename.c_str());

	if (!infile.is_open())
	{
		printf("ERROR - loadMatrixFromFile() - File is not open.\n");
		return false;
	}

	for (j = 0; j < data.cols(); ++j)
	{
		for (i = 0; i < data.rows(); ++i)
		{
			if (infile.eof())
			{
				printf("ERROR - loadMatrixFromFile() - End of file reached too soon.\n");
				return false;
			}

			infile >> data(i, j);
		}
	}

	infile.close();

	return true;
}


