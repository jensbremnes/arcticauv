/* 
	Source file for functions defining the s-function.

	References:		S-function template created by Roger Skjetne.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#####################################  MODEL FUNCTIONS	 ##############################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Parameters:
SystemStruct->Param[i].tag			i, j zero - indexed.
SystemStruct->Param[i].size
SystemStruct->Param[i].pvec[j], The row array 'pvec[]' stores a matrix row - by - row.

Inputs :
		u[0], u[1], u[2], etc.

Continuous states :
		xc[0], xc[1], xc[2], etc.

Discrete states :
		xd[0], xd[1], xd[2], etc.

Objective of function : To assign the outputs
		y[0], y[1], y[2], etc.

		Copyright: 			Department of Marine Technology, NTNU
		Author: 				Petter Norgren
*/

#include "sFunction_model.h"

#include "IcebergModel.hpp"

sFunctionModel::sFunctionModel(SimStruct *S, sSystemCFG *System)
	: m_iceberg(nullptr)
{
	std::string icePath = System->Param[2].tag;
	std::string iceName = System->Param[3].tag;
	
	m_iceberg = std::unique_ptr<IcebergModel>(new IcebergModel(icePath, iceName, &System->Param[0].pvec[0], &System->Param[1].pvec[0]));

	UNREFERENCED_PARAMETER(S);
}

sFunctionModel::~sFunctionModel()
{
	;
}

// Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, real_T *y)
{
	memcpy_s(&y[0], 6 * sizeof(real_T), xd, SystemStruct->NumDiscStates * sizeof(real_T));
	m_iceberg->getRelativePosition(&y[6], 6 * sizeof(real_T));
	m_iceberg->getRelativeVelocity(&y[12], 6 * sizeof(real_T));

	UNREFERENCED_PARAMETER(S);
	UNREFERENCED_PARAMETER(xc);
}

// Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc_dot, real_T *xc, real_T *xd, const real_T *u)
{
	UNREFERENCED_PARAMETER(S); 
	UNREFERENCED_PARAMETER(SystemStruct);
	UNREFERENCED_PARAMETER(xc_dot); 
	UNREFERENCED_PARAMETER(xc);
	UNREFERENCED_PARAMETER(xd);
	UNREFERENCED_PARAMETER(u);
}

// Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
void sFunctionModel::ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u)
{
	m_iceberg->setVelocity(u[0], u[1], u[2]);
	m_iceberg->setAuvState(&u[3]);
	m_iceberg->step((double)ssGetStepSize(S));
	m_iceberg->getIcebergPosition(xd, 3 * sizeof(real_T));
	m_iceberg->getIcebergVelocity(&xd[3], 3 * sizeof(real_T));

	UNREFERENCED_PARAMETER(xc);
	UNREFERENCED_PARAMETER(SystemStruct);
}
