/* 
	Header file for functions defining the model of the s-function.

	References:		S-function template created by Roger Skjetne.

	Copyright: 			Department of Marine Technology, NTNU
	Author: 				Petter Norgren
*/

#pragma once

// Library header files
#include "libGenericFunctions.h"

#include <memory>

class IcebergModel;

/****************************************************************************
* Input parameters       		  																					*
*		-PS: Specify the parameters as one column vector, row after row!    *
****************************************************************************/
#define SimParam_IDX 		0	// [SampleTime, NumContStates, NumDiscStates, NumInputSignals, NumOutputSignals, NumParameters]
#define SimParam_PARAM(S) ssGetSFcnParam(S,SimParam_IDX)

#define InitFname_IDX 		1	// Filename initialization (configuration) file.
#define InitFname_PARAM(S) ssGetSFcnParam(S,InitFname_IDX)

#define Xc0_IDX 			2	// Initial conditions continuous states
#define Xc0_PARAM(S) ssGetSFcnParam(S,Xc0_IDX)

#define Xd0_IDX 			3	// Initial conditions discrete states
#define Xd0_PARAM(S) ssGetSFcnParam(S,Xd0_IDX)

/*********************************
* Vector and array sizes		 *
*********************************/

// DO NOT CHANGE THESE PARAMTERS UNLESS SIMULINK S-FUNCTION MAIN IS CHANGED (number of output ports, input ports and parameters are set in matlab)
#define NP 		4	/* Number of INPUT DIALOG PARAMETERS (DO NOT CHANGE)					*///
#define NIP   2	/* Number of Input Ports (DO NOT CHANGE)										*/
#define NU0		2	/* Input 0: Mode control input vector (DO NOT CHANGE)				*/
#define NOP   1	/* Number of Output Ports	(DO NOT CHANGE)										*/

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* #####################################  Function declarations	################################### */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

class sFunctionModel
{
public:
	// Do not change these functions. Called by main.
	sFunctionModel(SimStruct *S, sSystemCFG *SystemStruct);
	~sFunctionModel(void);
	void ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, real_T *y);
	void ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc_dot, real_T *xc, real_T *xd, const real_T *u);
	void ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u);

private:
	std::unique_ptr<IcebergModel> m_iceberg;
};
