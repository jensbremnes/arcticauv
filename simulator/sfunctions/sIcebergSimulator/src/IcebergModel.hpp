/*
IcebergModel.hpp

Header file for global defines

Copyright: 		Petter Norgren, Department of Marine Technology, NTNU
Author: 			Petter Norgren

*/

#pragma once

#include <Eigen/Dense>

template <class T>
inline Eigen::Matrix<T, 3, 3>
Rz(const T &yaw)
{
	Eigen::Matrix<T, 3, 3> R;

	R = Eigen::AngleAxisd(yaw, Eigen::Matrix<T, 3, 1>::UnitZ());

	return R;
}

template <class T>
inline Eigen::Matrix<T, 3, 3>
RzT(const T &yaw)
{
	Eigen::Matrix<T, 3, 3> R;

	R = Eigen::AngleAxisd(yaw, Eigen::Matrix<T, 3, 1>::UnitZ());
	R.transposeInPlace();

	return R;
}

class IcebergModel
{
public: 
	IcebergModel(const std::string &mapPath, const std::string &mapName,
		const double *x0_iceberg, const double *x0_auv);
	~IcebergModel();

	void step(const double &ts);

	void setVelocity(const double &u, const double &v, const double &r);
	void setAuvState(const double *x);

	void getIcebergPosition(double *y, int size) const;
	void getIcebergVelocity(double *y, int size) const;
	void getRelativePosition(double *y, int size) const;
	void getRelativeVelocity(double *y, int size) const;

	// Forcing heap alignment with 16
	void *operator new(size_t size) { return _aligned_malloc(size, 16); }
	void operator delete(void *ptr) { return _aligned_free(static_cast<IcebergModel *>(ptr)); }

private:
	bool loadMatrixFromFile(Eigen::MatrixXd &data, const std::string &filename) const;

private:
	Eigen::Vector3d m_eta_iceberg_n;
	Eigen::Vector3d m_nu_iceberg_i;
	Eigen::Vector3d m_eta_offset;
	Eigen::Matrix<double, 6, 1> m_eta_auv_n;
	Eigen::Matrix<double, 6, 1> m_nu_auv_b;
};
