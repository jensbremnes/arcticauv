function [Param, Fname, Xc0, Xd0] = sIcebergSimParam(Ts, conf)
%
%   [Param, Fname, Xc0, Xd0] = sIcebergSimParam(Ts, conf)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%      

    %% Dynamical system configuration File
    % Project description 
    System.Project          = 'AUV simulator';
    System.Model            = 'Iceberg Simulator';
    
    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= 6;
    System.NumInputSignals	= 15;
    System.NumOutputSignals	= 18;
    System.ConfigFile       = 'sIcebergSimulatorParam';

    %% Model parameters
    System.Param{1}.tag  	= 'Initial iceberg state';
    System.Param{1}.values	= [conf.iceberg.eta0; conf.iceberg.nu0];
    
    System.Param{2}.tag  	= 'Initial auv pose';
    System.Param{2}.values	= conf.x0;
    
    System.Param{3}.tag  	= conf.terrain.icebergPath;
    System.Param{3}.values	= '';
    
    System.Param{4}.tag  	= conf.terrain.icebergName;
    System.Param{4}.values	= '';
    
    System.NumParameters	= length(System.Param);

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = [conf.iceberg.eta0; conf.iceberg.nu0];
end