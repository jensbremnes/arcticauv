function [Param, Fname, Xc0, Xd0] = sDvlMbeSimulatorParam(Ts, conf)
%
%   [Param, Fname, Xc0, Xd0] = sDvlMbeSimulatorParam(Ts, conf)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%    Date created:  2014.11.26
%      

    %% Dynamical system configuration File
    % Project description 
    System.Project          = 'AUV simulator';
    System.Model            = 'DVL/MBE Simulator';
    
    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= 0;
    System.NumInputSignals	= 6;                           % 6 AUV states
    System.NumOutputSignals	= 1 + 4 + 4 + 2*conf.mbe.nBeams;   % 1 altimeter, 4 DVL up, 4 DVL down and MBE ranges and angles
    System.ConfigFile       = 'sDvlMbeSimulatorParam';

    %% Model parameters
    System.Param{1}.tag  	= conf.terrain.bathymetryPath;
    System.Param{1}.values	= '';
    
    System.Param{2}.tag  	= conf.terrain.bathymetryName;
    System.Param{2}.values	= '';
    
    System.Param{3}.tag  	= conf.terrain.icebergPath;
    System.Param{3}.values	= '';
    
    System.Param{4}.tag  	= conf.terrain.icebergName;
    System.Param{4}.values	= '';
    
    System.Param{5}.tag  	= 'MBE looking up';
    System.Param{5}.values	= conf.mbe.isUpLooking;
    
    System.Param{6}.tag  	= 'Number of MBE beams';
    System.Param{6}.values	= conf.mbe.nBeams;
    
    System.Param{7}.tag  	= 'MBE sector angle (radians)';
    System.Param{7}.values	= conf.mbe.sectorRadians;
    
    System.Param{8}.tag  	= 'MBE min/max range';
    System.Param{8}.values	= [conf.mbe.minRange, conf.mbe.maxRange];
    
    System.Param{9}.tag  	= 'DVL beam angle (radians)';
    System.Param{9}.values	= conf.dvl.beamAngleRadians;
    
    System.Param{10}.tag  	= 'DVL min/max range';
    System.Param{10}.values	= [conf.dvl.minRange, conf.dvl.maxRange];
    
    System.Param{11}.tag  	= 'MBE/DVL enable';
    System.Param{11}.values	= [conf.mbe.enable, conf.dvl.enable];
    
    System.Param{12}.tag  	= 'Return NAN for beams above water surface';
    System.Param{12}.values	= or(conf.mbe.isBeamNanAboveSurface, conf.dvl.isBeamNanAboveSurface);
    
    System.Param{13}.tag  	= conf.mbe.savefile;
    System.Param{13}.values	= conf.mbe.enableLogging;
    
    System.NumParameters	= length(System.Param);

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = []';   
end