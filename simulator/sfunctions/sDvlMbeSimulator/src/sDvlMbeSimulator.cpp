/*
    sDvlMbeSimulator.cpp

    Source file for functions defining the s-function functionality.

    Copyright: Department of Marine Technology, NTNU
    Author:    Petter Norgren
*/

#include "sDvlMbeSimulator.h"

dvlMbeSimulator::dvlMbeSimulator(const int &nMbeBeams, const double &mbeSectorRad,
	const float &mbeMinRange, const float &mbeMaxRange, const bool &mbeEnable, 
	const bool &isNanAboveSurf, const double &dvlBeamAngleRad, const float &dvlMinRange,
	const float &dvlMaxRange, const bool &dvlEnable, const bool &isMbeUp, 
	const std::string &bathyPath, const std::string &bathyName, const std::string &icePath, 
	const std::string &iceName, const std::string &saveFile, const bool &enableLogging) :
	m_bathyMap(nullptr),
	m_iceMap(nullptr),
	m_dvlDown(nullptr),
	m_dvlUp(nullptr),
	m_mbe(nullptr),
	m_altimeter(nullptr),
	m_nMbeBeams(nMbeBeams),
	m_altitude{ 200 },
	m_dvlDownRanges{ nan("0"), nan("0"), nan("0"), nan("0") },
	m_dvlUpRanges{ nan("0"), nan("0"), nan("0"), nan("0") },
	m_mbeRanges(nMbeBeams),
	m_bIsInitialized(false),
	m_bIsLogging(enableLogging)
{
	// Initialize maps
	if (dvlEnable || !isMbeUp)
	{
		m_bathyMap = std::shared_ptr<MapSimulator>(new MapSimulator(bathyPath, bathyName));
	}
	if (dvlEnable || isMbeUp)
	{
		m_iceMap = std::shared_ptr<MapSimulator>(new MapSimulator(icePath, iceName, true));
	}

	if (m_bathyMap != nullptr)
	{
		if (!m_bathyMap->isInitialized())
		{
			return;
		}
	}

	if (m_iceMap != nullptr)
	{
		if (!m_iceMap->isInitialized())
		{
			return;
		}
	}

	for (int i = 0; i < m_nMbeBeams; ++i)
	{
		m_mbeRanges[i] = nan("0");
	}

	// Initialize beam sensors
	if (m_bathyMap != nullptr)
	{
		if (dvlEnable)
		{
			m_dvlDown = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_bathyMap, 4, 2, dvlBeamAngleRad, dvlBeamAngleRad, false, dvlMinRange, dvlMaxRange));
		}
		if (!isMbeUp && mbeEnable)
		{
			m_mbe = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_bathyMap, m_nMbeBeams, 1, mbeSectorRad, 0, isMbeUp, mbeMinRange, mbeMaxRange));
		}
		if (!isMbeUp)
		{
			m_altimeter = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_bathyMap, 1, 1, 0, 0, isMbeUp, 1, 200));
		}
	}
	if (m_iceMap != nullptr)
	{
		if (dvlEnable)
		{
			m_dvlUp = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_iceMap, 4, 2, dvlBeamAngleRad, dvlBeamAngleRad, true, dvlMinRange, dvlMaxRange, isNanAboveSurf));
		}
		if (isMbeUp && mbeEnable)
		{
			m_mbe = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_iceMap, m_nMbeBeams, 1, mbeSectorRad, 0, isMbeUp, mbeMinRange, mbeMaxRange, isNanAboveSurf));
		}
		if (isMbeUp)
		{
			m_altimeter = std::unique_ptr<BeamSimulator>(
				new BeamSimulator(m_iceMap, 1, 1, 0, 0, isMbeUp, 1, 200));
		}
	}

	if (m_bIsLogging)
	{
		m_saveFile.open(saveFile, std::ios_base::out);

		if (!m_saveFile.is_open())
		{
			printf("dvlMbeSimulator::dvlMbeSimulator() - Error: Unable to open savefile: %s.\n", saveFile.c_str());
		}
	}

	m_bIsInitialized = true;
}

dvlMbeSimulator::~dvlMbeSimulator()
{
	if (m_saveFile.is_open())
	{
		m_saveFile.close();
	}
}

void dvlMbeSimulator::getDvlUpRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams)
{
	if (m_dvlUp != nullptr)
	{
		m_dvlUp->getBeamRangesAtPosition(m_dvlUpRanges, eta);
	}
	memcpy_s(ranges, Nbeams * sizeof(double), &m_dvlUpRanges[0], m_dvlUpRanges.size() * sizeof(double));
}
void dvlMbeSimulator::getDvlDownRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams)
{
	if (m_dvlDown != nullptr)
	{
		m_dvlDown->getBeamRangesAtPosition(m_dvlDownRanges, eta);
	}
	memcpy_s(ranges, Nbeams * sizeof(double), &m_dvlDownRanges[0], m_dvlDownRanges.size() * sizeof(double));
}

void dvlMbeSimulator::getMbeRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams)
{
	if (m_mbe != nullptr)
	{
		m_mbe->getBeamRangesAtPosition(m_mbeRanges, eta);

		if (m_bIsLogging)
		{
			if (m_saveFile.is_open())
			{
				std::vector<Vector3d> points(Nbeams);
				char buffer[64];

				m_mbe->getPointsFromRanges(points, eta, m_mbeRanges);

				for (int i = 0; i < points.size(); ++i)
				{
					sprintf_s(buffer, 64, "%f %f %f\n", points[i](0), points[i](1), points[i](2));
					m_saveFile << buffer;
				}
			}
		}
	}

	memcpy_s(ranges, Nbeams * sizeof(double), &m_mbeRanges[0], m_mbeRanges.size() * sizeof(double));
}

void dvlMbeSimulator::getAltimeterAtPosition(double *ranges, const std::vector<double> &eta)
{
	if (m_altimeter != nullptr)
	{
		m_altimeter->getBeamRangesAtPosition(m_altitude, eta);

		if (isnan(m_altitude[0]))
			m_altitude[0] = 0;
	}
	memcpy_s(ranges, 1 * sizeof(double), &m_altitude[0], m_altitude.size() * sizeof(double));
}

void dvlMbeSimulator::getMbeAngles(double *angles, const size_t &Nbeams)
{
	Eigen::VectorXd angVec = Eigen::VectorXd::Zero(Nbeams);

	if (m_mbe != nullptr)
		m_mbe->getBeamAngles(angVec);

	memcpy_s(angles, Nbeams * sizeof(double), angVec.data(), angVec.size() * sizeof(double));
}
