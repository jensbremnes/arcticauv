/* 
    sDvlMbeSimulator_model.cpp

    Source file for functions defining the s-function.

    References: S-function template created by Roger Skjetne.

    Copyright:  Department of Marine Technology, NTNU
    Author:     Petter Norgren   
*/

#include "sDvlMbeSimulator_model.h"
#include "sDvlMbeSimulator.h"

sFunctionModel::sFunctionModel(SimStruct *S, sSystemCFG *System)
	: m_dvlMbeSim(nullptr)
{
	UNREFERENCED_PARAMETER(S);

	// Extract filename and relative path to mat-file
	std::string bathyPath = System->Param[0].tag;
	std::string bathyName = System->Param[1].tag;
	std::string icePath = System->Param[2].tag;
	std::string iceName = System->Param[3].tag;
	std::string saveFile = System->Param[12].tag;

	// Get parameters
	bool isMbeUp = (System->Param[4].pvec[0] != 0);
	int nMbeBeams = (int)System->Param[5].pvec[0];
	double mbeSectorRad = (double)System->Param[6].pvec[0];
	float mbeMinRangeM = (float)System->Param[7].pvec[0];
	float mbeMaxRangeM = (float)System->Param[7].pvec[1];
	double dvlBeamAngleRad = (double)System->Param[8].pvec[0];
	float dvlMinRangeM = (float)System->Param[9].pvec[0];
	float dvlMaxRangeM = (float)System->Param[9].pvec[1];
	bool mbeEnable = (System->Param[10].pvec[0] != 0);
	bool dvlEnable = (System->Param[10].pvec[1] != 0);
	bool isBeamNanAboveSurf = (System->Param[11].pvec[0] != 0);
	bool enableLogging = (System->Param[12].pvec[0] != 0);

	// Set up dvlMbeSim member object
	m_dvlMbeSim = std::unique_ptr<dvlMbeSimulator>(
		new dvlMbeSimulator(nMbeBeams, mbeSectorRad, mbeMinRangeM, mbeMaxRangeM, mbeEnable, 
			isBeamNanAboveSurf, dvlBeamAngleRad, dvlMinRangeM, dvlMaxRangeM, dvlEnable, isMbeUp,
			bathyPath, bathyName, icePath, iceName, saveFile, enableLogging)
		);

	if (!m_dvlMbeSim->isInitialized())
	{
		exit(0);
	}
}

sFunctionModel::~sFunctionModel()
{
	;
}

/*	
	Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
*/
void sFunctionModel::ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, double *Xc, double *Xd, const double *u, double *y)
{
	std::vector<double> eta;
	const size_t N = (size_t)m_dvlMbeSim->getNMbeBeams();

	// Set state vector
	eta.assign(&u[0], &u[0] + 6);

	// Get ranges of Altimeter, MBE and DVL
	m_dvlMbeSim->getAltimeterAtPosition(&y[0], eta);
	m_dvlMbeSim->getDvlUpRangesAtPosition(&y[1], eta, 4);
	m_dvlMbeSim->getDvlDownRangesAtPosition(&y[5], eta, 4);
	m_dvlMbeSim->getMbeRangesAtPosition(&y[9], eta, N);
	m_dvlMbeSim->getMbeAngles(&y[9+N], N);

	UNREFERENCED_PARAMETER(S);
	UNREFERENCED_PARAMETER(SystemStruct);
	UNREFERENCED_PARAMETER(Xd);
	UNREFERENCED_PARAMETER(Xc);
}

/*	
	Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
*/
void sFunctionModel::ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, double *Xc_dot, double *Xc, double *Xd, const double *u)
{
	/* 
		No derivatives in sDvlMbeSimulator 
	*/
	
	// No continuous states
	UNREFERENCED_PARAMETER(S);
	UNREFERENCED_PARAMETER(SystemStruct);
	UNREFERENCED_PARAMETER(Xc_dot);
	UNREFERENCED_PARAMETER(Xd);
	UNREFERENCED_PARAMETER(Xc);
	UNREFERENCED_PARAMETER(u);
}

/*	
	Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
*/
void sFunctionModel::ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, double *Xc, double *Xd, const double *u)
{
	/*
		No discrete derivatives in sDvlMbeSimulator
	*/

	// No discrete states.
	UNREFERENCED_PARAMETER(S);
	UNREFERENCED_PARAMETER(SystemStruct);
	UNREFERENCED_PARAMETER(Xd);
	UNREFERENCED_PARAMETER(Xc);
	UNREFERENCED_PARAMETER(u);
}
