/*
    sDvlMbeSimulator.h
	
    Header file for functions defining the DVL and MBE simulator

    Copyright: Department of Marine Technology, NTNU
    Author:    Petter Norgren
*/

#pragma once

#include <memory>
#include <vector>

#include "MapSimulator.hpp"
#include "BeamSimulator.hpp"

using namespace AURLAB::RangeSimulator;

class dvlMbeSimulator
{

	public:
		dvlMbeSimulator(const int &nMbeBeams, const double &mbeSectorRad, 
			const float &mbeMinRange, const float &mbeMaxRange, const bool &mbeEnable, 
			const bool &isNanAboveSurf, const double &dvlBeamAngleRad, 
			const float &dvlMinRange, const float &dvlMaxRange, const bool &dvlEnable,
			const bool &isMbeUp, const std::string &bathyPath, 
			const std::string &bathyName, const std::string &icePath, 
			const std::string &iceName, const std::string &saveFile, const bool &enableLogging);
		~dvlMbeSimulator();

		void getDvlUpRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams);
		void getDvlDownRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams);
		void getMbeRangesAtPosition(double *ranges, const std::vector<double> &eta, const size_t &Nbeams);
		void getAltimeterAtPosition(double *ranges, const std::vector<double> &eta);
		void getMbeAngles(double *angles, const size_t &Nbeams);
		int getNMbeBeams() { return m_nMbeBeams; };
		bool isInitialized() { return m_bIsInitialized; }

	private:

		const int m_nMbeBeams;
		std::vector<double> m_mbeRanges;
		std::vector<double> m_dvlDownRanges;
		std::vector<double> m_dvlUpRanges;
		std::vector<double> m_altitude;

		std::shared_ptr<MapSimulator> m_bathyMap;
		std::shared_ptr<MapSimulator> m_iceMap;
		std::unique_ptr<BeamSimulator> m_dvlUp;
		std::unique_ptr<BeamSimulator> m_dvlDown;
		std::unique_ptr<BeamSimulator> m_mbe;
		std::unique_ptr<BeamSimulator> m_altimeter;

		std::fstream m_saveFile;
		const bool m_bIsLogging;

		bool m_bIsInitialized;
};
