/* 
	Header file for functions defining the model of the s-function.

	References:		S-function template created by Roger Skjetne.

	Copyright: 		Department of Marine Technology, NTNU
	Author: 		Petter Norgren
	Date created: 	30.10.2015
	Revised:		
*/

#pragma once

#include "ImcReceiver.h"
#include "ImcListener.h"
#include "ImcSender.h"

enum maneuovers
{
	E_IDLE = 0,
	E_GOTO = 1,
	E_FOLLOWPATH = 2
};

struct startupManeouver
{
	startupManeouver(const char* str) 
	{
		if (!strcmp(str, "idle"))
		{
			maneuver = E_IDLE;
		}
		else if (!strcmp(str, "goto"))
		{
			maneuver = E_GOTO;
		}
		else if (!strcmp(str, "followpath"))
		{
			maneuver = E_FOLLOWPATH;
		}
		else
		{
			maneuver = E_IDLE;
		}
	};

	~startupManeouver() {};

	maneuovers maneuver;
};

// Library header files
#include "libGenericFunctions.h"
#include "mex.h"

/****************************************************************************
* Input parameters       		  						                    *
*		-PS: Specify the parameters as one column vector, row after row!    *
****************************************************************************/
#define SimParam_IDX 		0	// [SampleTime, NumContStates, NumDiscStates, NumInputSignals, NumOutputSignals, NumParameters]
#define SimParam_PARAM(S) ssGetSFcnParam(S,SimParam_IDX)

#define InitFname_IDX 		1	// Filename initialization (configuration) file.
#define InitFname_PARAM(S) ssGetSFcnParam(S,InitFname_IDX)

#define Xc0_IDX 			2	// Initial conditions continuous states
#define Xc0_PARAM(S) ssGetSFcnParam(S,Xc0_IDX)

#define Xd0_IDX 			3	// Initial conditions discrete states
#define Xd0_PARAM(S) ssGetSFcnParam(S,Xd0_IDX)

/*********************************
* Vector and array sizes		 *
*********************************/

// DO NOT CHANGE THESE PARAMTERS UNLESS SIMULINK S-FUNCTION IS CHANGED
#define NP 		4	/* Number of INPUT DIALOG PARAMETERS					*///

#define NIP     2	/* Number of Input Ports								*/
#define NU0   	2	/* Input 0: Mode control input vector					*/

#define NOP     1	/* Number of Output Ports								*/

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* #####################################  Function declarations	################################### */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

class sFunctionModel
{
public:
	// Do not change these functions. Called by main.
	sFunctionModel(SimStruct *S, sSystemCFG *SystemStruct);
	~sFunctionModel(void);
	void ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u, real_T *y);
	void ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc_dot, real_T *xc, real_T *xd, const real_T *u);
	void ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u);

private:
	ImcSender *m_sender;
	ImcReceiver *m_receiver;
	ImcListener *m_listener;
	DUNE::Tasks::Context *m_context;
	DUNE::Network::UDPSocket *m_sock;

	int m_simExit;

	double m_lat_ref;
	double m_lon_ref;
	double m_height_ref;

	bool m_trace;
	bool m_debug;
};
