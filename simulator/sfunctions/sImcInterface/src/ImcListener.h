/*
	Header file for functions defining the IMC listener task

	Copyright: 			Department of Marine Technology, NTNU
	Author: 				Petter Norgren
	Date created: 	09.12.2015
	Revised:
*/

#pragma once

#pragma warning(push,3)
#include <DUNE/Tasks/Task.hpp>
#include <DUNE/Network/UDPSocket.hpp>
#include <DUNE/IO/Poll.hpp>
#include <DUNE/Concurrency/Thread.hpp>
#include <DUNE/Time/Clock.hpp>
#include <DUNE/Time/Delay.hpp>
#include <DUNE/IMC.hpp>
#pragma warning(pop)

#include "sFunctionDefines.h"

struct ImcListener : public DUNE::Tasks::Periodic
{
private:

	DUNE::Network::UDPSocket& m_sock;
	bool m_trace;
	bool m_debug;

	static const size_t c_bfr_size = 65535;
	static const int c_poll_tout = 1000;
	static const int c_frequency = 100;

	uint8_t* m_bfr;

public:
	ImcListener(DUNE::Tasks::Context &ctx, DUNE::Network::UDPSocket& sock,
		bool bTrace = false, bool bDebug = false) :
		DUNE::Tasks::Periodic("ImcListener", ctx),
		m_sock(sock),
		m_trace(bTrace),
		m_debug(bDebug)
	{
		m_bfr = new uint8_t[c_bfr_size];
	}

	~ImcListener() 
	{
		requestDeactivation();

		if (this->isRunning())
		{
			this->stop();
		}

		for (int i = 0; i < 10; i++)
		{
			if (isDead())
			{
				break;
			}
			DUNE::Time::Delay::waitMsec(100);
		}

		delete[] m_bfr;
	}

	void
		onResourceAcquisition()
	{
		requestActivation();

		setFrequency(c_frequency);
	}

	void
		onResourceRelease()
	{
		;
	}

	void readMessage()
	{
		static const unsigned int flag = DUNE::Tasks::DispatchFlags::DF_KEEP_SRC_EID
																			| DUNE::Tasks::DispatchFlags::DF_KEEP_TIME;

		DUNE::Network::Address addr;
		size_t rv;

		if (!isActive())
		{
			return;
		}

		try {
			DUNE::IMC::Message* msg;
			// Read udp stream
			rv = m_sock.read(m_bfr, c_bfr_size, &addr);

			// Deserialize message into generic IMC buffer
			msg = DUNE::IMC::Packet::deserialize(m_bfr, (uint16_t)rv);

			// Convert IMC message and dispatch internally
			if (msg->getId() == DUNE_IMC_VEHICLESTATE)
			{
				DUNE::IMC::VehicleState *state = static_cast<DUNE::IMC::VehicleState*>(msg);
				dispatch(state, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() VehicleState msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_GPSFIX)
			{
				DUNE::IMC::GpsFix *gps = static_cast<DUNE::IMC::GpsFix*>(msg);
				dispatch(gps, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() GpsFix msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_RPM)
			{
				DUNE::IMC::Rpm *thrust = static_cast<DUNE::IMC::Rpm*>(msg);
				dispatch(thrust, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Rpm msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_SERVOPOSITION)
			{
				DUNE::IMC::ServoPosition *servo = static_cast<DUNE::IMC::ServoPosition*>(msg);
				dispatch(servo, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() ServoPosition msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_VEHICLECOMMAND)
			{
				DUNE::IMC::VehicleCommand *cmd = static_cast<DUNE::IMC::VehicleCommand*>(msg);
				dispatch(cmd, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Vehicle command msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_DESIREDHEADING)
			{
				DUNE::IMC::DesiredHeading *psiD = static_cast<DUNE::IMC::DesiredHeading*>(msg);
				dispatch(psiD, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Desired heading msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_DESIREDHEADINGRATE)
			{
				DUNE::IMC::DesiredHeadingRate *psiRateD = static_cast<DUNE::IMC::DesiredHeadingRate*>(msg);
				dispatch(psiRateD, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Desired yaw rate msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_DESIREDPITCH)
			{
				DUNE::IMC::DesiredPitch *thetaD = static_cast<DUNE::IMC::DesiredPitch*>(msg);
				dispatch(thetaD, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Desired pitch msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_DESIREDZ)
			{
				DUNE::IMC::DesiredZ *zD = static_cast<DUNE::IMC::DesiredZ*>(msg);
				dispatch(zD, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Desired depth msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_DESIREDSPEED)
			{
				DUNE::IMC::DesiredSpeed *speedD = static_cast<DUNE::IMC::DesiredSpeed*>(msg);
				dispatch(speedD, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Desired speed msg received.\n");
				}
			}
			else if (msg->getId() == DUNE_IMC_ESTIMATEDSTATE)
			{
				DUNE::IMC::EstimatedState *est = static_cast<DUNE::IMC::EstimatedState*>(msg);
				dispatch(est, flag);

				if (m_debug)
				{
					debug("DEBUG: ImcListener::readMessage() Estimated state msg received.\n");
				}
			}
			else
			{
				inf("INFO: ImcListener::readMessage() msg with unknown identifier received. msg->id = %i.\n", msg->getId());
			}

			if (m_trace)
			{
				msg->toText(std::cerr);
			}

			// Clean-up
			delete msg;
		}
		catch (std::exception& e)
		{
			throw e;
		}
	}

	void 
		task(void)
	{
		static const double poll_tout = c_poll_tout / 1000;

		try
		{
			if (!DUNE::IO::Poll::poll(m_sock, poll_tout))
			{
				return;
			}
			readMessage();
		}
		catch (std::exception& e)
		{
			war("WARNING: ImcListener::task() - Error while unpacking message: %s\n", e.what());
		}
	}
};
