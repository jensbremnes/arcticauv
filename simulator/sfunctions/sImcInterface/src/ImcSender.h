/*
	Header file for functions defining the IMC sender class

	Copyright: 		Department of Marine Technology, NTNU
	Author: 			Petter Norgren
	Date created:	09.12.2015
	Revised:
*/

#pragma once

#pragma warning(push, 3)
#include <DUNE/Network/UDPSocket.hpp>
#include <DUNE/IMC.hpp>
#pragma warning(pop)

#include "sFunctionDefines.h"

class UdpSender
{
private:
	uint16_t m_port;
	std::string m_address;
	bool m_connected;
	bool m_trace;

public:
	UdpSender(std::string address, uint16_t port, bool bTrace = false) : 
		m_connected(false),
		m_trace(bTrace)
	{
		connect(address, port);
	}

	UdpSender() : 
		m_connected(false),
		m_trace(false)
	{
		;
	}

	void
		connect(std::string address, uint16_t port, bool bTrace = false)
	{
		m_address = address;
		m_port = port;
		m_connected = true;
		m_trace = bTrace;
	}

	void
		send(uint8_t* bfr, size_t length)
	{
		if (!m_connected)
		{
			throw std::runtime_error("ERROR: UdpSender::send() Not connected.");
			return;
		}

		DUNE::Network::Address address(m_address.c_str());
		DUNE::Network::UDPSocket socket;

		socket.write(bfr, length, address, m_port);
	}
};

class ImcSender : public UdpSender
{
private:
	static const int c_bfr_size = 65535;
	uint8_t m_bfr[c_bfr_size];
	bool m_trace;

public:
	ImcSender(std::string address, uint16_t port, bool bTrace = false) : 
		UdpSender(address, port, bTrace),
		m_trace(bTrace) 
	{
		;
	}

	ImcSender() : UdpSender(), m_trace(false) {}

	void send(const DUNE::IMC::Message* msg)
	{
		uint16_t rv = DUNE::IMC::Packet::serialize(msg, m_bfr, c_bfr_size);
		UdpSender::send(m_bfr, rv);

		if (m_trace)
		{
			ssPrintf("TRACE: ImcSender::send() - Message sent with identifier %i and size %i.\n", msg->getId(), rv);
		}
	}

	void heartBeat()
	{
		DUNE::IMC::Heartbeat heart;
		DUNE::IMC::Announce announce;

		this->send(&heart);
		this->send(&announce);

		if (m_trace)
		{
			ssPrintf("TRACE: ImcSender::heartBeat() - beat.\n");
		}
	}
};