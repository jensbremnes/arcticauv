/*
	Header file for functions defining the IMC receiver.

	Copyright: 			Department of Marine Technology, NTNU
	Author: 				Petter Norgren
	Date created: 	09.12.2015
	Revised:
*/

#pragma once

#pragma warning(push, 3)
#include <DUNE/Tasks.hpp>
#include <DUNE/Concurrency/Mutex.hpp>
#include <DUNE/Concurrency/ScopedMutex.hpp>
#include <DUNE/Network/UDPSocket.hpp>
#include <DUNE/Time/Delay.hpp>
#include <DUNE/IMC.hpp>
#pragma warning(pop)

#include "ImcSender.h"

#include "sFunctionDefines.h"

struct ImcReceiver : public DUNE::Tasks::Task
{
private:
	static const int c_time_count = 4;
	
	double m_timeStamps[c_time_count];
	fp64_t m_x_hat[12];
	fp64_t m_desiredHeading;
	fp64_t m_desiredDepth;
	fp64_t m_desiredSpeed;
	int8_t m_depthUnits;

	double m_lat;
	double m_lon;
	double m_height;

	bool m_gpsFixReceived;
	bool m_cmdAcknowledged;
	bool m_newHeadingRef;
	bool m_newDepthRef;
	bool m_newSpeedRef;
	bool m_newEstimate;

	int m_vehicleState;

	bool m_trace;
	bool m_debug;
	bool m_exit;

	ImcSender& m_sender;

	DUNE::Concurrency::Mutex m_mutex;

public:
	ImcReceiver(DUNE::Tasks::Context& ctx, ImcSender& sender, bool bTrace = false, bool bDebug = false) :
		DUNE::Tasks::Task("ImcReceiver", ctx),
		m_trace(bTrace),
		m_debug(bDebug),
		m_sender(sender),
		m_timeStamps{ 0, 0, 0, 0 },
		m_x_hat{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
		m_desiredHeading(0),
		m_desiredDepth(0),
		m_desiredSpeed(0),
		m_gpsFixReceived(false),
		m_cmdAcknowledged(false),
		m_newHeadingRef(false),
		m_newDepthRef(false),
		m_newSpeedRef(false),
		m_newEstimate(false),
		m_exit(false)
	{
		bind < DUNE::IMC::GpsFix >(this);
		bind < DUNE::IMC::VehicleState >(this);
		bind < DUNE::IMC::VehicleCommand >(this);
		bind < DUNE::IMC::DesiredHeading >(this);
		bind < DUNE::IMC::DesiredZ >(this);
		bind < DUNE::IMC::DesiredSpeed >(this);
		bind < DUNE::IMC::EstimatedState >(this);
	}

	~ImcReceiver()
	{
		requestDeactivation();

		if (this->isRunning())
		{
			this->stop();
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		for (int i = 0; i < 10; i++)
		{
			if (!this->isDead())
			{
				DUNE::Time::Delay::waitMsec(100);
			}
			else
			{
				break;
			}
		}
	}

	void
		onResourceAcquisition()
	{
		;
	}

	void
		onResourceRelease()
	{
		;
	}

	void
		consume(const DUNE::IMC::GpsFix* msg)
	{
		if (!m_gpsFixReceived)
		{
			if (!isActive())
			{
				requestActivation();
			}

			if (msg->type != DUNE::IMC::GpsFix::GFT_MANUAL_INPUT)
			{
				DUNE::Concurrency::ScopedMutex l(m_mutex);

				m_lat = msg->lat;
				m_lon = msg->lon;
				m_height = msg->height;

				m_gpsFixReceived = true;

				if (m_debug)
				{
					debug("DEBUG: ImcReciver::consume(GpsFix) Info: GPS origin updated.\n");
				}
			}
		}
	}

	void 
		consume(const DUNE::IMC::VehicleState* msg)
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		m_vehicleState = msg->op_mode;
	}

	void
		consume(const DUNE::IMC::VehicleCommand*)
	{
		if (!m_cmdAcknowledged)
		{
			DUNE::Concurrency::ScopedMutex l(m_mutex);
			m_cmdAcknowledged = true;
		}
	}

	void
		consume(const DUNE::IMC::EstimatedState* msg)
	{
		if (!isActive())
		{
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		m_x_hat[0] = msg->x;
		m_x_hat[1] = msg->y;
		m_x_hat[2] = msg->z;
		m_x_hat[3] = msg->phi;
		m_x_hat[4] = msg->theta;
		m_x_hat[5] = msg->psi;
		m_x_hat[6] = msg->u;
		m_x_hat[7] = msg->v;
		m_x_hat[8] = msg->w;
		m_x_hat[9] = msg->p;
		m_x_hat[10] = msg->q;
		m_x_hat[11] = msg->r;

		m_timeStamps[3] = msg->getTimeStamp();
		m_newEstimate = true;

		if (m_debug)
		{
			debug("DEBUG: ImcReciver::consume(EstimatedState) New EstimatedState received.\n");
		}
	}

	void
		consume(const DUNE::IMC::DesiredHeading *msg)
	{
		if (!isActive())
		{
			return;
		}

		if (!isnan((double)msg->value))
		{
			DUNE::Concurrency::ScopedMutex l(m_mutex);

			m_desiredHeading = msg->value;
			m_timeStamps[0] = msg->getTimeStamp();
			m_newHeadingRef = true;

			if (m_debug)
			{
				debug("DEBUG: ImcReciver::consume(DesiredHeading) New DesiredHeading received.\n");
			}
		}
		else
		{
			err("ERROR: ImcReceiver::consume(DesiredHeading) - NaN received.\n");
			m_exit = true;
		}
	}

	void
		consume(const DUNE::IMC::DesiredZ *msg)
	{
		if (!isActive())
		{
			return;
		}

		if (!isnan((double)msg->value))
		{
			DUNE::Concurrency::ScopedMutex l(m_mutex);

			m_desiredDepth = msg->value;
			m_depthUnits = msg->z_units;
			m_timeStamps[1] = msg->getTimeStamp();
			m_newDepthRef = true;

			if (m_debug)
			{
				debug("DEBUG: ImcReciver::consume(DesiredZ) New DesiredZ received.\n");
			}
		}
		else
		{
			err("ERROR: ImcReceiver::consume(DesiredZ) - NaN received.\n");
			m_exit = true;
		}
	}

	void
		consume(const DUNE::IMC::DesiredSpeed *msg)
	{
		if (!isActive())
		{
			return;
		}

		if (!isnan((double)msg->value))
		{
			DUNE::Concurrency::ScopedMutex l(m_mutex);

			m_desiredSpeed = msg->value;
			m_timeStamps[2] = msg->getTimeStamp();
			m_newSpeedRef = true;

			if (m_debug)
			{
				debug("DEBUG: ImcReciver::consume(DesiredSpeed) New DesiredSpeed received.\n");
			}
		}
		else
		{
			err("ERROR: ImcReceiver::consume(DesiredSpeed) - NaN received.\n");
			m_exit = true;
		}

		if (msg->speed_units != DUNE::IMC::SUNITS_METERS_PS)
		{
			err("ERROR: ImcReceiver::consume(DesiredSpeed) - Speed units is not m/s. Received speed units is: %i\n", msg->speed_units);
			m_exit = true;
		}
	}

	void
		getGpsOrigin(double& lat, double& lon, double& height)
	{
		if (!isActive())
		{
			lat = 0;
			lon = 0;
			height = 0;
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		lat= m_lat;
		lon = m_lon;
		height = m_height;
	}

	void getEstimatedState(double *x_hat, double &time)
	{
		if (!isActive())
		{
			memset(x_hat, 0, 12*sizeof(double));
			time = 0;
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		memcpy(x_hat, m_x_hat, 12 * sizeof(double));
		time = m_timeStamps[3];
		m_newEstimate = false;
	}

	void
		getDesiredHeading(double &headingD, double &time)
	{
		if (!isActive())
		{
			headingD = 0;
			time = 0;
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		headingD = m_desiredHeading;
		time = m_timeStamps[0];
		m_newHeadingRef = false;
	}

	void
		getDesiredDepth(double &depthD, double &time)
	{
		if (!isActive())
		{
			depthD = 0;
			time = 0;
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		depthD = m_desiredDepth;
		time = m_timeStamps[1];
		m_newDepthRef = false;
	}

	void
		getDesiredSpeed(double &speedD, double &time)
	{
		if (!isActive())
		{
			speedD = 0;
			time = 0;
			return;
		}

		DUNE::Concurrency::ScopedMutex l(m_mutex);

		speedD = m_desiredSpeed;
		time = m_timeStamps[2];
		m_newSpeedRef = false;
	}

	bool 
		hasGpsFix()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_gpsFixReceived;
	}

	bool isVehicleInManeuverMode()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);
		
		if (m_vehicleState == DUNE::IMC::VehicleState::VS_MANEUVER)
		{
			return true;
		}
		return false;
	}

	bool isCmdAcknowledged()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_cmdAcknowledged;
	}

	bool 
		hasNewHeadingRef()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_newHeadingRef;
	}

	bool
		hasNewDepthRef()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_newDepthRef;
	}

	bool
		hasNewSpeedRef()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_newSpeedRef;
	}

	bool 
		hasNewEstimate()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		return m_newEstimate;
	}

	int
		doExit()
	{
		DUNE::Concurrency::ScopedMutex l(m_mutex);

		if (m_exit)
		{
			return 1;
		} 
		else
		{
			return 0;
		}
	}

	void 
		onMain()
	{
		while (!stopping()) 
		{
			waitForMessages(0.1);
		}
	}
};
