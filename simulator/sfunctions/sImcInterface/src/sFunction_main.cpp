/* 
    sFunction_main.cpp

	The sFunction_main module implements an s-function for using c++ with simulink. The function calls the
	subfunctions in sFunction_model.c where the models of the system is implemented.


    Input parameters:

	Parameter 0 -	Simulation parameters: [SampleTime, NumContStates, NumDiscStates, NumInputSignals, NumOutputSignals, NumParameters]
	Parameter 1 -	File name: Configuration data initialization file.
	Parameter 2 -	Xc0: Initial conditions continuous states.
	Parameter 3 -	Xd0: Initial conditions discrete states.


	Input signals (size):
	
	I0 - Mode control (Not implemented functionality yet):
						0: Debug information report flag; See function DebugReport for description of the 
							input values and generated reports.
						1: Controls if jumps or flows are prioritized (0 = flows, 1 = jumps)
	I1 - Control input u



    Output signals (size):
	
	O0 - Continuous states Xc 
	O1 - Discrete states Xd (not in use in the hybrid systems)
	O2 - System outputs y

	Copyright: 		Petter Norgren, Department of Marine Technology, NTNU
    Author: 		Petter Norgren
    Date created: 	2015.10.30  PN Based on the template by Roger Skjetne. Ported code to c++.
    Revised:      	
			
*/

// Define S-function parameters
#ifndef S_FUNCTION_NAME
	#define S_FUNCTION_NAME sImcInterface
#endif

#ifndef S_FUNCTION_LEVEL
	#define S_FUNCTION_LEVEL 2
#endif

#ifndef FILENAME
	#define FILE_NAME "sImcInterface"
#endif

#include "sFunction_model.h"

static sFunctionModel *model;

/*====================*
 * S-function methods *
 *====================*/


#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
    /*	Function: mdlCheckParameters ===============================================
     *	Abstract: Validate our parameters to verify they are okay. 
	 */
    static void mdlCheckParameters(SimStruct *S)
    {
        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumContStates = (int)simPtr[1];
        int_T	NumDiscStates = (int)simPtr[2];

        /* Check parameters: */
        {
            if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
                /* Return if number of expected != number of actual parameters */
                ssSetErrorStatus(S, "The number of parameters to S-function does "
                    "not match the expected number.");
                return;
            }
        }

        /* Check 1st parameter: CtrlParam */
        {
            if (!mxIsNumeric(SimParam_PARAM(S)) ||
                mxGetNumberOfElements(SimParam_PARAM(S)) != 6 ||
                simPtr[0] < -1.0 ||			// SampleTime
                simPtr[1] < 0 ||			// NumContStates
                simPtr[2] < 0 ||			// NumDiscStates
                simPtr[3] <= 0 ||			// NumInputSignals
                simPtr[4] <= 0 ||			// NumOutputSignals
                simPtr[5] <= 0) {			// NumParameters
                ssSetErrorStatus(S, "1st parameter to S-function must be a vector of size 6 with the simulation parameters: "
                    "SampleTime	    Ts = -1 (variable) or Ts > 0.0 (fixed), "
                    "NumContStates    >= 0, "
                    "NumDiscStates    >= 0, "
                    "NumInputSignals  >= 1, "
                    "NumOutputSignals >= 1, and "
                    "NumParameters    >= 1. ");
                return;
            }
        }
        /* Check 2nd parameter: Filename to config file. */
        {
            if (!mxIsChar(InitFname_PARAM(S))) {
                ssSetErrorStatus(S, "2nd parameter to S-function must be a string "
                    "specifying the filename with path to the file "
                    "giving the initialization data.");
                return;
            }
        }
        /* Check 3rd parameter: Xc0 - Initial conditions continuous states */
        {
            if (!mxIsNumeric(Xc0_PARAM(S)) ||
                (int_T)mxGetNumberOfElements(Xc0_PARAM(S)) != NumContStates) {
                ssSetErrorStatus(S, "3rd parameter to S-function, Xc0, must be a vector of initial "
                    "conditions for the continuous states of the dynamic system.");
                return;
            }
        }
        /* Check 4th parameter: Xc0 - Initial conditions discrete states */
        {
            if (!mxIsNumeric(Xd0_PARAM(S)) ||
                (int_T)mxGetNumberOfElements(Xd0_PARAM(S)) != NumDiscStates) {
                ssSetErrorStatus(S, "4th parameter to S-function, Xd0, must be a vector of initial "
                    "conditions for the discrete states of the dynamic system.");
                return;
            }
        }
    }
#endif /* MDL_CHECK_PARAMETERS */

/*	Function: mdlInitializeSizes ===============================================
*	Abstract:
*   The sizes information is used by Simulink to determine the S-function
*   block's characteristics (number of inputs, outputs, states, etc.).
*/
static void mdlInitializeSizes(SimStruct *S)
{

	ssSetNumSFcnParams(S, NP);  /* Number of expected parameters */
	#if defined(MATLAB_MEX_FILE)
			if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
				mdlCheckParameters(S);
				if (ssGetErrorStatus(S) != NULL) {
					return;
				}
			}
			else {
				return; /* Parameter mismatch will be reported by Simulink */
			}
	#endif

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, DYNAMICALLY_SIZED);

    if (!ssSetNumInputPorts(S, NIP)) return;
    ssSetInputPortWidth(S, 0, NU0);
    ssSetInputPortWidth(S, 1, DYNAMICALLY_SIZED);
	ssSetInputPortRequiredContiguous(S, 1, 1);		/* Required for using ssGetInputPortRealSignal(S, port) */

    ssSetInputPortDirectFeedThrough(S, 0, 1);
    ssSetInputPortDirectFeedThrough(S, 1, 1);

    if (!ssSetNumOutputPorts(S, NOP)) return;
    ssSetOutputPortWidth(S, 0, DYNAMICALLY_SIZED);

    ssSetNumSampleTimes(S, 1);

    ssSetNumRWork(S, DYNAMICALLY_SIZED);
    ssSetNumIWork(S, DYNAMICALLY_SIZED);
    ssSetNumPWork(S, DYNAMICALLY_SIZED);

    ssSetNumModes(S, DYNAMICALLY_SIZED);
    ssSetNumNonsampledZCs(S, DYNAMICALLY_SIZED);

    /* Take care when specifying exception free code - see sfuntmpl.doc */
    //ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

#if defined(MATLAB_MEX_FILE)
#define MDL_SET_DEFAULT_PORT_DIMENSION_INFO
    void mdlSetDefaultPortDimensionInfo(SimStruct *S)
    {
        double	*simPtr = mxGetPr(SimParam_PARAM(S));
        int		NumContStates = (int)simPtr[1];
        int		NumDiscStates = (int)simPtr[2];
        int		NumInputSignals = (int)simPtr[3];
        int		NumOutputSignals = (int)simPtr[4];

        if (ssGetInputPortWidth(S, 0) == DYNAMICALLY_SIZED) {
            ssSetInputPortWidth(S, 0, NU0);
        }
        if (ssGetInputPortWidth(S, 1) == DYNAMICALLY_SIZED) {
            if (NumInputSignals < 1)
                ssSetInputPortWidth(S, 0, 1);
            else
                ssSetInputPortWidth(S, 0, NumInputSignals);
        }
        if (ssGetOutputPortWidth(S, 0) == DYNAMICALLY_SIZED) {
            if (NumOutputSignals < 1)
                ssSetOutputPortWidth(S, 0, 1);
            else
                ssSetOutputPortWidth(S, 0, NumOutputSignals);
        }

				UNREFERENCED_PARAMETER(NumContStates);
				UNREFERENCED_PARAMETER(NumDiscStates);
    }

# define MDL_SET_INPUT_PORT_WIDTH
    static void mdlSetInputPortWidth(SimStruct *S, int_T port, int_T inputPortWidth)
    {
        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumInputSignals = (int)simPtr[3];


        if (port == 1) {
            if ((inputPortWidth != NumInputSignals) && (inputPortWidth != 1)) {
				char_T msg[MAX_LINE_SIZE];
				sprintf_s(msg, MAX_LINE_SIZE, "sDvlMbeSimulator_main: Width for input port 1 (Control input u) should be equal to 1 or "
					"the value given by parameter 0 (SimParam), element 3, i.e. NumInputSignals = %d\n", NumInputSignals);
                ssSetErrorStatus(S, msg);
                return;
            }
            else {
                if (NumInputSignals < 1)
                    ssSetInputPortWidth(S, port, 1);				/* Control input signals	*/
                else
                    ssSetInputPortWidth(S, port, inputPortWidth);	/* Control input signals	*/
            }
        }
        else {
            ssSetErrorStatus(S, "sDvlMbeSimulator_main: mdlSetInputPortWidth called with erroneous port number.");
            return;
        }
    }

# define MDL_SET_OUTPUT_PORT_WIDTH
    static void mdlSetOutputPortWidth(SimStruct *S, int_T port, int_T outputPortWidth)
    {
        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumOutputSignals = (int)simPtr[4];

        if ((port == 0) && ((outputPortWidth != NumOutputSignals))) {
			char_T msg[MAX_LINE_SIZE];
			sprintf_s(msg, MAX_LINE_SIZE, "sDvlMbeSimulator_main: Width for output port 0 (output y) should be equal to 1 or "
				"the value given by parameter 5 (SimParam), element 4, i.e. NumOutputSignals = %d\n", NumOutputSignals);
			ssSetErrorStatus(S, msg);
            return;
        }

        ssSetOutputPortWidth(S, port, outputPortWidth);

    }

# define MDL_SET_WORK_WIDTHS
    static void mdlSetWorkWidths(SimStruct *S)
    {
        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumContStates = (int)simPtr[1];
        int_T	NumDiscStates = (int)simPtr[2];

        if (NumContStates < 1)
            ssSetNumContStates(S, 0);
        else
            ssSetNumContStates(S, NumContStates);

        if (NumDiscStates < 1)
            ssSetNumDiscStates(S, 0);
        else
            ssSetNumDiscStates(S, NumDiscStates);


        ssSetNumRWork(S, 0);

        // I-work vectors
        //0 FAIL_FLAG
        ssSetNumIWork(S, 1);

        // P-work vectors
        //0 MsgList
        //1 tick
        //2 System
        ssSetNumPWork(S, 3);

        ssSetNumModes(S, 0);
        ssSetNumNonsampledZCs(S, 0);
    }
#endif

/*	Function: mdlInitializeSampleTimes =========================================
 *	Abstract: S-function is comprised of only continuous sample time elements 
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
    real_T	Ts = simPtr[0];

    if (Ts <= 0.0) {
        ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
        ssSetOffsetTime(S, 0, 0.0);
    }
    else {
        ssSetSampleTime(S, 0, Ts);
        ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_INITIALIZE_CONDITIONS /* Change to #undef to remove function */
#if defined(MDL_INITIALIZE_CONDITIONS) 
    /*	Function: mdlInitializeConditions ========================================
     *	Abstract:
     *  If the initial condition parameter (X0) is not an empty matrix,
     *  then use it to set up the initial conditions, otherwise,
     *  set the intial conditions to all 0.0
     */
    static void mdlInitializeConditions(SimStruct *S)
    {
        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumContStates = (int)simPtr[1];
        int_T	NumDiscStates = (int)simPtr[2];
        int_T	i;

        real_T  *xc0 = ssGetContStates(S);
        real_T	*xd0 = ssGetDiscStates(S);
        int_T   numXc = ssGetNumContStates(S);
        int_T	numXd = ssGetNumDiscStates(S);
        real_T *pr;

        if ((NumContStates <= 0) || (mxGetM(Xc0_PARAM(S)) == 0)) {
            for (i = 0; i < numXc; i++) {
                xc0[i] = 0.0;
            }
        }
        else {
            pr = mxGetPr(Xc0_PARAM(S));
            for (i = 0; i < numXc; i++) {
                xc0[i] = pr[i];
            }
        }

        if ((NumDiscStates <= 0) || (mxGetM(Xd0_PARAM(S)) == 0)) {
            for (i = 0; i < numXd; i++) {
                xd0[i] = 0.0;
            }
        }
        else {
            pr = mxGetPr(Xd0_PARAM(S));
            for (i = 0; i < numXd; i++) {
                xd0[i] = pr[i];
            }
        }
    }
#endif /* MDL_INITIALIZE_CONDITIONS */


#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START) 
    /*	Function: mdlStart =======================================================
     *	Abstract:
     *  This function is called once at start of model execution. If you
     *  have states that should be initialized once, this is the place
     *  to do it.
     */
    static void mdlStart(SimStruct *S)
    {
        int_T	FAIL_FLAG = 1, status;
        real_T	*tick;

        real_T	*simPtr = mxGetPr(SimParam_PARAM(S));
        int_T	NumContStates = (int)simPtr[1];
        int_T	NumDiscStates = (int)simPtr[2];
        int_T	NumInputSignals = (int)simPtr[3];
        int_T	NumOutputSignals = (int)simPtr[4];
        int_T	NumParameters = (int)simPtr[5];

        int_T	nu = (int)mxGetNumberOfElements(InitFname_PARAM(S));
        char_T	filename[MAX_STRING_SIZE], msg[MAX_LINE_SIZE];


        /* Message function initialization */
        const char_T	*sfunc = ssGetModelName(S);
        const char_T	*sfunc_path = ssGetPath(S);
        char			logfile[MAX_MSG_CHAR + 1];
        sMsgList		*MsgList;
        FILE			*fp;

        sSystemCFG		*System;

        // Initializing work pointers to NULL pointer:
        // P-work vectors
        //0 MsgList
        //1 tick
        //2 System
        MsgList = NULL;
        tick = NULL;
        System = NULL;

        MsgList = (sMsgList *)malloc((MAX_MSG + 1)*sizeof(sMsgList));
        if (!MsgList) {
            FAIL_FLAG = -3;		// Memory allocation problems
            ssSetErrorStatus(S, "sDvlMbeSimulator_main-mdlStart: Memory request failed for \'MsgList\' struct in mdlStart.\n");
        }
        else {
            MsgList[0].msg_length = -1;
            ssSetPWorkValue(S, 0, MsgList);

            strcpy_s(logfile, MAX_MSG_CHAR + 1, sfunc);
            strcat_s(logfile, MAX_MSG_CHAR + 1, ".log");
            status = fopen_s(&fp, logfile, "w+");
            if (status != 0) {
                sprintf_s(msg, MAX_LINE_SIZE, "The log file %s could not be opened.", logfile);
                ssSetErrorStatus(S, msg);
            }
            else {
                fprintf(fp, ">>>>>>>>>>>>>>>>>>>>>> LOGFILE <<<<<<<<<<<<<<<<<<<<<<<<\n");
                fprintf(fp, ">>>> %s/%s \n\n", sfunc_path, sfunc);
                fclose(fp);
            }
            /* End of message function initialization */

            mxGetString(InitFname_PARAM(S), filename, nu + 1);  // Getting input filename 
        }

        if (FAIL_FLAG > 0) {
            tick = (double *)malloc(3 * sizeof(double));
            if (!tick) {
                FAIL_FLAG = -3;		// Memory allocation problems
                sprintf_s(msg, MAX_LINE_SIZE, "Memory request failed for tick.");
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
            else {
                tick[0] = ticker(S);
                tick[1] = tick[0];
                tick[2] = tick[0];
            }
        }

        if (FAIL_FLAG > 0) {
            /* Assigning system data to sSystemData struct *System */
            System = (sSystemCFG *)malloc(sizeof(sSystemCFG));
            if (!System) {
                FAIL_FLAG = -3;		// Memory allocation problems
                sprintf_s(msg, MAX_LINE_SIZE, "Memory request failed for \'System\' struct in mdlStart.");
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
                System = NULL;
            }
            else {
                // Reading in configuration data:
                FAIL_FLAG = Param2Struct(S, System, filename);
            }
        }

        if (FAIL_FLAG > 0) {
            if (System->NumContStates != NumContStates) {
                FAIL_FLAG = -1;
                sprintf_s(msg, MAX_LINE_SIZE, "The number of continuous states (Xc) assigned in initialization file is %d and does not match the number as specified by mask parameter 0, element 1, i.e. NumContStates = %d.", System->NumContStates, NumContStates);
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
            else if (System->NumDiscStates != NumDiscStates) {
                FAIL_FLAG = -1;
                sprintf_s(msg, MAX_LINE_SIZE, "The number of discrete states (Xd) assigned in initialization file is %d and does not match the number as specified by mask parameter 0, element 2, i.e. NumDiscStates = %d.", System->NumDiscStates, NumDiscStates);
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
            else if (System->NumInputSignals != NumInputSignals) {
                FAIL_FLAG = -1;
                sprintf_s(msg, MAX_LINE_SIZE, "The number of control input signals (u) assigned in initialization file is %d and does not match the number as specified by mask parameter 0, element 3, i.e. NumInputSignals = %d.", System->NumInputSignals, NumInputSignals);
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
            else if (System->NumOutputSignals != NumOutputSignals) {
                FAIL_FLAG = -1;
                sprintf_s(msg, MAX_LINE_SIZE, "The number of output signals (y) assigned in initialization file is %d and does not match the number as specified by mask parameter 0, element 4, i.e. NumOutputSignals = %d.", System->NumOutputSignals, NumOutputSignals);
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
            else if (System->NumParameters != NumParameters) {
                FAIL_FLAG = -1;
                sprintf_s(msg, MAX_LINE_SIZE, "The number of parameters assigned in initialization file is %d and does not match the number as specified by mask parameter 0, element 5, i.e. NumParameters = %d.", System->NumParameters, NumParameters);
                SendMsg(S, ERROR_LOG, msg, "mdlStart");
            }
        }

        if (FAIL_FLAG > 0) {
            //sprintf_s(msg, MAX_LINE_SIZE, "Successful initialization of S-function in mdlStart.");
            //SendMsg(S, DEBUG_LOG, msg, "mdlStart");
        }
        else if (FAIL_FLAG == 0) {
            sprintf_s(msg, MAX_LINE_SIZE, "No fields were assigned from initialization data file.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        else if (FAIL_FLAG == -1) {
            sprintf_s(msg, MAX_LINE_SIZE, "Error in parameters from initialization file.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        else if (FAIL_FLAG == -2) {
            sprintf_s(msg, MAX_LINE_SIZE, "Cannot open initialization file.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        else if (FAIL_FLAG == -3) {
            sprintf_s(msg, MAX_LINE_SIZE, "Memory allocation problems in S-function.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        else if (FAIL_FLAG == -4) {
            sprintf_s(msg, MAX_LINE_SIZE, "Error in dynamic sizing of input/output ports for S-function.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        else {
            sprintf_s(msg, MAX_LINE_SIZE, "Unknown configuration error of data structures. Check input parameters and initialization file.");
            SendMsg(S, ERROR_LOG, msg, "mdlStart");
        }
        // I-work vectors
        //0 FAIL_FLAG
        ssSetIWorkValue(S, 0, FAIL_FLAG);

        // P-work vectors
        //0 MsgList
        //1 tick
        //2 System
        ssSetPWorkValue(S, 1, tick);
        ssSetPWorkValue(S, 2, System);

		// Initialize the model
		model = new sFunctionModel(S, System);
    }
#endif /*  MDL_START */
	
/* Function: mdlOutputs =======================================================
    */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    int_T								i, InfoFlag = 0;
    int_T								FAIL_FLAG = ssGetIWorkValue(S, 0);
    real_T							*tack, tick[20];
    char_T							msg[15 * MAX_LINE_SIZE], str[MAX_LINE_SIZE];

    sSystemCFG					*System;

    real_T							*Xd = ssGetDiscStates(S);
    real_T							*Xc = ssGetContStates(S);
		int_T								numXc = ssGetNumContStates(S);

    real_T							*y0_Y = ssGetOutputPortRealSignal(S, 0);
    int_T								y0Width = ssGetOutputPortWidth(S, 0);

    InputRealPtrsType 	u0_Control = ssGetInputPortRealSignalPtrs(S, 0);
    int_T								u0Width = ssGetInputPortWidth(S, 0);

	const real_T					*u1_U = ssGetInputPortRealSignal(S, 1);
    int_T								u1Width = ssGetInputPortWidth(S, 1);

    const char_T				*sfunc = FILE_NAME;//ssGetModelName(S);
    const char_T				*sfunc_path = ssGetPath(S);
    sMsgList						*MsgList;

		UNREFERENCED_PARAMETER(tid);
		UNREFERENCED_PARAMETER(numXc);
		UNREFERENCED_PARAMETER(u0Width);
		UNREFERENCED_PARAMETER(u1Width);
		UNREFERENCED_PARAMETER(InfoFlag);

    // Initializing work pointers:
    // P-work vectors
    //0 MsgList
    //1 tick
    //2 System
    MsgList = static_cast<sMsgList*>(ssGetPWorkValue(S, 0));
    System = static_cast<sSystemCFG*>(ssGetPWorkValue(S, 2));

    /* Calculating and setting outputs */
    if (FAIL_FLAG == 1) { // Continuing only if configuration of system structure has been successful 

        if (((int_T)(*u0_Control[0]) > 99)) tick[0] = ticker(S);

		if (ssIsMajorTimeStep(S))
		{
			model->ModelOutputs(S, System, Xc, Xd, u1_U, y0_Y);
		}
        else {
            for (i = 0; i<y0Width; i++)
                y0_Y[i] = 0.0;
        }

        if (((int_T)(*u0_Control[0]) > 99)) tick[2] = ticker(S);

        ///* Debugging reporting. */
        //InfoFlag = (int)(*u0_Control[0]);
        //if((InfoFlag != (int)(x[0])) && ((InfoFlag > 0) && (InfoFlag <= 5))) {
        //	DebugReport(S, System, Location, Pipe, Input, TouchDetect, ProximityDetect, &InfoFlag);
        //}

        // Timing reporting
        if (((int_T)(*u0_Control[0]) > 99)) {
            // P-work vectors
            //1 tick
            tack = static_cast<double*>(ssGetPWorkValue(S, 1));
            tick[19] = ticker(S);

            if (((int)(*u0_Control[0]) == 100)) {
                if ssIsMajorTimeStep(S) {
                    sprintf_s(str, MAX_LINE_SIZE, "mdlOutput - MAJOR timestep: Elapsed CPU time: %8.4f msec. since last. Execution time: %8.4f msec.\n", tick[19] - tack[0], tick[19] - tick[0]);
                    printf("%s", str);
                }
                else {
                    sprintf_s(str, MAX_LINE_SIZE, "mdlOutput - MINOR timestep: Elapsed CPU time: %8.4f msec. since last. Execution time: %8.4f msec.\n", tick[19] - tack[0], tick[19] - tick[0]);
                    printf("%s", str);
                }
            }
            else if (((int_T)(*u0_Control[0]) == 102)) {
                sprintf_s(str, MAX_LINE_SIZE, "mdlOutput: Elapsed CPU time: %8.6f msec. since last. Execution time: %8.6f msec.\n", tick[19] - tack[0], tick[19] - tick[0]);
                printf("%s", str);

                sprintf_s(str, MAX_LINE_SIZE, "   1- 0: %8.6f msec. - Calculating outputs y2\n", tick[1] - tick[0]);	printf("%s", str);
                sprintf_s(str, MAX_LINE_SIZE, "   2- 1: %8.6f msec. - Assigning outputs y0 - y1\n", tick[2] - tick[1]);	printf("%s", str);
                sprintf_s(str, MAX_LINE_SIZE, "  19- 2: %8.6f msec. - Debug reporting; DebugReport()\n", tick[19] - tick[2]);	printf("%s", str);

            }
            tack[0] = tick[19];
        }

        // Decreasing Message quarantine status of all messages in list.
        for (i = 0; i <= MAX_MSG; i++) {
            if (MsgList[i].msg_length < 0) {
                break;
            }
            if (MsgList[i].status > 0) {
                if (MsgList[i].status == 1) {
                    sprintf_s(msg, 15 * MAX_LINE_SIZE, "%d %s/%s", -i - 1, sfunc_path, sfunc);	// Contra message for message going low. Last in mdlOutpouts.
                    printf(msg);
                }
                MsgList[i].status = MsgList[i].status - 1;
            }
        }
    }
    else if (FAIL_FLAG != -3) {
        sprintf_s(str, MAX_LINE_SIZE, "Error in initialization of configuration data (Code %d). Sending fail-safe values to the outputs.", FAIL_FLAG);
        SendMsg(S, INFO_LOG, str, "mdlOutputs");
        /* Assigning outputs y0 - y2 */
        for (i = 0; i < y0Width; i++)
            y0_Y[i] = 0.0;
    }
    else {
        sprintf_s(str, MAX_LINE_SIZE, "sDvlMbeSimulator-mdlOutputs: Error in execution of S-function (Code %d). Sending fail-safe values to the outputs.", FAIL_FLAG);
        ssSetErrorStatus(S, str);
        /* Assigning outputs y0 - y2 */
        for (i = 0; i < y0Width; i++)
            y0_Y[i] = 0.0;
    }
}

#define MDL_DERIVATIVES /* Change to #undef to remove function */
#if defined(MDL_DERIVATIVES)
    /* Function: mdlDerivatives =================================================
        */
    static void mdlDerivatives(SimStruct *S)
    {
        int_T				i, FAIL_FLAG = ssGetIWorkValue(S, 0);
        sSystemCFG			*System;

        real_T				*Xd = ssGetDiscStates(S);
        real_T				*Xc = ssGetContStates(S);
        real_T				*Xc_dot = ssGetdX(S);
        int_T				numXc = ssGetNumContStates(S);

				InputRealPtrsType 	u0_U = ssGetInputPortRealSignalPtrs(S, 0);
				int_T				u0Width = ssGetInputPortWidth(S, 0);

				const real_T		*u1_U = ssGetInputPortRealSignal(S, 1);
        int_T				u1Width = ssGetInputPortWidth(S, 1);

				UNREFERENCED_PARAMETER(u0Width);
				UNREFERENCED_PARAMETER(u1Width);
				UNREFERENCED_PARAMETER(u0_U);

        // Initializing work pointers:
        // P-work vectors
        //0 MsgList
        //1 tick
        //2 System
        System = static_cast<sSystemCFG *>(ssGetPWorkValue(S, 2));

        /* Calculating and setting derivatives */
        if ((FAIL_FLAG == 1) && (System->NumContStates > 0)) {
            model->ModelXcDerivatives(S, System, Xc_dot, Xc, Xd, u1_U);
        }
        else {
            for (i = 0; i < numXc; i++)
                Xc_dot[i] = 0.0;
        }
    }
#endif /* MDL_DERIVATIVES */

#define MDL_UPDATE /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
    /* Function: mdlUpdate ======================================================
        * Abstract:
        *      xdot = Ax + Bu
        */
    static void mdlUpdate(SimStruct *S, int_T)
    {
        int_T				FAIL_FLAG = ssGetIWorkValue(S, 0);
        sSystemCFG			*System;

        real_T				*Xd = ssGetDiscStates(S);
        real_T				*Xc = ssGetContStates(S);
        int_T				numXd = ssGetNumDiscStates(S);

				const real_T		*u1_U = ssGetInputPortRealSignal(S, 1);
        int_T				u1Width = ssGetInputPortWidth(S, 1);

				UNREFERENCED_PARAMETER(numXd);
				UNREFERENCED_PARAMETER(u1Width);

        // Initializing work pointers:
        // P-work vectors
        //0 MsgList
        //1 tick
        //2 System
        System = static_cast<sSystemCFG *>(ssGetPWorkValue(S, 2));

        /* Calculating and setting derivatives */
        if ((FAIL_FLAG == 1) && (System->NumDiscStates > 0)) {
            model->ModelXdUpdate(S, System, Xc, Xd, u1_U);
        }
        else {
            ;	// No update
        }
    }
#endif /* MDL_UPDATE */

/*	Function: mdlTerminate =====================================================
 *	Abstract:
 *  We are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
    int_T			i, num;
    void**			ptr;
    sSystemCFG		*System;

		// Terminate the model
		delete(model);

    System = static_cast<sSystemCFG *>(ssGetPWorkValue(S, 2));

    num = ssGetNumPWork(S);
    ptr = ssGetPWork(S);
    for (i = 0; i < num; i++) {
        free(ptr[i]);
    }
}

#ifdef  MATLAB_MEX_FILE					/* Is this file being compiled as a MEX-file? */
	#pragma warning ( push )
	#pragma warning ( disable : 4457 )

	extern "C" {
		#include "simulink.c"			/* MEX-file interface mechanism */
	}
	#pragma warning ( pop )
#else
	extern "C"{
		#include "cg_sfun.h"			/* Code generation registration function */
	}
#endif
