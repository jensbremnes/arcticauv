/* 
	Source file for functions defining the s-function.

	References:		S-function template created by Roger Skjetne.

	Copyright: 			Department of Marine Technology, NTNU
  Author: 				Petter Norgren
  Date created: 	21.01.2016
  Revised:    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#####################################  MODEL FUNCTIONS	 ##############################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Parameters:
SystemStruct->Param[i].tag			i, j zero - indexed.
SystemStruct->Param[i].size
SystemStruct->Param[i].pvec[j], The row array 'pvec[]' stores a matrix row - by - row.

Inputs :
		u[0], u[1], u[2], etc.

Continuous states :
		xc[0], xc[1], xc[2], etc.

Discrete states :
		xd[0], xd[1], xd[2], etc.

Objective of function : To assign the outputs
		y[0], y[1], y[2], etc.

*/

#include "sFunction_model.h"

#include <chrono>
#include <thread>

#pragma warning(push,3)
#include <DUNE/Coordinates/WGS84.hpp>
#pragma warning(pop)

sFunctionModel::sFunctionModel(SimStruct*, sSystemCFG* System) : 
	m_sender(NULL),
	m_sock(NULL),
	m_receiver(NULL),
	m_listener(NULL),
	m_context(NULL),
	m_simExit(0),
	m_lat_ref(0),
	m_lon_ref(0),
	m_height_ref(0)
{
	const int c_ipAddr_idx = 0;
	const int c_dunePort_idx = 1;
	const int c_matlabPort_idx = 2;
	const int c_depthControl_idx = 3;
	const int c_speedControl_idx = 4;
	const int c_referencePosition_idx = 5;
	const int c_startManeuverPosition_idx = 6;
	const int c_waypointList_idx = 7;
	const int c_initialConditions_idx = 8;
	const int c_debug_idx = 9;
	const int c_maneuverSelect_idx = 10;

	// Get input parameters
	std::string ipAddrDune = System->Param[c_ipAddr_idx].tag;
	uint16_t senderPort = (uint16_t)System->Param[c_dunePort_idx].pvec[0];			// DUNE input port
	uint16_t receivePort = (uint16_t)System->Param[c_matlabPort_idx].pvec[0];		// AUVsim input
	m_trace = System->Param[c_debug_idx].pvec[0] != 0;
	m_debug = System->Param[c_debug_idx].pvec[1] != 0;

	// Construct context for DUNE task
	m_context = new DUNE::Tasks::Context();

	// Create IMC sender object
	m_sender = new ImcSender(ipAddrDune, senderPort, false);
	m_sender->heartBeat();

	try
	{
		DUNE::Network::Address address(ipAddrDune.c_str());
		m_sock = new DUNE::Network::UDPSocket();
		m_sock->bind(receivePort, address, true);
	}
	catch (std::runtime_error& e)
	{
		ssPrintf("ERROR: sImcInterface::sFunctionModel() - Failed to bind to port %u: %s", receivePort, e.what());
		m_simExit = 1;
		return;
	}

	// Start IMC listener task
	m_listener = new ImcListener(*m_context, *m_sock, m_trace, m_debug);
	m_listener->start();

	// Start IMC receiver task
	m_receiver = new ImcReceiver(*m_context, *m_sender, m_trace, m_debug);
	m_receiver->start();

	m_lat_ref = System->Param[c_referencePosition_idx].pvec[0];
	m_lon_ref = System->Param[c_referencePosition_idx].pvec[1];
	m_height_ref = System->Param[c_referencePosition_idx].pvec[1];

	DUNE::IMC::GpsFix gps;
	gps.clear();
	gps.satellites = 8;
	gps.type = DUNE::IMC::GpsFix::GFT_MANUAL_INPUT;
	gps.validity = 0xffff;
	gps.lat = (fp64_t)m_lat_ref;
	gps.lon = (fp64_t)m_lon_ref;
	gps.height = (fp32_t)m_height_ref;

	m_sender->send(&gps);

	DUNE::IMC::SimulatedState initialState;

	// Send InitialState message to start Dune
	initialState.lat = static_cast<fp64_t>(m_lat_ref);
	initialState.lon = static_cast<fp64_t>(m_lon_ref);
	initialState.height = static_cast<fp32_t>(m_height_ref);
	initialState.x = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[0]);
	initialState.y = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[1]);
	initialState.z = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[2]);
	initialState.phi = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[3]);
	initialState.theta = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[4]);
	initialState.psi = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[5]);
	initialState.u = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[6]);
	initialState.v = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[7]);
	initialState.w = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[8]);
	initialState.p = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[9]);
	initialState.q = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[10]);
	initialState.r = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[11]);
	initialState.svx = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[12]);
	initialState.svx = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[13]);
	initialState.svx = static_cast<fp32_t>(System->Param[c_initialConditions_idx].pvec[14]);
	initialState.setTimeStamp(0);

	// Wait for GPS fix from DUNE
	int wait = 1000;
	int timeout = (int)(10*(wait/1000));
	for (int i = 1; i < timeout; i++)
	{
		m_sender->send(&initialState);
		std::this_thread::sleep_for(std::chrono::milliseconds(wait));
		m_sender->heartBeat();

		if (m_receiver->hasGpsFix())
		{
			if (m_trace)
			{
				ssPrintf("TRACE: sImcInterface::sFunctionModel() GpsFix received.\n");
			}
			break;
		}

		if (i == timeout - 1)
		{
			ssPrintf("ERROR: sImcInterface::sFunctionModel() No GPS fix received from Dune. Exiting simulation.\n");
			m_simExit = 1;
			return;
		}

		if (m_trace)
		{
			ssPrintf("TRACE: sImcInterface::sFunctionModel() Waiting for GPS fix from Dune...\n");
		}
	}

	m_receiver->getGpsOrigin(m_lat_ref, m_lon_ref, m_height_ref);

	if (isnan(m_lat_ref) || isnan(m_lon_ref) || isnan(m_height_ref))
	{
		ssPrintf("ERROR: sImcInterface::sFunctionModel() NaN in GPS origin.\n");
		m_simExit = 1;
	}

	// Set up command message
	DUNE::IMC::VehicleCommand startCmd;
	startCmd.type = DUNE::IMC::VehicleCommand::VC_REQUEST;
	startCmd.command = DUNE::IMC::VehicleCommand::VC_EXEC_MANEUVER;
	startCmd.request_id = 608;		// Random number
	startCmd.calib_time = 1;			// [s]
	startCmd.info = "AuvSim start command";

	uint8_t z_units, speed_units;
	fp64_t start_north, start_east, start_lat, start_lon;
	fp32_t z_value, speed_value;
	
	z_value = (fp32_t)System->Param[c_depthControl_idx].pvec[0];
	speed_value = (fp32_t)System->Param[c_speedControl_idx].pvec[0];
	start_north = (fp64_t)System->Param[c_startManeuverPosition_idx].pvec[0];
	start_east = (fp64_t)System->Param[c_startManeuverPosition_idx].pvec[1];

	// Set to start reference lat/lon and displace to desired goto
	start_lat = static_cast<fp64_t>(m_lat_ref);
	start_lon = static_cast<fp64_t>(m_lon_ref);
	DUNE::Coordinates::WGS84::displace(start_north, start_east, &start_lat, &start_lon);

	// Find z control type
	if (strcmp(System->Param[c_depthControl_idx].tag, "altitude") == 0)
		z_units = DUNE::IMC::Z_ALTITUDE;
	else
		z_units = DUNE::IMC::Z_DEPTH;

	// Find speed control type
	if (strcmp(System->Param[c_speedControl_idx].tag, "percent") == 0)
		speed_units = DUNE::IMC::SUNITS_PERCENTAGE;
	else if (strcmp(System->Param[c_speedControl_idx].tag, "rpm") == 0)
		speed_units = DUNE::IMC::SUNITS_RPM;
	else
		speed_units = DUNE::IMC::SUNITS_METERS_PS;

	DUNE::IMC::IdleManeuver idle;
	DUNE::IMC::Goto go;
	DUNE::IMC::FollowPath path;

	startupManeouver startup(System->Param[c_maneuverSelect_idx].tag);

	switch (startup.maneuver)
	{
	case E_IDLE:

		idle.duration = 0;						// Idle indefinitely 

		startCmd.maneuver.set(idle);

		break;
	case E_GOTO:

		go.timeout = 0;								// Run indefinitely 
		go.lat = start_lat;
		go.lon = start_lon;
		go.pitch = -1;								// Not considered
		go.roll = -1;									// Not considered
		go.yaw = -1;									// Not considered
		go.z = z_value;
		go.z_units = z_units;
		go.speed = speed_value;
		go.speed_units = speed_units;

		startCmd.maneuver.set(go);

		break;

	case E_FOLLOWPATH:

		path.timeout = 0;								// Run indefinitely 
		path.lat = m_lat_ref;
		path.lon = m_lon_ref;
		path.z = z_value;
		path.z_units = z_units;
		path.speed = speed_value;
		path.speed_units = speed_units;

		for (int i = 0; i < 32; i++)
		{
			if (isnan(System->Param[c_waypointList_idx].pvec[i * 2]))
				break;

			DUNE::IMC::PathPoint p;
			p.x = (fp32_t)System->Param[c_waypointList_idx].pvec[i * 2]; // +(fp32_t)start_north;
			p.y = (fp32_t)System->Param[c_waypointList_idx].pvec[i * 2 + 1]; // +(fp32_t)start_east;
			p.z = 0;

			path.points.push_back(&p);
		}

		startCmd.maneuver.set(path);

		break;
	}

	// Wait for vehicle to enter maneuver mode
	for (int i = 1; i < timeout; i++)
	{
		m_sender->send(&startCmd);
		std::this_thread::sleep_for(std::chrono::milliseconds(wait));
		m_sender->heartBeat();

		if (m_receiver->isVehicleInManeuverMode() && m_receiver->isCmdAcknowledged())
		{
			if (m_trace)
			{
				ssPrintf("TRACE: sImcInterface::sFunctionModel() Vehicle is in maneuver mode.\n");
			}

			break;
		}

		if (i == timeout - 1)
		{
			ssPrintf("ERROR: sImcInterface::sFunctionModel() No command acknowledge received from Dune. Exiting simulation.\n");
			m_simExit = 1;
			return;
		}

		if (m_trace)
		{
			ssPrintf("TRACE: sImcInterface::sFunctionModel() Waiting for command acknowledge from Dune...\n");
		}
	}
}

sFunctionModel::~sFunctionModel()
{
	if (m_sender != NULL)
	{
		delete m_sender;
		m_sender = NULL;
	}

	if (m_receiver != NULL)
	{
		delete m_receiver;
		m_receiver = NULL;
	}

	if (m_listener != NULL)
	{
		delete m_listener;
		m_listener = NULL;
	}

	if (m_sock != NULL)
	{
		delete m_sock;
		m_sock = NULL;
	}

	if (m_context != NULL)
	{
		delete m_context;
		m_context = NULL;
	}
}

// Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelOutputs(SimStruct* S, sSystemCFG* SystemStruct, real_T*, real_T*, const real_T* u, real_T* y)
{
	DUNE::IMC::SimulatedState simState;
	int noEstimateCount;

	m_sender->heartBeat();

	memset(y, 0, SystemStruct->NumOutputSignals*sizeof(real_T));

	for (int i = 1; i < SystemStruct->NumOutputSignals; i++)
	{
		if (isnan(u[i]))
		{
			ssPrintf("ERROR: NaN in ImcInterface inputs.\n");
			m_simExit = 1;
		}
	}

	// Fill in SimulatedState message
	simState.lat = static_cast<fp64_t>(m_lat_ref);
	simState.lon = static_cast<fp64_t>(m_lon_ref);
	simState.height = static_cast<fp32_t>(m_height_ref);
	simState.x = static_cast<fp32_t>(u[0]);
	simState.y = static_cast<fp32_t>(u[1]);
	simState.z = static_cast<fp32_t>(u[2]);
	simState.phi = static_cast<fp32_t>(u[3]);
	simState.theta = static_cast<fp32_t>(u[4]);
	simState.psi = static_cast<fp32_t>(u[5]);
	simState.u = static_cast<fp32_t>(u[6]);
	simState.v = static_cast<fp32_t>(u[7]);
	simState.w = static_cast<fp32_t>(u[8]);
	simState.p = static_cast<fp32_t>(u[9]);
	simState.q = static_cast<fp32_t>(u[10]);
	simState.r = static_cast<fp32_t>(u[11]);
	simState.svx = static_cast<fp32_t>(u[12]);
	simState.svy = static_cast<fp32_t>(u[13]);
	simState.svz = static_cast<fp32_t>(u[14]);
	simState.setTimeStamp(ssGetT(S));

	// Send IMC message over UDP
	m_sender->send(&simState);

	// Set output equal to the last received message
	if (m_receiver != NULL)
	{
		// Check if DUNE have computed new control commands
		noEstimateCount = 0;
		while (!m_receiver->hasNewEstimate() && noEstimateCount < 100 && !m_simExit)
		{
			noEstimateCount++;
			std::this_thread::sleep_for(std::chrono::milliseconds(1));

			if (noEstimateCount >= 100)
			{
				ssPrintf("WARNING: No new estimated state received from DUNE. Maybe reduce speedup?\n");
				break;
			}
		}

		// Set output equal to received commands
		m_receiver->getDesiredHeading(y[4], y[0]);
		m_receiver->getDesiredDepth(y[5], y[1]);
		m_receiver->getDesiredSpeed(y[6], y[2]);
		m_receiver->getEstimatedState(&y[7], y[3]);
		 
		y[SystemStruct->NumOutputSignals - 1] = (m_simExit | m_receiver->doExit());
	}
}

// Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelXcDerivatives(SimStruct*, sSystemCFG*, real_T*, real_T*, real_T*, const real_T*)
{
	;
}

// Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
void sFunctionModel::ModelXdUpdate(SimStruct*, sSystemCFG*, real_T*, real_T*, const real_T*)
{
	;
}
