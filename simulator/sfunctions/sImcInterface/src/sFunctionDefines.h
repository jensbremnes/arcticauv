/*
	Header file for global defines

	Copyright: 			Department of Marine Technology, NTNU
	Author: 				Petter Norgren
	Date created: 	09.12.2015
	Revised:
*/

#pragma once

#ifndef MATLAB_MEX_FILE
	#define MATLAB_MEX_FILE
#endif

#include "simstruc.h"

#define inf mexPrintf
#define war mexPrintf
#define err mexPrintf
#define debug mexPrintf
#define trace mexPrintf
