function [Param, Fname, Xc0, Xd0] = sImcInterfaceParam(Ts, conf)
%
%   [Param, Fname, Xc0, Xd0] = sImcInterfaceParam(Ts, nMbeBeams, conf)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2015.11.23   
%    Revisions:
%

    %% Dynamical system configuration File
    % Project description
    System.Project          = 'AuvSim';
    System.Model            = 'IMC Interface';

    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= 0;
    System.NumInputSignals	= 15;
    System.NumOutputSignals	= 20;    
    System.NumParameters	= 11;
    System.ConfigFile       = 'sImcInterfaceParam';

    %% Model parameters  
    System.Param{1}.tag = conf.duneIpAddr;  % DUNE ip-address
    System.Param{1}.values = 0;
    
    System.Param{2}.tag = 'DUNE input port';
    System.Param{2}.values = conf.duneBridePort;
    
    System.Param{3}.tag = 'AuvSim input port';
    System.Param{3}.values = conf.udpReceivePort;
    
    System.Param{4}.tag = conf.depthcontrol.tag;
    System.Param{4}.values = conf.depthcontrol.value;
    
    System.Param{5}.tag = conf.speedcontrol.tag;
    System.Param{5}.values = conf.speedcontrol.value;
    
    System.Param{6}.tag = 'Reference position';
    System.Param{6}.values = conf.referencePosition;
    
    System.Param{7}.tag = 'Start position';
    System.Param{7}.values = conf.startGoto;
    
    System.Param{8}.tag = 'Waypoint list';
    System.Param{8}.values = conf.wplist;
                                  
    System.Param{9}.tag = 'Initial state';
    System.Param{9}.values = [conf.x0; 0; 0; 0];
    
    System.Param{10}.tag = 'Trace on, Debug on';
    System.Param{10}.values = [conf.imcTraceOn; conf.imcDebugOn];
    
    System.Param{11}.tag = conf.duneStartupManeuver; % Startup maneuver
    System.Param{11}.values = 0;

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = []';
end
