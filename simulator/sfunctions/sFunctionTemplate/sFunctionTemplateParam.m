function [Param, Fname, Xc0, Xd0] = sFunctionTemplateParam(Ts)
%
%   [Param, Fname, Xc0, Xd0] = sFunctionTemplateParam(Ts, nMbeBeams, mbeSectorRadians, dvlBeamAngleRadians, isMbeUpLooking, bathymetryFileName, icebergFileName)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB
%    Author:        Petter Norgren
%    Date created:  2014.10.26
%      

    %% Dynamical system configuration File
    % Project description 
    System.Project          = 'Template';
    System.Model            = 'Test';
    
    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= 0;
    System.NumInputSignals	= 1;
    System.NumOutputSignals	= 1;
    System.NumParameters	= 1;
    System.ConfigFile       = 'sFunctionTemplateParam';

    %% Model parameters
    System.Param{1}.tag  	= 'Test param';
    System.Param{1}.values	= 10;

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = []';   
end