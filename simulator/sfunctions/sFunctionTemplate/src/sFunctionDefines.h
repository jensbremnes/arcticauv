/*
sFunctionDefines.hpp

Header file for global defines

Copyright: 		Petter Norgren, Department of Marine Technology, NTNU
Author: 			Petter Norgren

*/

#pragma once

// Define compilation type
#ifndef MATLAB_MEX_FILE
	#define MATLAB_MEX_FILE
#endif

// Define S-function parameters
#ifndef S_FUNCTION_NAME
	#define S_FUNCTION_NAME sFunctionTemplate
#endif

#ifndef S_FUNCTION_LEVEL
	#define S_FUNCTION_LEVEL 2
#endif

#ifndef FILE_NAME
	#define FILE_NAME "sFunction_main"
#endif

// For printing to matlab command window
#include "simstruc.h"
