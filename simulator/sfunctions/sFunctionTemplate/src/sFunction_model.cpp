/* 
	Source file for functions defining the s-function.

	References:		S-function template created by Roger Skjetne.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#####################################  MODEL FUNCTIONS	 ##############################################
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Parameters:
SystemStruct->Param[i].tag			i, j zero - indexed.
SystemStruct->Param[i].size
SystemStruct->Param[i].pvec[j], The row array 'pvec[]' stores a matrix row - by - row.

Inputs :
		u[0], u[1], u[2], etc.

Continuous states :
		xc[0], xc[1], xc[2], etc.

Discrete states :
		xd[0], xd[1], xd[2], etc.

Objective of function : To assign the outputs
		y[0], y[1], y[2], etc.

		Copyright: 			Department of Marine Technology, NTNU
		Author: 				Petter Norgren
*/

#include "sFunction_model.h"

sFunctionModel::sFunctionModel(SimStruct *S, sSystemCFG *System)
{
	UNREFERENCED_PARAMETER(S); // Remove if used
	UNREFERENCED_PARAMETER(System); // Remove if used
	// Perform initialization
	;
}

sFunctionModel::~sFunctionModel()
{
	// Perform clean-up
	;
}

// Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u, real_T *y)
{
	UNREFERENCED_PARAMETER(S); // Remove if used
	UNREFERENCED_PARAMETER(xc); // Remove if used
	UNREFERENCED_PARAMETER(xd); // Remove if used
	UNREFERENCED_PARAMETER(u); // Remove if used
	// Set outputs (y) to desired values
	memset(y, 0, SystemStruct->NumOutputSignals*sizeof(real_T));
}

// Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
void sFunctionModel::ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc_dot, real_T *xc, real_T *xd, const real_T *u)
{
	UNREFERENCED_PARAMETER(S); // Remove if used
	UNREFERENCED_PARAMETER(SystemStruct); // Remove if used
	UNREFERENCED_PARAMETER(xc_dot); // Remove if used
	UNREFERENCED_PARAMETER(xc); // Remove if used
	UNREFERENCED_PARAMETER(xd); // Remove if used
	UNREFERENCED_PARAMETER(u); // Remove if used
	// Calculate continuous time derivatives
	;
}

// Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
void sFunctionModel::ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, real_T *xc, real_T *xd, const real_T *u)
{
	UNREFERENCED_PARAMETER(S); // Remove if used
	UNREFERENCED_PARAMETER(SystemStruct); // Remove if used
	UNREFERENCED_PARAMETER(xc); // Remove if used
	UNREFERENCED_PARAMETER(xd); // Remove if used
	UNREFERENCED_PARAMETER(u); // Remove if used
	// Calculate discrete time derivatives
	;
}
