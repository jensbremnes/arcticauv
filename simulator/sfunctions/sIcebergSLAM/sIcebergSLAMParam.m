function [Param, Fname, Xc0, Xd0] = sIcebergSLAMParam(Ts, conf)
%
%   [Param, Fname, Xc0, Xd0] = sIcebergSLAMParam(Ts, conf)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%      

    %% Dynamical system configuration File
    % Project description 
    System.Project          = 'AUV simulator';
    System.Model            = 'Iceberg SLAM';
    
    %% System parameters
    System.SampleTime       = Ts;
    System.NumContStates	= 0;
    System.NumDiscStates	= conf.iceslam.nParticles*3;        % nParticles * 3 states
    System.NumInputSignals	= 12 + 3 + 4 + 5*conf.mbe.nBeams + 2;     % 12 AUV states + 3 AUV states dot + 4 current meas + 5xN MBE beams + measurement indicator + time
    System.NumOutputSignals	= 19;                               % 3 relative pose + 12 ekf state + 3 slam state + 1 entropy
    System.ConfigFile       = 'sIcebergSLAMParam';

    %% Model parameters
    System.Param{1}.tag  	= 'Number of MBE beams';
    System.Param{1}.values	= conf.mbe.nBeams;
    
    System.Param{2}.tag  	= 'MBE sector size in radians';
    System.Param{2}.values	= conf.mbe.sectorRadians;
    
    System.Param{3}.tag  	= 'MBE lever arm';
    System.Param{3}.values	= conf.mbe.leverArm;
    
    System.Param{4}.tag  	= 'is MBE uplooking';
    System.Param{4}.values	= conf.mbe.isUpLooking;
    
    System.Param{5}.tag  	= 'Variance of MBE sensor';         % [range, acrosstrack, alongtrack]
    System.Param{5}.values	= conf.iceslam.mbeSigma;
    
    System.Param{6}.tag  	= 'Variance of ice model';          % [U, V, Psi]
    System.Param{6}.values	= [conf.iceslam.iceSigma, conf.iceslam.diableRotationEstimate]; 
    
    System.Param{7}.tag  	= 'Time constants of ice model';
    System.Param{7}.values	= conf.iceslam.timeconstants;
    
    System.Param{8}.tag  	= 'Particle filter white noise enable';
    System.Param{8}.values	= conf.iceslam.pfNoiseEnable;
    
    System.Param{9}.tag  	= 'Number of particles in PF';
    System.Param{9}.values	= conf.iceslam.nParticles;
    
    System.Param{10}.tag  	= 'Map size in meters';
    System.Param{10}.values	= conf.iceslam.mapSize;
    
    System.Param{11}.tag  	= 'Map resolution in meters';
    System.Param{11}.values	= conf.iceslam.mapResolution;
    
    System.Param{12}.tag  	= 'Part of map in negative axis';
    System.Param{12}.values	= conf.iceslam.mapNegativePart;
    
    System.Param{13}.tag  	= 'Map center of rotation';
    System.Param{13}.values	= conf.iceslam.mapCenter;
    
    System.Param{14}.tag  	= 'Minimum overlap to allow resampling';
    System.Param{14}.values	= conf.iceslam.minOverlapResampling;
    
    System.Param{15}.tag  	= 'Maximum percentage of particles resampled';
    System.Param{15}.values	= conf.iceslam.maxResampled;
    
    System.Param{16}.tag  	= 'Min beam weight';
    System.Param{16}.values	= conf.iceslam.minBeamWeight;
    
    System.Param{17}.tag  	= 'Upper and lower limit on Neff';
    System.Param{17}.values	= [conf.iceslam.NeffDivide, conf.iceslam.NeffMinLimit];
    
    System.Param{18}.tag  	= 'Minimum number of beams covering overlapped area';
    System.Param{18}.values	= conf.iceslam.minOverlapBeams;
    
    System.Param{19}.tag  	= 'Minimum grids in swath and min swaths in patch to use measurement';
    System.Param{19}.values	= [conf.iceslam.minGridsInSwath, conf.iceslam.minSwathsInPatch];
    
    System.Param{20}.tag  	= 'Minimum time limits (resampling and use of added estimates)';
    System.Param{20}.values	= [conf.iceslam.minTimeBetweenResampling, conf.iceslam.minTimeWeight];

    System.Param{21}.tag  	= 'Mode to extract state vector';
    System.Param{21}.values	= conf.iceslam.extractStateVectorMode;
    
    System.Param{22}.tag  	= 'Resampling strategy and weight method';
    System.Param{22}.values	= [conf.iceslam.resamplingStratgy, conf.iceslam.weightingMethod];
    
    System.Param{23}.tag  	= conf.saveDir;
    System.Param{23}.values	= conf.iceslam.saveObservationMapOnExit;
    
    System.Param{24}.tag  	= 'SLAM sample time and auv pos meas sample time';
    System.Param{24}.values	= [conf.iceslam.TsSlamSpin, conf.iceekf.TsPosMeas];
    
    System.Param{25}.tag  	= 'EKF Q-matrix';
    System.Param{25}.values	= conf.iceekf.q;
    
    System.Param{26}.tag  	= 'EKF R-matrix';
    System.Param{26}.values	= conf.iceekf.r;
    
    System.Param{27}.tag  	= 'EKF-ice velocity noise';
    System.Param{27}.values	= conf.iceekf.sigmaIceVel;
    
    System.Param{28}.tag  	= 'ICP parameters';
    System.Param{28}.values	= [conf.icp.maxIcpIterPointFit;
                               conf.icp.maxIcpIterPlaneFit;
                               conf.icp.maxMotionEstIter;
                               conf.icp.nNeighboursNormalEstimation;
                               conf.icp.stopLimitPointFit;
                               conf.icp.stopLimitPlaneFit;
                               conf.icp.fitMethod;
                               conf.icp.mapNeigbourHoodSizeforFit];
    
    System.Param{29}.tag  	= 'Initial ice velocity';
    System.Param{29}.values	= [conf.iceberg.nu0];
    
    System.Param{30}.tag  	= 'Enable use of EKF estimates in SLAM';
    System.Param{30}.values	= [conf.iceslam.useEkfPoseInSLAM; conf.iceslam.useEkfVelocityInSLAM];
    
    System.Param{31}.tag  	= 'Use MBE xyz coordinates';
    System.Param{31}.values	= conf.iceslam.useMbeXyz;
    
    System.NumParameters	= length(System.Param);

    [status, error] = m2ini(System, System.ConfigFile);

    %% Model mask parameters
    Param = [System.SampleTime, System.NumContStates, System.NumDiscStates, ...
                      System.NumInputSignals, System.NumOutputSignals, System.NumParameters]';
    Fname = [System.ConfigFile,'.ini'];
    Xc0   = []';
    Xd0   = zeros(System.NumDiscStates, 1);   
end