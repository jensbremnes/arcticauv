/*
	sIcebergSLAM.cpp

	Source file for functions defining the IcebergSLAM class

	Copyright: 	Department of Marine Technology, NTNU
	Author: 		Petter Norgren
*/

#include "sIcebergSLAM.hpp"

#include <chrono>

#define STATESIZE 12
#define CURRENTSIZE 2

IcebergSLAM::IcebergSLAM(const SLAMParams &param)
	: m_slam(nullptr)
	, m_ekf(nullptr)
	, m_nuIce(param.ice.iceNu0)
	, m_nMbeBeams(param.mbe.nBeams)
	, m_nParticles(param.nParticles)
	, m_spinPeriod(param.timeBetweenSpins)
	, m_posUpdatePeriod(param.timeBetweenPosMeas)
	, m_useEkfPose(param.useEkfPose)
	, m_useEkfVelocity(param.useEkfVelocity)
	, m_disableRotation(param.ice.disableRotation)
{
	m_slam = std::unique_ptr<SLAMimpl>(
		new SLAMimpl(param) );

	m_ekf = std::unique_ptr<RelativePositionEstimator>(
		new RelativePositionEstimator(param.ekf, param.saveDir));
}

IcebergSLAM::~IcebergSLAM()
{
	;
}

void IcebergSLAM::addSLAMMeasurements(Measurement &meas, const uint64 &t)
{
	static double last_t = 0.0;
	double td;

	if (isInitialized())
	{
		if (meas.isNew)
		{
			// Set measurement time delta
			td = (double)t / 1000;
			meas.deltaT =  td - last_t;
			last_t = td;

			m_slam->pushMeasurements(meas);
		}

		m_slam->setTimestampMSec(t);
	}
}

void IcebergSLAM::run(double *y, Measurement meas, const double &t)
{
	static double H = 0.0;
	static Vector3d slamState = Vector3d::Zero();
	Vector3d relPose = Vector3d::Zero();
	Vector12d ekfState = Vector12d::Zero();
	Vector3d yEkf_etaAuv = Vector3d::Zero();
	Vector3d yEkf_nuRel = Vector3d::Zero();
	Vector3d yEkf_etaIce = Vector3d::Zero();
	static double last_step = 0.0;
	static double last_pos = 0.0;
	const double eps = 1e-6;

	if (m_ekf == nullptr || m_slam == nullptr)
	{
		printf("ERROR: IcebergSLAM - EKF, AUV, or SLAM is nullptr.\n");
		return;
	}

	if (!m_ekf->isInitialized() || !m_slam->isInitialized())
	{
		printf("ERROR: IcebergSLAM - EKF or SLAM not initialized.\n");
		return;
	}

	m_ekf->predict(meas.nuDotAuv);

	// Update EKF based on AUV state
	if (t - last_pos >= (m_posUpdatePeriod - eps))
	{
		if (m_posUpdatePeriod >= 30)
			printf("INFO: AUV position update at time = %f.\n", t);

		yEkf_etaAuv << meas.stateAuv(0), meas.stateAuv(1), meas.stateAuv(5);
		m_ekf->update_etaAuv(yEkf_etaAuv);
		last_pos = t;
	}
	else
	{
		m_ekf->update_psiAuv(Vector1d(meas.stateAuv(5)));
	}

	// Update EKF based on relative velocity
	yEkf_nuRel << meas.nuRel_meas(0), meas.nuRel_meas(1), meas.stateAuv(11);
	m_ekf->update_nuRelAuvRate(yEkf_nuRel);

	ekfState = m_ekf->x();

	// Include iceberg velocity in measurement and add to SLAM
	if (m_useEkfVelocity)
	{
		if (m_disableRotation)
			meas.nuIceHat << ekfState.tail<3>().head<2>(), 0.0;
		else
			meas.nuIceHat << ekfState.tail<3>();
	}
	else
	{
		if (m_disableRotation)
			meas.nuIceHat << m_nuIce.head<2>(), 0.0;
		else
			meas.nuIceHat << m_nuIce;
	}

	// Add relative state estimate to SLAM measurement
	if(m_useEkfPose)
		meas.etaAuvHat = am::Rz(ekfState(8))*ekfState.head<3>() + ekfState.segment<3>(6);
	else
		meas.etaAuvHat << meas.stateAuv(0), meas.stateAuv(1), meas.stateAuv(5);

	addSLAMMeasurements(meas, static_cast<uint64>(t * 1000));

	// Run SLAM algorithm
	if (t - last_step >= (m_spinPeriod - eps))
	{
		m_slam->spin();
		last_step = t;

		// Get results
		m_slam->getCurrentSlamState(slamState);
		H = m_slam->getParticleEntropy();

		// Update EKF based on SLAM estimate
		m_ekf->update_etaIce(slamState);
		/*for (int i = 0; i < m_slam->getNumberOfParticles(); ++i)
		{
			double w = 0.0;
			if (m_slam->getPoseAtIndex(yEkf_etaIce, w, (uint16)i))
				m_ekf->update_etaIce(yEkf_etaIce, w);
		}*/
		
		ekfState = m_ekf->x();
	}
	relPose = ekfState.head<3>();
	m_ekf->printCovarianceToFile();

	// Set output
	memcpy_s(&y[0], 3 * sizeof(double), relPose.data(), relPose.size() * sizeof(double));
	memcpy_s(&y[3], 12 * sizeof(double), ekfState.data(), ekfState.size() * sizeof(double));
	memcpy_s(&y[15], 3 * sizeof(double), slamState.data(), slamState.size() * sizeof(double));
	memcpy_s(&y[18], 1 * sizeof(double), &H, 1 * sizeof(double));
}

void IcebergSLAM::getAllParticleStates(double *x) const
{
	m_slam->copyAllStateVectors(x);
}
