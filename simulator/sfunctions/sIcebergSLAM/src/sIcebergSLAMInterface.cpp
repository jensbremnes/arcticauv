/* 
    sIcebergSLAM_model.cpp

	Source file for functions defining the s-function.

	References:		S-function template created by Roger Skjetne.

	Copyright: 		Department of Marine Technology, NTNU
  Author: 			Petter Norgren   
*/

// Define parameter indices
#define NBEAMS_IDX 0
#define MBESECTOR_IDX 1
#define MBELEVERARM_IDX 2
#define MBEDIR_IDX 3
#define MBESIGMA_IDX 4
#define ICESIGMA_IDX 5
#define TIMECONST_IDX 6
#define NOISEENABLE_IDX 7
#define NPARTICLES_IDX 8
#define MAPSIZE_IDX 9
#define MAPRES_IDX 10
#define MAPNEG_IDX 11
#define MAPOFF_IDX 12
#define MINOVERLAP_IDX 13
#define MAXSAMPLED_IDX 14
#define BEAMWEIGHT_IDX 15
#define NEFFLIM_IDX 16
#define MINBEAMOVERLAP_IDX 17
#define MINGRIDSINSWATH_IDX 18
#define MINTIMEDIFFWEIGHT_IDX 19
#define STATEVEC_IDX 20
#define RESAMPLE_IDX 21
#define SAVEFILE_IDX 22
#define SPIN_IDX 23
#define EKFQ_IDX 24
#define EKFR_IDX 25
#define EKFS_IDX 26
#define ICP_PAR 27
#define ICE_VEL 28
#define EKFSLAM_CONTROL 29
#define MBE_XYZ 30

#include "sIcebergSLAM.hpp"
#include "sIcebergSLAMInterface.hpp"

sIcebergSLAMInterface::sIcebergSLAMInterface(SimStruct *, sSystemCFG *System)
	: m_icebergSlam(nullptr)
	, m_param(nullptr)
{
	m_param = std::unique_ptr<SLAMParams>(new SLAMParams());

	// Get parameters
	m_param->mbe.nBeams = (unsigned short)System->Param[NBEAMS_IDX].pvec[0];
	m_param->mbe.sectorRadians = (double)System->Param[MBESECTOR_IDX].pvec[0];
	m_param->mbe.leverArm = { (double)System->Param[MBELEVERARM_IDX].pvec[0],
                           (double)System->Param[MBELEVERARM_IDX].pvec[1], 
                           (double)System->Param[MBELEVERARM_IDX].pvec[2] };
	m_param->mbe.sigma = { (float)System->Param[MBESIGMA_IDX].pvec[0],
                        (float)System->Param[MBESIGMA_IDX].pvec[1], 
                        (float)System->Param[MBESIGMA_IDX].pvec[2] };
	m_param->ice.sigma = { (float)System->Param[ICESIGMA_IDX].pvec[0],
                        (float)System->Param[ICESIGMA_IDX].pvec[1], 
                        (float)System->Param[ICESIGMA_IDX].pvec[2] };
	m_param->ice.disableRotation = (bool)System->Param[ICESIGMA_IDX].pvec[3];
	m_param->ice.timeConstants = { (float)System->Param[TIMECONST_IDX].pvec[0],
                                (float)System->Param[TIMECONST_IDX].pvec[1],
                                (float)System->Param[TIMECONST_IDX].pvec[2] };
	m_param->ice.noiseEnable = (System->Param[NOISEENABLE_IDX].pvec[0] != 0);
	m_param->nParticles = (unsigned short)System->Param[NPARTICLES_IDX].pvec[0];
	m_param->map.mapSize = (unsigned short)System->Param[MAPSIZE_IDX].pvec[0];
	m_param->map.mapResolution = (float)System->Param[MAPRES_IDX].pvec[0];
	m_param->map.negativeMapPart = (short)System->Param[MAPNEG_IDX].pvec[0];
	m_param->map.center << (double)System->Param[MAPOFF_IDX].pvec[0],
                        (double)System->Param[MAPOFF_IDX].pvec[1],
                        (double)System->Param[MAPOFF_IDX].pvec[2];
	m_param->saveDir = System->Param[SAVEFILE_IDX].tag;
	m_param->saveObsMapOnExit = (System->Param[SAVEFILE_IDX].pvec[0] != 0);
	m_param->timeBetweenSpins = (float)System->Param[SPIN_IDX].pvec[0];
	m_param->timeBetweenPosMeas = (float)System->Param[SPIN_IDX].pvec[1];
	m_param->minOverlapResampling = (float)System->Param[MINOVERLAP_IDX].pvec[0];
	m_param->maxResampled = (float)System->Param[MAXSAMPLED_IDX].pvec[0];
	m_param->minBeamWeight = (float)System->Param[BEAMWEIGHT_IDX].pvec[0];
	m_param->NeffDivide = (int)System->Param[NEFFLIM_IDX].pvec[0];
	m_param->NeffMinLimit = (float)System->Param[NEFFLIM_IDX].pvec[1];
	//m_param->minBeamOverlap = (float)System->Param[MINBEAMOVERLAP_IDX].pvec[0];
	m_param->minGridsInSwath = (int)System->Param[MINGRIDSINSWATH_IDX].pvec[0];
	m_param->minSwathsInPatch = (int)System->Param[MINGRIDSINSWATH_IDX].pvec[1];
	m_param->minTimeBetweenResampling = (double)System->Param[MINTIMEDIFFWEIGHT_IDX].pvec[0];
	m_param->minTimeDiffWeight = (int)System->Param[MINTIMEDIFFWEIGHT_IDX].pvec[1];
	m_param->getStateVectorMode = (uint16)System->Param[STATEVEC_IDX].pvec[0];
	m_param->resamplingStrategy = (uint16)System->Param[RESAMPLE_IDX].pvec[0];
	m_param->weightingMethod = (uint16)System->Param[RESAMPLE_IDX].pvec[1];
	m_param->timeStep = (float)System->SampleTime;
	m_param->ekf.ts = (double)System->SampleTime;
	m_param->ekf.q << (double)System->Param[EKFQ_IDX].pvec[0],
					(double)System->Param[EKFQ_IDX].pvec[1],
					(double)System->Param[EKFQ_IDX].pvec[2],
					(double)System->Param[EKFQ_IDX].pvec[3],
					(double)System->Param[EKFQ_IDX].pvec[4],
					(double)System->Param[EKFQ_IDX].pvec[5],
					(double)System->Param[EKFQ_IDX].pvec[6],
					(double)System->Param[EKFQ_IDX].pvec[7],
					(double)System->Param[EKFQ_IDX].pvec[8],
					(double)System->Param[EKFQ_IDX].pvec[9],
					(double)System->Param[EKFQ_IDX].pvec[10],
					(double)System->Param[EKFQ_IDX].pvec[11];
	m_param->ekf.r << (double)System->Param[EKFR_IDX].pvec[0],
					(double)System->Param[EKFR_IDX].pvec[1],
					(double)System->Param[EKFR_IDX].pvec[2],
					(double)System->Param[EKFR_IDX].pvec[3],
					(double)System->Param[EKFR_IDX].pvec[4],
					(double)System->Param[EKFR_IDX].pvec[5],
					(double)System->Param[EKFR_IDX].pvec[6],
					(double)System->Param[EKFR_IDX].pvec[7],
					(double)System->Param[EKFR_IDX].pvec[8];
	m_param->ekf.sigmaIceVel << (double)System->Param[EKFS_IDX].pvec[0],
							(double)System->Param[EKFS_IDX].pvec[1],
							(double)System->Param[EKFS_IDX].pvec[2];
	m_param->ice.iceNu0 << (double)System->Param[ICE_VEL].pvec[0],
						(double)System->Param[ICE_VEL].pvec[1],
						(double)System->Param[ICE_VEL].pvec[2];
	m_param->icp.maxIterPointFit = (int)System->Param[ICP_PAR].pvec[0];
	m_param->icp.maxIterPlaneFit = (int)System->Param[ICP_PAR].pvec[1];
	m_param->icp.maxMotionEstIter = (int)System->Param[ICP_PAR].pvec[2];
	m_param->icp.nNeighboursNormalEstimation = (int)System->Param[ICP_PAR].pvec[3];
	m_param->icp.stopLimitPointFit = (double)System->Param[ICP_PAR].pvec[4];
	m_param->icp.stopLimitPlaneFit = (double)System->Param[ICP_PAR].pvec[5];
	m_param->icp.fitMethod = (int)System->Param[ICP_PAR].pvec[6];
	m_param->icp.mapNeigbourHoodSizeforFit = (float)System->Param[ICP_PAR].pvec[7];
	m_param->useEkfPose = (bool)System->Param[EKFSLAM_CONTROL].pvec[0];
	m_param->useEkfVelocity = (bool)System->Param[EKFSLAM_CONTROL].pvec[1];
	m_param->useMbeXyz = (bool)System->Param[MBE_XYZ].pvec[0];

	if (System->Param[MBEDIR_IDX].pvec[0])
	{
		m_param->mbe.dir = -1; // Up-looking MBE
	}
	else
	{
		m_param->mbe.dir = 1; // Down-looking MBE
	}

	if (m_param->NeffDivide < 1)
	{
		printf("WARNING: sIcebergSLAMInterface::sIcebergSLAMInterface() - NeffDivide = %i not supported (must be >= 1). Setting to 2.\n", m_param->NeffDivide);
		m_param->NeffDivide = 2;
	}
	

	// Set up iceberg SLAM member object
	m_icebergSlam = std::unique_ptr<IcebergSLAM>( new IcebergSLAM(*m_param) );
}

sIcebergSLAMInterface::~sIcebergSLAMInterface()
{
	;
}

/*	
	Function that calculates the output y = h(xc,xd(k*Ts),u) of the system.
*/
void sIcebergSLAMInterface::ModelOutputs(SimStruct *S, sSystemCFG *, 
	double *, double *, const double *u, double *y)
{
	if (m_icebergSlam == nullptr)
		return;

	if (!m_icebergSlam->isInitialized())
	{
		// Initialize only when iceberg is detected
		// TODO (for now, SLAM will start when simulation starts)
		Measurement meas(u, m_icebergSlam->getNBeams());

		double H = 0.0;
		Vector12d x0e;
		Vector3d etaRel_i(meas.stateAuv(0), meas.stateAuv(1), meas.stateAuv(5));
		Vector3d nuRel_b(u[15], u[16], meas.stateAuv(11));
		Vector3d etaIce_n(0, 0, 0);

		if (m_param->ice.disableRotation)
			x0e << etaRel_i, nuRel_b, etaIce_n, m_param->ice.iceNu0.head<2>(), 0;
		else
			x0e << etaRel_i, nuRel_b, etaIce_n, m_param->ice.iceNu0;

		m_icebergSlam->initializeSLAM(etaIce_n);
		m_icebergSlam->initializeEkf(x0e);

		// Set output
		memcpy_s(&y[0], 3 * sizeof(double), &etaRel_i[0], etaRel_i.size() * sizeof(double));
		memcpy_s(&y[3], 12 * sizeof(double), &x0e[0], x0e.size() * sizeof(double));
		memcpy_s(&y[15], 3 * sizeof(double), &etaIce_n[0], etaIce_n.size() * sizeof(double));
		memcpy_s(&y[18], 1 * sizeof(double), &H, 1 * sizeof(double));
		return;
	}

	m_icebergSlam->run(y, Measurement(u, m_icebergSlam->getNBeams()), ssGetT(S)); //, ssGetSampleTime(S, 0)

	// Flush printf buffer
	fflush(stdout);
}

/*	
	Function that calculates the continuous state derivatives xc_dot = fc(xc,xd(k*Ts),u) of the system.
*/
void sIcebergSLAMInterface::ModelXcDerivatives(SimStruct *, sSystemCFG *,
	double *, double *, double *, const double *)
{
}

/*	
	Function that calculates the discrete state updates xd(k+1) = fd(xd(k),xc,u) of the system.
*/
void sIcebergSLAMInterface::ModelXdUpdate(SimStruct *, sSystemCFG *,
	double *, double *Xd, const double *)
{
	if (m_icebergSlam == nullptr)
		return;

	m_icebergSlam->getAllParticleStates(Xd);
}
