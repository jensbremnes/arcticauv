/* 
    sIcebergSLAM_model.h

	Header file for functions defining the model of the s-function.

	References:		S-function template created by Roger Skjetne.

	Copyright: 		Department of Marine Technology, NTNU
	Author: 			Petter Norgren

*/

#pragma once

#include <memory>

// Library header files
#include "libGenericFunctions.h"

class IcebergSLAM;
namespace AURLAB { namespace SLAM { class SLAMParams; } }

/****************************************************************************
* Input parameters       		  						                    *
*		-PS: Specify the parameters as one column vector, row after row!    *
****************************************************************************/
#define SimParam_IDX 		0	// [SampleTime, NumContStates, NumDiscStates, NumInputSignals, NumOutputSignals, NumParameters]
#define SimParam_PARAM(S) ssGetSFcnParam(S,SimParam_IDX)

#define InitFname_IDX 		1	// Filename initialization (configuration) file.
#define InitFname_PARAM(S) ssGetSFcnParam(S,InitFname_IDX)

#define Xc0_IDX 			2	// Initial conditions continuous states
#define Xc0_PARAM(S) ssGetSFcnParam(S,Xc0_IDX)

#define Xd0_IDX 			3	// Initial conditions discrete states
#define Xd0_PARAM(S) ssGetSFcnParam(S,Xd0_IDX)

/*********************************
* Vector and array sizes		 *
*********************************/

// DO NOT CHANGE THESE PARAMTERS UNLESS SIMULINK S-FUNCTION IS CHANGED
#define NP 		4	/* Number of INPUT DIALOG PARAMETERS					*///

#define NIP     2	/* Number of Input Ports								*/
#define NU0   	2	/* Input 0: Mode control input vector					*/

#define NOP     1	/* Number of Output Ports								*/

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* #####################################  Function declarations	################################### */
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

class sIcebergSLAMInterface
{
public:
	// Do not change these functions. Called by main.
	sIcebergSLAMInterface(SimStruct *S, sSystemCFG *SystemStruct);
	~sIcebergSLAMInterface(void);
	void ModelOutputs(SimStruct *S, sSystemCFG *SystemStruct, double *xc, double *xd, const double *u, double *y);
	void ModelXcDerivatives(SimStruct *S, sSystemCFG *SystemStruct, double *xc_dot, double *xc, double *xd, const double *u);
	void ModelXdUpdate(SimStruct *S, sSystemCFG *SystemStruct, double *xc, double *xd, const double *u);

private:
	std::unique_ptr<IcebergSLAM> m_icebergSlam;
	std::unique_ptr<AURLAB::SLAM::SLAMParams> m_param;
};
