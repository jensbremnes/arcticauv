/*
	sIcebergSLAM.h

	Header file for functions defining the IcebergSLAM class

	References:		-

	Copyright: 		Department of Marine Technology, NTNU
	Author: 		Petter Norgren
*/

#pragma once

#include "SLAMimpl.hpp"
#include "IceEKF.hpp"

#include <memory>

using namespace AURLAB::SLAM;

class IcebergSLAM
{
	public:
		IcebergSLAM(const SLAMParams &param);
		~IcebergSLAM();

		void run(double *y, Measurement meas, const double &t);

		void getAllParticleStates(double *x) const;
		bool isInitialized() const { return (m_slam->isInitialized() && m_ekf->isInitialized()); }

		const bool &IcebergSLAM::initializeSLAM(const Vector3d &x0) { return m_slam->initialize(x0); }
		const bool &IcebergSLAM::initializeEkf(const Vector12d &x0) { return m_ekf->initialize(x0); }

		const int &getNBeams() const { return m_nMbeBeams; }
		const int &getNParticles() const { return m_nParticles; }

	private: 
		void addSLAMMeasurements(Measurement &meas, const uint64 &t);

	private:
		const int m_nMbeBeams;
		const int m_nParticles;
		const float m_spinPeriod;
		const float m_posUpdatePeriod;
		const bool m_useEkfPose;
		const bool m_useEkfVelocity;
		const bool m_disableRotation;
		Vector3d m_nuIce;
		Vector8d m_yEkf;

		std::unique_ptr<SLAMimpl> m_slam;
		std::unique_ptr<RelativePositionEstimator> m_ekf;
};
