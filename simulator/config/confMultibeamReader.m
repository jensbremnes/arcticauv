function [conf, modules, mdlFiles] = confMultibeamReader(simRoot)
% confMultibeamReader returns the configuration for the multibeam reader.
%
% [conf, modules, mdlFiles] = confMultibeamReader(simRoot)
%
% Input parameters:
%    simRoot        - Root directory where auvsim is located (terminated with \)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
    conf = confPaths(simRoot);
    
    % Misc
    conf.DUNE = 0;
    conf.runSimulation = true;
    
    % Modules
    modules = {};
    modules{end+1} = 'sIcebergSLAM';
    modules{end+1} = 'sIcebergSimulator';
    modules{end+1} = 'sMultibeamReader';
    
    % Mdl files
    mdlFiles = {};
    mdlFiles{end+1} = 'Iceberg';
    mdlFiles{end+1} = 'MultibeamReader';
    mdlFiles{end+1} = 'IceSLAM';
    mdlFiles{end+1} = 'IcebergMappingMultibeamReader';
    conf.mdlRoot = mdlFiles{end};

    % Simulation
    conf.T = 2000;
    conf.stepSize = 0.05;
    conf.debugOn = 0;
    conf.enable2dAnimation = 0;
    
    % Animation
    conf.anim.auvSize = [2.0, 0.5];
    conf.anim.icebergFile = '';
    conf.anim.enableParticlePlots = 0;
    conf.anim.animationStop = conf.T;
    conf.anim.saveAnimation = 0;
    conf.anim.animationFps = 10;
    
    % Multibeam reader
    conf.TS_mbeReader = 0.05;
    conf.mbe.startIndex = 0;
    conf.mbe.endIndex = -1;
    conf.mbeDir = [conf.dataDir,  'Multibeam/'];
    conf.mbe.missionName = 'Mission_171116_2';
    conf.mbe.navlabDataFile = 'navlab_smooth.hdf';
    conf.mbe.navlabOutFile = 'navlab.txt';
    conf.mbe.posDataFile = 'pos.hdf';
    conf.mbe.attitudeDataFile = 'attitude.hdf';
    conf.mbe.beamDataFile = 'rawrangeangle.hdf';
    conf.mbe.nsdDataFile = 'navsolution.hdf';
    conf.mbe.xyzDataFile = 'xyz.hdf';
    conf.mbe.sensor = 'EM2040';
    conf.mbe.nBeams = 400;
    conf.mbe.nStates = 12 + 9;
    conf.mbe.nOutputs = 5 + 5*conf.mbe.nBeams + conf.mbe.nStates; 
    conf.mbe.sectorRadians = deg2rad(120);
    conf.mbe.leverArm = [0, 0, 0]';
    conf.mbe.isUpLooking = 0;
    conf.mbe.realData = 1;
    
    % Initialze SLAM parameters
    conf.TS_SLAM = 0.05;
    conf.iceslam.TsSlamSpin = 10.00;
    conf.iceslam.mbeSigma = [0.4, deg2rad(0.05), deg2rad(0.05)];
    conf.iceslam.iceSigma = [1.0, 1.0, 1.0e-3].*0.5;
    conf.iceslam.diableRotationEstimate = 1;
    conf.iceslam.baselineCloud = [conf.dataDir, 'particleCloud/', 'baseCloud1000_1_0_1_0_45e4_1350s'];
    conf.iceslam.timeconstants = [0, 0, 0];
    conf.iceslam.pfNoiseEnable = 1;
    conf.iceslam.nParticles = 200;
    conf.iceslam.mapSize = 500;
    conf.iceslam.mapResolution = 0.5;
    conf.iceslam.mapNegativePart = 50;
    conf.iceslam.mapCenter = [0, 0, 0]';
    conf.iceslam.minOverlapResampling = 0.40;
    conf.iceslam.maxResampled = 1.0;
    conf.iceslam.NeffDivide = 2;
    conf.iceslam.NeffMinLimit = 1.00;
    conf.iceslam.minBeamWeight = 0.00;
    conf.iceslam.minTimeBetweenResampling = 0.0;
    conf.iceslam.minOverlapBeams = 1.00; %% NOT IN USE
    conf.iceslam.minGridsInSwath = 100;
    conf.iceslam.minSwathsInPatch = 0.5*(conf.iceslam.TsSlamSpin / conf.TS_SLAM); %% NOT IN USE
    conf.iceslam.minTimeWeight = 15; % [s]
    conf.iceslam.saveObservationMapOnExit = 1;
    conf.iceslam.useMbeXyz = 1;                 % 0 - Use raw range / angle measurements, 1 - Use MBE processed XYZ
    conf.iceslam.extractStateVectorMode = 0;    % 0 - average, 1 - weighted average, 2 - highest weight
    conf.iceslam.resamplingStratgy = 1;         % 0 - Multinomial, 1 - Systematic
    conf.iceslam.weightingMethod = 2;           % 0 - BPSLAM, 1 - PatchBPSLAM 2 - Modified_PatchBPSLAM, 3 - ICP_BPSLAM
    
    conf.icp.maxIcpIterPointFit = 150;
    conf.icp.maxIcpIterPlaneFit = 500;
    conf.icp.maxMotionEstIter = 30;
    conf.icp.nNeighboursNormalEstimation = 4;   % min 3, max 24
    conf.icp.stopLimitPointFit = 1e-1;
    conf.icp.stopLimitPlaneFit = 3e-2;
    conf.icp.fitMethod = 0;                     % 0 - Combined point and plane, % 1 - Only plane fit, 2 - Only point fit
    conf.icp.mapNeigbourHoodSizeforFit = 35;
    conf.icp.gridsInSwathMultiplier = 7.5;
    
    if(conf.iceslam.weightingMethod == 3)
        conf.iceslam.minGridsInSwath = conf.iceslam.minGridsInSwath*conf.icp.gridsInSwathMultiplier;
    end
    
%     conf.iceekf.q = [0.5, 0.5, 0.1, 0.1, 0.1, 0.1, 0.3, 0.3, 1e-8, 1e-6, 1e-6, 1e-10];
%     conf.iceekf.r = [0.1, 0.1, 0.005, 1e-4, 1e-4, 1e-4, 5.0, 5.0, 1e-5];
    conf.iceekf.q = [1.0, 1.0, 0.1, 0.005, 0.005, 0.005, 0.1, 0.1, 1e-8, 1e-14, 1e-14, 1e-16];
    conf.iceekf.r = [0.5, 0.5, 0.0001, 1e-6, 1e-6, 1e-8, 1.0, 1.0, 1e-7];
    conf.iceekf.sigmaIceVel = [5e-4, 5e-4, 1e-6];
    conf.iceekf.TsPosMeas = 2000;
    
    conf.xDot0 = [0.0190; 0.9494; 1.1826];
    
    conf.iceslam.relMeasurementSampleTime = 0.5;
    conf.iceslam.useEkfPoseInSLAM = 0;
    conf.iceslam.useEkfVelocityInSLAM = 0;
    
    % Iceberg
    conf.TS_iceberg = conf.stepSize;
    conf.iceberg.eta0 = [0, 0, 0]';
    conf.iceberg.nu0 = [0, 0, 0]';
    conf.iceberg.etaRel0 = [0, 0, 0]';
    conf.iceberg.nuRel0 = [2, 0, 0]';
    conf.x0 = zeros(12,1);
    
    % Animation
    conf.anim.auvSize = [2.0, 0.5];
    conf.anim.icebergFile = ''; % [conf.terrain.icebergPath, conf.terrain.icebergName];
    conf.anim.enableParticlePlots = 0;
    conf.anim.animationStop = conf.T;
    conf.anim.saveAnimation = 0;
    conf.anim.animationFps = 10;
    
    % Todo remove dependency on rotation offset in iceberg simulator
    % Define terrain files
    conf.terrain.bathymetryPath = [conf.dataDir, 'Bathymetry/SIMdata/'];        % Must end with '/'
    conf.terrain.icebergPath = [conf.dataDir, 'Icebergs/SIMdata/'];             % Must end with '/'
    conf.terrain.bathymetryName = 'default_bathy_low_res';                      % Must have corresponding files: <terrainName>_Z.dat, <terrainName>_dim.dat, <terrainName>_bounds.dat, <terrainName>_offset.dat.
    conf.terrain.icebergName = 'iceberg_R11I01';                                % Must have corresponding files: <terrainName>_Z.dat, <terrainName>_dim.dat, <terrainName>_bounds.dat, <terrainName>_offset.dat.
    
    conf = orderfields(conf);
end
