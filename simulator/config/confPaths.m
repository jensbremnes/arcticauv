function conf = confPaths(simRoot)
% confPaths is a function that returns the default paths for AUVsim.
%
% conf = confPaths(simRoot)
%
% Input parameters:
%    simRoot        - Root directory where auvsim is located (terminated with \)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    % Add paths
    conf.rootDir = strrep(simRoot, '\', '/');
    conf.buildDir = [conf.rootDir, 'build/'];
    conf.libDir = [conf.rootDir, 'lib/'];
    conf.vendorDir = [conf.rootDir, 'vendor/'];
    conf.dataDir = [conf.rootDir, 'data/'];
    conf.simDir = [conf.rootDir, 'simulator/'];
    conf.configDir = [conf.simDir, 'config/'];
    conf.initializationDir = [conf.simDir, 'initialization/'];
    conf.sfunctionDir = [conf.simDir, 'sfunctions/'];
    conf.modelDir = [conf.simDir, 'simulink/', version('-release'), '/'];
    conf.gfxDir = [conf.rootDir, 'simulator/gfx/matlab/'];
    conf.toolsDir = [conf.simDir, 'tools/'];
    conf.plottingDir = [conf.rootDir, 'simulator/plotting/'];
    conf.videoDir = [conf.rootDir, 'build/video/'];
    conf.saveDir = [conf.rootDir 'save/', datestr(datetime('now'), 'yymmdd/HHMMSS'), '/'];
    conf.figDir = [conf.saveDir, 'figures/'];

    % List paths to be added to matlab search path
    addpaths{1} = conf.configDir;
    addpaths{2} = conf.gfxDir;
    addpaths{3} = conf.initializationDir;
    addpaths{4} = conf.plottingDir;
    addpaths{5} = conf.toolsDir;
    addpaths{6} = conf.modelDir;
    addpaths{7} = conf.simDir;
    
    addpathsRecursive{1} = [conf.vendorDir, 'matlab'];
    addpathsRecursive{2} = conf.sfunctionDir;
    addpathsRecursive{3} = conf.libDir;
    
    % Add neccessary matlab search paths
    for i=1:length(addpaths)
       addpath(addpaths{i}); 
    end
    for i=1:length(addpathsRecursive)
       addpath(genpath(addpathsRecursive{i})); 
    end

    conf.name = ['run_', datestr(datetime('now'), 'yymmdd\\HHMMSS')];
    
    % Create build and save directories if they do not exist
    if(~exist(conf.buildDir, 'dir'))
       mkdir(conf.buildDir); 
    end
    if(~exist(conf.saveDir, 'dir'))
        mkdir(conf.saveDir);
    end
end