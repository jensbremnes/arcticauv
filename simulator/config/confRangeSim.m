function [conf, modules, mdlFiles] = confRangeSim(conf, modules, mdlFiles)
% confRangeSim is a function that returns the default configuration
% structure for the AUV simulator with range simulation.
%
% [conf, modules, mdlFiles] = confRangeSim(conf, modules, mdlFiles)
%
% Input parameters:
%    conf           - Prefilled default structure
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
   
    % Define necessary modules
    modules{end+1} = 'sDvlMbeSimulator';
    mdlFiles{end+1} = 'AuvObserver';
    mdlFiles{end+1} = 'SensorSimulator';
    mdlFiles{end+1} = 'AuvSimulatorWithObserver';
    conf.mdlRoot = 'AuvSimulatorWithObserver';
    
    % Define terrain files
    conf.terrain.bathymetryPath = [conf.dataDir, 'Bathymetry/SIMdata/'];        % Must end with '/'
    conf.terrain.icebergPath = [conf.dataDir, 'Icebergs/SIMdata/'];             % Must end with '/'
    conf.terrain.bathymetryName = 'default_bathy_low_res';                      % Must have corresponding files: <terrainName>_Z.dat, <terrainName>_dim.dat, <terrainName>_bounds.dat, <terrainName>_offset.dat.
    conf.terrain.icebergName = 'iceberg_R11I01';                                % Must have corresponding files: <terrainName>_Z.dat, <terrainName>_dim.dat, <terrainName>_bounds.dat, <terrainName>_offset.dat.
    conf.terrain.icebergRotationOffset = dlmread([conf.terrain.icebergPath, conf.terrain.icebergName, '_rotation.dat'])';
    conf.terrain.icebergRotationOffset = [conf.terrain.icebergRotationOffset; 0];
    
    % Setup DVL and MBE range simulator
    conf.mbe.nBeams = 120;                                      % [-]
    conf.mbe.sectorRadians = 120*pi/180;                        % [rad]
    conf.mbe.isUpLooking = 1;                                   % [0,1]
    conf.mbe.leverArm = [0, 0, 0]';                             % [m]
    conf.mbe.maxRange = 250;
    conf.mbe.minRange = 1.0;
    conf.mbe.enable = 0;
    conf.mbe.isBeamNanAboveSurface = 1;
    conf.mbe.savefile = [conf.saveDir, 'PointCloud.txt'];
    conf.mbe.enableLogging = 0;
    conf.mbe.noiseEnable = 1;
    conf.mbe.rangeNoisePower = 0.5;                             % [%] Standard deviation
    conf.mbe.realData = 0;
    conf.dvl.beamAngleRadians = 20*pi/180;                      % [rad]
    conf.dvl.leverArm = [0, 0, 0]';                             % [m]
    conf.dvl.maxRange = 30;
    conf.dvl.minRange = 1;
    conf.dvl.enable = 0;
    conf.dvl.isBeamNanAboveSurface = 0;
    conf.dvl.rangeNoisePower = 0.5;                             % [%] Standard deviation
   
    conf = orderfields(conf);
end
