function [conf, modules, mdlFiles] = confDefault(simRoot)
% confDefault is a function that returns the default configuration
% structure for the AUV simulator.
%
% [conf, modules, mdlFiles] = confDefault(simRoot)
%
% Input parameters:
%    simRoot        - Root directory where auvsim is located (terminated with \)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    
    % Add paths
    conf.rootDir = strrep(simRoot, '\', '/');
    conf.buildDir = [conf.rootDir, 'build/'];
    conf.libDir = [conf.rootDir, 'lib/'];
    conf.vendorDir = [conf.rootDir, 'vendor/'];
    conf.dataDir = [conf.rootDir, 'data/'];
    conf.simDir = [conf.rootDir, 'simulator/'];
    conf.configDir = [conf.simDir, 'config/'];
    conf.initializationDir = [conf.simDir, 'initialization/'];
    conf.sfunctionDir = [conf.simDir, 'sfunctions/'];
    conf.modelDir = [conf.simDir, 'simulink/', version('-release'), '/'];
    conf.gfxDir = [conf.rootDir, 'simulator/gfx/matlab/'];
    conf.toolsDir = [conf.simDir, 'tools/'];
    conf.plottingDir = [conf.rootDir, 'simulator/plotting/'];
    conf.videoDir = [conf.rootDir, 'build/video/'];
    conf.saveDir = [conf.rootDir 'save/', datestr(datetime('now'), 'yymmdd/HHMMSS'), '/'];
    conf.figDir = [conf.saveDir, 'figures/'];

    % List paths to be added to matlab search path
    addpaths{1} = conf.configDir;
    addpaths{2} = conf.gfxDir;
    addpaths{3} = conf.initializationDir;
    addpaths{4} = conf.plottingDir;
    addpaths{5} = conf.toolsDir;
    addpaths{6} = conf.modelDir;
    addpaths{7} = conf.simDir;
    
    addpathsRecursive{1} = [conf.vendorDir, 'matlab'];
    addpathsRecursive{2} = conf.sfunctionDir;
    addpathsRecursive{3} = conf.libDir;
    
    % Add neccessary matlab search paths
    for i=1:length(addpaths)
       addpath(addpaths{i}); 
    end
    for i=1:length(addpathsRecursive)
       addpath(genpath(addpathsRecursive{i})); 
    end

    conf.name = ['run_', datestr(datetime('now'), 'yymmdd\\HHMMSS')];
    
    % Create build directory if it do not exist
    if(~exist(conf.buildDir, 'dir'))
       mkdir(conf.buildDir); 
    end
    
    % Empty modules struct
    modules = {};

    % Simulation
    conf.T = 2000;
    conf.plot.Tend = conf.T;
    conf.stepSize = 0.01;
    conf.debugOn = 0;
    conf.Speedup = 1;                                    % Not in use unless real-time pacer is used
    mdlFiles{1} = 'AuvSimulatorBase';
    conf.mdlRoot = 'AuvSimulatorBase';
    conf.enable2dAnimation = 0;
    conf.rtSpeedUp = 50;
    
    % Modules
    conf.TS_low_level = 1/100;                           % [1/Hz]
    conf.TS_high_level = 1/10;                           % [1/Hz]
    conf.TS_observer = 1/100;                            % [1/Hz]
    conf.TS_guidance = 1/50;                             % [1/Hz]
    conf.TS_MBE = 1/10;                                  % [1/Hz]
    conf.TS_ANIM = 1/1;                                  % [1/Hz]
    conf.TS_iceberg = 1/100;                             % [1/Hz]
    
    % Observer
    conf.overrideObserver = 1;
    
    % Guidance
    conf.WPGuidance = 0;                                 % Mandatory (used in auvSimulator_exe.m)
    conf.DUNE = 0;                                       % Mandatory (used in auvSimulator_exe.m)
    conf.altitudeRef = 25;
    conf.altitudeControl = 0;
    conf.altitudeDir = -1;                               % 1 for downwards alt, -1 for upwards
    
    % Current
    conf.VCurrentMs = 0.0;                % [m/s]
    conf.betaCurrentDeg = 0;              % [degrees]
    conf.rotationRateCurrentDegs = 0;     % [degrees/s]
    
    % Run simulation or just perform plotting and saving
    conf.runSimulation = true;
    
    % Initial condition
    conf.x0 = [0, 0, 40, 0, 0, deg2rad(0), knots2ms(2.5), 0, 0, 0, 0, 0]';
    conf.propellerRpmInitial = 600;         % [RPM]

    % Model and controller
    cd(conf.buildDir);
    conf.auvStruct = initializeAuvModel();
    conf.referenceStruct = initializeReferenceModel();
    conf.controllerStruct = initializeControllers();
    cd(conf.rootDir);
    
    % Plots
    conf.plot.fontsize = 20;
    conf.plot.linewidth = 2.5;
    
    conf = orderfields(conf);
end
