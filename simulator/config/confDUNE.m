function conf = confDUNE(conf, duneRoot)
% confDUNE is a function that returns the default DUNE configuration
% structure for the AUV simulator.
%
% conf = confDUNE(conf, duneRoot)
%
% Input parameters:
%    conf           - Prefilled default structure
%    duneRoot       - Root directory where dune is located (terminated with \)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    % Add paths
    conf.duneRoot = duneRoot;
    conf.duneBatDir = [conf.duneRoot , 'user\etc\'];
    
    % Simulation
    conf.mdlFile = 'DUNE';
    
    % Define necessary modules
    conf.modules = {};
    conf.modules{1} = 'sImcInterface';
    
    % Dune
    conf.DUNE = 1;
    conf.udpReceivePort = 9090;
    conf.duneBridePort = 8889;
    conf.duneIpAddr = '127.0.0.1';
    conf.duneConfigFile = 'remusAuvSim';
    conf.duneProfile = 'AuvSim';
    conf.imcTraceOn = 0;                        % For printing trace of imc interface
    conf.imcDebugOn = 0;                        % For printing debug of imc interface
    conf.duneStartupManeuver = 'idle';          % Possible values: 'idle', 'goto', 'followpath'
    conf.startGoto = [-100, 25];                
    conf.referencePosition = [deg2rad([ 63.440783, 10.349350]), 0];
    conf.wplist = [-100, 25; 150, 25; 150, 50; -100, 50; -100, 75; 150, 75; 150, 100; -100, 100; -100, 125; 150, 125; 150, 150; -100, 150; -100, 175; 150, 175; 150, 200; -100, 200; -100, 225; 150, 225; 150, 250; -100, 250; -100, 275; 150, 275; 150, 300; nan, nan];
    conf.depthcontrol.tag = 'depth';            % 'depth' -> enabled depth control, 'altitude' -> enables altitude control 
    conf.depthcontrol.value = 50;               % [m] Depth or altitude setpoint
    conf.speedcontrol.tag = 'mps';              % 'mps' -> speed in m/s, 'percent' -> speed in % of max, 'rpm' -> speed in rpm
    conf.speedcontrol.value = knots2ms(3);      % [m/s] Speed setpoint (corresponding to tag)    
    conf.bypassDuneObserver = 1;                % Use Dune or Matlab observer
  
    conf = orderfields(conf);
end
