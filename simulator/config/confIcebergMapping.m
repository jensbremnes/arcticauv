function [conf, modules, mdlFiles] = confIcebergMapping(conf, modules, mdlFiles)
% confIcebergMapping is a function that returns the updated configuration
% structure for the iceberg mapping case.
%
% [conf, modules, mdlFiles] = confIcebergMapping(conf, modules, mdlFiles)
%
% Input parameters:
%   conf         - Structure containing default configuration.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%
    if(conf.DUNE)
        conf.icebergPoseFileName = [conf.buildDir, 'icebergPose.txt'];
        conf.mbeRangesFileName = [conf.buildDir, 'mbeRanges.txt'];
        conf.mbePointCloudFileName = [conf.buildDir, 'mbeXYZ.txt'];
    end
    
    % Simulation
    modules{end+1} = 'sIcebergSLAM';
    modules{end+1} = 'sIcebergSimulator';
    
    mdlFiles{end+1} = 'Iceberg';
    mdlFiles{end+1} = 'IceSLAM';
    mdlFiles{end+1} = 'IceGuidance';
    mdlFiles{end+1} = 'AuvSimulatorIcebergMapping';
    conf.mdlRoot = 'AuvSimulatorIcebergMapping';
    
     % Animation
    conf.anim.auvSize = [2.0, 0.5];
    conf.anim.icebergFile = [conf.terrain.icebergPath, conf.terrain.icebergName];
    conf.anim.enableParticlePlots = 0;
    conf.anim.animationStop = conf.T;
    conf.anim.saveAnimation = 0;
    conf.anim.animationFps = 10;

    % Guidance
%     depthRef = 150;
%     lineSpacing = 20;
%     numRows = 11;
%     startWp = [0, 0];
%     direction = 0;
%     distance = 150;
%     firstTurnToLeft = false;
%     conf.wplist = navigateRows(startWp, direction, distance, lineSpacing, numRows, firstTurnToLeft, depthRef);
%     startDepth = 50;
    depthRef = 115;
    lineSpacing = 25;
%     conf.wplist = [0, 0, depthRef; 120, 0, depthRef; 120, lineSpacing, depthRef; 5, lineSpacing, depthRef; 5, 2*lineSpacing, depthRef; 140, 2*lineSpacing, depthRef; 140, 3*lineSpacing, depthRef; 0, 3*lineSpacing, depthRef; 0, 4*lineSpacing, depthRef; 170, 4*lineSpacing, depthRef; 170, 5*lineSpacing, depthRef; 30, 5*lineSpacing, depthRef; 30, 6*lineSpacing, depthRef; 170, 6*lineSpacing, depthRef; 170, 7*lineSpacing, depthRef; 30, 7*lineSpacing, depthRef;
%                     30, 6*lineSpacing, depthRef; 170, 6*lineSpacing, depthRef; 170, 5*lineSpacing, depthRef; 15, 5*lineSpacing, depthRef; 15, 4*lineSpacing, depthRef; 170, 4*lineSpacing, depthRef; 170, 3*lineSpacing, depthRef; 0, 3*lineSpacing, depthRef; 0, 2*lineSpacing, depthRef; 125, 2*lineSpacing, depthRef; 125, 1*lineSpacing, depthRef; 30, 1*lineSpacing, depthRef; 30, 0*lineSpacing, depthRef; 120, 0*lineSpacing, depthRef;
%                   ];
%     conf.wplist = [0, 0, startDepth; 120, 0, startDepth; 120, lineSpacing, startDepth; 5, lineSpacing, startDepth; 5, 2*lineSpacing, depthRef; 140, 2*lineSpacing, depthRef; 140, 3*lineSpacing, depthRef; 0, 3*lineSpacing, depthRef; 0, 4*lineSpacing, depthRef; 170, 4*lineSpacing, depthRef; 170, 5*lineSpacing, depthRef; 30, 5*lineSpacing, depthRef; 30, 6*lineSpacing, depthRef; 170, 6*lineSpacing, depthRef; 170, 7*lineSpacing, depthRef; 30, 7*lineSpacing, depthRef;
%                     30, 6*lineSpacing, depthRef; 170, 6*lineSpacing, depthRef; 170, 5*lineSpacing, depthRef; 15, 5*lineSpacing, depthRef; 15, 4*lineSpacing, depthRef; 170, 4*lineSpacing, depthRef; 170, 3*lineSpacing, depthRef; 0, 3*lineSpacing, depthRef; 0, 2*lineSpacing, depthRef; 125, 2*lineSpacing, depthRef; 125, 1*lineSpacing, startDepth; 30, 1*lineSpacing, startDepth; 30, 0*lineSpacing, startDepth; 120, 0*lineSpacing, startDepth;
%                   ];
    conf.wplist = [0, 0, depthRef; 0, 100, depthRef; lineSpacing, 100, depthRef; lineSpacing, 30, depthRef; 2*lineSpacing, 30, depthRef; 2*lineSpacing, 170, depthRef; 3*lineSpacing, 170, depthRef; 3*lineSpacing, 0, depthRef; 4*lineSpacing, 0, depthRef; 4*lineSpacing, 170, depthRef; 5*lineSpacing, 170, depthRef; 5*lineSpacing, 20, depthRef; 6*lineSpacing, 20, depthRef; 6*lineSpacing, 175, depthRef;
                    30, 7*lineSpacing, depthRef; 30, 6*lineSpacing, depthRef; 170, 6*lineSpacing, depthRef; 170, 5*lineSpacing, depthRef; 20, 5*lineSpacing, depthRef; 20, 4*lineSpacing, depthRef; 170, 4*lineSpacing, depthRef; 170, 3*lineSpacing, depthRef; 0, 3*lineSpacing, depthRef; 0, 2*lineSpacing, depthRef; 130, 2*lineSpacing, depthRef; 130, 1*lineSpacing, depthRef; 50, 1*lineSpacing, depthRef; 50, 0*lineSpacing, depthRef; 120, 0*lineSpacing, depthRef;
                   ];
    conf.Uref = 1.7;
    
    conf.altitudeRef = 70;
    conf.altitudeControl = 0;
    conf.altitudeDir = -1;
    
    conf.guidanceStruct = initializeLOS();
    
    % Initialze SLAM parameters
    conf.TS_SLAM = 0.1;
    conf.iceslam.TsSlamSpin = 15;
    conf.iceslam.mbeSigma = [0.5, deg2rad(1.5), deg2rad(1.5)];
    conf.iceslam.iceSigma = [1.0, 1.0, 1.0e-3];
    conf.iceslam.diableRotationEstimate = 0;
    conf.iceslam.baselineCloud = [conf.dataDir, 'particleCloud/', 'baseCloud1000_1_0_1_0_1e-3_1350s'];
    conf.iceslam.timeconstants = [0, 0, 0];
    conf.iceslam.pfNoiseEnable = 0;
    conf.iceslam.nParticles = 200;
    conf.iceslam.mapSize = 225;
    conf.iceslam.mapResolution = 1.0;
    conf.iceslam.mapNegativePart = 25;
    conf.iceslam.mapCenter = [dlmread([conf.anim.icebergFile, '_rotation.dat']), 0]';
    conf.iceslam.minOverlapResampling = 0.50;
    conf.iceslam.maxResampled = 1.0;
    conf.iceslam.NeffDivide = 2;
    conf.iceslam.NeffMinLimit = 1.00;
    conf.iceslam.minBeamWeight = 0.00;
    conf.iceslam.minTimeBetweenResampling = 0.0;
    conf.iceslam.minOverlapBeams = 1.00; %% NOT IN USE
    conf.iceslam.minGridsInSwath = 50;
    conf.iceslam.minSwathsInPatch = 0.5*(conf.iceslam.TsSlamSpin / conf.TS_SLAM); %% NOT IN USE
    conf.iceslam.minTimeWeight = 15; % [s]
    conf.iceslam.saveObservationMapOnExit = 1;
    conf.iceslam.useMbeXyz = 0;                 % 0 - Use raw range / angle measurements, 1 - Use MBE processed XYZ
    conf.iceslam.extractStateVectorMode = 0;    % 0 - average, 1 - weighted average, 2 - highest weight
    conf.iceslam.resamplingStratgy = 1;         % 0 - Multinomial, 1 - Systematic
    conf.iceslam.weightingMethod = 2;           % 0 - BPSLAM, 1 - PatchBPSLAM 2 - Modified_PatchBPSLAM, 3 - ICP_BPSLAM
    
    conf.icp.maxIcpIterPointFit = 150;
    conf.icp.maxIcpIterPlaneFit = 350;
    conf.icp.maxMotionEstIter = 15;
    conf.icp.nNeighboursNormalEstimation = 4;   % min 3, max 24
    conf.icp.stopLimitPointFit = 1e-1;
    conf.icp.stopLimitPlaneFit = 3e-2;
    conf.icp.fitMethod = 0;                     % 0 - Combined point and plane, % 1 - Only plane fit, 2 - Only point fit
    conf.icp.mapNeigbourHoodSizeforFit = 25;
    conf.icp.gridsInSwathMultiplier = 10;
    
    if(conf.iceslam.weightingMethod == 3)
        conf.iceslam.minGridsInSwath = conf.iceslam.minGridsInSwath*conf.icp.gridsInSwathMultiplier;
    end
    
%     conf.iceekf.q = [0.5, 0.5, 0.1, 0.1, 0.1, 0.1, 0.3, 0.3, 1e-8, 1e-6, 1e-6, 1e-10];
%     conf.iceekf.r = [0.1, 0.1, 0.005, 1e-4, 1e-4, 1e-4, 5.0, 5.0, 1e-5];
    conf.iceekf.q = [0.05, 0.05, 0.005, 0.01, 0.01, 0.005, 0.1, 0.1, 1e-8, 3e-6, 3e-6, 5e-14];
    conf.iceekf.r = [0.05, 0.05, 0.005, 1e-4, 1e-4, 1e-4, 2.0, 2.0, 1e-7];
    conf.iceekf.sigmaIceVel = [5e-4, 5e-4, 1e-6];
    conf.iceekf.TsPosMeas = 60.0;
    
    conf.xDot0 = [0.0190; 0.9494; 1.1826];
    
    conf.iceslam.relMeasurementSampleTime = 0.1;
    conf.iceslam.useEkfPoseInSLAM = 1;
    conf.iceslam.useEkfVelocityInSLAM = 1;
    
    % Iceberg
    conf.iceberg.eta0 = [0, 0, 0]';
    conf.iceberg.nu0 = [0, 0, 0]';
    conf.iceberg.etaRel0 = [0, 0, 0]';
    conf.iceberg.nuRel0 = [0, 0, 0]';
    
    conf.iceslam.minSwathsInPatch = floor(max(min(conf.iceslam.TsSlamSpin / conf.TS_SLAM, conf.iceslam.minSwathsInPatch), 1));
end