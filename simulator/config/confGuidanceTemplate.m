function [conf, modules, mdlFiles] = confGuidanceTemplate(conf, modules, mdlFiles)
% confGuidanceTemplat is a function that returns the default configuration
% structure for the AUV simulator and the guidance template.
%
% [conf, modules, mdlFiles] = confGuidanceTemplate(conf, modules, mdlFiles)
%
% Input parameters:
%    conf           - Prefilled default structure
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    % Simulation
    mdlFiles{end+1} = 'AuvSimulatorGuidanceTemplate';
    conf.mdlRoot = 'AuvSimulatorGuidanceTemplate';

    % Guidance
    depthRef = 130;
    lineSpacing = 25;
    numRows = 10;
    startWp = [-10, 0];
    direction = 0;
    distance = 195;
    firstTurnToLeft = true;
    conf.wplist = navigateRows(startWp, direction, distance, lineSpacing, numRows, firstTurnToLeft, depthRef);
    conf.Uref = 1.7;
    
    conf.guidanceStruct = initializeLOS();
    
    conf = orderfields(conf);
end
