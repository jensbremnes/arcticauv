function simReturn = auvSimulator_exe(conf, modules, mdlFiles)
% auvSimulator_exe is a function that will run the AuvSimulator.
% The script initializes and runs the simulink model defined in the config.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    %% Model initialization
    if(~exist(conf.buildDir, 'dir'))
       mkdir(conf.buildDir); 
    end
    if ~exist(conf.saveDir, 'dir')
        mkdir(conf.saveDir);
    end
    cd(conf.buildDir);
      
    for i=1:length(mdlFiles)
        switch mdlFiles{i}
            case 'AuvSimulatorIcebergMapping'
                solver = 'normal';
                stepSize = conf.stepSize;
            case 'AuvSimulatorGuidanceTemplate'
                solver = 'normal';
                stepSize = conf.stepSize;
            case 'IcebergMappingMultibeamReader'
                solver = 'normal';
                stepSize = conf.stepSize;
                case 'IcebergMappingMultibeamReaderSimple'
                solver = 'normal';
                stepSize = conf.stepSize;
            case 'IceSLAM'
                solver = 'rapid';
                stepSize = conf.TS_SLAM;
            case 'IceGuidance'
                solver = 'rapid';
                stepSize = conf.TS_guidance;
            case 'MultibeamReader'
                solver = 'rapid';
                stepSize = conf.TS_mbeReader;
            otherwise
                solver = 'rapid';
                stepSize = conf.stepSize;
        end
        load_system(mdlFiles{i});
        cs = getActiveConfigSet(mdlFiles{i});
        set_param(cs, 'SolverType', 'Fixed-step', 'FixedStep' ...
            , num2str(stepSize),  'Solver', 'ode4' ...
            , 'ReturnWorkspaceOutputs', 'on', 'SimulationMode', solver ...
            , 'StopTime', num2str(conf.T), 'SaveFormat', 'StructureWithTime' ...
            , 'ModelReferenceNumInstancesAllowed', 'Single');
    end
    clear solver stepSize;

    %% Create ini-files
    try
        for i=1:length(modules)
            switch modules{i}
                case 'sImcInterface'
                    [conf.sfunc.ImcInterfaceParam, conf.sfunc.ImcInterfaceFname, conf.sfunc.ImcInterfaceXc0, conf.sfunc.ImcInterfaceXd0] = sImcInterfaceParam(conf.TS_guidance, conf);
                case 'sDvlMbeSimulator'
                    [DvlMbeSimParam, DvlMbeSimFname, DvlMbeSimXc0, DvlMbeSimXd0] = sDvlMbeSimulatorParam(conf.TS_MBE, conf);
                case 'sIcebergSLAM'
                    [IcebergSLAMParam, IcebergSLAMFname, IcebergSLAMXc0, IcebergSLAMXd0] = sIcebergSLAMParam(conf.TS_SLAM, conf);
                case 'sIcebergSimulator'
                    [IcebergSimParam, IcebergSimFname, IcebergSimXc0, IcebergSimXd0] = sIcebergSimParam(conf.TS_iceberg, conf);
                case 'sMultibeamReader'
                    [MultibeamReaderParam, MultibeamReaderFname, MultibeamReaderXc0, MultibeamReaderXd0] = sMultibeamReaderParam(conf.TS_mbeReader, conf);
            end
        end
    catch ex
        if(conf.DUNE)
            dos('taskkill /im dune.exe');
        end
        for i=1:length(ex.stack)
            fprintf('ERROR: Simulation failed. Exception: "%s" on line %i in file %s.\n'...
                , ex.message, ex.stack(i).line, ex.stack(i).file);
        end
        simReturn = struct();
        cd(conf.rootDir);
        return;
    end 

    %% Copy necessary build files to build-folder
    try
        copyBuildFiles(modules, conf.sfunctionDir, conf.buildDir, conf.debugOn);
        cd(conf.rootDir);
    catch ex
        if(conf.DUNE)
            dos('taskkill /im dune.exe');
        end
        for i=1:length(ex.stack)
            fprintf('ERROR: Simulation failed. Exception: "%s" on line %i in file %s.\n'...
                , ex.message, ex.stack(i).line, ex.stack(i).file);
        end
        simReturn = struct();
        cd(conf.rootDir);
        return;
    end

    %% Run simulation   
    cd(conf.buildDir);
    options = simset();
    options.SrcWorkspace = 'current';
    options.DstWorkspace = 'current';
    options.OutputVariables = 'txy';
    
    fprintf('INFO: Running simulation...\n');

    % Simulation
%      try
        if conf.runSimulation
            simOut = sim(conf.mdlRoot, conf.T, options);
            pause(5);
            if(conf.DUNE)
                dos('taskkill /im dune.exe');
            end
        end
%     catch ex
%         if(conf.DUNE)
%             dos('taskkill /im dune.exe');
%         end
%         for i=1:length(ex.stack)
%             fprintf('ERROR: Simulation failed. Exception: "%s" on line %i in file %s.\n'...
%                 , ex.message, ex.stack(i).line, ex.stack(i).file);
%         end
%         simReturn = struct();
%         cd(conf.rootDir);
%         return;
%     end

    clear DvlMbeSimParam DvlMbeSimFname DvlMbeSimXc0 DvlMbeSimXd0;
    clear IcebergSLAMParam IcebergSLAMFname IcebergSLAMXc0 IcebergSLAMXd0;
    clear IcebergSimParam IcebergSimFname IcebergSimXc0 IcebergSimXd0;
    %% Extract states
    res.simElapsedTime = simOut.getSimulationMetadata.TimingInfo.ExecutionElapsedWallTime;
    
    % Sim time
    res.t = simOut.tout;
    if(~isempty(simOut.find('WS_x_dot')))
        res.ts = simOut.get('WS_x_dot').time;
        res.x_dot = simOut.get('WS_x_dot').signals.values;
    end
    if(~isempty(simOut.find('WS_eta')))
        res.eta = simOut.get('WS_eta').signals.values;
    end
    if(~isempty(simOut.find('WS_nu')))
        res.nu = simOut.get('WS_nu').signals.values;
    end
    if(~isempty(simOut.find('WS_tau')))
        res.tau = simOut.get('WS_tau').signals.values;
    end
    if(~isempty(simOut.find('WS_rudder_angle')))
        res.rudder_angle = simOut.get('WS_rudder_angle').signals.values;
    end
    if(~isempty(simOut.find('WS_fin_angle')))
        res.fin_angle = simOut.get('WS_fin_angle').signals.values;
    end
    if(~isempty(simOut.find('WS_rpm')))
        res.rpm_motor = simOut.get('WS_rpm').signals.values;
    end
    if(~isempty(simOut.find('WS_rpm_feedforward')))
        res.rpm_feedforward = simOut.get('WS_rpm_feedforward').signals.values;
    end

    % DUNE time
    if(conf.DUNE && ~isempty(simOut.find('WS_dune_t')))
        res.td = simOut.get('WS_dune_t').time; % T DUNE
        res.dune_timestamps = simOut.get('WS_dune_t').signals.values;
    end

    % Guidance time
    if(~isempty(simOut.find('WS_psi_r')))
        res.tg = simOut.get('WS_psi_r').time;
        res.psi_r = simOut.get('WS_psi_r').signals.values;
    end
    if(~isempty(simOut.find('WS_z_r')))
        res.z_r = simOut.get('WS_z_r').signals.values;
    end
    if(~isempty(simOut.find('WS_U_r')))
        res.U_r = simOut.get('WS_U_r').signals.values;
    end

    % Observer time
    if(~isempty(simOut.find('WS_x_hat')))
        res.to = simOut.get('WS_x_hat').time;
        res.x_hat = simOut.get('WS_x_hat').signals.values;
    end

    % High-level controller time
    if(~isempty(simOut.find('WS_z_d')))
        res.th = simOut.get('WS_z_d').time; % T high level
        res.z_d = simOut.get('WS_z_d').signals.values;
    end
    if(~isempty(simOut.find('WS_z_error')))
        res.z_error = simOut.get('WS_z_error').signals.values;
    end
    if(~isempty(simOut.find('WS_theta_r')))
        res.theta_r = simOut.get('WS_theta_r').signals.values;
    end
    if(~isempty(simOut.find('WS_theta_d')))
        res.theta_d = simOut.get('WS_theta_d').signals.values;
    end

    % Low-level controller time
    if(~isempty(simOut.find('WS_u')))
        res.tl = simOut.get('WS_u').time;
        res.u = simOut.get('WS_u').signals.values;
    end
    if(~isempty(simOut.find('WS_U_d')))
        res.U_d = simOut.get('WS_U_d').signals.values;
    end
    if(~isempty(simOut.find('WS_U_error')))
        res.U_error = simOut.get('WS_U_error').signals.values;
    end
    if(~isempty(simOut.find('WS_theta_error')))
        res.theta_error = simOut.get('WS_theta_error').signals.values;
    end
    if(~isempty(simOut.find('WS_psi_d')))
        res.psi_d = simOut.get('WS_psi_d').signals.values;
    end
    if(~isempty(simOut.find('WS_psi_error')))
        res.psi_error = simOut.get('WS_psi_error').signals.values;
    end

    %% Saving
    % Save config
    save([conf.saveDir, 'conf.mat'], 'conf');
    
    % Save result and simOut
    outInfo = whos('simOut');
    if(outInfo.bytes/1024^3 > 1.9)
        save([conf.saveDir, 'auvsim.mat'], '-struct', 'res', '-v7.3');
    else
        save([conf.saveDir, 'auvsim.mat'], '-struct', 'res');
    end
    save([conf.saveDir, 'auvsim.mat'], 'simOut', '-append');
    clear simOut res solver;

    % Save rest of workspace
    clear i;
    auvsim_workspace = ws2struct();
    save([conf.saveDir, '\auvsim.mat'], 'auvsim_workspace', '-append');
    clear auvsim_workspace;

    % Return
    simReturn = load([conf.saveDir, 'auvsim.mat']);
    cd(conf.rootDir);
    fprintf('INFO: Program end: %s\n', conf.name);
end
