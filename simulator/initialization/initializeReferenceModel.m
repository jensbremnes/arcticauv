function referenceStruct = initializeReferenceModel()
% initializeReferenceModel() saves a struct with the parameters for the 
% guidance system.
%
%   referenceStruct = initializeReferenceModel()
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2013.09.26
%

    %% System parameters
    
    psi_rate_max = 0.28;	% 0.18
    psi_acc_max = 1000;     % 1000
    psi_damping = 1.0;      % 1.0
    psi_frequency = 1.5;    % 0.8
    
    theta_rate_max = 10000;   % 
    theta_acc_max = 1000;     % 
    theta_damping = 1.0;      % 
    theta_frequency = 1.5;    % 
    
    heave_vel_max = 10000;    % 0.5 1.1
    heave_acc_max = 10000;   % 1000
    z_damping = 1.0;        % 1.0 0.8
    z_frequency = 0.8;      % 0.2
    
    surge_acc_max = 10000;   % 1000
    surge_jerk_max = 10000;  % 1000
    surge_damping = 1.9;      % 1.0
    surge_frequency = 1.0;    % 2.0 1.0 
    
    referenceStruct.psi_rate_max = psi_rate_max;
    referenceStruct.psi_acc_max = psi_acc_max;
    referenceStruct.psi_damping = psi_damping;
    referenceStruct.psi_frequency = psi_frequency;
    
    referenceStruct.theta_rate_max = theta_rate_max;
    referenceStruct.theta_acc_max = theta_acc_max;
    referenceStruct.theta_damping = theta_damping;
    referenceStruct.theta_frequency = theta_frequency;
    
    referenceStruct.heave_vel_max = heave_vel_max;
    referenceStruct.heave_acc_max = heave_acc_max;
    referenceStruct.z_damping = z_damping;
    referenceStruct.z_frequency = z_frequency;
    
    referenceStruct.surge_acc_max = surge_acc_max;
    referenceStruct.surge_jerk_max = surge_jerk_max;
    referenceStruct.surge_damping = surge_damping;
    referenceStruct.surge_frequency = surge_frequency;

end