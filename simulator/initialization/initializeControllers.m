function controllerStruct = initializeControllers()
% initializeControllers() saves a struct with the parameters for the 
% control system.
%
%   controllerStruct = initializeControllers()
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2013.09.26
%
    %% Low-level controllers

    % Pitch
    K_p_theta = 1.2;        % 1.0
    K_d_theta = 1.5;        % 1.2
    K_i_theta = 0.50;       % 0.15
    i_max_theta = 0.75;     % 0.60
    omega_theta_dot = 0.0;

    % Shaft speed
    shaftRev2SpeedLookup = [0, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1350, 1500; ...
                            0.0, 0.4326, 1.1, 1.384, 1.608, 1.826, 2.04, 2.251, 2.46, 2.774, 3.085]';
    K_p_U = 300.0;           % 75 
    K_i_U = 100.0;           % 50
    K_f_U = 1.0;
    i_max_U = 100;         % 1000

    % Heading
    K_p_psi = 1.6;          % 1.2
    K_d_psi = 1.5;          % 1.2
    K_i_psi = 0.2;         % 0.20
    i_max_psi = 0.50;       % 0.05
    omega_psi_dot = 0.0;
    psi_integrator_enable = 1000;

    %% High-level Controllers

    % Depth
    K_p_z = 0.10;           % 0.05
    K_i_z = 0.01;          % 0.003
    i_max_z = 4.5;         % 10

    %% Saturation
    prop_n_max = 2500;              % [RPM]
    U_max = 2.57;                   % [m/s]
    delta_r_max = deg2rad(30.0);    % [rad]
    delta_s_max = deg2rad(13.6);    % [rad]
    theta_max = deg2rad(25);        % [rad]
    
    %% Low pass filters for outputs
    delta_s_Ti = 1.0;       % 0.2
    delta_r_Ti = 1.0;       % 0.2
    prop_n_Ti = 1.0;        % 0.0

    %% Save struct
    controllerStruct.K_p_theta = K_p_theta;
    controllerStruct.K_d_theta = K_d_theta;
    controllerStruct.K_i_theta = K_i_theta;
    controllerStruct.i_max_theta = i_max_theta;
    controllerStruct.K_p_U = K_p_U;
    controllerStruct.K_i_U = K_i_U;
    controllerStruct.K_f_U = K_f_U;
    controllerStruct.i_max_U = i_max_U;
    controllerStruct.K_p_psi = K_p_psi;
    controllerStruct.K_d_psi = K_d_psi;
    controllerStruct.K_i_psi = K_i_psi;
    controllerStruct.i_max_psi = i_max_psi;
    controllerStruct.psi_integrator_enable = psi_integrator_enable;
    controllerStruct.K_p_z = K_p_z;
    controllerStruct.K_i_z = K_i_z;
    controllerStruct.i_max_z = i_max_z;
    controllerStruct.prop_n_max = prop_n_max;
    controllerStruct.delta_r_max = delta_r_max;
    controllerStruct.delta_s_max = delta_s_max;
    controllerStruct.theta_max = theta_max;
    controllerStruct.prop_n_Ti = prop_n_Ti;
    controllerStruct.delta_r_Ti = delta_r_Ti;
    controllerStruct.delta_s_Ti = delta_s_Ti;
    controllerStruct.U_max = U_max;
    controllerStruct.shaftRev2SpeedLookup = shaftRev2SpeedLookup;
    controllerStruct.omega_theta_dot = omega_theta_dot;
    controllerStruct.omega_psi_dot = omega_psi_dot;
end