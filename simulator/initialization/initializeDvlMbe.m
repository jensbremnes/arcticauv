function dvlMbeStruct = initializeDvlMbe(isMbeUpLooking)
% dvlMbeStruct saves a struct with the parameters for the noise of the
% sensors
%
%   dvlMbeStruct = initializeDvlMbe(isMbeUpLooking)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.10.07
%    

    % MBE
    dvlMbeStruct.nMbeBeams = 120;                       % [-]
    dvlMbeStruct.mbeSectorRadians = 120*pi/180;         % [rad]
    dvlMbeStruct.isMbeUpLooking = isMbeUpLooking;       % [0,1]
    dvlMbeStruct.mbeLeverArm = [0, 0, 0]';              % [m]
    
    % DVL
    dvlMbeStruct.dvlBeamAngleRadians = 20*pi/180;       % [rad]
    dvlMbeStruct.dvlLeverArm = [0, 0, 0]';              % [m]
end