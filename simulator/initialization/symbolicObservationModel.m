syms Ex Ey Ez x_r y_r z phi theta psi_r dir real;

eta_rel = [x_r; y_r; z];
E_i = [Ex; Ey; Ez];

bad = Rzyx(phi, theta, psi_r)'*diag([1, 1, dir])*(E_i - eta_rel);

h = [sqrt(bad(1)^2 + bad(2)^2 + bad(3)^2); atan(bad(2)/bad(3)); atan(bad(1)/bad(3))];
H = jacobian(h, Ez)