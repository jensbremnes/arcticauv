function noiseStruct = initializeNoise(auvNoiseOn, mbeNoiseOn, dvlUpNoiseOn, dvlDownNoiseOn)
% initializeNoise saves a struct with the parameters for the noise of the
% sensors
%
%   noiseStruct = initializeNoise(auvNoiseOn, mbeNoiseOn, dvlUpNoiseOn, dvlDownNoiseOn)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.02.07
%    
%% AUV measurements
    xPosMeanNoise = 1;
    yPosMeanNoise = 1;
    zPosMeanNoise = 0.01;
    rollMeanNoise = 0.001;
    pitchMeanNoise = 0.001;
    yawMeanNoise = 0.001;
    surgeMeanNoise = 1;
    swayMeanNoise = 1;
    heaveMeanNoise = 0.1;
    rollRateMeanNoise = 0.001;
    pitchRateMeanNoise = 0.001;
    yawRateMeanNoise = 0.001;
    
    xPosVarNoise = 1;
    yPosVarNoise = 1;
    zPosVarNoise = 0.01;
    rollVarNoise = 0.001;
    pitchVarNoise = 0.001;
    yawVarNoise = 0.001;
    surgeVarNoise = 1;
    swayVarNoise = 1;
    heaveVarNoise = 0.1;
    rollRateVarNoise = 0.001;
    pitchRateVarNoise = 0.001;
    yawRateVarNoise = 0.001;
    
    auvMeanNoise = [xPosMeanNoise, yPosMeanNoise, zPosMeanNoise, rollMeanNoise, pitchMeanNoise, yawMeanNoise, surgeMeanNoise, swayMeanNoise, heaveMeanNoise, rollRateMeanNoise, pitchRateMeanNoise, yawRateMeanNoise]';
    auvVarNoise = [xPosVarNoise, yPosVarNoise, zPosVarNoise, rollVarNoise, pitchVarNoise, yawVarNoise, surgeVarNoise, swayVarNoise, heaveVarNoise, rollRateVarNoise, pitchRateVarNoise, yawRateVarNoise]';

%% Range measurements

    % MBE
    mbeMeanNoise = 0;       % [m]
    mbeVarNoise = 1;       % [m]  

    % DVL up
    dvlUpMeanNoise = 0;     % [m]
    dvlUpVarNoise = 1;      % [m]
    
    % DVL down
    dvlDownMeanNoise = 0;   % [m]
    dvlDownVarNoise = 1;    % [m]

    %% Save struct
    noiseStruct.mbeNoiseOn = logical(mbeNoiseOn);
    noiseStruct.mbeMeanNoise = mbeMeanNoise;
    noiseStruct.mbeVarNoise = mbeVarNoise;
    noiseStruct.dvlUpNoiseOn = logical(dvlUpNoiseOn);
    noiseStruct.dvlUpMeanNoise = dvlUpMeanNoise;
    noiseStruct.dvlUpVarNoise = dvlUpVarNoise;
    noiseStruct.dvlDownNoiseOn = logical(dvlDownNoiseOn);
    noiseStruct.dvlDownMeanNoise = dvlDownMeanNoise;
    noiseStruct.dvlDownVarNoise = dvlDownVarNoise;
    noiseStruct.auvNoiseOn = auvNoiseOn;
    noiseStruct.auvMeanNoise = auvMeanNoise;
    noiseStruct.auvVarNoise = auvVarNoise;
end