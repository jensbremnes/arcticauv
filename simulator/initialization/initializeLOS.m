function guidanceStruct = initializeLOS()
% initializeLOS() saves a struct with the parameters for the 
% LOS guidance system.
%
%   guidanceStruct = initializeLOS()
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    K_p = 0.05;
    K_i = 0.001;
    R_acc = 10;
    
    guidanceStruct.K_p = K_p;
    guidanceStruct.K_i = K_i;
    guidanceStruct.R_acc = R_acc;
end