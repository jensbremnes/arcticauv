function auvStruct = initializeAuvModel()
% initializeAuvModel() saves a struct with the parameters for the 
% AUV model.
%
%   auvStruct = initializeAuvModel()
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2013.09.24
%

    %% System parameters
    W = 299;                                        % [N]
    B = 306;                                        % [N]
    g = 9.81;                                       % [m/s^2]
    r_b = [0, 0 0]';                                % [m] CO at CB
    r_g = [0, 0, 0.0196]';                          % [m]
    m = W/g;
    I_bb = diag([0.177, 3.45, 3.45]);               % [kg*m^2]
    
    % Hydrodynamic parameters
    X_ud = -0.930;
    Y_vd = -35.5;
    K_pd = -0.0704;
    M_qd = -4.88;
    M_wd = -1.93;
    Z_wd = Y_vd;
    Z_qd = M_wd;
    N_vd = -M_wd;
    Y_rd = N_vd;
    N_rd = M_qd;
    
    X_uu = -1.62;
    Y_vv = -1310;
    M_ww = 3.18;
    Y_rr = 0.632;
    M_qq = -188;
    K_pp = -0.13;
    Z_ww = Y_vv;
    N_vv = -M_ww;
    Z_qq = -Y_rr;
    N_rr = M_qq;
    
    % Propulsion and rudder/fin parameters
    Y_uudr = 9.64;
    Z_uuds = -9.64;
    Y_urf = 6.15;
    M_uuds = -6.15;
    N_uudr = -6.15;
    M_uqf = -3.93;
    %V_max = knots2ms(5.6);
    V_max = convvel(5.6, 'kts', 'm/s');
    
    K_T = 2.5075;
    K_Q = 0.32033;
    D_prop = 0.1397;
    rho_w = 1025;
    
    %% Actuator dynamics
    finAngleMaxRad = deg2rad(13.6);
    finRateMax = deg2rad(10.0);
    finTimeConstant = 0.01;
    
    rudderAngleMaxRad = deg2rad(30.0);
    rudderRateMax = deg2rad(10.0);
    rudderTimeConstant = 0.01;
    
    propellerMaxRpm = 2400;
    propellerMaxRate = 200;
    propellerTimeConstant = 0.5;
    
    %% Construct system matrices
    M_RB = [m*eye(3), -m*Smtrx(r_g); m*Smtrx(r_g),  I_bb - m*Smtrx(r_g)^2];
            
    M_A = -diag([X_ud, Y_vd, Z_wd, K_pd, M_qd, N_rd]);
    M_A(5,3) = -M_wd;
    M_A(6,2) = -N_vd;
    M_A(3,5) = -Z_qd;
    M_A(2,6) = -Y_rd;
    
    syms u_r v_r w_r p q r real;
    nu_r = [u_r; v_r; w_r; p; q; r];
    
    D = -diag(  [X_uu*abs(nu_r(1)), Y_vv*abs(nu_r(2)), Z_ww*abs(nu_r(3)), ...
                    K_pp*abs(nu_r(4)), M_qq*abs(nu_r(5)), N_rr*abs(nu_r(6))] );
    D(5,3) = -M_ww*abs(nu_r(3));
    D(6,2) = -N_vv*abs(nu_r(2));
    D(3,5) = -Z_qq*abs(nu_r(5));
    D(2,6) = -Y_rr*abs(nu_r(6));
        
    matlabFunction(D, 'file', 'Dmtrx', 'vars', {nu_r});
    %% Store struct
    auvStruct.M_RB = M_RB;
    auvStruct.M_A = M_A;
    %auvStruct.Dp = Dp;
    auvStruct.r_b = r_b;
    auvStruct.r_g = r_g;
    auvStruct.W = W;
    auvStruct.B = B;

    auvStruct.Y_uudr = Y_uudr;
    auvStruct.Z_uuds = Z_uuds;
    auvStruct.Y_urf = Y_urf;
    auvStruct.M_uudr = M_uuds;
    auvStruct.N_uudr = N_uudr;
    auvStruct.M_uqf = M_uqf;
    auvStruct.V_max = V_max;
    
    auvStruct.K_T = K_T;
    auvStruct.K_Q = K_Q;
    auvStruct.D_prop = D_prop;
    auvStruct.rho_w = rho_w;
    
    auvStruct.finAngleMaxRad = finAngleMaxRad; 
    auvStruct.finRateMax = finRateMax;
    auvStruct.finTimeConstant = finTimeConstant;
    
    auvStruct.rudderAngleMaxRad = rudderAngleMaxRad;
    auvStruct.rudderRateMax = rudderRateMax;
    auvStruct.rudderTimeConstant = rudderTimeConstant;
    
    auvStruct.propellerMaxRpm = propellerMaxRpm;
    auvStruct.propellerMaxRate = propellerMaxRate;
    auvStruct.propellerTimeConstant = propellerTimeConstant;
    
    auvStruct.param.W = W;
    auvStruct.param.B = B; 
    auvStruct.param.g = g;
    auvStruct.param.r_b = r_b;
    auvStruct.param.r_g = r_g;
    auvStruct.param.m = m;
    auvStruct.param.I_bb = I_bb;
    auvStruct.param.X_ud = X_ud;
    auvStruct.param.Y_vd = Y_vd;
    auvStruct.param.K_pd = K_pd;
    auvStruct.param.M_qd = M_qd;
    auvStruct.param.M_wd = M_wd;
    auvStruct.param.Z_wd = Z_wd;
    auvStruct.param.Z_qd = Z_qd;
    auvStruct.param.N_vd = N_vd;
    auvStruct.param.Y_rd = Y_rd;
    auvStruct.param.N_rd = N_rd;
    auvStruct.param.X_uu = X_uu;
    auvStruct.param.Y_vv = Y_vv;
    auvStruct.param.M_ww = M_ww;
    auvStruct.param.Y_rr = Y_rr;
    auvStruct.param.M_qq = M_qq;
    auvStruct.param.K_pp = K_pp;
    auvStruct.param.Z_ww = Z_ww;
    auvStruct.param.N_vv = N_vv;
    auvStruct.param.Z_qq = Z_qq;
    auvStruct.param.N_rr = N_rr;
    auvStruct.param.Y_uudr = Y_uudr;
    auvStruct.param.Z_uuds = Z_uuds;
    auvStruct.param.Y_urf = Y_urf;
    auvStruct.param.M_uuds = M_uuds;
    auvStruct.param.N_uudr = N_uudr;
    auvStruct.param.M_uqf = M_uqf;

%% Testing
% clear all;
% syms x y z phi theta psii u v w p q r real;
% syms tau_u tau_v tau_w tau_p tau_q tau_r real;
% syms z_g I_x I_y I_z m W B real;
% syms X_ud Y_vd Y_rd Z_wd Z_qd K_pd M_wd M_qd N_vd N_rd real;
% syms X_uu Y_vv Z_ww K_pp M_qq N_rr M_ww N_vv Z_qq Y_rr real; 
% syms u_r v_r w_r p q r real;
% 
% eta = [x y 0 0 0 psii]';
% nu = [u v 0 0 0 r]';
% nu_r = [u_r v_r 0 0 0 r]';
% tau = [tau_u tau_v 0 0 0 tau_r]';
% 
% r_g = [0, 0, z_g]';
% r_b = [0, 0, 0]';
% I_g = diag([I_x, I_y, I_z]);
% M_RB_CO = [m*eye(3), zeros(3,3); zeros(3,3), I_g];
% M_RB = Hmtrx(r_g)'*M_RB_CO*Hmtrx(r_g);
% M_A_CO = [-X_ud, 0, 0, 0, 0, 0; 0, -Y_vd, 0, 0, 0, -Y_rd; 0, 0, -Z_wd, 0, -Z_qd, 0; 0, 0, 0, -K_pd, 0, 0; 0, 0, -M_wd, 0, -M_qd, 0; 0, -N_vd, 0, 0, 0, -N_rd];
% M_A = Hmtrx(r_g)'*M_A_CO*Hmtrx(r_g);
% M = M_RB + M_A;
% C_RB = m2c(M_RB, nu);
% C_A = m2c(M_A, nu_r);
% 
% D_L_CO = zeros(6,6);
% D_L = Hmtrx(r_g)'*D_L_CO*Hmtrx(r_g);
% D_N_CO = -diag(  [X_uu*abs(nu_r(1)), Y_vv*abs(nu_r(2)), Z_ww*abs(nu_r(3)), ...
%                     K_pp*abs(nu_r(4)), M_qq*abs(nu_r(5)), N_rr*abs(nu_r(6))] );
% D_N_CO(5,3) = -M_ww*abs(nu_r(3));
% D_N_CO(6,2) = -N_vv*abs(nu_r(2));
% D_N_CO(3,5) = -Z_qq*abs(nu_r(5));
% D_N_CO(2,6) = -Y_rr*abs(nu_r(6));
% 
% D_N = Hmtrx(r_g)'*D_N_CO*Hmtrx(r_g);
% D = D_L + D_N;
% gv = gvect(W, B, eta(5), eta(4), r_g, r_b);
% 
% eta_dot = eulerang(eta(4), eta(5), eta(6))*nu;
% nu_dot = M\(tau - C_RB*nu - C_A*nu_r - D*nu_r - gv);
% 
% % 3DOF
% eta_3DOF = [x, y, psii]';
% nu_3DOF = [u, v, r]';
% nu_r_3DOF = [u_r, v_r, r]';
% tau_3DOF = [tau_u, tau_v, tau_r]';
% 
% M_RB_3DOF = [M_RB(1,1), M_RB(1,2) M_RB(1,6); M_RB(2,1), M_RB(2,2) M_RB(2,6); M_RB(6,1), M_RB(6,2) M_RB(6,6)];
% M_A_3DOF = [M_A(1,1), M_A(1,2) M_A(1,6); M_A(2,1), M_A(2,2) M_A(2,6); M_A(6,1), M_A(6,2) M_A(6,6)];
% 
% D_3DOF = [D(1,1), D(1,2) D(1,6); D(2,1), D(2,2) D(2,6); D(6,1), D(6,2) D(6,6)];      
% 
% M_3DOF = M_RB_3DOF + M_A_3DOF;
% C_RB_3DOF = [0, 0, -m*v; 0, 0, m*u; m*v, -m*u, 0];
% C_A_3DOF = [0, 0, Y_vd*v_r + Y_rd*r; 0, 0, -X_ud*u_r; -Y_vd*v_r - Y_rd*r, X_ud*u_r, 0];
% gvtmp = gvect(W, B, 0, 0, r_g, r_b);
% gv_3DOF = [gvtmp(1), gvtmp(2), gvtmp(6)]';
% 
% eta_dot_3DOF = Rzyx(0, 0, eta_3DOF(3))*nu_3DOF;
% nu_dot_3DOF = M_3DOF\(tau_3DOF - C_RB_3DOF*nu_3DOF - C_A_3DOF*nu_r_3DOF - D_3DOF*nu_r_3DOF - gv_3DOF);

end