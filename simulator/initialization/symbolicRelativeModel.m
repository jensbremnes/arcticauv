syms x_r y_r psi_r v_r u_r r_r x_i y_i psi_i v_i u_i r_i real;
syms tau_u tau_v tau_psi real;
eta_rel = [x_r; y_r; psi_r];
nu_rel = [u_r; v_r; r_r];
eta_ice = [x_i; y_i; psi_i];
nu_ice = [u_i; v_i; r_i];
tau = [tau_u; tau_v; tau_psi];

eta_ice_dot = Rzyx(0, 0, psi_i)*nu_ice;
nu_ice_dot  = [0; 0; 0];
eta_rel_dot = Rzyx(0, 0, psi_r)*nu_rel - Smtrx([0; 0; r_i])*eta_rel;
nu_rel_dot = tau + Smtrx([0;0;r_r])*Rzyx(0, 0, psi_r)'*nu_ice - Rzyx(0, 0, psi_r)'*nu_ice_dot;

x = [eta_rel; nu_rel; eta_ice; nu_ice];
x_dot = [eta_rel_dot; nu_rel_dot; eta_ice_dot; nu_ice_dot];
J = jacobian(x_dot, x);

% C = sym(zeros(9,12));
% C(1:3, 1:3) = Rzyx(0,0, psi_i);
% C(1:3, 7:9) = eye(3);
% C(4:6, 4:6) = eye(3);
% C(4:6, 10:12) = diag([0, 0, 1])*Rzyx(0, 0, psi_r)';
% C(7:9, 7:9) = eye(3);

h1 = Rzyx(0,0, psi_i)*eta_rel + eta_ice;
h2 = nu_rel + diag([0, 0, 1])*Rzyx(0, 0, psi_r)'*nu_ice;
h3 = eta_ice;

h = [h1; h2; h3];
H = jacobian(h, x);

psi_ice_vec = [0, pi/2, pi];
u_ice_vec = [0, 0.1];
v_ice_vec = [0, 0.15];
r_ice_vec = [0, 0.002];

x_rel_vec = [0, 1];
y_rel_vec = [0, 3];
psi_rel_vec = [0, pi/2, pi];
u_rel_vec = [0, 1.2];
v_rel_vec = [0, 0.6];
r_rel_vec = [0, 0.001];

% count = 0;
% for x_rel = x_rel_vec
%     for y_rel = y_rel_vec
%         for psi_rel = psi_rel_vec
%             for psi_ice = psi_ice_vec
%                 for u_rel = u_rel_vec
%                     for v_rel = v_rel_vec
%                         for r_rel = r_rel_vec
%                             for u_ice = u_ice_vec
%                                 for v_ice = v_ice_vec
%                                     for r_ice = r_ice_vec
%                                         count = count + 1;
%                                         x_val = [x_rel; y_rel; psi_rel; u_rel; v_rel; r_rel; 0; 0; psi_ice; u_ice; v_ice; r_ice];
%                                         A = subs(J, x, x_val);
%                                         C = subs(H, x, x_val);
%                                         OB = obsv(A, C);
%                                         rnk = rank(OB);
%                                         if(rnk < 12)
%                                             error('Rank is: %f\n', rnk);
%                                         end
%                                         disp(count);
%                                     end
%                                 end
%                             end
%                         end
%                     end
%                 end
%             end
%         end
%     end
% end