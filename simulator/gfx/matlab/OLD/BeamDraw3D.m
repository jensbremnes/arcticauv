classdef BeamDraw3D < handle
% BeamDraw3D is a class that contains methods necessary for drawing and
% updating a 3D Beam line object.
%
%   References: Code written by MSc student Sigurd Holsen.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2014.12.12
%  

	properties (SetAccess = private)
        mbeStruct
        mbeLineHandles
        mbePointHandles
        beamThickness
    end

	methods
        function obj = BeamDraw3D(mbeStruct, beamThickness)
            obj.mbeStruct = mbeStruct;
            obj.mbeLineHandles = gobjects(mbeStruct.nMbeBeams, 1);
            obj.mbePointHandles = gobjects(mbeStruct.nMbeBeams, 1);
            obj.beamThickness = beamThickness;
        end
        
        function createBeams(obj, eta0, mbeRanges0)
            
            [mbeEta, mbeBeamEndPoints] =  getMbeBeamPoints3D(obj.mbeStruct, eta0, mbeRanges0);
            
            for i = 1:obj.mbeStruct.nMbeBeams
                if(sum(isnan(mbeBeamEndPoints(:, i))) == 0)
                    obj.mbeLineHandles(i) = line('XData', [mbeEta(1), mbeBeamEndPoints(1, i)], 'YData', [mbeEta(2), mbeBeamEndPoints(2, i)], 'ZData', [mbeEta(3), mbeBeamEndPoints(3, i)], 'Color', 'r', 'LineWidth', obj.beamThickness);
                    obj.mbePointHandles(i) = line('XData', mbeBeamEndPoints(1, i), 'YData', mbeBeamEndPoints(2, i), 'ZData', mbeBeamEndPoints(3, i), 'Color', 'r', 'Marker', '*');
                else
                    obj.mbeLineHandles(i) = line('XData', [], 'YData', [], 'ZData', [], 'Color', 'r', 'LineWidth', obj.beamThickness);
                    obj.mbePointHandles(i) = line('XData', [], 'YData', [], 'ZData', [], 'Color', 'r', 'Marker', '*');
                end
            end
            
            obj.update(eta0, mbeRanges0);
        end
        
        function update(obj, eta, mbeRanges)

            [mbeEta, mbeBeamEndPoints] =  getMbeBeamPoints3D(obj.mbeStruct, eta, mbeRanges);
            
            for i = 1:obj.mbeStruct.nMbeBeams
                if(sum(isnan(mbeBeamEndPoints(:, i))) == 0)
                    set(obj.mbeLineHandles(i), 'XData', [mbeEta(1), mbeBeamEndPoints(1, i)], 'YData', [mbeEta(2), mbeBeamEndPoints(2, i)], 'ZData', [mbeEta(3), mbeBeamEndPoints(3, i)]);
                    set(obj.mbePointHandles(i), 'XData', mbeBeamEndPoints(1, i), 'YData', mbeBeamEndPoints(2, i), 'ZData', mbeBeamEndPoints(3, i));
                else
                    set(obj.mbeLineHandles(i), 'XData', [], 'YData', [], 'ZData', []);
                    set(obj.mbePointHandles(i), 'XData', [], 'YData', [], 'ZData', []);
                end
            end
            
        end
    end % End methods
end % End classdef