classdef BeamDraw2D < handle
% BeamDraw2D is a class that contains methods necessary for drawing and
% updating a 2D beam patch of the beam ranges.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2014.12.17
%    Revision:      2016.02.04 PN Updated when converted to new structure.
%  

	properties (SetAccess = private)
        mbeStruct
        mbeLineHandles
        mbePointHandles
        beamThickness
        timeTickHandle
    end

	methods
        function obj = BeamDraw2D(mbeStruct, beamThickness)
            obj.mbeStruct = mbeStruct;
            obj.mbeLineHandles = gobjects(mbeStruct.nMbeBeams, 1);
            obj.mbePointHandles = gobjects(mbeStruct.nMbeBeams, 1);
            obj.beamThickness = beamThickness;
            obj.timeTickHandle = gobjects(0);
        end
        
        function createBeams(obj, eta0, mbeRanges0, t0)
            
            [mbeEta, beamEndPoints] =  getMbeBeamPoints2D(obj.mbeStruct, eta0, mbeRanges0);
            
            obj.timeTickHandle = annotation('textbox', [0.85,0.88,0.1,0.1], 'String', sprintf('t = %.1f', t0));
            
            for i = 1:obj.mbeStruct.nMbeBeams
                if(sum(isnan(beamEndPoints(:, i))) == 0)
                    obj.mbeLineHandles(i) = line('XData', [0, beamEndPoints(1, i)], 'YData', [mbeEta(3), beamEndPoints(2, i)], 'Color', 'r', 'LineWidth', obj.beamThickness);
                    obj.mbePointHandles(i) = line('XData', beamEndPoints(1, i), 'YData', beamEndPoints(2, i), 'Color', 'r', 'Marker', '*');
                else
                    obj.mbeLineHandles(i) = line('XData', [], 'YData', [], 'Color', 'r', 'LineWidth', obj.beamThickness);
                    obj.mbePointHandles(i) = line('XData', [], 'YData', [], 'Color', 'r', 'Marker', '*');
                end
            end
            obj.update(eta0, mbeRanges0, t0);
        end
        
        function update(obj, eta, mbeRanges, t)
            
            [mbeEta, beamEndPoints] =  getMbeBeamPoints2D(obj.mbeStruct, eta, mbeRanges);
            
            obj.timeTickHandle.String = sprintf('t = %.1f', t);
            
            for i = 1:obj.mbeStruct.nMbeBeams
                if(sum(isnan(beamEndPoints(:, i))) == 0)
                    set(obj.mbeLineHandles(i), 'XData', [0, beamEndPoints(1, i)], 'YData', [mbeEta(3), beamEndPoints(2, i)]);
                    set(obj.mbePointHandles(i), 'XData', beamEndPoints(1, i), 'YData', beamEndPoints(2, i));
                else
                    set(obj.mbeLineHandles(i), 'XData', [], 'YData', []);
                    set(obj.mbePointHandles(i), 'XData', [], 'YData', []);
                end
            end
        end
        
    end % End methods
end % End classdef

