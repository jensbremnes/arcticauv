classdef AUVDraw3D < handle
% AUVDraw3D is a class that contains methods necessary for drawing and
% updating a 3D AUV patch object.
%
%   References: Code written by MSc student Sigurd Holsen.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2014.12.12
%  

	properties (SetAccess = private)
        ptc
        scale = 1
        auvBodyVertices
        auvBodyFaces
        figN
        transformedVertices
    end

	methods
        function createAuv(obj, scale, eta0, zoom, rotOffset)
            
            obj.createAuvPoints(scale);
            obj.ptc = patch('Vertices', obj.auvBodyVertices', 'Faces', obj.auvBodyFaces, 'FaceColor', 'y', 'EdgeColor', 'none');
            camproj('perspective');
            set(gca,'CameraViewAngleMode','manual');
            camva(1/zoom);
            obj.update(eta0, rotOffset);
        end
        
        function update(obj, eta, rotationOffset)
            
            obj.transformAuvPoints(eta);
            set(obj.ptc, 'Vertices', obj.transformedVertices');
            
            camVOffset = -20;
            camHOffset = 1000;
            
            campos([eta(1) - camHOffset*cos(eta(6) + rotationOffset), eta(2) - camHOffset*sin(eta(6) + rotationOffset), eta(3) - camVOffset]);
            camtarget([eta(1), eta(2), eta(3)]);     
        end

        function transformAuvPoints(obj, eta)
                                    
            [~, R, ~] = eulerang(eta(4,1), eta(5,1), eta(6,1));
            
            for j = 1:obj.figN
                obj.transformedVertices(1:3, j) = eta(1:3,1) + R * obj.auvBodyVertices(:,j);
            end
        end
        
        function createAuvPoints(obj, scale)
            L = 1;
            W = 0.3;
            W2 = 0.2;
            AuvMain = [ 0,    L,    L,     0,     0,      L,      L,     0,     0,    L,    L,     0,     0,      L,      L,      0;
                        W,    W,    W2,    W2,    0,      0,      -W2,   -W2,   -W,   -W,   W2,    W2,    0,      0,      -W2,   -W2;
                        0,    0,    W2,    W2,    W,      W,      W2,    W2,    0,    0,    -W2,   -W2,   -W,    -W,      -W2,   -W2];
            L2 = 1.1;
            L3 = 0.1;
            W3 = 0.15;
            AuvFront = [L2,    L2,    L2,    L2,    L2,    L2,    L2,    L2,    L2;
                        -W3,  -L3,    0,     L3,    W3,   -L3,    0,     L3,    0;
                        0,     L3,    W3,    L3,    0,    -L3,    -W3,  -L3,    0];
            L4 = 0.4;        
            AuvBack = [-L4; 0; 0];
                            
            obj.auvBodyFaces = [1, 2, 3, 4;
                                4, 5, 6, 3;
                                6, 7, 8, 5;
                                8, 9, 10, 7;
                                9, 10, 15, 16;
                                13, 14, 15, 16; 
                                13, 14, 11, 12;
                                1, 2, 11, 12;
                                10, 7, 18, 17;
                                7, 6, 19, 18;
                                6, 3, 20, 19;
                                3, 2, 21, 20;
                                2, 11, 24, 21;
                                11, 14, 23, 24;
                                14, 15, 22, 23;
                                15, 10, 17, 22;
                                17, 18, 19, 25;
                                19, 20 21, 25;
                                21, 24, 23, 25;
                                23, 22, 17, 25
                                1, 4, 26, 26;
                                4, 5, 26, 26;
                                5, 8, 26, 26;
                                8, 9, 26, 26;
                                9, 16, 26, 26;
                                16, 13, 26, 26;
                                13, 12, 26, 26;
                                12, 1, 26, 26   ];

            obj.auvBodyVertices = [AuvMain, AuvFront, AuvBack];
            
            obj.figN = length(obj.auvBodyVertices(1,:));
            obj.transformedVertices = zeros(3, obj.figN);
            
            % Offset center along x-axis
            offsetX = -0.45;
            obj.auvBodyVertices = offsetX.*[ones(1, obj.figN); zeros(2, obj.figN)] + obj.auvBodyVertices;
            
            % Scale body according to input scale
            obj.auvBodyVertices = scale * obj.auvBodyVertices;
        end
    end % End methods
end % End classdef

