classdef IcebergDraw3D < handle
% IcebergDraw3D is a class that contains methods necessary for drawing and
% updating a 3D iceberg patch object.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.05.06
%  

	properties (SetAccess = private)
        centerHandle
        icebergHandle
        %icebergPatch
        waterHandle
        %verticesStatic
        iceberg
        conf
        T
    end

	methods
        function obj = IcebergDraw3D(conf)
            obj.centerHandle = gobjects(1);
            %obj.icebergHandle = gobjects(1);
            %obj.icebergPatch = patch;
            obj.waterHandle = gobjects(1);
            obj.iceberg = load([conf.icebergDataDir, conf.icebergName, '.mat']);
            obj.iceberg.grid.Zq = -obj.iceberg.grid.Zq;
            obj.iceberg.grid.Zq(obj.iceberg.grid.Zq == 0) = NaN;
            obj.conf = conf;
            %obj.verticesStatic = [];
            obj.T = hgtransform;
        end
        function createIceberg(obj, eta_iceberg)
            obj.centerHandle = plot3(eta_iceberg(1), eta_iceberg(2), 0, 'r+');
            %north = obj.iceberg.bounds(1):obj.iceberg.bounds(2);
            %east = obj.iceberg.bounds(3):obj.iceberg.boundingBox(4);
            obj.icebergHandle = surf(obj.iceberg.grid.Nq, obj.iceberg.grid.Eq, obj.iceberg.grid.Zq, 'EdgeColor', 'none', 'Parent', obj.T, 'FaceColor', obj.conf.animoptions.icebergColor);
            alpha(obj.icebergHandle, 0.7);
            %obj.icebergPatch = patch(surf2patch(icebergHandle));
            obj.waterHandle = surf(obj.conf.animoptions.xLim3d(1):obj.conf.animoptions.xLim3d(2), obj.conf.animoptions.yLim3d(1):obj.conf.animoptions.yLim3d(2), zeros(length(obj.conf.animoptions.xLim3d(1):obj.conf.animoptions.xLim3d(2)), length(obj.conf.animoptions.xLim3d(1):obj.conf.animoptions.xLim3d(2))), 'FaceColor', obj.conf.animoptions.waterColor, 'EdgeColor','none', 'FaceAlpha', 0.5);
            %obj.verticesStatic = obj.icebergPatch.Vertices;
            
            % Color formatting
            %obj.icebergPatch.FaceColor = [255/255 255/255 255/255];
            %obj.icebergPatch.LineStyle = 'none';
            
            % Delete surface handles
            %delete(icebergHandle);
            
            % Update figure with translation and rotation of iceberg
            obj.update(eta_iceberg);
        end
        
        function update(obj, eta_iceberg)
%             T_matrix = repmat([eta_iceberg(1), eta_iceberg(2), 0], length(obj.verticesStatic), 1);
%             R_psi = [cos(eta_iceberg(3)), -sin(eta_iceberg(3)), 0; sin(eta_iceberg(3)), cos(eta_iceberg(3)), 0; 0, 0, 1];
%             R_matrix = zeros(size(T_matrix));
%             
%             for i=1:length(obj.verticesStatic)
%                 R_matrix(i,:) = (R_psi*obj.verticesStatic(i,:)')';
%             end
            
            obj.centerHandle.XData = eta_iceberg(1);
            obj.centerHandle.YData = eta_iceberg(2);
%             obj.icebergPatch.Vertices = R_matrix + T_matrix;
%             clear T_matrix R_psi R_matrix;
            R_psi = makehgtform('zrotate', eta_iceberg(3));
            T_xy = makehgtform('translate', [eta_iceberg(1), eta_iceberg(2), 0]);
            obj.T.Matrix = T_xy*R_psi;
            camlight headlight;
        end
    end % End methods
end % End classdef

