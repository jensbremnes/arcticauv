function plotIcebergMapping2dAnimation( block, ei )
% plotIcebergMapping2dAnimation updates the plot for the iceberg mapping
% simulation.
%
%   plotIcebergMapping2dAnimation( block, ei )
%
% Input parameters:
%   block       - S-function block
%   ei          - structure of event data
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

    persistent trj;
    persistent mbe;
    persistent ice;
    persistent wb;
    persistent icebergBlock;
    persistent enableParticles;
    
    if(isempty(trj) || isempty(mbe) || isempty(ice) || isempty(wb) || ...
            isempty(icebergBlock) || isempty(enableParticles))
        ud = get_param(block.BlockHandle,'UserData');
        trj = ud.trj;
        mbe = ud.mbe;
        ice = ud.ice;
        wb = ud.wb;
        icebergBlock = ud.icebergBlock;
        enableParticles = ud.enableParticles;
    end
    
    if(block.IsMajorTimeStep)  
        % Get input data
        etaAuv = block.InputPort(2).Data(1:12);
        mbeBeams = block.InputPort(2).Data(15:end);
        etaIceberg = icebergBlock.OutputPort(1).Data;
        etaRelHat = block.OutputPort(1).Data(1:3);
        etaIcebergHat = block.OutputPort(1).Data(4:6);
        t = block.CurrentTime;
        
        if(enableParticles)
            %particles = repmat(struct('etaAuv',[0; 0; 0],'etaIceberg',[0; 0; 0], 'nuIceberg', [0; 0; 0]), round(length(block.DiscStates.Data)/9),1);
            particles = repmat(struct('etaIceberg',[0; 0; 0], 'nuIceberg', [0; 0; 0]), round(length(block.DiscStates.Data)/6),1);
            for i=1:length(particles)
                idx = (i-1)*6 + 1;
                particles(i).etaIceberg = block.DiscStates.Data(idx:idx+2);
                particles(i).nuIceberg = block.DiscStates.Data(idx+3:idx+5);
                %idx = (i-1)*9 + 1;
                %particles(i).etaAuv = block.DiscStates.Data(idx:idx+2);
                %particles(i).etaIceberg = block.DiscStates.Data(idx+3:idx+5);
                %particles(i).nuIceberg = block.DiscStates.Data(idx+6:idx+8);
            end
        end
    
        % Update trajectory
        if~isempty(trj) && isa(trj,'animateAuvAndIcebergTrajectory') && trj.isAlive
            trj.setAuvPose(etaAuv(1), etaAuv(2), etaAuv(6));		  
            trj.setIcebergPose(etaIceberg(1), etaIceberg(2), etaIceberg(3));
            trj.update();
        end
        
        % Update mbe
        if(~isempty(mbe) && isa(mbe,'animateMbe2dBeams') && mbe.isAlive)
            mbe.update(etaAuv(1:6), etaIceberg, mbeBeams);
        end
        
        % Update ice grid
        if(~isempty(ice) && isa(ice,'animateIceGrid') && ice.isAlive)
            ice.update(etaRelHat, etaIceberg, etaIcebergHat, etaAuv(1:3));
            if(enableParticles)
                ice.updateParticles(particles);
            end
        end
        
        % Update waitbar
        if(~isempty(wb) && isa(wb,'animateWaitbar') && wb.isAlive)
            wb.update(t);
        end
        
        % Draw figure
        drawnow();
    end
end

