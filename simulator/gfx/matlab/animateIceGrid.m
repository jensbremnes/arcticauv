% animateIceGrid is a class for animating the ice grid.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
classdef animateIceGrid < handle
    %
    properties (SetAccess=private)
        subplot_arg
        fig_handle = gobjects(1);
        auv = gobjects(2);
        iceberg = gobjects(2);
        traces = gobjects(3);
        waypoint = gobjects(1);
        auvParticles
%         icebergParticles
        nParticles
        etaAuv = zeros(3,1);
        etaNavlab = zeros(3,1);
        etaRelEst = zeros(3,1);
        etaRelReal = zeros(3,1);
        etaIcebergReal = zeros(3,1);
        etaIcebergEst = zeros(3,1);
        auvSize
        mapSize
        mapNegPart
        icebergRealOutline
        icebergEstOutline
        wp
        ax
        disableEstTrajecory
    end
    %
    % Public methods
    methods
        function obj = animateIceGrid(fig_handle, subplot_arg, auvSize, ...
                mapSize, mapNegPart, icebergFileName, etaRelEst0, etaRelReal0, ...
                etaIcebergEst0, etaIcebergReal0, etaAuv0, waypoints, etaNavlab, ...
                nParticles, fontSize, lineWidth, colorEstTrack, isNewFig, disableEstTrajecory)
            obj.disableEstTrajecory = disableEstTrajecory;
            obj.subplot_arg = subplot_arg;
            obj.fig_handle = fig_handle;
            obj.auvSize = auvSize;
            obj.mapSize = mapSize;
            obj.mapNegPart = mapNegPart;
            obj.nParticles = nParticles;
            obj.auvParticles = gobjects(obj.nParticles);
            obj.setWp(waypoints);
            if(~isempty(etaNavlab))
                obj.setNavlab(etaNavlab);
            end
%             obj.icebergParticles = gobjects(obj.nParticles);
            obj.setIcebergOutline(icebergFileName);
            obj.createGeometry(fontSize, lineWidth, colorEstTrack, isNewFig);
            obj.setEtaAuv(etaAuv0);
            obj.setEtaIceberg(etaIcebergReal0, etaIcebergEst0);
            obj.setEtaRelative(etaRelEst0, etaRelReal0);
            obj.updatePoses();
        end
        function update(obj, etaRelEst, etaRelReal, etaIcebergReal, etaIcebergEst, etaAuv, etaNavlab)
            obj.addTrace();
            obj.setEtaAuv(etaAuv);
            if(~isempty(etaNavlab))
                obj.setNavlab(etaNavlab);
            end
            obj.setEtaIceberg(etaIcebergReal, etaIcebergEst);
            obj.setEtaRelative(etaRelEst, etaRelReal);
            obj.updatePoses();
        end
        function updateParticles(obj, particles, color, keepOldParticles, useInteralAuvState)
            newData = zeros(obj.nParticles, 3);
            for i=1:obj.nParticles
                if(~useInteralAuvState)
                    newData(i,:) = (Rzyx(0, 0, particles(i).etaIceberg(3))'*(obj.etaAuv - particles(i).etaIceberg))';
                else
                    newData(i,:) = (Rzyx(0, 0, particles(i).etaIceberg(3))'*(particles(i).etaAuv - particles(i).etaIceberg))';
                end
            end
            if(keepOldParticles)
                if(isempty(get(obj.auvParticles(1), 'XData')))
                    set(obj.auvParticles(1), 'XData', newData(:,1), 'YData', newData(:,2), 'Color', color);
                else
                    obj.auvParticles(end) = line('Parent', obj.ax, 'XData', newData(:,1), 'YData', newData(:,2), 'Color', color, 'Marker', 'x', 'MarkerSize', 10, 'LineStyle', 'none', 'LineWidth', 1);
                end
            else
                set(obj.auvParticles, 'XData', newData(:,1), 'YData', newData(:,2), 'Color', color);
            end
        end
        function addFullRealTrajectory(obj, trajectory)
            obj.traces(1).clearpoints();
           for i=1:length(trajectory(:,1))
               obj.traces(1).addpoints(trajectory(i,1), trajectory(i,2)); 
           end
        end
        function addFullEstTrajectory(obj, trajectory)
            obj.traces(2).clearpoints();
            if(~obj.disableEstTrajecory)
               for i=1:length(trajectory(:,1))
                   obj.traces(2).addpoints(trajectory(i,1), trajectory(i,2)); 
               end
            end
        end
        function addFullNavlabTrajectory(obj, trajectory)
            obj.traces(3).clearpoints();
           for i=1:length(trajectory(:,1))
               obj.traces(3).addpoints(trajectory(i,1), trajectory(i,2)); 
           end
        end
        function setEtaAuv(obj, etaAuv)
            obj.etaAuv = etaAuv;
        end
        function setWp(obj, waypoints)
            obj.wp = waypoints; 
        end
        function r = isAlive(obj)
            r = isvalid(obj) && isvalid(obj.fig_handle) && isvalid(obj.auv(1)) && ...
                isvalid(obj.auv(1)) && isvalid(obj.iceberg(1)) && isvalid(obj.iceberg(1)) && ...
                isvalid(obj.traces(1)) && isvalid(obj.traces(2));
        end
    end
    %
    % Private methods
    methods (Access=private)
        function setEtaIceberg(obj, etaIcebergReal, etaIcebergEst)
            obj.etaIcebergReal = etaIcebergReal;
            obj.etaIcebergEst = etaIcebergEst;
        end
        function setNavlab(obj, etaNavlab)
            obj.etaNavlab = etaNavlab;
        end
        function setEtaRelative(obj, etaRelEst, etaRelReal)
            obj.etaRelEst = etaRelEst;
            obj.etaRelReal = etaRelReal;
        end
        function setIcebergOutline(obj, fileName)
            if(~isempty(fileName))
                ib = load([fileName, '.mat']);
                k = boundary(ib.XYZ(:,1), ib.XYZ(:,2));
                obj.icebergRealOutline = [ib.XYZ(k,1), ib.XYZ(k,2)];
                obj.icebergEstOutline = [ib.XYZ(k,1), ib.XYZ(k,2)];
            end
        end
        function createAuv(obj, p, colAuvReal, colAuvEst)
            len = obj.auvSize(1);
            w = obj.auvSize(2);
            auv_real = rectangle('Parent', p(1));
            auv_real.Position = [-w/2, -len/2, w, len];
            auv_real.FaceColor = colAuvReal;
            auv_real.EdgeColor = 'none';
            auv_est = rectangle('Parent', p(2));
            auv_est.Position = [-w/2, -len/2, w, len];
            if(~obj.disableEstTrajecory)
                auv_est.FaceColor = colAuvEst;
            else
                auv_est.FaceColor = 'none';
            end
            auv_est.EdgeColor = 'none';
        end
        function [iceR, iceE] = createIceberg(obj, p, colReal, colEst)
            iceR = patch('Parent', p(1), 'XData', obj.icebergRealOutline(:,1), 'YData', obj.icebergRealOutline(:,2));
            iceR.FaceColor = colReal;
            iceR.EdgeColor = colReal;
            iceE = patch('Parent', p(2), 'XData', obj.icebergEstOutline(:,1), 'YData', obj.icebergEstOutline(:,2));
            iceE.FaceColor = 'none';
            iceE.EdgeColor = 'none'; % colEst
        end
        function createWaypoints(obj, ax,  size, col)
            if(~isempty(obj.wp))
                for i=1:length(obj.wp(:,1))
                   obj.waypoint = [obj.waypoint; patch('Parent', ax, 'XData', obj.wp(i,1), 'YData', obj.wp(i,2), ...
                       'Marker', 'o', 'MarkerSize', size, 'MarkerEdgeColor', col)]; 
                end
            end
        end
        function auvP = createParticles(obj, ax, colAuv, colIce, markerSize, lineWidth)
            obj.auvParticles = line('Parent', ax, 'XData', [], 'YData', [], 'Color', colAuv, 'Marker', 'x', 'MarkerSize', markerSize, 'LineStyle', 'none', 'LineWidth', lineWidth);
            auvP = obj.auvParticles;
        end
        function createGeometry(obj, fontSize, lineWidth, colRelTrackEst, isNewFig)
        % Creates all of the graphics objects for the visualization.
            colRelTrackReal = [0 0 0]./255;
%             colRelTrackEst = [87 176 255]./255;
            colNavlabTrack = [209 25 25]./255;
            colAuvReal = [28 189 0]./255;
            colAuvEst = [173 189 0]./255;
            colIcebergEst = [194 0 0]./255;
            colIcebergReal = [203 224 247]./255;
%             colAuvParticles = [102 0 128]./255;
            colIcebergParticles = [0 128 2]./255;
            colWp = [255 0 0]./255;
            if(~isNewFig)
                obj.ax = subplot(obj.subplot_arg, 'Parent', obj.fig_handle);
                set(obj.ax, 'Position', [0.04, 0.07, 0.5, 0.88]);
            else
                obj.ax = gca;
            end
            set(get(obj.ax, 'Title'), 'String', 'Relative trajectory in ICE-frame');
            set(obj.ax, 'FontSize', fontSize);
            xlabel(obj.ax, 'x^{i} [m]', 'FontSize', fontSize);
            ylabel(obj.ax, 'y^{i} [m]', 'FontSize', fontSize);
            view(obj.ax, -90,90); % swap the x and y axis
            set(obj.ax,'ydir','reverse');
            % Create the objects
            obj.iceberg(1) = hgtransform('Parent', obj.ax);
            obj.iceberg(2) = hgtransform('Parent', obj.ax);
            obj.traces(1) = animatedline('Parent', obj.ax, 'Color', colRelTrackReal, ...
                'LineWidth', lineWidth, 'LineStyle', '-'); % Real relative pos
            obj.traces(2) = animatedline('Parent', obj.ax, 'Color', colRelTrackEst, ...
                'LineWidth', lineWidth, 'LineStyle', '--'); % Est relative pos
            obj.traces(3) = animatedline('Parent', obj.ax, 'Color', colNavlabTrack, ...
                'LineWidth', lineWidth, 'LineStyle', '-'); % Est relative pos
            obj.auv(1) = hgtransform('Parent', obj.ax);
            obj.auv(2) = hgtransform('Parent', obj.ax);
            obj.createAuv(obj.auv, colAuvReal, colAuvEst);
            createWaypoints(obj, obj.ax, 5, colWp);
            if(~isempty(obj.icebergRealOutline) && ~isempty(obj.icebergEstOutline))
                [iceRealH, iceEstH] = obj.createIceberg(obj.iceberg, colIcebergReal, colIcebergEst);
            end
            if(obj.nParticles > 0)
%                 [auvPart, icePart] = obj.createParticles(ax, colAuvParticles, colIcebergParticles, 10, lineWidth);
                auvPart = obj.createParticles(obj.ax, colRelTrackEst, colIcebergParticles, 10, lineWidth);
            end
            % Initialize the axes.
            obj.ax.DataAspectRatio = [1 1 1];
%             offset = 10;
%             ax.XLim = [-obj.mapNegPart + offset, obj.mapSize - obj.mapNegPart - offset];
%             ax.YLim = [-obj.mapNegPart + offset, obj.mapSize - obj.mapNegPart - offset];
            grid(obj.ax, 'on');
            grid(obj.ax, 'minor');
            if(obj.nParticles > 0)
                if(isempty(obj.etaNavlab))
                    if(~isNewFig)
                        leg = legend(obj.ax, [obj.traces(1), obj.traces(2), auvPart], ...
                            'Real AUV trajectory', 'Estimated AUV trajectory', ...
                            'Proj. rel. pos. particles');
                    end
                else
                    if(~isNewFig)
                        leg = legend(obj.ax, [obj.traces(1), obj.traces(2), obj.traces(3), auvPart], ...
                            'AUV trajectory', 'Estimated AUV trajectory', 'Navlab AUV trajectory', ...
                            'Rel. AUV pos. particles');
                    end
                end
            elseif(~isempty(obj.icebergRealOutline) && ~isempty(obj.icebergEstOutline))
                leg = legend(obj.ax, [obj.traces(1), obj.traces(2), iceRealH, iceEstH], ...
                    'Real AUV trajectory', 'Estimated AUV trajectory', ...
                    'Real iceberg pose', 'Estimated iceberg pose');
            else
                leg = legend(obj.ax, [obj.traces(1,1), obj.traces(2,1)], ...
                    {'Real AUV trajectory', 'Estimated AUV trajectory'});
            end
            leg.FontSize = fontSize;
            if(~isNewFig)
                leg.Position = [0.41 0.10 0.0746 0.08];
%             leg.Position = [0.43 0.18 0.0746 0.08];
            else
                leg.Position = [0.62 0.13 0.0746 0.0528];
            end
            obj.ax.SortMethod = 'childorder';
        end
        function addTrace(obj)
            obj.traces(1).addpoints(obj.etaRelReal(1), obj.etaRelReal(2));  % Relative real trace
            if(~obj.disableEstTrajecory)
                obj.traces(2).addpoints(obj.etaRelEst(1), obj.etaRelEst(2));    % Relative est trace
            end
            if(~isempty(obj.etaNavlab))
                obj.traces(3).addpoints(obj.etaNavlab(1), obj.etaNavlab(2));    % Relative navlab trace
            end
        end
        function updatePoses(obj)
            obj.auv(1).Matrix = makehgtform('zrotate', obj.etaRelReal(3) + pi/2);
            obj.auv(1).Matrix(1,4) = obj.etaRelReal(1);
            obj.auv(1).Matrix(2,4) = obj.etaRelReal(2);
            obj.auv(2).Matrix = makehgtform('zrotate', obj.etaRelEst(3) + pi/2);
            obj.auv(2).Matrix(1,4) = obj.etaRelEst(1);
            obj.auv(2).Matrix(2,4) = obj.etaRelEst(2);
            obj.iceberg(2).Matrix = makehgtform('zrotate', obj.etaIcebergReal(3) - obj.etaIcebergEst(3));
            obj.iceberg(2).Matrix(1,4) = obj.etaIcebergReal(1) - obj.etaIcebergEst(1);
            obj.iceberg(2).Matrix(2,4) = obj.etaIcebergReal(2) - obj.etaIcebergEst(2); 
        end
    end
end