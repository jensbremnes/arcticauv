% animateMbe2dBeams is a class for animating the MBE beams
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
classdef animateMbe2dBeams < handle
    %
    properties (SetAccess=private)
        subplot_arg
        fig_handle
        mbeStruct
        ranges
        rangesNoise
        angles
        etaAuv
        etaIceberg
        etaRel
        beamThickness
        auv
        beams
        points
        pointsNoise
        icebergCenter
        icebergOrigoOffset
        iceberg_patch
        iceberg_outline
    end
    %
    % Public methods
    methods
        function obj = animateMbe2dBeams(fig_handle, subplot_arg, mbeStruct, etaAuv0, etaIceberg0, etaRel0, ranges0, rangesNoise0, angles0, beamThickness, fileName, fontSize, lineWidth, isNewFig)
            obj.subplot_arg = subplot_arg;
            obj.fig_handle = fig_handle;
            obj.mbeStruct = mbeStruct;
            obj.iceberg_outline = gobjects(1);
            obj.setIcebergOrigoOffset(fileName);
            obj.setIcebergCenterOffset(fileName);
            obj.setIcebergPatch(fileName);
            obj.setBeamThickness(beamThickness);
            obj.createGeometry(fontSize, lineWidth, isNewFig);
            obj.update(etaAuv0, etaIceberg0, etaRel0, ranges0, rangesNoise0, angles0);
        end
        function setRanges(obj, ranges, rangesNoise)
            obj.ranges = ranges;
            obj.rangesNoise = rangesNoise;
        end
        function setAngles(obj, angles)
            obj.angles = angles;
        end
        function setAuvState(obj, etaAuv)
            obj.etaAuv = etaAuv;
        end
        function setIcebergState(obj, etaIceberg)
            obj.etaIceberg = etaIceberg;
        end
        function setRelativeState(obj, etaRel)
            obj.etaRel = etaRel;
        end
        function setIcebergOrigoOffset(obj, fileName)
            if(~isempty(fileName))
                oo = dlmread([fileName, '_bounds.dat']);
                obj.icebergOrigoOffset = [oo(1); oo(3); 0];
            end
        end
        function setIcebergCenterOffset(obj, fileName)
            if(~isempty(fileName))
                co = dlmread([fileName, '_rotation.dat']);
                obj.icebergCenter = [co(1); co(2)];
            end
        end
        function setIcebergPatch(obj, fileName)
            if(~isempty(fileName))
                ib = load([fileName, '.mat']);
                ib.grid.Zq(ib.grid.Zq == 0) = nan;
                obj.iceberg_patch = surf2patch(ib.grid.Nq, ib.grid.Eq, ib.grid.Zq);
            end
        end
        function setBeamThickness(obj, thickness)
            obj.beamThickness = thickness;
        end
        function update(obj, etaAuv, etaIceberg, etaRel, ranges, rangesNoise, angles)
            obj.setAuvState(etaAuv);
            obj.setIcebergState(etaIceberg);
            obj.setRelativeState(etaRel);
            obj.setRanges(ranges, rangesNoise);
            obj.setAngles(angles)
            obj.updateBeamsAndPoints();
%             obj.updateOutline();
        end
        function r = isAlive(obj)
            r = isvalid(obj) && isvalid(obj.fig_handle) && isvalid(obj.auv) && ...
                all(isvalid(obj.beams)) && all(isvalid(obj.points));
        end
    end
    %
    % Private methods
    methods (Access=private)
        function createAuv(obj, ax, colA, markerSize)
            obj.auv = patch('Parent', ax, 'XData', 0, 'YData', [], ...
                            'Marker', 'o', 'MarkerSize', markerSize, 'MarkerEdgeColor', colA);
        end
        function createBeamsAndPoints(obj, ax, colB, colP, colPn, markerSize)
            
            obj.beams = gobjects(obj.mbeStruct.nBeams, 1);
            obj.points = gobjects(obj.mbeStruct.nBeams, 1);
                  
            for i = 1:obj.mbeStruct.nBeams
                obj.beams(i) = line('Parent', ax, 'XData', [], 'YData', [], 'Color', colB, 'LineWidth', obj.beamThickness);
                obj.points(i) = line('Parent', ax, 'XData', [], 'YData', [], 'Color', colP, 'Marker', '*', 'MarkerSize', markerSize);
                obj.pointsNoise(i) = line('Parent', ax, 'XData', [], 'YData', [], 'Color', colPn, 'Marker', 'o', 'MarkerSize', markerSize);
            end
            
        end
        function createIcebergOutline(obj, ax, colOutline, lineThickness)
            obj.iceberg_outline = line('Parent', ax, 'XData', [], 'YData', [], 'Color', colOutline, 'LineWidth', lineThickness);
        end
        function createGeometry(obj, fontSize, lineWidth, isNewFig)
        % Creates all of the graphics objects for the visualization.
            colA = 'blue';
            colB = 'red';
            colP = 'red';
            colPn = 'green';
%             colOutline = [169 226 245]./255;
            if(~isNewFig)
                ax = subplot(obj.subplot_arg, 'Parent', obj.fig_handle);
                set(ax, 'Position', [0.62 0.64 0.25 0.36]);
                ax.DataAspectRatio = [0.3 1 1];
            else
                ax = axes;
            end
%             set(ax, 'Position', [0.60 0.66 0.25 0.36]);
            set(get(ax, 'Title'), 'String', 'MBE beams in BEAM-frame');
            set(ax, 'FontSize', fontSize);
            xlabel(ax, 'Across-track distance [m]', 'FontSize', fontSize);
            ylabel(ax, 'Depth [m]', 'FontSize', fontSize);
            set(ax,'ydir','reverse');
            % Create the objects
            obj.createAuv(ax, colA, 10);
            obj.createBeamsAndPoints(ax, colB, colP, colPn, 5);
%             obj.createIcebergOutline(ax, colOutline, lineWidth)
            % Initialize the axes.
%             minx = -150;
%             maxx = 150;
%             miny = -5;
%             maxy = 133;
%             minx = -200;
%             maxx = 200;
%             miny = 45;
%             maxy = 300;
%             ax.XLim = [minx maxx];
%             ax.YLim = [miny maxy];
            grid(ax, 'off');
            ax.SortMethod = 'childorder';
        end
        function updateBeamsAndPoints(obj)
            
            set(obj.auv, 'YData', obj.etaRel(3));
            
            [~, mbeEtaBeam, beamEndPoints] =  getMbeBeamPoints2D(obj.mbeStruct, obj.etaRel, obj.ranges, obj.angles);
            [~, mbeEtaBeamNoise, beamEndPointsNoise] =  getMbeBeamPoints2D(obj.mbeStruct, obj.etaRel, obj.rangesNoise, obj.angles);
            
            for i = 1:obj.mbeStruct.nBeams
                if(sum(isnan(beamEndPoints(:, i))) == 0)
                    if(sum(obj.rangesNoise) == 0)
                        set(obj.beams(i), 'XData', [0, beamEndPoints(1, i)], 'YData', [mbeEtaBeam(3), beamEndPoints(2, i)]);
                    end
                    set(obj.points(i), 'XData', beamEndPoints(1, i), 'YData', beamEndPoints(2, i));
                else
                    set(obj.beams(i), 'XData', [], 'YData', []);
                    set(obj.points(i), 'XData', [], 'YData', []);
                end
                if(sum(obj.rangesNoise) ~= 0)
                    if(sum(isnan(beamEndPointsNoise(:, i))) == 0)
                        set(obj.beams(i), 'XData', [0, beamEndPointsNoise(1, i)], 'YData', [mbeEtaBeamNoise(3), beamEndPointsNoise(2, i)]);
                        set(obj.pointsNoise(i), 'XData', beamEndPointsNoise(1, i), 'YData', beamEndPointsNoise(2, i));
                    else
                        set(obj.beams(i), 'XData', [], 'YData', []);
                        set(obj.pointsNoise(i), 'XData', [], 'YData', []);
                    end
                end
            end
        end
        function updateOutline(obj)
            mbeRel = obj.etaRel(1:3) + Rzyx(obj.etaAuv(4), obj.etaAuv(5), 0)*obj.mbeStruct.leverArm;

            slice = getIcebergIntersection(mbeRel, obj.etaRel(6), obj.etaIceberg(3), obj.icebergCenter, obj.iceberg_patch.vertices, obj.iceberg_patch.faces, 1);
            
            if(~isempty(slice))
                set(obj.iceberg_outline, 'XData', slice(:,2), 'YData', slice(:,3));
            end
        end
    end
end