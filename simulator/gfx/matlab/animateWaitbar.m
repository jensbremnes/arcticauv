% animateWaitbar is a class for animating a waitbar
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
classdef animateWaitbar < handle
    %
    properties (SetAccess=private)
        figHandle = gobjects(1);
%         wbHandle = gobjects(1);
%         axHandle = gobjects(1);
        annHandle = gobjects(1);
        totalTime = 0;
        currentTime = 0;
    end
    
    % Public methods
    methods
        function obj = animateWaitbar(figHandle, T)
            obj.figHandle = figHandle;
            obj.totalTime = T;
            obj.createWaitbar();
        end
        function update(obj, t)
            obj.currentTime = t;
            obj.annHandle.String = sprintf('Complete: %d%%\nSimTime: %.01f s', round((t/obj.totalTime)*100), t);
%             waitbar(t/obj.totalTime, obj.wbHandle, sprintf('Sim time: %.01f s', t));
        end
        function r = isAlive(obj)
            r = isvalid(obj) && isvalid(obj.figHandle) && isvalid(obj.annHandle);
        end
        function delete(obj)
%            delete(obj.wbHandle); 
        end
    end
    
    % Private methods
    methods (Access=private)
        function createWaitbar(obj)
%             obj.wbHandle = waitbar(0, '', 'Visible', 'off', 'WindowStyle','modal');
%             ch = get(obj.wbHandle, 'Children');
%             set(ch, 'Parent', obj.figHandle);
%             set(ch, 'Units', 'normalized', 'Position', [0.95 0.09 0.03 0.80]);
%             set(ch, 'Units', 'points');
%             ch.Visible = 'On';
%             obj.axHandle = ch;
%             obj.axHandle.Visible = 'On';
            obj.annHandle = annotation('textbox',[0 0 0.1 0.1],...
                    'String',sprintf('Complete: %d%%\nSimTime: %.01fs', 0, 0),...
                    'FitBoxToText','on');
            obj.annHandle.Position = [0.92 0.88 0.1 0.1];
        end
    end
end