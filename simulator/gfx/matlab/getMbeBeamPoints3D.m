function [mbeEta, beamEndPoints] =  getMbeBeamPoints3D(mbeStruct, eta, mbeRanges)
% getMbeBeamPoints3D extracts the beampoints in 3D.
%
%   [mbeEta, beamEndPoints] =  getMbeBeamPoints3D(mbeStruct, eta, mbeRanges)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.02.08
%  
            
            [~, R, ~] = eulerang(eta(4,1), eta(5,1), eta(6,1));
            
            mbeEta = eta(1:3) + R*mbeStruct.mbeLeverArm;
            
            beamEndPoints = zeros(3, mbeStruct.nMbeBeams);
            beamAngle = -mbeStruct.mbeSectorRadians/2;
            angleInc = mbeStruct.mbeSectorRadians/(mbeStruct.nMbeBeams - 1);
            
            scale = 1;
            if(mbeStruct.isMbeUpLooking)
                scale = -1;
            end
            
            for i=1:mbeStruct.nMbeBeams
                if(mbeRanges(i) ~= -1)
                    beamEndPoints(:,i) = [0, mbeRanges(i)*sin(beamAngle), scale*mbeRanges(i)*cos(beamAngle)];
                    beamEndPoints(:,i) = (mbeEta + R*beamEndPoints(:,i));
                else
                    beamEndPoints(:,i) = [nan, nan, nan];
                end
                beamAngle = beamAngle + angleInc;
            end
        end