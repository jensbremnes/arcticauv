% animateAuvAndIcebergTrajectory is a class for animating the AUV and iceberg trajectory
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
classdef animateAuvAndIcebergTrajectory < handle
    %
    properties (SetAccess=private)
        subplot_arg;
        fig_handle;
        AUV = gobjects(1);
        Iceberg
        IcebergEst
        Traces = gobjects(3);
        Waypoint = gobjects(1);
        AuvPose = [0; 0; 0]
        IcebergPose = [0; 0; 0]
        EstimatorPose = [0; 0; 0]
        AuvSize = [0; 0];
        WP = [0; 0; 0];
        icebergOutline
        icebergCenter = [0; 0];
        icebergVertices
    end
    %
    % Public methods
    methods
        function obj = animateAuvAndIcebergTrajectory(fig_handle, subplot_arg, auvSize, fileName, waypoints, auvPose, icebergPose, estimatorPose, fontSize, lineWidth, isNewFig)
            obj.fig_handle = fig_handle;
            obj.subplot_arg = subplot_arg;
            obj.setAuvProperties(auvSize);
            obj.setIcebergOutline(fileName);
            obj.setIcebergCenterOffset(fileName);
            obj.setWp(waypoints);
            obj.createGeometry(fontSize, lineWidth, isNewFig);
            obj.setAuvPose(auvPose(1), auvPose(2), auvPose(3));
            obj.setIcebergPose(icebergPose(1), icebergPose(2), icebergPose(3));
            obj.setEstimatorPose(estimatorPose(1), estimatorPose(2), estimatorPose(3))
            obj.addTracePoints();
            obj.updateTransforms();
        end
        function setAuvPose(obj, x, y, psi)
            obj.AuvPose(1) = x;
            obj.AuvPose(2) = y;
            obj.AuvPose(3) = psi;
        end
        function setIcebergPose(obj, x, y, psi)
            obj.IcebergPose(1) = x;
            obj.IcebergPose(2) = y;
            obj.IcebergPose(3) = psi;
        end
        function setEstimatorPose(obj, x, y, psi)
            obj.EstimatorPose(1) = x;
            obj.EstimatorPose(2) = y;
            obj.EstimatorPose(3) = psi;
        end
        function update(obj)
            obj.addTracePoints();
            obj.updateTransforms();
        end
        function addFullAuvTrajectory(obj, trajectory)
            obj.Traces(1).clearpoints();
           for i=1:length(trajectory(:,1))
               obj.Traces(1).addpoints(trajectory(i,1), trajectory(i,2)); 
           end
        end
        function addFullIcebergTrajectory(obj, trajectory)
            obj.Traces(2).clearpoints();
           for i=1:length(trajectory(:,1))
               obj.Traces(2).addpoints(trajectory(i,1), trajectory(i,2)); 
           end
        end
        function addFullIcebergEstTrajectory(obj, trajectory)
            obj.Traces(3).clearpoints();
           for i=1:length(trajectory(:,1))
               obj.Traces(3).addpoints(trajectory(i,1), trajectory(i,2)); 
           end
        end
        function clearPoints(obj)
            obj.Traces(1).clearpoints();
            obj.Traces(2).clearpoints();
            obj.Traces(3).clearpoints();
        end
        function r = isAlive(obj)
            r = isvalid(obj) && isvalid(obj.AUV) && isvalid(obj.Traces(1)) && ... 
                isvalid(obj.Traces(2));
        end
        function setIcebergOutline(obj, fileName)
            if(~isempty(fileName))
                ib = load([fileName, '.mat']);
                k = boundary(ib.XYZ(:,1), ib.XYZ(:,2));
                obj.icebergOutline = [ib.XYZ(k,1), ib.XYZ(k,2)];
            end
        end
        function setAuvProperties(obj, auvSize)
            obj.AuvSize = auvSize;
        end
        function setWp(obj, waypoints)
            obj.WP = waypoints; 
        end
        function setIcebergCenterOffset(obj, fileName)
            if(~isempty(fileName))
                co = dlmread([fileName, '_rotation.dat']);
                obj.icebergCenter = [co(1); co(2)];
            end
        end
    end
    %
    % Private methods
    methods (Access=private)
        function createAuv(obj, p, col)
            len = obj.AuvSize(1);
            w = obj.AuvSize(2);
            auv = rectangle('Parent',p);
            auv.Position = [-w/2, -len/2, w, len];
            auv.FaceColor = col;
            auv.EdgeColor = 'none';
        end
        function createIceberg(obj, col)
            obj.Iceberg = patch('XData', obj.icebergOutline(:,1), 'YData', obj.icebergOutline(:,2));
            obj.Iceberg.FaceColor = col;
            obj.Iceberg.EdgeColor = col;
            obj.icebergVertices = obj.Iceberg.Vertices;
        end
        function createIcebergEstimator(obj, col)
            obj.IcebergEst = patch('XData', obj.icebergOutline(:,1), 'YData', obj.icebergOutline(:,2), 'LineStyle', '--');
            obj.IcebergEst.FaceColor = 'none';
            obj.IcebergEst.EdgeColor = col;
        end
        function createWaypoints(obj, ax,  size, col)
            for i=1:length(obj.WP(:,1))
               obj.Waypoint = [obj.Waypoint; patch('Parent', ax, 'XData', obj.WP(i,1), 'YData', obj.WP(i,2), ...
                   'Marker', 'o', 'MarkerSize', size, 'MarkerEdgeColor', col)]; 
            end
        end
        function addTracePoints(obj)
            x_auv = obj.AuvPose(1);
            y_auv = obj.AuvPose(2);
            x_iceberg = obj.IcebergPose(1);
            y_iceberg = obj.IcebergPose(2);
            x_est = obj.EstimatorPose(1);
            y_est = obj.EstimatorPose(2);
            obj.Traces(1).addpoints(x_auv, y_auv);
            obj.Traces(2).addpoints(x_iceberg, y_iceberg);
            obj.Traces(3).addpoints(x_est, y_est);
        end
        function createGeometry(obj, fontSize, lineWidth, isNewFig)
        % Creates all of the graphics objects for the visualization.
            colAuv = [28 189 0]./255;
            colAuvTrace = [0 53 99]./255;
            colIceberg = [203 224 247]./255;
            colEstimator = [212 144 163]./255;
%             colWP = [0 0 0]./255;
            if(~isNewFig)
                ax = subplot(obj.subplot_arg, 'Parent', obj.fig_handle);
                set(ax, 'Position', [0.57 0.03 0.35 0.57]);
%             set(ax, 'Position', [0.60 0.07 0.33 0.57]);
            else
                ax = axes;
            end
            set(get(ax, 'Title'), 'String', 'Trajectory in NED-frame');
            set(ax, 'FontSize', fontSize);
            xlabel(ax, 'North [m]', 'FontSize', fontSize);
            ylabel(ax, 'East [m]', 'FontSize', fontSize);
            view(ax, -90,90); % swap the x and y axis
            set(ax,'ydir','reverse');
            % Initialize graphic objects
            if(~isempty(obj.icebergOutline))
                createIceberg(obj, colIceberg);
                createIcebergEstimator(obj, colEstimator);
            end
            obj.Traces(2) = animatedline('Parent', ax, 'Color', colIceberg, 'LineWidth', lineWidth);
            obj.Waypoint = patch('Parent', ax, 'XData', [], 'YData', []);
            obj.Traces(1) = animatedline('Parent', ax, 'Color', colAuvTrace, 'LineWidth', lineWidth);
            obj.AUV = hgtransform('Parent', ax);
            obj.Traces(3) = animatedline('Parent', ax, 'Color', colEstimator, 'LineWidth', lineWidth, 'LineStyle', '--');
            %createWaypoints(obj, ax, 5, colWP);
            createAuv(obj, obj.AUV, colAuv);
            % Initialize the axes.
%             minx = -5;
%             maxx = 450;
%             miny = 0;
%             maxy = 455;
%             ax.XLim = [minx maxx];
%             ax.YLim = [miny maxy];
            ax.DataAspectRatio = [1 1 1];
            grid(ax, 'off');
            leg = legend(ax, [obj.Traces(1), obj.Traces(2), obj.Traces(3)], ...
                'Real AUV trajectory', 'Real iceberg (origin and outline)', 'Est. iceberg (origin and outline)', 'Location','southeast');
            leg.FontSize = fontSize;
            if(~isNewFig)
    %             leg.Position = [0.70 0.0799 0.1706 0.0794];
                leg.Position = [0.79 0.12 0.0746 0.0528];
    %             leg.Position = [0.85 0.15 0.0746 0.0528];
            else
                leg.Position = [0.58 0.14 0.0746 0.0528];
            end
            ax.SortMethod = 'childorder';
%             axis tight;
        end
        function updateTransforms(obj)
            
            if(~isempty(obj.Iceberg))
                obj.Iceberg.Vertices = obj.transformIcebergVertices(obj.icebergVertices, obj.IcebergPose(1:2), obj.IcebergPose(3), obj.icebergCenter(1:2));
                obj.IcebergEst.Vertices = obj.transformIcebergVertices(obj.icebergVertices, obj.EstimatorPose(1:2), obj.EstimatorPose(3), obj.icebergCenter(1:2));
            end
            if(~isempty(obj.AUV))
                obj.AUV.Matrix = makehgtform('zrotate', obj.AuvPose(3) + pi/2);
                obj.AUV.Matrix(1,4) = obj.AuvPose(1);
                obj.AUV.Matrix(2,4) = obj.AuvPose(2);
            end
        end
        function V = transformIcebergVertices(~, Vo, trans, angle, offset)
            R = [cos(angle), -sin(angle); sin(angle), cos(angle)];
            
            V = zeros(size(Vo));
            for i = 1:length(V(:,1))
                V(i,:) = trans +  R*(Vo(i,:)' - offset) + offset;
            end
        end
    end
end