% animationIcebergMappingSFunction is an s-function for animating the AUV and iceberg trajectory
%
% Input ports:
%   x_hat 			- AUV state vector
% 	eta_iceberg 	- Iceberg pose vector
%
% Output ports:
% 	-
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%

function animationIcebergMappingSFunction(block)
    % Level-2 MATLAB file S-function for visualizing AUV and iceberg trajectory
    setup(block)
end
%
% Called when the block is added to a model.
function setup(block)
    %
    block.NumDialogPrms = 10;
    block.NumInputPorts  = 5;
    block.NumOutputPorts = 0;
    %
    % Setup functional port properties
    block.SetPreCompInpPortInfoToDynamic;
    %
    % Set input dimensions
    block.InputPort(1).Dimensions = 12;
    block.InputPort(2).Dimensions = 3;
    block.InputPort(3).Dimensions = block.DialogPrm(9).Data;
    block.InputPort(4).Dimensions = 3;
    block.InputPort(5).Dimensions = 3;
    %
    % Register block methods
    if(block.DialogPrm(4).Data == 1)
        block.RegBlockMethod('Start',   @Start);
        block.RegBlockMethod('Outputs', @Output);
        block.RegBlockMethod('Terminate', @Terminate);
    end
    %
    % To work in external mode
    block.SetSimViewingDevice(true);
    %
    block.SampleTimes = [block.DialogPrm(10).Data 0];
end
%
% Called when the simulation starts.
function Start(block)

    % Get user data
%     auvSize = block.DialogPrm(1).Data;
%     icebergFileName = block.DialogPrm(2).Data;
%     WP = block.DialogPrm(3).Data;
%     mbeStruct = block.DialogPrm(5).Data;
%     mapSize = block.DialogPrm(6).Data;
%     
%     % Get initial input
%     etaAuv0 = block.DialogPrm(7).Data;
%     etaIceberg0 = block.DialogPrm(8).Data;
%     ranges0 = nan*ones(block.DialogPrm(9).Data, 1);
%     etaRelEst0 = [etaAuv0(1); etaAuv0(2); etaAuv0(6)] - etaIceberg0;
%     etaIcebergEst0 = block.DialogPrm(8).Data;
%     
%     % Initialize figure
%     fig = figure;
%     %obj.timeTickHandle = annotation('textbox', [0.85,0.88,0.1,0.1], 'String', sprintf('t = %.1f', t0));
%     set(fig, 'Position', get(0,'Screensize'));
%     set(fig, 'Units', 'Normalized');
%     ud.fig = fig;
%     
%     % Trajectory in NED frame
%     trj = animateAuvAndIcebergTrajectory(fig, 121, auvSize, icebergFileName, WP, etaAuv0, etaIceberg0);
%     ud.trj = trj;
%     
%     % Multibeam in BEAM frame
%     mbe = animateMbe2dBeams(fig, 222, mbeStruct, etaAuv0, etaIceberg0, ranges0, 1.0, icebergFileName);
%     ud.mbe = mbe;
%     
%     % Trajectory and particles in ICE frame
%     ice = animateIceGrid(fig, 224, auvSize, mapSize, icebergFileName, ...
%                 etaRelEst0, etaIcebergEst0, etaIceberg0, etaAuv0(1:3));
%     ud.ice = ice;
%             
%     % Draw figure
%     drawnow();
%     
%     % Save data in UserData
%     set_param(block.BlockHandle, 'UserData', ud);
end
%
% Called when the simulation time changes.
function Output(block)
%     persistent trj;
%     persistent mbe;
%     persistent ice;
%     
%     if(isempty(trj) || isempty(mbe) || isempty(ice))
%         ud = get_param(block.BlockHandle,'UserData');
%         trj = ud.trj;
%         mbe = ud.mbe;
%         ice = ud.ice;
%     end
%     
%     etaAuv = block.InputPort(1).Data;
%     etaIceberg = block.InputPort(2).Data;
%     mbeBeams = block.InputPort(3).Data;
%     etaIcebergHat = block.InputPort(4).Data;
%     etaRelHat = block.InputPort(5).Data;
%     
%     if(block.IsMajorTimeStep)  
%         % Update trajectory
%         if~isempty(trj) && isa(trj,'animateAuvAndIcebergTrajectory') && trj.isAlive
%             trj.setAuvPose(etaAuv(1), etaAuv(2), etaAuv(6));		  
%             trj.setIcebergPose(etaIceberg(1), etaIceberg(2), etaIceberg(3));
%             trj.update();
%         end
%         
%         % Update mbe
%         if(~isempty(mbe) && isa(mbe,'animateMbe2dBeams') && mbe.isAlive)
%             mbe.update(etaAuv(1:6), etaIceberg, mbeBeams);
%         end
%         
%         % Update ice grid
%         if(~isempty(ice) && isa(ice,'animateIceGrid') && ice.isAlive)
%             ice.update(etaIcebergHat, etaRelHat, etaIceberg, etaAuv(1:3));
%         end
%         
%         % Draw figure
%         drawnow();
%     end
end
%
% Called when the simulation stops
function Terminate(block)
    ud = [];
    set_param(block.BlockHandle, 'UserData', ud);
end