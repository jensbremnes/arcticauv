function h = initializeIcebergMapping2dAnimation(conf)
% initializeIcebergMapping2dAnimation initializes the plot for the iceberg 
% mapping simulation.
%
%   h = initializeIcebergMapping2dAnimation(conf)
%
% Input parameters:
%   conf        - structure with config data.
%
%   Copyright:  NTNU, Department of Marine Technology
%   Author:     Petter Norgren
%   
    persistent init;
    
    if(conf.enable2dAnimation && isempty(init))
        blk = conf.iceslam.blkName;
        
        % Get user data
        auvSize = conf.anim.auvSize;
        icebergFileName = conf.anim.icebergFile;
        WP = conf.wplist;
        mbeStruct = conf.mbe;
        mapSize = conf.iceslam.mapSize;

        % Get initial input
        etaAuv0 = conf.x0;
        etaIceberg0 = conf.iceberg.eta0;
        ranges0 = nan*ones(conf.mbe.nBeams, 1);
        etaRelEst0 = [etaAuv0(1); etaAuv0(2); etaAuv0(6)] - conf.iceberg.eta0;
        etaIcebergEst0 = conf.iceberg.eta0;

        fig = figure;
        set(fig, 'Position', get(0,'Screensize'));
        set(fig, 'Units', 'Normalized');

        % Trajectory and particles in ICE frame
        if(conf.anim.enableParticlePlots)
            nParticles = conf.iceslam.nParticles;
        else
            nParticles = 0;
        end
        ice = animateIceGrid(fig, 121, auvSize, mapSize, conf.iceslam.mapNegativePart, ...
                            icebergFileName, etaRelEst0, etaIcebergEst0, etaIceberg0, ...
                            etaAuv0(1:3), nParticles);
        ud.ice = ice;
        ud.enableParticles = conf.anim.enableParticlePlots;

        % Multibeam in BEAM frame
        mbe = animateMbe2dBeams(fig, 222, mbeStruct, etaAuv0, etaIceberg0, ...
                           ranges0, 1.0, icebergFileName);
        ud.mbe = mbe;
        
        % Trajectory in NED frame
        trj = animateAuvAndIcebergTrajectory(fig, 224, auvSize, icebergFileName, ...
                       WP, etaAuv0, etaIceberg0);
        ud.trj = trj;
        
        % Add waitbar and elapsed time
        wb = animateWaitbar(fig, conf.T);
        ud.wb = wb;
        
        ud.icebergBlock = get_param(conf.iceberg.blkName, 'RuntimeObject');

        set_param(blk, 'UserData', ud);

        h = add_exec_event_listener(blk, 'PostOutputs', @plotIcebergMapping2dAnimation);
        
        init = 1;
    else
        h = [];
    end
end