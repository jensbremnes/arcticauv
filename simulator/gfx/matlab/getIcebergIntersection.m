function sliceConv = getIcebergIntersection(eta, auvRotation, mapRotation, mapCenter, V, F, method)
% getIcebergIntersection returns a slice of the iceberg seen from the beam frame.
%
%   sliceConv = getIcebergIntersection(eta, auvRotation, mapRotation, mapCenter, V, F, method)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%          

    if(nargin == 4)
       method = 1; 
    end

    beamPlane = createPlane([eta(1) eta(2) eta(3)], [pi/2, auvRotation]);
    
    Rm = Rzyx(0, 0, mapRotation);
    Rm = Rm(1:2, 1:2);
    
    Vrot= size(V);
    for i=1:length(V)
        Vrot(i,1:2) = (Rm*(V(i,1:2)' - mapCenter) + mapCenter)';
        Vrot(i,3) = V(i,3);
    end
    
    switch(method)
        case {1}
            slice = intersectPlaneMesh(beamPlane, Vrot, F);
            sliceConv = NaN*ones(sum(cellfun('prodofsize', slice)/3), 3);
            if(~isempty(slice))
                j = 1;
                for i=1:length(slice)
                    sp = slice{i}(~isnan(slice{i}(:,1)), :);
                    for k=1:length(sp(:,1))
                        sliceConv(j,:) = sp(k,:);
                        j = j + 1;
                    end
                end
                sliceConv = sliceConv(1:j-1,:);
                sliceConv = sortrows(sliceConv, 2);
            end
        case {2}
            slice = polyhedronSlice(Vrot, F, beamPlane);
            sliceConv = slice;
        otherwise
            disp('Method not supported (only 1 and 2 is supported).');
            sliceConv = [];
            return;
    end
end