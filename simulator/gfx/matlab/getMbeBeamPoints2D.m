function [mbeEtaNed, mbeEtaBeam, beamEndPoints] =  getMbeBeamPoints2D(mbe, eta, mbeRanges, mbeAngles)
% getMbeBeamPoints2D extracts the beampoints in 2D (depth and cross-track).
%
%   [mbeEtaNed, mbeEtaBeam, beamEndPoints] =  getMbeBeamPoints2D(mbe, eta, mbeRanges)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%  
    R = Rzyx(eta(4,1), eta(5,1), 0);

    mbeEtaNed = eta(1:3) + R*mbe.leverArm;
    mbeEtaBeam = [0; 0; mbeEtaNed(3)];

    beamEndPoints = zeros(2, mbe.nBeams);

    scale = 1;
    if(mbe.isUpLooking)
        scale = -1;
    end

    for i=1:mbe.nBeams
        if(mbeRanges(i) ~= -1)
            beamEndPointsTmp = [0, mbeRanges(i)*sin(mbeAngles(i)), scale*mbeRanges(i)*cos(mbeAngles(i))]';
            beamEndPointsTmp = (mbeEtaBeam + R*beamEndPointsTmp);
            beamEndPoints(:,i) = [beamEndPointsTmp(2), beamEndPointsTmp(3)]';
        else
            beamEndPoints(:,i) = [nan, nan];
        end
    end
end