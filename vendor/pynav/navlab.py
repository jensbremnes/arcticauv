
# Reading of NavLab output data (bin).

import os
import fnmatch
import numpy as np
import pandas as pd

# The following is the columns in the exported bin file
# Found at "navlab319/Utilities/LogFileTools/logFileSpec.mat" under navlab_smooth
navlab_bin_columns = ['time', 'lat', 'lon', 'depth', 'roll', 'pitch', 'heading_north', 'velocity_north',
                      'velocity_east', 'velocity_down', 'std_lat', 'std_lon', 'std_depth', 'cov_latlon', 'std_roll',
                      'std_pitch', 'std_heading_north', 'std_vel_north', 'std_vel_east', 'std_vel_down', 'cov_vel_ne']

navlab_navsol_columns = ['time_since_start', 'lat', 'lon', 'depth', 'roll', 'pitch', 'heading', 'x_velocity_b',
                         'y_velocity_b', 'z_velocity_b', 'north_velocity', 'east_velocity', 'down_velocity',
                         'x_ang_rate_b', 'y_ang_rate_b', 'z_ang_rate_b', 'x_acc_b', 'y_acc_b', 'z_acc_b',
                         'lat_std', 'lon_std', 'depth_std', 'roll_std', 'pitch_std', 'heading_std',
                         'x_vel_b_std', 'y_vel_b_std', 'z_vel_b_std', 'sensor_input_status', 'latlon_covar',
                         'driver_status', 'time', 'slot_id', 'store_timestamp']


def read_bin(path: str):
    """
    Reads a binary exported navlab_smooth.bin file.
    This can also be done through "navlab319/NavLab/load_matrix_from_bin_file.p" in MATLAB (call with fname, 21 as arg)
    :param path: 
    :return: 
    """
    with open(path, 'rb') as f:
        b = np.fromfile(f, dtype=np.double)

    # Reshape to matrix
    b = np.reshape(b, (-1, len(navlab_bin_columns)))

    # Convert to pandas dataframe
    df = pd.DataFrame(data=b, index=range(b.shape[0]), columns=navlab_bin_columns)

    # Convert unix time
    df.time = pd.to_datetime(df.time, unit='s')  # Note: UTC

    return df

def read_navsol(path: str):
    """
    Reads the navsolution data from Hugin
    :param path:
    :return:
    """

    with open(path, 'r') as f:
        b = np.loadtxt(f, dtype=np.double)

    # Reshape to matrix
    b = np.reshape(b, (-1, len(navlab_navsol_columns)))

    # Convert to pandas dataframe
    df = pd.DataFrame(data=b, index=range(b.shape[0]), columns=navlab_navsol_columns)

    # Convert unix time
    df.time = pd.to_datetime(df.time, unit='us')  # Note: UTC

    return df


if __name__ == '__main__':
    # CHANGE IF NECESSARY
    src_dir = 'D:/Documents/Data/Hugin/DesemberTokt2017/Raw/'
    dst_dir = 'D:/Github/ArcticAuvSim/data/Multibeam/'
    mission_name = 'Mission_171116_2/'
    dst_path = dst_dir + mission_name

    # Setup NavLab
    src_bin = src_dir + mission_name + 'post/navlab_smooth.bin'
    dst_hdf = dst_path + 'navlab_smooth.hdf'
    dst_csv = dst_path + 'navlab_smooth.csv'

    nav_dat = src_dir + mission_name + 'cp/DB/NavigationSolutionData-0.txt'
    nav_hdf = dst_path + 'navsolution.hdf'


    df = read_bin(src_bin)
    #df.to_csv(dst_csv)
    df.to_hdf(dst_hdf, key='navlab')

    dfnav = read_navsol(nav_dat)
    dfnav.to_hdf(nav_hdf, key='navsolution')
