
/*
sFunctionDefines.h

Header file for setting compilation type

Copyright: 		Petter Norgren, Department of Marine Technology, NTNU
Author: 		Petter Norgren
Date created: 	2014.03.20  PN First version.
Revised:

*/

#ifndef S_FUNCTIONDEFINES_H
#define S_FUNCTIONDEFINES_H

// Define compilation type
#ifndef MATLAB_MEX_FILE
#define MATLAB_MEX_FILE
#endif

#endif // S_FUNCTIONDEFINES_H