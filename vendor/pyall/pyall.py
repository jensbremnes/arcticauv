#name:          pyALL
#created:       August 2016
#by:            p.kennedy@fugro.com
#description:   python module to read a Kongsberg ALL sonar file
#notes:         See main at end of script for example how to use this
#based on ALL Revision R October 2013

# See readme.md for more details

import math
import pprint
import struct
import os, os.path
import time
from datetime import datetime
from datetime import timedelta
# from datetime import timezone
# import geodetic

import pandas as pd
import numpy as np


def read_datagram(fname, dg_type='X'):
    r = ALLReader(fname)

    dg_out = []
    while r.moreData():
        TypeOfDatagram, datagram = r.readDatagram()
        if TypeOfDatagram == dg_type:
            datagram.read()
            dg_out.append(datagram)

    r.rewind()
    r.close()

    return dg_out


def read_xyz88_dir(dpath, hdf_path):
    """
    Recommendation: run the following command afterwards to compress, sort and chunk data
    ptrepack --chunkshape=auto --propindexes --complevel=9 --complib=blosc in.h5 out.h5
    """
    allfiles = [os.path.join(dpath, x) for x in os.listdir(dpath) if os.path.splitext(x)[1].lower() == '.all']
    allfiles.sort()

    with pd.HDFStore(hdf_path, mode='w') as hdf:
        for i, fname in enumerate(allfiles):
            print('Parsing file {}/{}'.format(i, len(allfiles)-1))
            dg = read_datagram(fname, 'X')

            # Sort by datagram time
            # Note: Datagrams are not necessarily stored in order, but we are assuming that here for performance reasons
            #dg = [x for x in sorted(dg, key=lambda y: y.get_time())]

            # Scalar fields
            time = np.array([x.get_time() for x in dg])
            ping = [(x.Heading, x.TransducerDepth, x.SoundSpeedAtTransducer) for x in dg]
            dx = np.array([x.AlongTrackDistance for x in dg])
            dy = np.array([x.AcrossTrackDistance for x in dg])
            dz = np.array([x.Depth for x in dg])
            quality_factor = np.array([x.QualityFactor for x in dg])
            detection_info = np.array([x.DetectionInformation for x in dg])
            detection_window = np.array([x.DetectionWindowsLength for x in dg])
            angle_adjustment = np.array([x.BeamIncidenceAngleAdjustment for x in dg])
            cleaning_info = np.array([x.RealtimeCleaningInformation for x in dg])
            reflectivity = np.array([x.Reflectivity for x in dg])

            hdf.append('ping', pd.DataFrame(ping, columns=['heading', 'transducer_depth', 'soundspeed'], index=time), index=False)
            hdf.append('dx', pd.DataFrame(dx, index=time), index=False)
            hdf.append('dy', pd.DataFrame(dy, index=time), index=False)
            hdf.append('dz', pd.DataFrame(dz, index=time), index=False)
            hdf.append('quality_factor', pd.DataFrame(quality_factor, index=time), index=False)
            hdf.append('detection_info', pd.DataFrame(detection_info, index=time), index=False)
            hdf.append('detection_window', pd.DataFrame(detection_window, index=time), index=False)
            hdf.append('angle_adjustment', pd.DataFrame(angle_adjustment, index=time), index=False)
            hdf.append('cleaning_info', pd.DataFrame(cleaning_info, index=time), index=False)
            hdf.append('reflectivity', pd.DataFrame(reflectivity, index=time), index=False)

            '''
            hdf['ping'] = pd.DataFrame(ping, index=time, columns=['heading', 'transducer_depth', 'soundspeed'])
            hdf['dx'] = pd.DataFrame(dx, index=time)
            hdf['dy'] = pd.DataFrame(dy, index=time)
            hdf['dz'] = pd.DataFrame(dz, index=time)
            hdf['quality_factor'] = pd.DataFrame(quality_factor, index=time)
            hdf['detection_info'] = pd.DataFrame(detection_info, index=time)
            hdf['detection_window'] = pd.DataFrame(detection_window, index=time)
            hdf['angle_adjustment'] = pd.DataFrame(angle_adjustment, index=time)
            hdf['cleaning_info'] = pd.DataFrame(cleaning_info, index=time)
            hdf['reflectivity'] = pd.DataFrame(reflectivity, index=time)
            '''


def read_rangeangle78_dir(dpath, hdf_path):
    allfiles = [os.path.join(dpath, x) for x in os.listdir(dpath) if os.path.splitext(x)[1].lower() == '.all']
    allfiles.sort()

    with pd.HDFStore(hdf_path) as hdf:
        for fname in allfiles:
            dg = read_datagram(fname, 'N')

            # Sort by datagram time
            # Note: Datagrams are not necessarily stored in order, but we are assuming that here for performance reasons
            dg = [x for x in sorted(dg, key=lambda y: y.get_time())]

            # Scalar fields
            time = np.array([x.get_time() for x in dg])
            dscale = np.array([x.DScale for x in dg])
            soundspeed = np.array([x.SoundSpeedAtTransducer*0.1 for x in dg])
            nreceivebeams = np.array([x.NumReceiveBeams for x in dg])
            nvalid = np.array([x.NumValidDetect for x in dg])
            counter = np.array([x.PingCounter for x in dg])

            # TX
            tilt = np.array([x.TiltAngle for x in dg])
            focus = np.array([x.FocusRange for x in dg])*0.1
            t_signal = np.array([x.SignalLength for x in dg])
            t_delay = np.array([x.SectorTransmitDelay for x in dg])
            c_freq = np.array([x.CentreFrequency for x in dg])
            mean_abs_coeff = np.array([x.MeanAbsorption for x in dg])*0.01
            waveform_id = np.array([x.SignalWaveformID for x in dg])
            tx_idx = np.array([x.TransmitSectorNumberTX for x in dg])
            tx_bw = np.array([x.SignalBandwidth for x in dg])

            # RX
            beam_angle = np.array([x.BeamPointingAngle for x in dg])
            tx_sector = np.array([x.TransmitSectorNumber for x in dg])
            detection_info = np.array([x.DetectionInfo for x in dg])
            detection_window = np.array([x.DetectionWindow for x in dg])
            qfactor = np.array([x.QualityFactor for x in dg])
            Dcorr = np.array([x.DCorr for x in dg])
            t_travel_twoway = np.array([x.TwoWayTravelTime for x in dg])
            reflectivity = np.array([x.Reflectivity for x in dg])*0.1  # in dB
            cleaning_info = np.array([x.RealtimeCleaningInformation for x in dg])

            hdf.append('dscale', pd.DataFrame(dscale, index=time), index=False)
            hdf.append('soundspeed', pd.DataFrame(soundspeed, index=time), index=False)
            hdf.append('nreceivebeams', pd.DataFrame(nreceivebeams, index=time), index=False)
            hdf.append('nvalid', pd.DataFrame(nvalid, index=time), index=False)
            hdf.append('counter', pd.DataFrame(counter, index=time), index=False)
            
            hdf.append('tx_tilt_angle', pd.DataFrame(tilt, index=time), index=False)
            hdf.append('tx_focus_range', pd.DataFrame(focus, index=time), index=False)
            hdf.append('tx_signal_length', pd.DataFrame(t_signal, index=time), index=False)
            hdf.append('tx_sector_delay', pd.DataFrame(t_delay, index=time), index=False)
            hdf.append('tx_centre_freq', pd.DataFrame(c_freq, index=time), index=False)
            hdf.append('tx_mean_abs_coeff', pd.DataFrame(mean_abs_coeff, index=time), index=False)
            hdf.append('tx_waveform_id', pd.DataFrame(waveform_id, index=time), index=False)
            hdf.append('tx_sector_idx', pd.DataFrame(tx_idx, index=time), index=False)
            hdf.append('tx_bandwidth', pd.DataFrame(tx_bw, index=time), index=False)

            hdf.append('rx_beam_angle', pd.DataFrame(beam_angle, index=time), index=False)
            hdf.append('rx_txsector', pd.DataFrame(tx_sector, index=time), index=False)
            hdf.append('rx_detection_info', pd.DataFrame(detection_info, index=time), index=False)
            hdf.append('rx_detection_window', pd.DataFrame(detection_window, index=time), index=False)
            hdf.append('rx_quality_factor', pd.DataFrame(qfactor, index=time), index=False)
            hdf.append('rx_dcorr', pd.DataFrame(Dcorr, index=time), index=False)
            hdf.append('rx_travel_time', pd.DataFrame(t_travel_twoway, index=time), index=False)
            hdf.append('rx_reflectivity', pd.DataFrame(reflectivity, index=time), index=False)
            hdf.append('rx_cleaning_info', pd.DataFrame(cleaning_info, index=time), index=False)

def read_depth_dir(dpath, hdf_path):
    allfiles = [os.path.join(dpath, x) for x in os.listdir(dpath) if os.path.splitext(x)[1].lower() == '.all']
    allfiles.sort()

    with pd.HDFStore(hdf_path, mode='w') as hdf:
        for i, fname in enumerate(allfiles):
            print('Parsing file {}/{}'.format(i, len(allfiles)-1))
            dg = read_datagram(fname, 'D')

            # Sort by datagram time
            # Note: Datagrams are not necessarily stored in order, but we are assuming that here for performance reasons
            #dg = [x for x in sorted(dg, key=lambda y: y.get_time())]

            # Scalar fields
            time = np.array([x.get_time() for x in dg])
            counter = np.array([x.Counter for x in dg])
            heading = np.array([x.Heading for x in dg])
            soundspeed = np.array([x.SoundSpeedAtTransducer for x in dg])
            transducerdepth = np.array([x.TransducerDepth for x in dg])
            maxbeams = np.array([x.MaxBeams for x in dg])
            nbeams = np.array([x.NBeams for x in dg])
            zres = np.array([x.ZResolution for x in dg])
            xyres = np.array([x.XYResolution for x in dg])
            samplingfreq = np.array([x.SamplingFrequency for x in dg])
            dx = np.array([x.AlongTrackDistance for x in dg])
            dy = np.array([x.AcrossTrackDistance for x in dg])
            dz = np.array([x.Depth for x in dg])
            beamdepression = np.array([x.BeamDepressionAngle for x in dg])
            beamazimuth = np.array([x.BeamAzimuthAngle for x in dg])
            ranges = np.array([x.Range for x in dg])
            quality_factor = np.array([x.QualityFactor for x in dg])
            detection_window = np.array([x.LengthOfDetectionWindow for x in dg])
            reflectivity = np.array([x.Reflectivity for x in dg])
            beamnumber = np.array([x.BeamNumber for x in dg])

            hdf.append('counter', pd.DataFrame(counter, index=time), index=False)
            hdf.append('soundspeed', pd.DataFrame(soundspeed, index=time), index=False)
            hdf.append('transducerdepth', pd.DataFrame(transducerdepth, index=time), index=False)
            hdf.append('maxbeams', pd.DataFrame(maxbeams, index=time), index=False)
            hdf.append('nbeams', pd.DataFrame(nbeams, index=time), index=False)
            hdf.append('zres', pd.DataFrame(zres, index=time), index=False)
            hdf.append('xyres', pd.DataFrame(xyres, index=time), index=False)
            hdf.append('samplingfreq', pd.DataFrame(samplingfreq, index=time), index=False)
            hdf.append('dx', pd.DataFrame(dx, index=time), index=False)
            hdf.append('dy', pd.DataFrame(dy, index=time), index=False)
            hdf.append('dz', pd.DataFrame(dz, index=time), index=False)
            hdf.append('beamdepression', pd.DataFrame(beamdepression, index=time), index=False)
            hdf.append('beamazimuth', pd.DataFrame(beamazimuth, index=time), index=False)
            hdf.append('ranges', pd.DataFrame(ranges, index=time), index=False)
            hdf.append('quality_factor', pd.DataFrame(quality_factor, index=time), index=False)
            hdf.append('detection_window', pd.DataFrame(detection_window, index=time), index=False)
            hdf.append('reflectivity', pd.DataFrame(reflectivity, index=time), index=False)
            hdf.append('beamnumber', pd.DataFrame(beamnumber, index=time), index=False)

def read_attitude_dir(dpath, hdf_path):
    allfiles = [os.path.join(dpath, x) for x in os.listdir(dpath) if os.path.splitext(x)[1].lower() == '.all']
    datagrams = []
    for fname in allfiles:
        print(fname)
        datagrams.extend(read_datagram(fname, 'A'))

    # Sort by datagram time
    dg = [x for x in sorted(datagrams, key=lambda y: y.get_time())]

    data = []
    time_offset = []
    for x in dg:
        for i in range(x.NumEntries):
            time_offset.append(x.get_time() + np.timedelta64(x.TimeMilliseconds[i], 'ms'))
            data.append((x.SensorStatus[i],
                         float(x.Roll[i]) * 0.01,
                         float(x.Pitch[i]) * 0.01,
                         float(x.Heave[i]) * 0.01,
                         float(x.Heading[i]) * 0.01))

    columns = ['SensorStatus', 'Roll', 'Pitch', 'Heave', 'Heading']
    with pd.HDFStore(hdf_path) as hdf:
        hdf['attitude'] = pd.DataFrame(data, index=time_offset, columns=columns)


def read_position_dir(dpath, hdf_path):
    allfiles = [os.path.join(dpath, x) for x in os.listdir(dpath) if os.path.splitext(x)[1].lower() == '.all']
    datagrams = []
    for fname in allfiles:
        print(fname)
        datagrams.extend(read_datagram(fname, 'P'))

    # Sort by datagram time
    dg = [x for x in sorted(datagrams, key=lambda y: y.get_time())]

    data = []
    time = []
    for x in dg:
        time.append(x.get_time())
        data.append((x.Latitude,
                     x.Longitude,
                     x.Quality,
                     x.SpeedOverGround,
                     x.CourseOverGround,
                     x.Heading,
                     x.Descriptor))

    columns = ['Latitude', 'Longitude', 'Quality', 'SpeedOverGround', 'CourseOverGround', 'Heading', 'Descriptor']
    with pd.HDFStore(hdf_path) as hdf:
        hdf['position'] = pd.DataFrame(data, index=time, columns=columns)


class ALLReader:
    ALLPacketHeader_fmt = '=LBBHLL'
    ALLPacketHeader_len = struct.calcsize(ALLPacketHeader_fmt)
    ALLPacketHeader_unpack = struct.Struct(ALLPacketHeader_fmt).unpack_from

    def __init__(self, ALLfileName):
        if not os.path.isfile(ALLfileName):
            print ("file not found:", ALLfileName)
        self.fileName = ALLfileName
        self.fileptr = open(ALLfileName, 'rb')        
        self.fileSize = os.path.getsize(ALLfileName)
        self.recordDate = ""
        self.recordTime = ""

    def __str__(self):
        return pprint.pformat(vars(self))

    def currentRecordDateTime(self):
        date_object = datetime.strptime(str(self.recordDate), '%Y%m%d') + timedelta(0,self.recordTime)
        return date_object

    def close(self):
        self.fileptr.close()
        
    def rewind(self):
        # go back to start of file
        self.fileptr.seek(0, 0)                
    
    def currentPtr(self):
        return self.fileptr.tell()

    def moreData(self):
        bytesRemaining = self.fileSize - self.fileptr.tell()
        # print ("current file ptr position:", self.fileptr.tell())
        return bytesRemaining
            
    def readDatagramHeader(self):
        try:
            curr = self.fileptr.tell()
            data = self.fileptr.read(self.ALLPacketHeader_len)
            s = self.ALLPacketHeader_unpack(data)

            NumberOfBytes   = s[0]
            STX             = s[1]
            TypeOfDatagram  = s[2]
            EMModel         = s[3]
            RecordDate      = s[4]
            RecordTime      = s[5] / 1000
            self.recordDate = RecordDate
            self.recordTime = RecordTime

            # now reset file pointer
            self.fileptr.seek(curr, 0)
            return NumberOfBytes + 4, STX, TypeOfDatagram, EMModel, RecordDate, RecordTime
        except struct.error:
            return 0,0,0,0,0,0

    def getRecordCount(self):
        count = 0
        self.rewind()
        while self.moreData():
            NumberOfBytes, STX, TypeOfDatagram, EMModel, RecordDate, RecordTime = self.readDatagramHeader()
            self.fileptr.seek(NumberOfBytes, 1)
            count += 1
        self.rewind()        
        return count

    def readDatagram(self):
        # read the datagram header.  This permits us to skip datagrams we do not support
        NumberOfBytes, STX, TypeOfDatagram, EMModel, RecordDate, RecordTime = self.readDatagramHeader()
        if TypeOfDatagram == 73: # I Installation 
            # create a class for this datagram, but only decode if the resulting class is called by the user.  This makes it much faster
            dg = I_INSTALLATION(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg 
        elif TypeOfDatagram == 78: # N Angle and Travel Time
            dg = N_TRAVELTIME(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg
        elif TypeOfDatagram == 80: # P Position
            dg = P_POSITION(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg 
        elif TypeOfDatagram == 88: # X Depth
            dg = X_DEPTH(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg 
        elif TypeOfDatagram == 68: # D DEPTH
            dg = D_DEPTH(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg
        elif TypeOfDatagram == 0x41:  # A ATTITUDE
            dg = A_ATTITUDE(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg
        else:
            dg = UNKNOWN_RECORD(self.fileptr, NumberOfBytes)
            return dg.TypeOfDatagram, dg
            # self.fileptr.seek(NumberOfBytes, 1)

    def loadNavigation(self):    
        '''loads all the navigation into lists'''
        navigation = []
        selectedPositioningSystem = None
        self.rewind()
        while self.moreData():
            TypeOfDatagram, datagram = self.readDatagram()
            if (TypeOfDatagram == 'P'):
                datagram.read()
                recDate = self.currentRecordDateTime()
                if (selectedPositioningSystem == None):
                    selectedPositioningSystem = datagram.Descriptor
                if (selectedPositioningSystem == datagram.Descriptor):
                    # for python 2.7
                    navigation.append([to_timestamp(recDate), datagram.Latitude, datagram.Longitude])
                    # for python 3.4
                    # navigation.append([recDate.timestamp(), datagram.Latitude, datagram.Longitude])
        self.rewind()
        return navigation

class UNKNOWN_RECORD:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'U'
        self.offset = fileptr.tell()
        self.bytes = bytes
        self.fileptr = fileptr
        self.fileptr.seek(bytes, 1)
        self.data = ""
    def read(self):
        self.data = self.fileptr.read(self.NumberOfBytes)
    
class D_DEPTH:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'D'
        self.offset = fileptr.tell()
        self.bytes = bytes
        self.fileptr = fileptr
        self.fileptr.seek(bytes, 1)
        self.data = ""
    
    def read(self):
        self.fileptr.seek(self.offset, 0)
        rec_fmt = '=LBBHLLHHHHHBBBBH'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        self.data = self.fileptr.read(rec_len)
        s = rec_unpack(self.data)

        self.NumberOfBytes   = s[0]
        self.STX             = s[1]
        self.TypeOfDatagram  = s[2]
        self.EMModel         = s[3]
        self.RecordDate      = s[4]
        self.Time            = s[5]
        self.Counter         = s[6]
        self.SerialNumber    = s[7]
        self.Heading                = float (s[8] / float (100))
        self.SoundSpeedAtTransducer = float (s[9] / float (10))
        self.TransducerDepth        = float (s[10] / float (100))
        self.MaxBeams                 = s[11]
        self.NBeams                 = s[12]
        self.ZResolution            = float (s[13] / float (100))
        self.XYResolution           = float (s[14] / float (100))
        self.SamplingFrequency      = s[15]

        self.Depth                        = [0 for i in range(self.NBeams)]
        self.AcrossTrackDistance          = [0 for i in range(self.NBeams)]
        self.AlongTrackDistance           = [0 for i in range(self.NBeams)]
        self.BeamDepressionAngle          = [0 for i in range(self.NBeams)]
        self.BeamAzimuthAngle             = [0 for i in range(self.NBeams)]
        self.Range                        = [0 for i in range(self.NBeams)]
        self.QualityFactor                = [0 for i in range(self.NBeams)]
        self.LengthOfDetectionWindow      = [0 for i in range(self.NBeams)]
        self.Reflectivity                 = [0 for i in range(self.NBeams)]
        self.BeamNumber                   = [0 for i in range(self.NBeams)]

        # now read the variable part of the Record
        if self.EMModel < 700 :
            rec_fmt = '=H3h2H2BbB'
        else:
            rec_fmt = '=4h2H2BbB'            
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack

        i = 0
        while i < self.NBeams:
            data = self.fileptr.read(rec_len)
            s = rec_unpack(data)
            self.Depth[i]                       = float (s[0] / float (100))
            self.AcrossTrackDistance[i]         = float (s[1] / float (100))
            self.AlongTrackDistance[i]          = float (s[2] / float (100))
            self.BeamDepressionAngle[i]         = float (s[3] / float (100))
            self.BeamAzimuthAngle[i]            = float (s[4] / float (100))
            self.Range[i]                       = float (s[5] / float (100))
            self.QualityFactor[i]               = s[6]
            self.LengthOfDetectionWindow[i]     = s[7]
            self.Reflectivity[i]                = float (s[8] / float (100))
            self.BeamNumber[i]                  = s[9]

            # now do some sanity checks.  We have examples where the Depth and Across track values are NaN
            if (math.isnan(self.Depth[i])):
                self.Depth[i] = 0
            if (math.isnan(self.AcrossTrackDistance[i])):
                self.AcrossTrackDistance[i] = 0
            if (math.isnan(self.AlongTrackDistance[i])):
                self.AlongTrackDistance[i] = 0
            i = i + 1

        rec_fmt = '=bBH'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        data = self.fileptr.read(rec_len)
        s = rec_unpack(data)

        self.RangeMultiplier    = s[0]
        self.ETX                = s[1]
        self.checksum           = s[2]

class X_DEPTH:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'X'
        self.offset = fileptr.tell()
        self.bytes = bytes
        self.fileptr = fileptr
        self.fileptr.seek(bytes, 1)
        self.data = ""

    def read(self):        
        self.fileptr.seek(self.offset, 0)                
        rec_fmt = '=LBBHLL4Hf2Hf4B'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        self.data = self.fileptr.read(rec_len)
        s = rec_unpack(self.data)

        self.NumberOfBytes   = s[0]
        self.STX             = s[1]
        self.TypeOfDatagram  = s[2]
        self.EMModel         = s[3]
        self.RecordDate      = s[4]
        self.Time            = s[5]
        self.Counter         = s[6]
        self.SerialNumber    = s[7]
        
        self.Heading         = float (s[8] / 100)
        self.SoundSpeedAtTransducer = float (s[9] / 10)
        self.TransducerDepth        = s[10]
        self.NBeams                 = s[11]
        self.NValidDetections       = s[12]
        self.SamplingFrequency      = s[13]
        self.ScanningInfo           = s[14]
        self.spare                  = s[15]

        self.Depth                        = [0 for i in range(self.NBeams)]
        self.AcrossTrackDistance          = [0 for i in range(self.NBeams)]
        self.AlongTrackDistance           = [0 for i in range(self.NBeams)]
        self.DetectionWindowsLength       = [0 for i in range(self.NBeams)]
        self.QualityFactor                = [0 for i in range(self.NBeams)]
        self.BeamIncidenceAngleAdjustment = [0 for i in range(self.NBeams)]
        self.DetectionInformation         = [0 for i in range(self.NBeams)]
        self.RealtimeCleaningInformation   = [0 for i in range(self.NBeams)]
        self.Reflectivity                 = [0 for i in range(self.NBeams)]

        # # now read the variable part of the Record
        rec_fmt = '=fffHBBBbh'            
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        for i in range(self.NBeams):
            data = self.fileptr.read(rec_len)
            s = rec_unpack(data)
            self.Depth[i] = s[0]
            self.AcrossTrackDistance[i] =  s[1]
            self.AlongTrackDistance[i] = s[2]
            self.DetectionWindowsLength[i] = s[3]
            self.QualityFactor[i] =  s[4]
            self.BeamIncidenceAngleAdjustment[i] =  float (s[5] / 10)
            self.DetectionInformation[i] = s[6]
            self.RealtimeCleaningInformation[i] =  s[7]
            self.Reflectivity[i] = float (s[8] / 10)

            # now do some sanity checks.  We have examples where the Depth and Across track values are NaN
            if (math.isnan(self.Depth[i])):
                self.Depth[i] = 0
            if (math.isnan(self.AcrossTrackDistance[i])):
                self.AcrossTrackDistance[i] = 0
            if (math.isnan(self.AlongTrackDistance[i])):
                self.AlongTrackDistance[i] = 0

        rec_fmt = '=BBH'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        data = self.fileptr.read(rec_len)
        s = rec_unpack(data)
            
        self.ETX                = s[1]
        self.checksum           = s[2]


    def get_time(self):
        Y = self.RecordDate // 10000
        M = (self.RecordDate - Y * 10000) // 100
        D = self.RecordDate - M * 100 - Y * 10000

        # Calculate time using common fields
        p_time = np.datetime64(Y, 'Y') + \
                 np.timedelta64(M, 'M') + \
                 np.timedelta64(D, 'D') + \
                 np.timedelta64(self.Time, 'ms')

        t = self.Time
        dt = np.datetime64('{}-{:0>2}-{:0>2}'.format(Y, M, D))
        dt += np.timedelta64(t, 'ms')
        return dt


class P_POSITION:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'P'       # assign the KM code for this datagram type
        self.offset = fileptr.tell()    # remember where this packet resides in the file so we can return if needed
        self.bytes = bytes              # remember how many bytes this packet contains
        self.fileptr = fileptr          # remember the file pointer so we do not need to pass from the host process
        self.fileptr.seek(bytes, 1)     # move the file pointer to the end of the record so we can skip as the default actions
        self.data = ""

    def read(self):        
        self.fileptr.seek(self.offset, 0)   # move the file pointer to the start of the record so we can read from disc              
        rec_fmt = '=LBBHLLHHll4HBB'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        self.data = self.fileptr.read(rec_len)   # read the record from disc
        bytesRead = rec_len
        s = rec_unpack(self.data)
        
        self.NumberOfBytes   = s[0]
        self.STX             = s[1]
        self.TypeOfDatagram  = s[2]
        self.EMModel         = s[3]
        self.RecordDate      = s[4]
        self.Time            = s[5]
        self.Counter         = s[6]
        self.SerialNumber    = s[7]
        self.Latitude        = float (s[8] / float(20000000))
        self.Longitude       = float (s[9] / float(10000000))
        self.Quality         = float (s[10] / float(100))
        self.SpeedOverGround = float (s[11] / float(100))
        self.CourseOverGround= float (s[12] / float(100))
        self.Heading         = float (s[13] / float(100))
        self.Descriptor      = s[14]
        self.NBytesDatagram  = s[15]

        # now read the variable position part of the Record 
        if self.NBytesDatagram % 2 == 0:
            rec_fmt = '=' + str(self.NBytesDatagram) + 'sBBH'
        else:
            rec_fmt = '=' + str(self.NBytesDatagram) + 'sBH'
            
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        data = self.fileptr.read(rec_len)
        bytesRead += rec_len
        s = rec_unpack(data)
        self.DatagramAsReceived = s[0].decode('utf-8').rstrip('\x00')
        if self.NBytesDatagram % 2 == 0:
            self.ETX                = s[2]
            self.checksum           = s[3]
        else:        
            self.ETX                = s[1]
            self.checksum           = s[2]
        
        #read any trailing bytes.  We have seen the need for this with some .all files.
        if bytesRead < self.bytes:
            self.fileptr.read(int(self.bytes - bytesRead))

    def get_time(self):
        Y = self.RecordDate // 10000
        M = (self.RecordDate - Y * 10000) // 100
        D = self.RecordDate - M * 100 - Y * 10000

        # Calculate time using common fields
        p_time = np.datetime64(Y, 'Y') + \
                 np.timedelta64(M, 'M') + \
                 np.timedelta64(D, 'D') + \
                 np.timedelta64(self.Time, 'ms')

        t = self.Time
        dt = np.datetime64('{}-{:0>2}-{:0>2}'.format(Y, M, D))
        dt += np.timedelta64(t, 'ms')
        return dt


class N_TRAVELTIME:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'N'
        self.offset = fileptr.tell()
        self.bytes = bytes
        self.fileptr = fileptr
        self.fileptr.seek(bytes, 1)
        self.data = ""

    def read(self):
        self.fileptr.seek(self.offset, 0)
        rec_fmt = '=LBBHLLHHHHHHfL'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        self.data = self.fileptr.read(rec_len)
        bytesRead = rec_len
        s = rec_unpack(self.data)

        self.NumberOfBytes   = s[0]
        self.STX             = s[1]
        self.TypeOfDatagram  = s[2]
        self.EMModel         = s[3]
        self.RecordDate      = s[4]
        self.Time            = s[5]
        self.PingCounter     = s[6]
        self.SerialNumber    = s[7]
        self.SoundSpeedAtTransducer = s[8]
        self.NumTransitSector= s[9]
        self.NumReceiveBeams = s[10]
        self.NumValidDetect  = s[11]
        self.SampleFrequency = float (s[12])
        self.DScale          = s[13]

        self.TiltAngle                    = [0 for i in range(self.NumTransitSector)]
        self.FocusRange                   = [0 for i in range(self.NumTransitSector)]
        self.SignalLength                 = [0 for i in range(self.NumTransitSector)]
        self.SectorTransmitDelay          = [0 for i in range(self.NumTransitSector)]
        self.CentreFrequency              = [0 for i in range(self.NumTransitSector)]
        self.MeanAbsorption               = [0 for i in range(self.NumTransitSector)]
        self.SignalWaveformID             = [0 for i in range(self.NumTransitSector)]
        self.TransmitSectorNumberTX       = [0 for i in range(self.NumTransitSector)]
        self.SignalBandwidth              = [0 for i in range(self.NumTransitSector)]

        self.BeamPointingAngle            = [0 for i in range(self.NumReceiveBeams)]
        self.TransmitSectorNumber         = [0 for i in range(self.NumReceiveBeams)]
        self.DetectionInfo                = [0 for i in range(self.NumReceiveBeams)]
        self.DetectionWindow              = [0 for i in range(self.NumReceiveBeams)]
        self.QualityFactor                = [0 for i in range(self.NumReceiveBeams)]
        self.DCorr                        = [0 for i in range(self.NumReceiveBeams)]
        self.TwoWayTravelTime             = [0 for i in range(self.NumReceiveBeams)]
        self.Reflectivity                 = [0 for i in range(self.NumReceiveBeams)]
        self.RealtimeCleaningInformation  = [0 for i in range(self.NumReceiveBeams)]
        self.Spare                        = [0 for i in range(self.NumReceiveBeams)]

        # # now read the variable part of the Transmit Record
        rec_fmt = '=hHfffHBBf'            
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        for i in range(self.NumTransitSector):
            data = self.fileptr.read(rec_len)
            bytesRead += rec_len
            s = rec_unpack(data)
            self.TiltAngle[i] = float (s[0]) / float (100)
            self.FocusRange[i] =  s[1]
            self.SignalLength[i] = float (s[2])
            self.SectorTransmitDelay[i] = float(s[3])
            self.CentreFrequency[i] =  float (s[4])
            self.MeanAbsorption[i] =  s[5]
            self.SignalWaveformID[i] = s[6]
            self.TransmitSectorNumberTX[i] =  s[7]
            self.SignalBandwidth[i] = float (s[8])
        
        # now read the variable part of the recieve record
        rx_rec_fmt = '=hBBHBbfhbB'
        rx_rec_len = struct.calcsize(rx_rec_fmt)
        rx_rec_unpack = struct.Struct(rx_rec_fmt).unpack
        for i in range(self.NumReceiveBeams):
            data = self.fileptr.read(rx_rec_len)
            bytesRead += rx_rec_len
            rx_s = rx_rec_unpack(data)
            self.BeamPointingAngle[i] = float (rx_s[0]) / float (100)
            self.TransmitSectorNumber[i] = rx_s[1]
            self.DetectionInfo[i] = rx_s[2]
            self.DetectionWindow[i] = rx_s[3]
            self.QualityFactor[i] = rx_s[4]
            self.DCorr[i] = rx_s[5]
            self.TwoWayTravelTime[i] = float (rx_s[6])
            self.Reflectivity[i] = rx_s[7]
            self.RealtimeCleaningInformation[i] = rx_s[8]
            self.Spare[i]                       = rx_s[9]
        
        rec_fmt = '=BBH'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack_from
        data = self.fileptr.read(rec_len)
        s = rec_unpack(data)
            
        self.ETX                = s[1]
        self.checksum           = s[2]

    def get_time(self):
        Y = self.RecordDate // 10000
        M = (self.RecordDate - Y * 10000) // 100
        D = self.RecordDate - M * 100 - Y * 10000

        # Calculate time using common fields
        p_time = np.datetime64(Y, 'Y') + \
                 np.timedelta64(M, 'M') + \
                 np.timedelta64(D, 'D') + \
                 np.timedelta64(self.Time, 'ms')

        t = self.Time
        dt = np.datetime64('{}-{:0>2}-{:0>2}'.format(Y, M, D))
        dt += np.timedelta64(t, 'ms')
        return dt

class I_INSTALLATION:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'I'       # assign the KM code for this datagram type
        self.offset = fileptr.tell()    # remember where this packet resides in the file so we can return if needed
        self.bytes = bytes              # remember how many bytes this packet contains
        self.fileptr = fileptr          # remember the file pointer so we do not need to pass from the host process
        self.fileptr.seek(bytes, 1)     # move the file pointer to the end of the record so we can skip as the default actions
        self.data = ""

    def read(self):        
        self.fileptr.seek(self.offset, 0)   # move the file pointer to the start of the record so we can read from disc              
        rec_fmt = '=LBBHLL3H'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        self.data = self.fileptr.read(rec_len)   # read the record from disc
        bytesRead = rec_len
        s = rec_unpack(self.data)
        
        self.NumberOfBytes   = s[0]
        self.STX             = s[1]
        self.TypeOfDatagram  = s[2]
        self.EMModel         = s[3]
        self.RecordDate      = s[4]
        self.Time            = s[5]
        self.SurveyLineNumber= s[6]
        self.SerialNumber    = s[7]
        self.SecondarySerialNumber = s[8]

        totalAsciiBytes = self.bytes - rec_len; # we do not need to read the header twice
        data = self.fileptr.read(totalAsciiBytes)   # read the record from disc
        bytesRead = bytesRead + totalAsciiBytes 
        parameters = data.decode('utf-8', errors="ignore").split(",")
        self.installationParameters = {}
        for p in parameters:
            parts = p.split("=")
            if len(parts) > 1:
                self.installationParameters[parts[0]] = parts[1].strip()

        #read any trailing bytes.  We have seen the need for this with some .all files.
        if bytesRead < self.bytes:
            self.fileptr.read(int(self.bytes - bytesRead))


class A_ATTITUDE:
    def __init__(self, fileptr, bytes):
        self.TypeOfDatagram = 'A'
        self.offset = fileptr.tell()
        self.bytes = bytes
        self.fileptr = fileptr
        self.fileptr.seek(bytes, 1)
        self.data = ""

    def read(self):
        self.fileptr.seek(self.offset, 0)
        rec_fmt = '=LBBHLL3H'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        self.data = self.fileptr.read(rec_len)
        bytesRead = rec_len
        s = rec_unpack(self.data)

        self.NumberOfBytes = s[0]
        self.STX = s[1]
        self.TypeOfDatagram = s[2]
        self.EMModel = s[3]
        self.RecordDate = s[4]
        self.Time = s[5]
        self.AttitudeCounter = s[6]
        self.SerialNumber = s[7]
        self.NumEntries = s[8]

        self.TimeMilliseconds = [0 for _ in range(self.NumEntries)]
        self.SensorStatus = [0 for _ in range(self.NumEntries)]
        self.Roll = [0 for _ in range(self.NumEntries)]
        self.Pitch = [0 for _ in range(self.NumEntries)]
        self.Heave = [0 for _ in range(self.NumEntries)]
        self.Heading = [0 for _ in range(self.NumEntries)]

        # # now read the variable part
        rec_fmt = '=HHhhhH'
        rec_len = struct.calcsize(rec_fmt)
        rec_unpack = struct.Struct(rec_fmt).unpack
        for i in range(self.NumEntries):
            data = self.fileptr.read(rec_len)
            bytesRead += rec_len
            s = rec_unpack(data)
            self.TimeMilliseconds[i] = s[0]
            self.SensorStatus[i] = s[1]
            self.Roll[i] = s[2]
            self.Pitch[i] = s[3]
            self.Heave[i] = s[4]
            self.Heading[i] = s[5]

        # read any trailing bytes.  We have seen the need for this with some .all files.
        if bytesRead < self.bytes:
            self.fileptr.read(int(self.bytes - bytesRead))

    def get_time(self):
        Y = self.RecordDate // 10000
        M = (self.RecordDate - Y * 10000) // 100
        D = self.RecordDate - M * 100 - Y * 10000

        # Calculate time using common fields
        p_time = np.datetime64(Y, 'Y') + \
                 np.timedelta64(M, 'M') + \
                 np.timedelta64(D, 'D') + \
                 np.timedelta64(self.Time, 'ms')

        t = self.Time
        dt = np.datetime64('{}-{:0>2}-{:0>2}'.format(Y, M, D))
        dt += np.timedelta64(t, 'ms')
        return dt

def to_timestamp(recordDate):
    return (recordDate - datetime(1970, 1, 1)).total_seconds()

def from_timestamp(unixtime):
    return datetime(1970, 1, 1) + timedelta(unixtime)

if __name__ == "__main__":
        # CHANGE IF NECESSARY
        src_dir = 'D:/Documents/Data/Hugin/DesemberTokt2017/Raw/'
        dst_dir = 'D:/Github/ArcticAuvSim/data/Multibeam/'
        mission_name = 'Mission_171116_2/'

        # Setup
        src_full = src_dir + mission_name + 'pp/EM2040/'
        dst_path = dst_dir + mission_name

        # Remove all existing files
        filelist = [ f for f in os.listdir(dst_path) if f.endswith(".hdf") ]
        for f in filelist:
            os.remove(os.path.join(dst_path, f))

        # Generate HDF files
        read_xyz88_dir(src_full, os.path.join(src_full, dst_path + 'xyz.hdf'))
        read_position_dir(src_full, os.path.join(src_full, dst_path + 'pos.hdf'))
        read_attitude_dir(src_full, os.path.join(src_full, dst_path + 'attitude.hdf'))
        #read_depth_dir(src_full, os.path.join(src_full, dst_path + 'depth.hdf'))
        read_rangeangle78_dir(src_full, os.path.join(src_full, dst_path + 'rawrangeangle.hdf'))
