function speedInKnots = ms2knots(speedInMs)
% ms2knots - Convert speed from m/s to knots.
%
% speedInKnots = ms2knots(speedInMs)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.10.06
%
    speedInKnots = 1.94384449*speedInMs;
end
