function speedInMs = knots2ms(speedInKnots)
% knots2ms - Convert speed from knots to m/s.
%
% speedInMs = knots2ms(speedInKnots)
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.10.06
%
    speedInMs = 0.514444444*speedInKnots;
end
