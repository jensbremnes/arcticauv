classdef WGS84
    %WGS84 Summary of this class goes here
    %   Detailed explanation goes here

    properties
    end

    methods (Static)
        function out = computeRn(lat)
            WGS84_constants;
            out = c_wgs84_a * cos(lat);
        end

        function [x, y, z] = toECEF(lat, lon, hae)
            WGS84_constants;

            cos_lat = cos(lat);
            sin_lat = sin(lat);
            cos_lon = cos(lon);
            sin_lon = sin(lon);
            rn = WGS84.computeRn(lat);

            x = (rn + hae) * cos_lat * cos_lon;
            y = (rn + hae) * cos_lat * sin_lon;
            z = (((1.0 - c_wgs84_e2) * rn) + hae) * sin_lat;
        end

        function [lat, lon] = toLatlon(rLat, rLot, x, y)
            WGS84_constants;
            dLat = x / c_wgs84_a;
            dLon = y / WGS84.computeRn(rLat);

            lat = rLat + dLat;
            lon = rLot + dLon;
        end

        function [x, y] = toOffset(rLat, rLot, lat, lon)
            WGS84_constants;
            N = length(lat);
            x = zeros(N,1);
            y = zeros(N,1);
            for i = 1:N
                dLat = lat(i) - rLat;
                dLon = lon(i) - rLot;

                x(i) = dLat * c_wgs84_a;
                y(i) = dLon * WGS84.computeRn(rLat);
            end
            
        end
        
    end

end

