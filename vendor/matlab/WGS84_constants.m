% Semi-major axis.
c_wgs84_a = 6378137.0;
% Semi-minor axis.
c_wgs84_b = 6356752.3142;
% First eccentricity squared.
c_wgs84_e2 = 0.00669437999013;
% Second (prime) eccentricity squared.
c_wgs84_ep2 = 0.00673949674228;
% Flattening.
c_wgs84_f = 0.0033528106647475;

% Origin of Trondheim bay. Used as origo in simulations
c_hop_lat = deg2rad(63.437817);
c_hop_lon = deg2rad(10.353168);