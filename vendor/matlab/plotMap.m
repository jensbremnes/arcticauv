function plotMap(Z, bounds, plotMethod, N)

if ~exist('plotMethod', 'var')
    plotMethod = 'surf';
end
if ~exist('N', 'var')
    N = 50;
end

northMin = bounds(1);
northMax = bounds(2);
eastMin = bounds(3);
eastMax = bounds(4);

[nNorth, nEast] = size(Z);
y = linspace(northMin, northMax, nNorth);
x = linspace(eastMin, eastMax, nEast);

[x, y] = meshgrid(x, y);


if N ~= 0
    northStrip = floor(linspace(1, nNorth, N));
    eastStrip = floor(linspace(1, nEast, N));
    yStrip = linspace(northMin, northMax, N);
    xStrip = linspace(eastMin, eastMax, N);
    ZStrip = Z(northStrip,eastStrip);
else
    xStrip = x;
    yStrip = y;
    ZStrip = Z;
end

if strcmp(plotMethod, 'surf-with-edge')
	surf(xStrip, yStrip, -ZStrip);
elseif strcmp(plotMethod, 'surf')
	surf(xStrip, yStrip, -ZStrip, 'EdgeColor', 'none');
elseif strcmp(plotMethod, 'contour')
	contour(xStrip, yStrip, -ZStrip, 50);
elseif strcmp(plotMethod, 'whitesurf')
	surf(xStrip, yStrip, -ZStrip, 'FaceColor', 'white');
else
	error(['Plot method ''' plotMethod ''' not supported']);
end

xlabel East[m];
ylabel North[m];
zlabel Up[m];
axis normal;
end

