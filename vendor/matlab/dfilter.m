function [y] = dfilter(x, alpha)

N = length(x);
y = zeros(N,1);
for i = 2:N
	y(i) = y(i-1) + alpha * (x(i) - y(i-1));
end
