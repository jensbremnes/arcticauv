function Z = random_plane(N, M, alpha, method)

    walk1 = getWalk(N, alpha);
    walk2 = getWalk(M, alpha);

    Z = zeros(N, M);
    for i = 1:N
        for j = 1:M
           Z(i,j) = walk1(i) + walk2(j);

        end
    end

    switch(method)
        case {1}
            Z = Z + abs(min(Z(:))) + zPpm*20;
        case {2}
            zPeaks = [peaks(N/2), peaks(N/2); peaks(M/2), peaks(M/2)];
            zRand = randn(N);

            Z = Z + 0.8*zPeaks + 0.2*zRand;
        otherwise
            disp('Unknown method.')
    end
end
% Det gir noe bedre resultater med mer variasjon i kartet.
% zPeaks = [peaks(N/2), peaks(N/2); peaks(M/2), peaks(M/2)];
% zRand = randn(N);
% 
% % Z = Z + 0.3*zPeaks + 0.1*zRand;
% % Z = Z + 0.3*zPeaks;
% % Z = Z + 0.1*randn(zN);
% 
% %plotMap(Z, [0 N 0 N]);
% %axis equal

function walk = getWalk(N, alpha)
    r = randn(N, 1);
    walk = zeros(N, 1);
    for i = 2:N
        walk(i) = walk(i-1) + r(i);
    end

    walk = dfilter(walk, alpha);
end

