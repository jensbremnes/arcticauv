function wplist = navigateRows(startWp, direction, distance, lineSpacing, numRows, firstTurnToLeft, depthRef)
% navigateRows generates a waypoint list for navigate rows.
%
%   wplist = navigateRows(startWp, direction, distance, lineSpacing, numRows, firstTurnToLeft, depthRef)
%
%   Input parameters:
%       wplist      - List of wapoints for navigate rows objective.
%
%   Copyright:  NTNU, Department of Marine Technology
%   Author:     Petter Norgren
%

    wplist = nan*ones(1000, 3);

    wplist(1,:) = [startWp(1), startWp(2), depthRef];
    
    row = 1;
    idx = 2;
    while(row ~= numRows)
        origin = [wplist(idx-1, 1), wplist(idx-1, 2)]; 
        wplist(idx,:) = [origin(1) + distance*cos(direction), origin(2) + distance*sin(direction), depthRef];
        idx = idx + 1;
        
        row = row + 1;
        if(row >= numRows)
           break; 
        end
        
        if(firstTurnToLeft)
            turnDirection = direction - pi/2;
        else
            turnDirection = direction + pi/2;
        end
        
        origin = [wplist(idx-1, 1), wplist(idx-1, 2)];
        wplist(idx,:) = [origin(1) + lineSpacing*cos(turnDirection), origin(2) + lineSpacing*sin(turnDirection), depthRef];
        idx = idx + 1;
        
        distance = distance*-1;
    end
    
    wplist(isnan(wplist(:,1)), :) = [];
end