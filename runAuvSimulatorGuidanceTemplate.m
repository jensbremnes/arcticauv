% runAuvSimulatorGuidanceTemplate is a script that initializes and runs the
% guidance template of the AUV simulator.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
close all; clear all; clc;
tStart = tic;

svnDir = 'D:\Subversion\'; % Change directory
simRoot = [svnDir, 'auvsim\'];

cd(simRoot);
addpath([simRoot, 'simulator\config']);

% Define flags
flags.run = 1;
flags.plot = 1;

% Select plots to run
plotSelect = [1, 0, 0, 0, 1];                               % [state_pos, state_vel, forces, actuator, trajectory]
showSelect = [0, 0];                                        % [show_estimated, show_commanded]

% Select plot group
% TODO

%% Setup

% Get default AUV configuration (always call confDefault first)
[conf, modules, mdlFiles] = confDefault(simRoot);

% Add custom conf here
[conf, modules, mdlFiles] = confRangeSim(conf, modules, mdlFiles);
[conf, modules, mdlFiles] = confGuidanceTemplate(conf, modules, mdlFiles);

% Add custom conf here
% TODO

% Change desired fields of config for current run
conf.T = 200;
conf.x0 = [0, 0, 120, 0, 0, deg2rad(0), knots2ms(2.5), 0, 0, 0, 0, 0]';
conf.VCurrent = 0.1;
conf.betaCurrent = 45;
conf.debugOn = 0;

simReturn = struct;

%% Run simulator
if(flags.run)
    simReturn = auvSimulator_exe(conf, modules, mdlFiles);
end

%% Run plot routines
if(isfield(simReturn, 't') && flags.plot)
    h = plotGeneral(simReturn, plotSelect, showSelect);
   
    %% Add custom plot functions here
    

end

%% Report time used on simulation 
simReturn.tElapsed = toc(tStart);
fprintf('INFO: Total time on a %is simulation: %.02fs (%.02f x RT). Time used for sim setup: %.02fs.\n', conf.T, simReturn.simElapsedTime, conf.T/simReturn.simElapsedTime, (simReturn.tElapsed - simReturn.simElapsedTime));

%% Clear temp variables
clear tStart svnDir simRoot modules mdlFiles;