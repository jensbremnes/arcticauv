% runAuvSimulatorIcebergMapping is a script that initializes and runs the
% iceberg mapping algorithm of the AUV simulator.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
close all; clc; clear all;
tStart = tic;

[folder, ~,  ~] = fileparts(mfilename('fullpath'));
simRoot = [folder, '/'];
clear folder;
cd(simRoot);
addpath([simRoot, 'simulator/config']);
rng('shuffle');
%% Flags
% Define flags
flags.run = 1;
flags.plot = 1;

% Select plots to run
flags.plotSelect = [0, 0, 0, 0, 0];                               % [state_pos, state_vel, forces, actuator, trajectory]
flags.customPlotSelect = [1, 1, 1, 1];                            % [observation_map, entropy, timing, ekf]
flags.showSelect = [0, 0];                                        % [show_estimated, show_commanded]
flags.plotIcebergTrajectory = 1;
flags.icebergPlots = [1, 1];
flags.exportFigs = 0;
flags.exportAsPdf = 0; 
flags.loadPreviousRun = 0;
flags.disableTrajectoryExport = 0;
   
if(~flags.loadPreviousRun)

    %% Setup
    % Get default AUV configuration (always call confDefault first)
    [conf, modules, mdlFiles] = confDefault(simRoot);
    
    % Add custom conf here
    [conf, modules, mdlFiles] = confRangeSim(conf, modules, mdlFiles);
    [conf, modules, mdlFiles] = confIcebergMapping(conf, modules, mdlFiles);

    % Change desired fields of config for current run
    conf.T = 200;      % 1300 is complete coverage twice
    conf.x0 = [0, 30, 116, 0, 0, deg2rad(80), knots2ms(2.5), 0, 0, 0, 0, 0]';
    conf.VCurrentMs = 0.0;
    conf.betaCurrentDeg = 45;
    conf.VIceMs = 0.30;                 % Use 0.30 m/s for drift estimation 
    conf.betaIceDeg = 45;
    conf.rotationRateIceDegs = 0/3600;  % Use 30 deg/h for rotation simulation
    conf.iceberg.initalVelocityError = 0.0; % Error in percent 
    conf.iceberg.initalRotRateError = 0.0; % Error in percent
    conf.iceberg.nu0 = diag([1 - conf.iceberg.initalVelocityError,...
                            1 - conf.iceberg.initalVelocityError,...
                            1 - conf.iceberg.initalRotRateError])*...
                        [conf.VIceMs*cos(deg2rad(conf.betaIceDeg)), ...
                        conf.VIceMs*sin(deg2rad(conf.betaIceDeg)), ...
                        deg2rad(conf.rotationRateIceDegs)]';
    conf.iceberg.nuRel0 = conf.x0([7:8,11]) - Rzyx(0, 0, conf.x0(6))'*conf.iceberg.nu0;
    conf.debugOn = 0;                   % up to 35x faster with debug off
    conf.mbe.enable = 1;
    conf.mbe.enableLogging = 0;
    conf.mbe.noiseEnable = 1;
    conf.iceslam.pfNoiseEnable = 1;
    conf.iceslam.saveObservationMapOnExit = 1;
    conf.enable2dAnimation = 0;
    conf.anim.enableParticlePlots = 1;
    conf.anim.saveAnimation = 0;
    conf.anim.animationFps = 10;
    conf.anim.plotMbePointsInMap = 0;
    conf.plot.Tend = conf.T;
    conf.anim.animationStop = conf.plot.Tend;

    simReturn = struct;

    %% Run simulator
    if(flags.run)
        simReturn = auvSimulator_exe(conf, modules, mdlFiles);
    end
else
    loadDir = uigetdir([simRoot, 'save']);
    if(loadDir == 0)
       return; 
    end
    load([loadDir, '\conf.mat']);
    simReturn = load([loadDir, '\auvsim.mat']);
    conf.saveDir = [loadDir, '\'];
    clear loadDir;
    conf.plot.Tend = 1300;
    conf.anim.plotMbePointsInMap = 1;
    conf.anim.animationStop = conf.plot.Tend;
    conf.iceslam.baselineCloud = [conf.dataDir, 'particleCloud/', 'baseCloud1000_1_0_1_0_1e-3_1350s'];
end

%% Get custom datafields and add to results
if(isfield(simReturn, 'simOut'))
    simReturn.tAuv = simReturn.simOut.get('WS_t_auv').signals.values;
    simReturn.tMbe = simReturn.simOut.get('WS_MbeRanges').time;
    simReturn.mbeRanges = simReturn.simOut.get('WS_MbeRanges').signals.values;
    simReturn.mbeAngles = simReturn.simOut.get('WS_MbeAngles').signals.values;
    simReturn.mbeBeamsWNoise = simReturn.simOut.get('WS_MbeBeams_wnoise').signals.values;
    simReturn.altitude = simReturn.simOut.get('WS_Altimeter').signals.values;
    simReturn.tIce = simReturn.simOut.get('WS_eta_iceberg').time;
    simReturn.etaIceberg = simReturn.simOut.get('WS_eta_iceberg').signals.values;
    simReturn.nuIceberg = simReturn.simOut.get('WS_nu_iceberg').signals.values;
    simReturn.etaRelative = simReturn.simOut.get('WS_eta_rel').signals.values;
    simReturn.nuRelative = simReturn.simOut.get('WS_nu_rel').signals.values;
    simReturn.nuRelativeMeas = simReturn.simOut.get('WS_nu_rel_meas').signals.values;
    simReturn.tSlam = simReturn.simOut.get('WS_eta_relative_hat').time;
    simReturn.etaRelativeHat = simReturn.simOut.get('WS_eta_relative_hat').signals.values;
    simReturn.xSlam = simReturn.simOut.get('WS_x_slam').signals.values;
    simReturn.xEkf = simReturn.simOut.get('WS_x_ekf').signals.values;
    simReturn.tEkf = simReturn.simOut.get('WS_x_ekf').time;
    simReturn.entropy = simReturn.simOut.get('WS_entropy_icebergslam').signals.values;
    simReturn.etaIcebergSlam = simReturn.xSlam(:,1:3);
    simReturn.etaRelativeEkf = simReturn.xEkf(:,1:3);
    simReturn.nuRelativeEkf = simReturn.xEkf(:,4:6);
    simReturn.etaIcebergEkf = simReturn.xEkf(:,7:9);
    simReturn.nuIcebergEkf = simReturn.xEkf(:,10:12);
    simReturn.xDot = simReturn.simOut.get('WS_nu_dot_3dof').signals.values;
    
    for i=1:length(simReturn.simOut.get('xout').signals)
        if(strcmp(simReturn.simOut.get('xout').signals(i).blockName(end-11:end), 'sIcebergSLAM'))
           simReturn.slamParticleStates = simReturn.simOut.get('xout').signals(i).values;
           simReturn.tPart = simReturn.simOut.get('xout').time;
           break;
        end
    end
end

%% Run plot routines
conf.plot.fontsize = 20;
conf.plot.linewidth = 2.5;
plot_handles = [];
if(isfield(simReturn, 't') && flags.plot)
   plot_handles = [plot_handles; plotGeneral(simReturn, flags.plotSelect, flags.showSelect)];
   
    %% Custom plots
    if(flags.customPlotSelect(1))
        plot_handles = [plot_handles; plotObservationMap(simReturn, conf)];
    end
    if(flags.customPlotSelect(2))
        plot_handles = [plot_handles; plotEntropy(simReturn, conf)];
    end
    if(flags.customPlotSelect(3))
        plot_handles = [plot_handles; plotTiming(simReturn, conf)];
    end
    if(flags.customPlotSelect(4))
        plot_handles = [plot_handles; plotEkf(simReturn, conf, flags)];
    end
end
if(~flags.loadPreviousRun && exist('tStart', 'var'))
    simReturn.tElapsed = toc(tStart);
    fprintf('INFO: Total time on a %is simulation: %.02fs (%.02f x RT). Time used for sim setup: %.02fs.\n', conf.T, simReturn.simElapsedTime, conf.T/simReturn.simElapsedTime, (simReturn.tElapsed - simReturn.simElapsedTime));
end

%% Run animation
if(flags.plotIcebergTrajectory)
    if(conf.enable2dAnimation)
        fprintf('INFO: Running animation...\n');
        tStart = tic;
    end
    
    plot_handles = [plot_handles; plotIcebergRelativeTrajectory(simReturn, conf)];
    
    if(conf.enable2dAnimation)
        simReturn.tElapsedAnimation = toc(tStart);
        fprintf('INFO: Animation complete. Total time on a %is animation: %.02fs.\n', conf.T, simReturn.tElapsedAnimation);
    end
end
%% Custom iceberg plots
if(sum(flags.icebergPlots))
    plot_handles = [plot_handles; plotIcebergPlots(simReturn, conf, flags)]; 
end
%% Export figures
if(flags.exportFigs)
    figDir = [conf.saveDir, 'figures/'];
    if(~exist(figDir, 'dir'))
        mkdir(figDir);
    end
    for i=1:length(plot_handles)
        str_name = [figDir, get(plot_handles(i), 'Name')];
        if(contains(str_name, 'ObservationMap'))
            continue;
        end
        if(contains(str_name, 'IcebergRelativeTrajectory') && flags.disableTrajectoryExport)
            continue;
        end
        fprintf('printing %s\n', str_name);
        if(flags.exportAsPdf)
            export_fig(str_name, '-pdf', '-q100', '-transparent', plot_handles(i));
        else
            export_fig(str_name, '-png', '-q100', '-transparent', plot_handles(i));
        end
    end
end
%% Clear temp variables
clear tStart svnDir simRoot modules mdlFiles folder i;