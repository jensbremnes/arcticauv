% runAuvSimulatorDefault is a script that illustrates the use of the AuvSimulator.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Project:	    AUR-LAB, SAMCoT
%    Author:        Petter Norgren
%    Date created:  2015.10.06
%
close all; clear all; clc;

githubDir = 'D:\GitHub\';           % Main: D:\GitHub\          Laptop C:\Users\norgren\Documents\Github\
svnDir = 'D:\Subversion\';          % Main: D:\Subversion\      Laptop: C:\Users\norgren\Documents\Subversion\
simRoot = [svnDir, 'auvsim\'];
duneRoot = [githubDir, 'dune-aurlab\'];

if(strcmp(computer('arch'), 'win64'))
    duneBuild = [githubDir, 'dune-build\msvc14_x64\'];
else
    duneBuild = [githubDir, 'dune-build\msvc14\'];
end

cd(simRoot);
addpath([simRoot, 'simulator\config']);

% Define flags
flags.startDune = 1;
flags.run = 1;
flags.plot = 1;

% Select plots to run
plotSelect = [1, 0, 0, 0];                                  % [state_pos, state_vel, forces, actuator]
showSelect = [0, 0];                                        % [show_estimated, show_commanded]

% Select plot group
plotGroup.icebergMapping.enable = 1;
plotGroup.icebergMapping.showIcebergTrajectory = 1;
plotGroup.icebergMapping.show2DBeamAnim = 0;
plotGroup.icebergMapping.show3DBeamAnim = 0;
plotGroup.icebergMapping.showPointCloud = 0;                % Causes memory issues on 32-bit MATLAB

%% Setup

% Get default AUV configuration
conf = confDefault(simRoot);
conf = confDUNE(conf, duneRoot);

% Add custom conf here
conf  = confIcebergMapping(conf);

% Change desired fields of config for current run
conf.T = 50;
conf.x0 = [0, 0, 120, 0, 0, deg2rad(0), knots2ms(2.5), 0, 0, 0, 0, 0]';
conf.VCurrent = 0.1;
conf.betaCurrent = 45;
conf.debug = 1;
conf.Speedup = 10.0;     % Max speed-up: 10 (release), 5 (debug)
conf.imcTraceOn = 0;
conf.imcDebugOn = 0;
conf.duneStartupManeuver = 'followpath';
conf.depthcontrol.value = 120;
conf.bypassDuneObserver = 1;

simReturn = struct;

%% Run simulator
if(flags.run)
    if(flags.startDune && conf.DUNE)
        dos('taskkill /im dune.exe');
        clc;
        bDune = runDune(duneBuild, conf);
    else
        bDune = 0;
    end
    if(~bDune)  
        simReturn = auvSimulator_exe(conf);
        close all;
    end
end

%% Run plot routines
if(isfield(simReturn, 't') && flags.plot)
    h = plotGeneral(simReturn, plotSelect, showSelect);
   
    %% Add custom plot functions here
    conf.animoptions.showFirstFrameOnly = 1;
    conf.animoptions.startTime = 100;
    conf.animoptions.endTime = 120;
    
    if(plotGroup.icebergMapping.enable)
        simReturn.iceberg = loadIcebergPoseFromFile(conf);
        simReturn.mbe = loadMbeRangesFromFile(conf);
%         if(~isfield(simReturn.iceberg, 'time') || ~isfield(simReturn.mbe, 'time')) % Return if simulation failed
%            return; 
%         end
        if(plotGroup.icebergMapping.showIcebergTrajectory)
            simReturn.icebergFig = plotIcebergTrajectory(simReturn);
        end
        if(plotGroup.icebergMapping.show2DBeamAnim)
            simReturn.beamFig2D = animateMbeBeams2D(simReturn, conf);
        end
        if(plotGroup.icebergMapping.show3DBeamAnim)
            simReturn.beamFig3D = animateMbeBeams3D(simReturn, conf);
        end
        if(plotGroup.icebergMapping.showPointCloud)
            simReturn.pointCloudFig = plotPointCloud(conf);
        end
    end
    
end