/*
	Copyright: 		Department of Marine Technology, NTNU
	Author :		Petter Norgren
	Date created :	2015.03.17  PN Created function.
	Revised :       
*/

#define S_FUNCTION_NAME referenceModel
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include <math.h>

/* Function: mdlInitializeSizes ===============================================
*
*   Setup sizes of the various vectors.
*/
static void mdlInitializeSizes(SimStruct *S)
{
	ssSetNumSFcnParams(S, 6); /* Number of expected parameters */
	if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
		return; /* Parameter mismatch will be reported by Simulink */
	}

	ssSetNumContStates(S, 0);
	ssSetNumDiscStates(S, 3);
	ssSetNumInputPorts(S, 2);
	ssSetNumOutputPorts(S, 2);
	
	ssSetInputPortDirectFeedThrough(S, 0, 0); //Direct feedthrough: 1 = true, 0 = false
	ssSetInputPortDirectFeedThrough(S, 1, 1); //Direct feedthrough: 1 = true, 0 = false

	ssSetInputPortWidth(S, 0, 1);
	ssSetInputPortWidth(S, 1, 1);
	ssSetOutputPortWidth(S, 0, 1);
	ssSetOutputPortWidth(S, 1, 1);

	ssSetNumSampleTimes(S, 1);
	ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);

	// Need this to use ssGetInputPortRealSignal
	ssSetInputPortRequiredContiguous(S, 0, 1);
	ssSetInputPortRequiredContiguous(S, 1, 1);

	// Set number of work vectors
	ssSetNumRWork(S, 0);
	ssSetNumIWork(S, 0);
	ssSetNumPWork(S, 0);
}


/* Function: mdlInitializeSampleTimes =========================================
* Abstract:
*    Specifiy that we inherit our sample time from the driving block.
*    However, we don't execute in minor steps.
*/
static void mdlInitializeSampleTimes(SimStruct *S)
{
	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
}


#define MDL_START  /* Change to #undef to remove function */
#if defined(MDL_START)
/* Function: mdlStart =======================================================
* Abstract:
*    This function is called once at start of model execution. If you
*    have states that should be initialized once, this is the place
*    to do it.
*/
static void mdlStart(SimStruct *S)
{
	real_T *Xd = ssGetRealDiscStates(S);
	real_T r0 = (mxGetPr(ssGetSFcnParam(S, 5)))[0];

	// Set initial value
	Xd[0] = r0;
	Xd[1] = 0;
	Xd[2] = 0;
}
#endif /*  MDL_START */

#define MDL_UPDATE  /* Change to #undef to remove function */
#if defined(MDL_UPDATE)
/* Function: mdlOutputs =======================================================
* Abstract:
*    Reference model implementation 
*/
static void mdlUpdate(SimStruct *S, int_T tid)
{
	real_T x_dot[3];
	real_T *Xd;
	real_T Ts, acc;

	// Get parameters from input
	const real_T omega = (mxGetPr(ssGetSFcnParam(S, 0)))[0];
	const real_T xi = (mxGetPr(ssGetSFcnParam(S, 1)))[0];
	const real_T acc_sat = (mxGetPr(ssGetSFcnParam(S, 2)))[0];
	const real_T vel_max = (mxGetPr(ssGetSFcnParam(S, 3)))[0];
	const real_T vel_min = (mxGetPr(ssGetSFcnParam(S, 4)))[0];

	const real_T *u = ssGetInputPortRealSignal(S, 0);

	Xd = ssGetRealDiscStates(S);
	Ts = ssGetSampleTime(S, 0);

	UNUSED_ARG(tid); /* not used in single tasking mode */

	// Mass-Spring-Damper system
  acc = pow(omega, 3)*(u[0] - Xd[0]) - (2 * xi + 1)*pow(omega, 2)*Xd[1] - (2 * xi + 1)*omega*Xd[2];

	// Set derivatives
  x_dot[0] = Xd[1];
	x_dot[1] = Xd[2];
	x_dot[2] = acc;

	// Velocity saturation
	if (x_dot[1] <= vel_min)
	{
		x_dot[1] = vel_min;
	}
	else if (x_dot[1] >= vel_max)
	{
		x_dot[1] = vel_max;
	}

	// Acceleration saturation
	if (x_dot[2] <= -acc_sat)
	{
		x_dot[2] = -acc_sat;
	}
	else if (x_dot[2] >= acc_sat)
	{
		x_dot[2] = acc_sat;
	}

	// Use forward Euler to return next state value
  Xd[0] += Ts*x_dot[0];
	Xd[1] += Ts*x_dot[1];
	Xd[2] += Ts*x_dot[2];
}
#endif

/* Function: mdlOutputs =======================================================
* Abstract:
*    y = x1
*/
static void mdlOutputs(SimStruct *S, int_T tid)
{
	real_T *output, *Xd;
    
    const real_T *u = ssGetInputPortRealSignal(S, 1);

	output = ssGetOutputPortRealSignal(S, 0);
	Xd = ssGetRealDiscStates(S);

	UNUSED_ARG(tid); /* not used in single tasking mode */

	// Set outputs
	output[0] = Xd[0];
	output[1] = u[0] - Xd[0];
}


/* Function: mdlTerminate =====================================================
* Abstract:
*    In this function, you should perform any actions that are necessary
*    at the termination of a simulation.  For example, if memory was
*    allocated in mdlStart, this is the place to free it.
*/
static void mdlTerminate(SimStruct *S)
{
	UNUSED_ARG(S); /* unused input argument */
}



#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
	#include "simulink.c"      /* MEX-file interface mechanism */
#else
	#include "cg_sfun.h"       /* Code generation registration function */
#endif
