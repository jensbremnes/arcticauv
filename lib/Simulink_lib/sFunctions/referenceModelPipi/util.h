#pragma once

#include <math.h>

#ifndef PI
	#define PI 3.14159265359
#endif

int sgn(double x)
{
	if (x > 0)
	{
		return 1;
	} 
	else if (x < 0)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

double inf2pipi(double x)
{
	return fmod(PI * sgn(x) + x, 2 * PI) - sgn(x)*PI;
}