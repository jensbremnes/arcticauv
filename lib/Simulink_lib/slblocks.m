function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'AURlabLib';
  Browser.Name    = 'AURlab Simulink library';
  blkStruct.Browser = Browser;