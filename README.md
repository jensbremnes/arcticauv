ArcticAuvSim
=====================

Before running the simulator:

 1. If running a custom simulation, copy one of the run-scripts (e.g. runAuvSimulatorIceMapping) and customize script.
 2. If not, run simulation by pressing play in matlab.

Compiling S-functions:
======================

1. Run CMake on desired s-function (select build folder as ../sfunction/<YourSFunction>/build).
2. Open Visual Studio solution (.sln-file found under build-folder selected above).
3. Run build-all (this will deploy the mex-file to the library folder where matlab will look for the module).
	
Debugging
======================

Debugging can be performed by:

 1. Open Matlab.
 2. Check that properties->Debugging->Command in Visual Studio points at correct Matlab version (e.g. C:\Program Files\MATLAB\R2016b\bin\win64\MATLAB.exe).
 3. Check that properties->Debugging->Working Directory is set to build directory (e.i. <ProjectRoot>/build).
 4. Check that properties->Debugging->Attach is set to 'yes'.
 5. Choose project as startup project (e.g. right-click on sDvlMbeSimulator and select 'Set as Startup project').
 6. Build and start debugging from VS (remember to select debug as build profile). If DUNE is being debugged, remember to turn off automatic launch of DUNE from MATLAB.
 7. Set debug flag to 1 (not necessary for DUNE debug) and run simulator (code will break at breakpoints in VS and show parameter values).
 