% createRandomBathymetry is a script that will create random bathymetry at
% around a specified depth and store mat- and VRML-files.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren

close all; clear all; clc;
addpath('./../../vendor/matlab');

NEW_BATHY = 0;
bathyName = 'default_bathy_low_res';
depthTarget = 120;
L = 3000;
zPpm = 10;           % [points per meter] Resolution of map
alpha = 0.15;
method = 2;

% EDIT HERE
if(NEW_BATHY)
    % Open files
    fid1 = fopen(['./SIMdata/', bathyName, '_Z.dat'], 'w+');
    fid2 = fopen(['./SIMdata/', bathyName, '_bounds.dat'], 'w+');
    fid3 = fopen(['./SIMdata/', bathyName, '_dim.dat'], 'w+');
    fid4 = fopen(['./SIMdata/', bathyName, '_offset.dat'], 'w+');
    
    northMin = -L/2 + zPpm;
    northMax = L/2;
    eastMin = -L/2 + zPpm;
    eastMax = L/2;

    % Set-up
    bounds = [northMin, northMax, eastMin, eastMax];    
    N = L/zPpm;

    % Run random walk
    zTmp = random_plane(N, N, alpha, method);

    % Calculate mean depth
    meanDepth = mean(mean(zTmp));

    % Offset map to target mean
    Z = zTmp + (depthTarget - meanDepth).*ones(N,N);
    
    fprintf(fid2, '%i %i %i %i\n', bounds);
    fclose(fid2);

    % Interpolate data to construct a gridded dataset
    Nq = bounds(1):zPpm:bounds(2); %linspace(bounds(1), bounds(2), (bounds(2) - bounds(1) + 1)/N);
    Eq = bounds(3):zPpm:bounds(4); %linspace(bounds(3), bounds(4), (bounds(4) - bounds(3) + 1)/N);

    for i=1:length(Nq)
        for j=1:length(Eq)
            if(Z(j, i) < 10)
                Z(j, i) = 10;
            end
            fprintf(fid1, '%.2f', Z(j, i));
            if(j == length(Eq))
                fprintf(fid1, '\n');
            else
                fprintf(fid1, ' ');
            end
        end
    end
    fclose(fid1);

    fprintf(fid3, '%i %i\n', length(Nq), length(Eq));
    fclose(fid3);

    offset(1) = 0;
    offset(2) = 0; 

    fprintf(fid4, '%i %i\n', offset(1), offset(2));
    fclose(fid4);
    
elseif(exist(['./SIMdata/', bathyName, '_Z.dat'], 'file') && exist(['./SIMdata/', bathyName, '_bounds.dat'], 'file'))
    Z = dlmread(['./SIMdata/', bathyName, '_Z.dat']);
    bounds = dlmread(['./SIMdata/', bathyName, '_bounds.dat']);
else
    error('No dat-files present. Create new bathymetry.');
end

% Plot result
 plotMap(Z, bounds);
 
 fprintf('Minimum: %f\n', min(Z(:)));
 fprintf('Maximum: %f\n', max(Z(:)));
 fprintf('Mean: %f\n', mean(mean(Z(:))));


