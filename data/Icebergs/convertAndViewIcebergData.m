% View and convert iceberg shape data from the
% PERD-database to a format used for simulation.
%   
%   Copyright:     NTNU, Department of Marine Technology
%   Project:	   AUR-LAB
%   Author:        Petter Norgren
%   Date created:  2016.02.01
%

close all;clear all; clc;
addpath('../../vendor/matlab/rgb');
addpath('../../vendor/matlab/exportfig');
addpath('../../vendor/matlab/geotiffinterp');
addpath('../../vendor/matlab/geotiffinterp/Data');

pathName = './PERD_data/';
fileName = 'iceberg_r11i01.txt';

Zoffset = 0;

useFullPatch = 0;
plotter = [0 1 0 0 0];
intersectionMethod = 1;     % [1,2] 1 is MUCH faster ~13x

%% Convert data to simdata
[XYZ, XYZ_all, bounds, offset, centerRot, s] = convertIcebergShapeData(pathName, fileName);

%% Write data to files
grid = gridAndStoreIcebergShape(XYZ, s, bounds, offset, centerRot);
grid.Zq = grid.Zq - Zoffset*ones(size(grid.Zq));
grid.Zq(grid.Zq == -Zoffset) = 0;
surf_patch = surf2patch(grid.Nq, grid.Eq, grid.Zq);
% red_patch = reducepatch(surf_patch, 0.5, 'fast');
[red_patch.vertices, red_patch.faces] = trimMesh(surf_patch.vertices, surf_patch.faces); 
%% Write all to mat-file
save(['./SIMdata/', 'iceberg_', s(1).icebergId, '.mat'], 's', 'grid', 'bounds', 'XYZ', 'XYZ_all');
clear 'grid' 'bounds' 'XYZ' 'XYZ_all';
%% Load written data
load(['./SIMdata/', 'iceberg_', s(1).icebergId, '.mat']);
%% Create volume
if(plotter(1))
    ts = figure;
    hold on;
    drawMesh(surf_patch.vertices, surf_patch.faces, 'FaceColor', 'b');
%     patch(surf_patch, 'EdgeColor','none', 'FaceColor', [196/255 240/255 255/255]);
    set(get(ts, 'CurrentAxes'), 'Zdir', 'reverse');
    plot3(centerRot(1), centerRot(2), 0, '*r');
end
%%
%% Show iceberg
if(plotter(2))
    h = figure;
    set(gcf, 'Color', 'w');
    hold on;
    lighting phong;
    camlight headlight;
    shading interp;
    lightangle(-45,30);
    %scatter3(XYZ(:,1), XYZ(:,2), XYZ(:,3), 'r', 'filled');
    %plot(shp_all,'EdgeColor','none', 'FaceColor', [196/255 240/255 255/255]);
    sfz = geotiffinterp('bedmap2_surface.tif',grid.Nq,grid.Eq,'cubic');
    bed = geotiffinterp('bedmap2_bed.tif',grid.Nq,grid.Eq,'cubic').*100;
    sfz = sfz - mean(mean(sfz));
%     hsfz = surface(grid.Nq, grid.Eq, sfz, 'EdgeColor','none');
%     hsfz(2) = surface([grid.Nq(:,1) grid.Nq(:,1)],[grid.Eq(:,1) grid.Eq(:,1)],[bed(:,1) sfz(:,1)], 'EdgeColor','none');
%     hsfz(3) = surface([grid.Nq(1,:);grid.Nq(1,:)],[grid.Eq(1,:);grid.Eq(1,:)],[bed(1,:);sfz(1,:)], 'EdgeColor','none');
%     hsfz(4) = surface([grid.Nq(:,end) grid.Nq(:,end)],[grid.Eq(:,end) grid.Eq(:,end)],[bed(:,end) sfz(:,end)], 'EdgeColor','none');
%     hsfz(5) = surface([grid.Nq(end,:);grid.Nq(end,:)],[grid.Eq(end,:);grid.Eq(end,:)],[bed(end,:);sfz(end,:)], 'EdgeColor','none');
%     hsfz(6) = surface(grid.Nq, grid.Eq, sfz + 125, 'EdgeColor','none');
%     set(hsfz,'FaceColor',rgb('ice blue'),'facealpha',.6);
%     bed = geotiffinterp('bedmap2_bed.tif',grid.Nq,grid.Eq,'cubic');
%     hbed = surface(grid.Nq, grid.Eq, bed);
    hice = surface(grid.Nq, grid.Eq, grid.Zq, 'EdgeColor','none');
    demcmap(-grid.Zq, 256); 
    xlabel('North [m]');
    ylabel('East [m]');
    zlabel('Depth [m]');
    set(get(h, 'CurrentAxes'), 'Zdir', 'reverse');
    zlim([-5, 125]);
    view(-26.8, -16.4); % Swap x and y axis
    ax = gca;
    ax.Visible = 'off';
end
if(plotter(3))
    %% Show 2D projection of iceberg
    mapHeading = deg2rad(70);
    auvHeading = deg2rad(-20);
    arrowMag = 5;
    center = [20.0, 25.0592, 128.9720];
    
    Rm = Rzyx(0, 0, mapHeading);
    Rm = Rm(1:2, 1:2);
    Ra = Rzyx(0, 0, auvHeading);
    Ra = Ra(1:2, 1:2);
    
    vela = Ra*[1;0].*arrowMag;
    velm = Rm*[1;0].*arrowMag;
    
    rotatedVertices = size(surf_patch.vertices);
    for i=1:length(surf_patch.vertices)
        rotatedVertices(i,1:2) = (Rm*(surf_patch.vertices(i,1:2)' - centerRot) + centerRot)';
        rotatedVertices(i,3) = surf_patch.vertices(i,3);
    end

    if(useFullPatch)
        slice = getIcebergIntersection(center, auvHeading, mapHeading, centerRot, surf_patch.vertices, surf_patch.faces, intersectionMethod);
    else
        slice = getIcebergIntersection(center, auvHeading, mapHeading, centerRot, red_patch.vertices, red_patch.faces, intersectionMethod);
    end
    
    fig = figure;
    hold on;
    hm = drawMesh(rotatedVertices, surf_patch.faces, 'FaceColor', 'b');
    if(~isempty(slice))
        line('XData', slice(:,1), 'YData', slice(:,2), 'ZData', slice(:,3), 'LineStyle', '-', 'Color', [1 0 0]);
    end
    plot3(center(1), center(2), center(3), '*r');
    plot3(centerRot(1), centerRot(2), center(3), '*g');
    quiver3(center(1), center(2), center(3), vela(1), vela(2), 0, 'r');
    quiver3(centerRot(1), centerRot(2), center(3), velm(1), velm(2), 0, 'g');
    ax = get(fig, 'CurrentAxes');
    set(ax, 'Zdir', 'reverse');
    view(ax, 90, -90); % Swap x and y axis
    ax.DataAspectRatio = [1 1 1];
    xlabel('North [m]');
    ylabel('East [m]');
    zlabel('Depth [m]');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]);
end
if(plotter(4))   
    fig2 = figure;
    hold on;
    ax = get(fig2, 'CurrentAxes');
    plot(0, center(3), '*r');
    line('XData', sliceRotated(:,2), 'YData', sliceRotated(:,3), 'LineStyle', '-', 'Color', [0 0 1]);
    xlabel('Across-track distance [m]');
    zlabel('Depth [m]');
    set(ax, 'Ydir', 'reverse');
    ax.DataAspectRatio = [1 1 1];
end
if(plotter(5))
    fig3 = figure;
    hold on;
    plot(XYZ(:,1), XYZ(:,2), '*b');
    P = boundary(XYZ(:,1), XYZ(:,2));
    patch(XYZ(P,1), XYZ(P,2), '-r');
    ax = get(fig3, 'CurrentAxes');
    ax.DataAspectRatio = [1 1 1];
end


