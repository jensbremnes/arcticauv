function [XYZ, XYZ_all, bounds, offset, centerOfRot, s] = convertIcebergShapeData(pathName, fileName)
% convertIcebergShapeData converts iceberg shape data from the
% PERD-database to a format used for simulation.
% 
%   [XYZ, XYZ_all, bounds, offset, centerOfRot, s] = convertIcebergShapeData(pathName, fileName)
%   
% Input parameters:
%   pathName        - Directory of PERD iceberg data (terminated with \)
%   fileName        - Name of iceberg data to be read (terminated with .txt)
%
% Output parameters:
%   XYZ             - Matrix containing iceberg data (only below surface).
%   XYZ_all         - Matrix containing all iceberg data.
%   bounds          - Vector with iceberg bounds.
%   offset          - Center offset.
%   centerOfRot     - Center of rotation.
%   s               - Structure with raw iceberg data.
%
%   Copyright:     NTNU, Department of Marine Technology
%   Author:        Petter Norgren
%
    fid = fopen([pathName, fileName], 'r');
    
    for i=1:100000
       tline = fgets(fid);
       
       % Skip first line
       if(i == 1)
           continue;
       end
       
       % Break if eof
       if(tline == -1)
           break;
       end
       
       conv = regexp(tline, '([^,]*)', 'tokens');
       
       s(i-1).reportNo = str2double(conv{1}{1});
       s(i-1).icebergId = strtrim(conv{2}{1});
       s(i-1).x_value = str2double(conv{3}{1}) + (str2double(conv{4}{1})/10^(numel(conv{4}{1})));
       if(length(conv) == 7)
           s(i-1).y_value = str2double(conv{5}{1}) + (str2double(conv{6}{1})/10^(numel(conv{6}{1})));
           s(i-1).z_value = str2double(conv{7}{1});
       else % Some y-values have only one digit (0)
           s(i-1).y_value = str2double(conv{5}{1});
           s(i-1).z_value = str2double(conv{6}{1});
       end
    end
    fclose(fid);
    
    max_north = -inf;
    max_east = -inf;
    min_north = inf;
    min_east = inf;
    
    XYZ = nan*ones(length(s), 3);
    XYZ_all = nan*ones(length(s), 3);
    
    for i=1:length(s)
       %fprintf(fid2, '%8.4f %8.4f %6.2f,\n', s(i).x_value, s(i).y_value, s(i).z_value);
       if(s(i).z_value < 0)
           XYZ(i, 1) = s(i).x_value;
           XYZ(i, 2) = s(i).y_value;
           XYZ(i, 3) = -s(i).z_value;
       end
       XYZ_all(i, 1) = s(i).x_value;
       XYZ_all(i, 2) = s(i).y_value;
       XYZ_all(i, 3) = -s(i).z_value;
       
       % Find bounds
       max_north = max(max_north, s(i).x_value);
       max_east = max(max_east, s(i).y_value);
       min_north = min(min_north, s(i).x_value);
       min_east = min(min_east, s(i).y_value);
    end
    % Remove nan-values from 3d-shape
    XYZ(isnan(XYZ(:,1)),:) = [];
    XYZ_all(isnan(XYZ_all(:,1)),:) = [];
    
    % Translate to make all x,y points positive
    offset(1) = min_north;
    offset(2) = min_east;
    XYZ(:,1) = XYZ(:,1) - offset(1)*ones(size(XYZ(:,1)));
    XYZ(:,2) = XYZ(:,2) - offset(2)*ones(size(XYZ(:,2)));
    
    % Find bounds
    bounds = [floor(min(XYZ(:,1))), ceil(max(XYZ(:,1))), floor(min(XYZ(:,2))), ceil(max(XYZ(:,2)))];
    
    % Find center of rotation
    centerOfRot = [(bounds(2) - bounds(1))/2; (bounds(4) - bounds(3))/2];
end