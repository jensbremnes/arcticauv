function grid = gridAndStoreIcebergShape(xyz, s, bounds, offset, centerOfRot)
% gridAndStoreIcebergShape grids and stores iceberg shape data from the
% PERD-database to a format used for simulation.
% 
%   grid = gridAndStoreIcebergShape(xyz, s, bounds, centerOfRot)
%   
% Input parameters:
%   shp             - Alpha shape of iceberg.
%   s               - Structure with raw iceberg data.
%   bounds          - Vector with iceberg bounds.
%   offset          - Center offset
%   centerOfRot     - Center of rotation
%
% Output parameters:
%   grid            - Gridded iceberg data.
%
%   Copyright:     NTNU, Department of Marine Technology
%   Author:        Petter Norgren
%

    % Open files
    fid1 = fopen(['./SIMdata/', 'iceberg_', s(1).icebergId, '_Z.dat'], 'w+');
    fid2 = fopen(['./SIMdata/', 'iceberg_', s(1).icebergId, '_bounds.dat'], 'w+');
    fid3 = fopen(['./SIMdata/', 'iceberg_', s(1).icebergId, '_dim.dat'], 'w+');
    fid4 = fopen(['./SIMdata/', 'iceberg_', s(1).icebergId, '_offset.dat'], 'w+');
    fid5 = fopen(['./SIMdata/', 'iceberg_', s(1).icebergId, '_rotation.dat'], 'w+');

    % Todo: fix offsets ???
    offset = [0; 0];
    
    % Store bounds
    fprintf(fid2, '%i %i %i %i\n', bounds);
    fclose(fid2);
    
    % Store center of rotation
    fprintf(fid5, '%i %i\n', centerOfRot);
    fclose(fid5);
    
    % Interpolate data to construct a gridded dataset
    north = linspace(bounds(1), bounds(2), (bounds(2) - bounds(1) + 1));
    east = linspace(bounds(3), bounds(4), (bounds(4) - bounds(3) + 1));
    
    [grid.Nq, grid.Eq] = meshgrid(north, east);
    grid.Zq = griddata(xyz(:,1), xyz(:,2), xyz(:,3), grid.Nq, grid.Eq, 'natural');
    
%     F = scatteredInterpolant(shp.Points(:,1), shp.Points(:,2), shp.Points(:,3), 'natural', 'none');
    %F = interp2(shp.Points(:,1), shp.Points(:,2), shp.Points(:,3), grid.Nq, grid.Eq, 'spline', 0);

    %grid.Zq = griddata(shp.Points(:,1), shp.Points(:,2), shp.Points(:,3), grid.Nq, grid.Eq');
    % Remove nan-values from 3d-shape
%     grid.Zq(isnan(grid.Zq)) = 0;
    
    % Store gridded depth values
    for i=1:length(north)
        for j=1:length(east)
            if(isnan(grid.Zq(j, i)))
                fprintf(fid1, '%.02f', 0.0);
            else
                fprintf(fid1, '%.02f', grid.Zq(j, i));
            end
            if(j == length(east))
                fprintf(fid1, '\n');
            else
                fprintf(fid1, ' ');
            end
        end
    end
    fclose(fid1);
    
    % Store dimension
    fprintf(fid3, '%i %i\n', length(north), length(east));
    fclose(fid3);
    
    % Store offsets
    fprintf(fid4, '%i %i\n', offset(1), offset(2));
    fclose(fid4);
end

