% runAuvSimulatorIcebergMapping is a script that initializes and runs the
% iceberg mapping algorithm of the AUV simulator.
%
%    Copyright:     NTNU, Department of Marine Technology
%    Author:        Petter Norgren
%
close all; clc; clear all;
tStart = tic;

[folder, ~,  ~] = fileparts(mfilename('fullpath'));
simRoot = [folder, '/'];
clear folder;
cd(simRoot);
addpath([simRoot, 'simulator/config']);
rng('shuffle');
%% Flags
% Define flags
flags.run = 1;
flags.plot = 1;

% Run simulation or just perform plotting and saving
conf.runSimulation = true;

% Select plots to run
flags.plotSelect = [0, 0, 0, 0, 0];                               
flags.customPlotSelect = [1, 0, 1, 1];                            % [observation_map, entropy, timing, ekf]
flags.showSelect = [0, 0];                                        % [show_estimated, show_commanded]
flags.plotIcebergTrajectory = 1;
flags.icebergPlots = [1, 0];
flags.exportFigs = 0;
flags.exportAsPdf = 0; 
flags.loadPreviousRun = 0;
flags.fixBrokenOldRun = 0;

%% Setup
[conf, modules, mdlFiles] = confMultibeamReader(simRoot);
    
if(~flags.loadPreviousRun)

    % Change desired fields of config for current run
    conf.T = 500;      % 1700 is complete mission
    conf.iceberg.eta0 = [0, 0, 0]';
    conf.VCurrentMs = 0.0;
    conf.betaCurrentDeg = 45;
    conf.VIceMs = 0.00;
    conf.betaIceDeg = 45;
    conf.rotationRateIceDegs = 0/3600; 
    conf.iceberg.nu0 = [conf.VIceMs*cos(deg2rad(conf.betaIceDeg)), ...
                        conf.VIceMs*sin(deg2rad(conf.betaIceDeg)), ...
                        deg2rad(conf.rotationRateIceDegs)]';
    conf.debugOn = 0;                   % up to 35x faster with debug off
    conf.iceslam.pfNoiseEnable = 1;
    conf.iceslam.saveObservationMapOnExit = 0;
    conf.enable2dAnimation = 0;
    conf.anim.enableParticlePlots = 1;
    conf.anim.saveAnimation = 0;
    conf.anim.animationFps = 10;
    conf.anim.plotMbePointsInMap = 1;
    conf.plot.Tend = conf.T;
    conf.anim.animationStop = conf.plot.Tend;
    
    % Reader config 
    conf.mbe.startIndex = 255373; %navlabtime: 1510835058588723000 beamIdx: 62176; beamTime: 1510835058626000000
    conf.mbe.endIndex = 289400; 
    simReturn = struct;

    %% Run simulator
    if(flags.run)
        simReturn = auvSimulator_exe(conf, modules, mdlFiles);
    end
else
    loadDir = uigetdir([simRoot, 'save']);
    if(loadDir == 0)
       return; 
    end
    load([loadDir, '\conf.mat']);
    simReturn = load([loadDir, '\auvsim.mat']);
    clear loadDir;
    conf.plot.Tend = 1700;
    conf.anim.animationStop = conf.plot.Tend;
end

%% Get custom datafields and add to results
if(isfield(simReturn, 'simOut'))
    % Iceberg
    if(~isempty(simReturn.simOut.find('WS_eta_iceberg')))
        simReturn.tIce = simReturn.simOut.get('WS_eta_iceberg').time;
        simReturn.etaIceberg = simReturn.simOut.get('WS_eta_iceberg').signals.values;
        simReturn.nuIceberg = simReturn.simOut.get('WS_nu_iceberg').signals.values;
        simReturn.etaRelative = simReturn.simOut.get('WS_eta_rel').signals.values;
        simReturn.nuRelative = simReturn.simOut.get('WS_nu_rel').signals.values;
        simReturn.nuRelativeMeas = simReturn.simOut.get('WS_nu_rel_meas').signals.values;
        %% REMOVE (FIX for wrong parameter in simulations)
        if(flags.fixBrokenOldRun)
            headingErrorRad = 60;
            simReturn.etaIceberg(:,3) = zeros(length(simReturn.etaIceberg(:,3)),1);
            simReturn.etaRelative(:,6) = simReturn.etaRelative(:,6) + headingErrorRad*ones(length(simReturn.etaRelative(:,6)),1);
            rotFix = mod(headingErrorRad, 2*pi);
            for i=1:length(simReturn.etaRelative)
                simReturn.etaRelative(i,1:3) = (Rzyx(0,0,rotFix)*simReturn.etaRelative(i,1:3)')';
            end
        end
    end
    % SLAM
    if(~isempty(simReturn.simOut.find('WS_eta_relative_hat')))
        simReturn.tSlam = simReturn.simOut.get('WS_eta_relative_hat').time;
        simReturn.etaRelativeHat = simReturn.simOut.get('WS_eta_relative_hat').signals.values;
        simReturn.xSlam = simReturn.simOut.get('WS_x_slam').signals.values;
        simReturn.xEkf = simReturn.simOut.get('WS_x_ekf').signals.values;
        simReturn.tEkf = simReturn.simOut.get('WS_x_ekf').time;
        simReturn.entropy = simReturn.simOut.get('WS_entropy_icebergslam').signals.values;
        simReturn.etaIcebergSlam = simReturn.xSlam(:,1:3);
        simReturn.etaRelativeEkf = simReturn.xEkf(:,1:3);
        simReturn.nuRelativeEkf = simReturn.xEkf(:,4:6);
        simReturn.etaIcebergEkf = simReturn.xEkf(:,7:9);
        simReturn.nuIcebergEkf = simReturn.xEkf(:,10:12);
    end
    % Multibeam reader
    if(~isempty(simReturn.simOut.find('WS_x_auv')))
        simReturn.tMbe = simReturn.simOut.get('WS_x_auv').time;
        simReturn.xAuv = simReturn.simOut.get('WS_x_auv').signals.values;
        simReturn.tAuv = simReturn.simOut.get('WS_t_auv').signals.values;
        simReturn.xNavlab = simReturn.simOut.get('WS_x_navlab').signals.values;
        simReturn.mbeRanges = simReturn.simOut.get('WS_mbe_ranges').signals.values;
        simReturn.mbeAngles = simReturn.simOut.get('WS_mbe_angles').signals.values;
        simReturn.eta = simReturn.xAuv(:,1:6);
        simReturn.nu = simReturn.xAuv(:,7:12);
        simReturn.etaNavlab = simReturn.xNavlab(:,1:6);
        simReturn.velNavlab = simReturn.xNavlab(:,7:9);
        simReturn.xDot = simReturn.simOut.get('WS_nu_dot_3dof').signals.values;
        simReturn.mbeDx = simReturn.simOut.get('WS_mbe_dx').signals.values;
        simReturn.mbeDy = simReturn.simOut.get('WS_mbe_dy').signals.values;
        simReturn.mbeDz = simReturn.simOut.get('WS_mbe_dz').signals.values;
    end
    
    if(~isempty(simReturn.simOut.find('xout')))
        for i=1:length(simReturn.simOut.get('xout').signals)
            if(strcmp(simReturn.simOut.get('xout').signals(i).blockName(end-11:end), 'sIcebergSLAM'))
               simReturn.slamParticleStates = simReturn.simOut.get('xout').signals(i).values;
               simReturn.tPart = simReturn.simOut.get('xout').time;
               break;
            end
        end
    end
end

if(~flags.loadPreviousRun && exist('tStart', 'var'))
    simReturn.tElapsed = toc(tStart);
    fprintf('INFO: Total time on a %is simulation: %.02fs (%.02f x RT). Time used for sim setup: %.02fs.\n', conf.T, simReturn.simElapsedTime, conf.T/simReturn.simElapsedTime, (simReturn.tElapsed - simReturn.simElapsedTime));
end

%% Run plot routines
conf.plot.fontsize = 20;
conf.plot.linewidth = 2.5;
plot_handles = [];
if(isfield(simReturn, 'tAuv') && flags.plot)
    plot_handles = [plot_handles; plotHuginData(simReturn, conf, flags.plotSelect)];
   
    %% Custom plots
    if(flags.customPlotSelect(1))
        plot_handles = [plot_handles; plotObservationMap(simReturn, conf)];
    end
    if(flags.customPlotSelect(2))
        plot_handles = [plot_handles; plotEntropy(simReturn, conf)];
    end
    if(flags.customPlotSelect(3))
        plot_handles = [plot_handles; plotTiming(simReturn, conf)];
    end
    if(flags.customPlotSelect(4))
        plot_handles = [plot_handles; plotEkf(simReturn, conf, flags)];
    end
end

%% Run animation
if(flags.plotIcebergTrajectory && length(simReturn.t) > 10)
    if(conf.enable2dAnimation)
        fprintf('INFO: Running animation...\n');
        tStart = tic;
    end
    
    plot_handles = [plot_handles; plotIcebergRelativeTrajectory(simReturn, conf)];
    
    if(conf.enable2dAnimation)
        simReturn.tElapsedAnimation = toc(tStart);
        fprintf('INFO: Animation complete. Total time on a %is animation: %.02fs.\n', conf.T, simReturn.tElapsedAnimation);
    end
end
%% Custom hugin bathy plots
if(sum(flags.icebergPlots))
    plot_handles = [plot_handles; plotHuginBathyPlots(simReturn, conf, flags)]; 
end
%% Export figures
if(flags.exportFigs)
    figDir = [conf.saveDir, 'figures/'];
    if(~exist(figDir, 'dir'))
        mkdir(figDir);
    end
    for i=1:length(plot_handles)
        str_name = [figDir, get(plot_handles(i), 'Name')];
        if(~isempty(strfind(str_name, 'ObservationMap')))
            continue;
        end
        fprintf('printing %s\n', str_name);
        if(flags.exportAsPdf)
            export_fig(str_name, '-pdf', '-q100', '-transparent', plot_handles(i));
        else
            export_fig(str_name, '-png', '-q100', '-transparent', plot_handles(i));
        end
    end
end
%% Clear temp variables
clear tStart svnDir simRoot modules mdlFiles folder i;